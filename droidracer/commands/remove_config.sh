#!/bin/bash

#usage: remove_config.sh emulator-ID

#Delete DroidRacer's configuration file.
android_emulator/sdk/platform-tools/adb -s emulator-$1 shell rm /mnt/sdcard/Download/abc.txt
