#!/bin/bash

#usage: process_config.sh path_to_droidracer.config

printf "" > android_emulator/abc.txt
while read -r line || [[ -n "$line" ]]; do
    strValue="$(echo $line | awk '{print $NF}')"
    echo $strValue >> android_emulator/abc.txt
done < "$1"
