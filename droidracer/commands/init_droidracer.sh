#!/bin/bash

#usage: init_droidracer.sh emulator-ID

#installs an app called AbcClientApp expected by DroidRacer to be present on the emulator. This app can be used to automate the downloading of traces generated. Read droidracer-src/readme-droidRacer.txt for more information.
/bin/bash commands/install_apk.sh $1 android_emulator/AbcClientApp.apk 
