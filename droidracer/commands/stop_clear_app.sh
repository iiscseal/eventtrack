#!/bin/bash

#usage: stop_clear_app.sh emulator-ID app_process_name

#stop the indicated app and clear its data files
android_emulator/sdk/platform-tools/adb -s emulator-$1 shell am force-stop $2
android_emulator/sdk/platform-tools/adb -s emulator-$1 shell pm clear $2
