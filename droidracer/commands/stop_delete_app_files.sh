#!/bin/bash

#usage: stop_delete_app_files.sh emulator-ID app_process_name

#stop the indicated app and only delete files created by DroidRacer. This will not remove
#any information stored about login credentials etc. Hence, when the app is restarted
#after performing this, it will login as previously logged in user in case of apps such
#as Facebook, Twitter etc which do not log out a user when the app is stopped.
android_emulator/sdk/platform-tools/adb -s emulator-$1 shell am force-stop $2
android_emulator/sdk/platform-tools/adb -s emulator-$1 shell rm /data/data/$2/abc_log.txt
android_emulator/sdk/platform-tools/adb -s emulator-$1 shell rm /data/data/$2/databases/abc.db
android_emulator/sdk/platform-tools/adb -s emulator-$1 shell rm /data/data/$2/databases/abc.db-journal
android_emulator/sdk/platform-tools/adb -s emulator-$1 shell rm /data/data/$2/droidracer.blog
android_emulator/sdk/platform-tools/adb -s emulator-$1 shell rm /data/data/$2/droidracerStringHelper.blog

