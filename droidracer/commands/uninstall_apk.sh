#!/bin/bash

#usage: uninstall_apk.sh emulator-ID app_process_name

android_emulator/sdk/platform-tools/adb -s emulator-$1 uninstall $2
