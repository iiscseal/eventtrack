#!/bin/bash

#usage: install_apk.sh emulator-ID path_to_app_apk

android_emulator/sdk/platform-tools/adb -s emulator-$1 install $2
