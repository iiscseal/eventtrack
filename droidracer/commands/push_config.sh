#!/bin/bash

#usage: push_config.sh emulator-ID path_to_droidracer.config

#takes abc.config as input, generates abc.txt and uploads it to the emulator
/bin/bash commands/process_config.sh $2
android_emulator/sdk/platform-tools/adb -s emulator-$1 push android_emulator/abc.txt /mnt/sdcard/Download/abc.txt
