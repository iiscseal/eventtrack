#!/bin/bash

#usage: pull_trace_files.sh emulator-ID app_process_name name_of_destination_folder

mkdir -p traces/$3
android_emulator/sdk/platform-tools/adb -s emulator-$1 pull /data/data/$2/abc_log.txt traces/$3/abc_log.txt
android_emulator/sdk/platform-tools/adb -s emulator-$1 pull /data/data/$2/droidracer.blog traces/$3/droidracer.blog
echo "Files droidracer.blog and abc_log.txt have been downloaded from emulator to traces/$3 folder." 
