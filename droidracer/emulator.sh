#!/usr/bin/env bash

#
# Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


ANDROID_EMULATOR=android_emulator
ANDROID_SDK_LINUX=${ANDROID_EMULATOR}/sdk
ANDROID_EXEC=${ANDROID_EMULATOR}/executable
ANDROID_BUILD=${ANDROID_EMULATOR}/build_dir

#if you do not want to retain all your previously installed 3rd party apps or any changes you have done
#to existing apps then comment out the below line.
rm ${ANDROID_BUILD}/userdata.img
#to retain your previous sdcard comment out the two line below
rm ${ANDROID_EMULATOR}/sdcard.img
${ANDROID_SDK_LINUX}/tools/mksdcard -l dracerSdcard 512M ${ANDROID_EMULATOR}/sdcard.img

${ANDROID_EXEC}/emulator \
    -sysdir ${ANDROID_BUILD}/ \
    -system ${ANDROID_BUILD}/system.img \
    -ramdisk ${ANDROID_BUILD}/ramdisk.img \
    -data userdata.img \
    -kernel ${ANDROID_SDK_LINUX}/system-images/android-14/armeabi-v7a/kernel-qemu \
    -skindir ${ANDROID_SDK_LINUX}/platforms/android-14/skins \
    -skin HVGA \
    -memory 1024 \
    -partition-size 1024 \
    -sdcard ${ANDROID_EMULATOR}/sdcard.img
