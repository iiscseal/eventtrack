#!/bin/bash

#
# Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


if [ $# -lt 1 ]; then
    echo "Aborting graph generation! Requires atleast one argument which indicates the graphs to be generated."
    exit 1
fi

#print preamble of tex document to graphs.tex
cat graphs/partial_tex_files/preamble.tex > graphs/graphs.tex

if [ "$1" == "graphs-all" ] || [ "$1" == "reproduce-paper" ]; then
    if [ "$1" == "graphs-all" ]; then
        parentDir="eventtrack/test_dir"
    else
        parentDir="eventtrack/paper_traces"
    fi

    #create table of Android app trace statistics
    printf "\n\n" >> graphs/graphs.tex
    cat graphs/partial_tex_files/stats_table_begin.tex >> graphs/graphs.tex
    printf "\n" >> graphs/graphs.tex
    echo ""
    echo "Generating the Table listing concurrency behaviour related stats of execution traces of Android apps in $parentDir folder."
    appCount=0
    for dir in $(find $parentDir/* -maxdepth 0 -type d); do
        app[$appCount]="$(echo $dir | awk -F/ '{print $NF}')"
        printf "%s\t&" "${app[$appCount]}" >> graphs/graphs.tex
        appCount=$((appCount+1))

        file="$dir/eventtrack/stats.log"
        if [ -f "$file" ]
        then
            res=`grep "Trace length after pre-processing" "$file" | head -1`
            ops=$(echo $res | awk '{print $NF}')
            printf "%d\t&" $ops >> graphs/graphs.tex

            res=`grep "No. of threads" "$file" | head -1`
            threads=$(echo $res | awk '{print $NF}')
            printf "%d\t&" $threads >> graphs/graphs.tex

            res=`grep "No. of events" "$file" | head -1`
            events=$(echo $res | awk '{print $NF}')
            printf "%d\t&" $events >> graphs/graphs.tex

            res=`grep "No. of event-loops" "$file" | head -1`
            loops=$(echo $res | awk '{print $NF}')
            printf "%d\t&" $loops >> graphs/graphs.tex

            res=`grep "No. of chains" "$file" | head -1`
            chains=$(echo $res | awk '{print $NF}')
            printf "%d" $chains >> graphs/graphs.tex
        else
            echo "$file not found. Run experiments to generate this file and then come back to generating graphs."
            exit 1
        fi

        printf "\\%s\n" "\\" >> graphs/graphs.tex
    done

    cat graphs/partial_tex_files/stats_table_end.tex >> graphs/graphs.tex


    #create table of memory consumed by EventTrack and ER-baseline
    printf "\n\n" >> graphs/graphs.tex
    cat graphs/partial_tex_files/memory_table_begin.tex >> graphs/graphs.tex
    printf "\n" >> graphs/graphs.tex
    echo ""
    echo "Generating the Table reporting memory consumed by EventTrack and ER-baseline when computing HB relation for Android app traces in $parentDir folder."
    appCount=0
    for dir in $(find $parentDir/* -maxdepth 0 -type d); do
        app[$appCount]="$(echo $dir | awk -F/ '{print $NF}')"
        printf "%s\t&" "${app[$appCount]}" >> graphs/graphs.tex
        appCount=$((appCount+1))

        file="$dir/eventtrack/memory.log"
        if [ -f "$file" ]
        then
            etUnit=`grep -E 'GB|MB' "$file" | head -1 | awk '{print $NF}'`
            etMem=`grep '^[0-9].*$' "$file" | head -1 | cut -d'^' -f 1`
            printf "%s " $etMem $etUnit >> graphs/graphs.tex
            printf "\t&" >> graphs/graphs.tex
        else
            echo "$file not found. Run experiments to generate this file and then come back to generating graphs."
            exit 1
        fi

        file="$dir/er-baseline/memory.log"
        if [ -f "$file" ]
        then
            erUnit=`grep -E 'GB|MB' "$file" | head -1 | awk '{print $NF}'`
            erMem=`grep '^[0-9].*$' "$file" | head -1 | cut -d'^' -f 1`
            printf "%s " $erMem $erUnit >> graphs/graphs.tex
        else
            echo "$file not found. Run experiments to generate this file and then come back to generating graphs."
            exit 1
        fi

        printf "\\%s\n" "\\" >> graphs/graphs.tex
    done

    cat graphs/partial_tex_files/memory_table_end.tex >> graphs/graphs.tex


    #create a pgfplots data table for execution time
    printf "\n\n" >> graphs/graphs.tex
    echo "\pgfplotstableread{" >> graphs/graphs.tex
    printf "%s\t" "app" "etrack" "erbase" "eropt" >> graphs/graphs.tex
    printf "\n" >> graphs/graphs.tex

    echo ""
    echo "Generating the graph comparing time taken by EventTrack, ER-baseline and ER-opt to compute HB relation of traces from Android apps in $parentDir folder."
    appCount=0
    for dir in $(find $parentDir/* -maxdepth 0 -type d); do
        app[$appCount]="$(echo $dir | awk -F/ '{print $NF}')"
        printf "%s\t" "${app[$appCount]}" >> graphs/graphs.tex
        appCount=$((appCount+1))

        file="$dir/eventtrack/time.log"
        if [ -f "$file" ]
        then
            res=`grep "Time taken for HB computation" "$file" | head -1`
            time=$(echo $res | cut -d' ' -f 6)
            etTime=`echo $time \* 1000 | bc -l`
            printf "%f\t" $etTime >> graphs/graphs.tex
        else
            echo "$file not found. Run experiments to generate this file and then come back to generating graphs."
            exit 1
        fi

        file="$dir/er-baseline/time.log"
        if [ -f "$file" ]
        then
            res=`grep "Time taken for HB computation" "$file" | head -1`
            time=$(echo $res | cut -d' ' -f 6)
            erbTime=`echo $time \* 1000 | bc -l`
            printf "%f\t" $erbTime >> graphs/graphs.tex
        else
            echo "$file not found. Run experiments to generate this file and then come back to generating graphs."
            exit 1
        fi

        file="$dir/er-opt/time.log"
        if [ -f "$file" ]
        then
            res=`grep "Time taken for HB computation" "$file" | head -1`
            time=$(echo $res | cut -d' ' -f 6)
            eroTime=`echo $time \* 1000 | bc -l`
            printf "%f\t" $eroTime >> graphs/graphs.tex
        else
            echo "$file not found. Run experiments to generate this file and then come back to generating graphs."
            exit 1
        fi

        printf "\n" >> graphs/graphs.tex
    done

    printf "}{\\%s}\n\n" "timedata" >> graphs/graphs.tex
    cat graphs/partial_tex_files/time_begin.tex >> graphs/graphs.tex
    printf "\n    symbolic x coords={" >> graphs/graphs.tex
    for i in ${app[@]}; do
        printf "%s," ${i} >> graphs/graphs.tex 
    done
    printf "},\n" >> graphs/graphs.tex
    cat graphs/partial_tex_files/time_end.tex >> graphs/graphs.tex


    #create a pgfplots data table for candidate events size
    printf "\n\n\n" >> graphs/graphs.tex
    echo "\pgfplotstableread{" >> graphs/graphs.tex
    printf "%s\t" "app" "etrack" "erbase" >> graphs/graphs.tex
    printf "\n" >> graphs/graphs.tex

    echo ""
    echo "Generating the graph comparing sizes of candidate events sets computed by EventTrack and ER-baseline for traces from Android apps $parentDir folder."
    appCount=0
    for dir in $(find $parentDir/* -maxdepth 0 -type d); do
        app[$appCount]="$(echo $dir | awk -F/ '{print $NF}')"
        printf "%s\t" "${app[$appCount]}" >> graphs/graphs.tex
        appCount=$((appCount+1))

        file="$dir/eventtrack/stats.log"
        if [ -f "$file" ]
        then
            res=`grep "candidate" "$file" | head -1`
            etCandidateSize=$(echo $res | awk '{print $NF}')
            printf "%d\t" $etCandidateSize >> graphs/graphs.tex
        else
            echo "$file not found. Run experiments to generate this file and then come back to generating graphs."
            exit 1
        fi

        file="$dir/er-baseline/stats.log"
        if [ -f "$file" ]
        then
            res=`grep "candidate" "$file" | head -1`
            erbCandidateSize=$(echo $res | awk '{print $NF}')
            printf "%d\t" $erbCandidateSize >> graphs/graphs.tex
        else
            echo "$file not found. Run experiments to generate this file and then come back to generating graphs."
            exit 1
        fi

        printf "\n" >> graphs/graphs.tex
    done

    printf "}{\\%s}\n\n" "candidatedata" >> graphs/graphs.tex
    cat graphs/partial_tex_files/candidate_begin.tex >> graphs/graphs.tex
    printf "\n    symbolic x coords={" >> graphs/graphs.tex
    for i in ${app[@]}; do
        printf "%s," ${i} >> graphs/graphs.tex 
    done
    printf "},\n" >> graphs/graphs.tex
    cat graphs/partial_tex_files/candidate_end.tex >> graphs/graphs.tex


    #create a pgfplots data table to count directly ordered pair of events
    printf "\n\n\n" >> graphs/graphs.tex
    echo "\pgfplotstableread{" >> graphs/graphs.tex
    printf "%s\t" "app" "etrack" "erbase" >> graphs/graphs.tex
    printf "\n" >> graphs/graphs.tex

    echo ""
    echo "Generating the graph comparing number of pairs of events directly ordered by performing VC-JOINS, by EventTrack and ER-baseline for traces from Android apps in $parentDir folder."
    appCount=0
    for dir in $(find $parentDir/* -maxdepth 0 -type d); do
        app[$appCount]="$(echo $dir | awk -F/ '{print $NF}')"
        printf "%s\t" "${app[$appCount]}" >> graphs/graphs.tex
        appCount=$((appCount+1))

        file="$dir/eventtrack/stats.log"
        if [ -f "$file" ]
        then
            res=`grep "covering" "$file" | head -1`
            etCandidateSize=$(echo $res | awk '{print $NF}')
            printf "%d\t" $etCandidateSize >> graphs/graphs.tex
        else
            echo "$file not found. Run experiments to generate this file and then come back to generating graphs."
            exit 1
        fi

        file="$dir/er-baseline/stats.log"
        if [ -f "$file" ]
        then
            res=`grep "end to begin edges" "$file" | head -1`
            erbCandidateSize=$(echo $res | awk '{print $NF}')
            printf "%d\t" $erbCandidateSize >> graphs/graphs.tex
        else
            echo "$file not found. Run experiments to generate this file and then come back to generating graphs."
            exit 1
        fi

        printf "\n" >> graphs/graphs.tex
    done

    printf "}{\\%s}\n\n" "orderedeventsdata" >> graphs/graphs.tex
    cat graphs/partial_tex_files/ordered_events_begin.tex >> graphs/graphs.tex
    printf "\n    symbolic x coords={" >> graphs/graphs.tex
    for i in ${app[@]}; do
        printf "%s," ${i} >> graphs/graphs.tex 
    done
    printf "},\n" >> graphs/graphs.tex
    cat graphs/partial_tex_files/ordered_events_end.tex >> graphs/graphs.tex


elif [ "$1" == "all-graphs-for-one" ]; then
    #generate tables and graphs corresponding to an app trace in test_dir
    if  [[ $# -lt 2 ]]; then
        echo "Aborting graph generation! gen_graphs.sh in all-graphs-for-one mode needs 2 args with the name of the app directory in test_dir folder whose results are to be visualized as the second argument."
        exit 1
    fi

    dir="eventtrack/test_dir/$2"


    #create table of Android app trace statistics
    printf "\n\n" >> graphs/graphs.tex
    cat graphs/partial_tex_files/stats_table_begin.tex >> graphs/graphs.tex
    printf "\n" >> graphs/graphs.tex
    echo ""
    echo "Generating the Table listing concurrency behaviour related stats of execution trace of Android app in $dir folder."
    printf "%s\t&" "$2" >> graphs/graphs.tex
    file="$dir/eventtrack/stats.log"
    if [ -f "$file" ]
    then
        res=`grep "Trace length after pre-processing" "$file" | head -1`
        ops=$(echo $res | awk '{print $NF}')
        printf "%d\t&" $ops >> graphs/graphs.tex

        res=`grep "No. of threads" "$file" | head -1`
        threads=$(echo $res | awk '{print $NF}')
        printf "%d\t&" $threads >> graphs/graphs.tex

        res=`grep "No. of events" "$file" | head -1`
        events=$(echo $res | awk '{print $NF}')
        printf "%d\t&" $events >> graphs/graphs.tex

        res=`grep "No. of event-loops" "$file" | head -1`
        loops=$(echo $res | awk '{print $NF}')
        printf "%d\t&" $loops >> graphs/graphs.tex

        res=`grep "No. of chains" "$file" | head -1`
        chains=$(echo $res | awk '{print $NF}')
        printf "%d" $chains >> graphs/graphs.tex
    else
        echo "$file not found. It looks like experiments could not be run successfully on droidracer.log trace of $2 app."
        exit 1
    fi

    printf "\\%s\n" "\\" >> graphs/graphs.tex

    cat graphs/partial_tex_files/stats_table_end.tex >> graphs/graphs.tex


    #create table of memory consumed by EventTrack and ER-baseline
    printf "\n\n" >> graphs/graphs.tex
    cat graphs/partial_tex_files/memory_table_begin.tex >> graphs/graphs.tex
    printf "\n" >> graphs/graphs.tex
    echo ""
    echo "Generating the Table reporting memory consumed by EventTrack and ER-baseline when computing HB relation for Android app in $dir folder."
    printf "%s\t&" "$2" >> graphs/graphs.tex

    file="$dir/eventtrack/memory.log"
    if [ -f "$file" ]
    then
        etUnit=`grep -E 'GB|MB' "$file" | head -1 | awk '{print $NF}'`
        etMem=`grep '^[0-9].*#$' "$file" | head -1 | cut -d'^' -f 1`
        printf "%s " $etMem $etUnit >> graphs/graphs.tex
        printf "\t&" >> graphs/graphs.tex
    else
        echo "$file not found. It looks like valgrind could not be run successfully on droidracer.log trace of $2 app to compute peak memory."
        exit 1
    fi

    file="$dir/er-baseline/memory.log"
    if [ -f "$file" ]
    then
        erUnit=`grep -E 'GB|MB' "$file" | head -1 | awk '{print $NF}'`
        erMem=`grep '^[0-9].*#$' "$file" | head -1 | cut -d'^' -f 1`
        printf "%s " $erMem $erUnit >> graphs/graphs.tex
    else
        echo "$file not found. It looks like valgrind could not be run successfully on droidracer.log trace of $2 app to compute peak memory."
        exit 1
    fi

    printf "\\%s\n" "\\" >> graphs/graphs.tex

    cat graphs/partial_tex_files/memory_table_end.tex >> graphs/graphs.tex


    #create a pgfplots data table for execution time
    printf "\n\n" >> graphs/graphs.tex
    echo "\pgfplotstableread{" >> graphs/graphs.tex
    printf "%s\t" "app" "etrack" "erbase" "eropt" >> graphs/graphs.tex
    printf "\n" >> graphs/graphs.tex

    echo ""
    echo "Generating the graph comparing time taken by EventTrack, ER-baseline and ER-opt to compute HB relation of traces from Android app in $dir folder."
    printf "%s\t" "$2" >> graphs/graphs.tex

    file="$dir/eventtrack/time.log"
    if [ -f "$file" ]
    then
        res=`grep "Time taken for HB computation" "$file" | head -1`
        time=$(echo $res | cut -d' ' -f 6)
        etTime=`echo $time \* 1000 | bc -l`
        printf "%f\t" $etTime >> graphs/graphs.tex
    else
        echo "$file not found. It looks like execution time collection experiments could not be run successfully on droidracer.log trace of $2 app."
        exit 1
    fi

    file="$dir/er-baseline/time.log"
    if [ -f "$file" ]
    then
        res=`grep "Time taken for HB computation" "$file" | head -1`
        time=$(echo $res | cut -d' ' -f 6)
        erbTime=`echo $time \* 1000 | bc -l`
        printf "%f\t" $erbTime >> graphs/graphs.tex
    else
        echo "$file not found. It looks like execution time collection experiments could not be run successfully on droidracer.log trace of $2 app."
        exit 1
    fi

    file="$dir/er-opt/time.log"
    if [ -f "$file" ]
    then
        res=`grep "Time taken for HB computation" "$file" | head -1`
        time=$(echo $res | cut -d' ' -f 6)
        eroTime=`echo $time \* 1000 | bc -l`
        printf "%f\t" $eroTime >> graphs/graphs.tex
    else
        echo "$file not found. It looks like execution time collection experiments could not be run successfully on droidracer.log trace of $2 app."
        exit 1
    fi

    printf "\n" >> graphs/graphs.tex

    printf "}{\\%s}\n\n" "timedata" >> graphs/graphs.tex
    cat graphs/partial_tex_files/time_begin.tex >> graphs/graphs.tex
    printf "\n    symbolic x coords={" >> graphs/graphs.tex
    printf "%s," "$2" >> graphs/graphs.tex 
    printf "},\n" >> graphs/graphs.tex
    cat graphs/partial_tex_files/time_end.tex >> graphs/graphs.tex


    #create a pgfplots data table for candidate events size
    printf "\n\n\n" >> graphs/graphs.tex
    echo "\pgfplotstableread{" >> graphs/graphs.tex
    printf "%s\t" "app" "etrack" "erbase" >> graphs/graphs.tex
    printf "\n" >> graphs/graphs.tex

    echo ""
    echo "Generating the graph comparing sizes of candidate events sets computed by EventTrack and ER-baseline for traces from Android app in $dir folder."
    printf "%s\t" "$2" >> graphs/graphs.tex

    file="$dir/eventtrack/stats.log"
    if [ -f "$file" ]
    then
        res=`grep "candidate" "$file" | head -1`
        etCandidateSize=$(echo $res | awk '{print $NF}')
        printf "%d\t" $etCandidateSize >> graphs/graphs.tex
    else
        echo "$file not found. It looks like stats collection experiments could not be run successfully on droidracer.log trace of $2 app."
        exit 1
    fi

    file="$dir/er-baseline/stats.log"
    if [ -f "$file" ]
    then
        res=`grep "candidate" "$file" | head -1`
        erbCandidateSize=$(echo $res | awk '{print $NF}')
        printf "%d\t" $erbCandidateSize >> graphs/graphs.tex
    else
        echo "$file not found. It looks like stats collection experiments could not be run successfully on droidracer.log trace of $2 app."
        exit 1
    fi

    printf "\n" >> graphs/graphs.tex

    printf "}{\\%s}\n\n" "candidatedata" >> graphs/graphs.tex
    cat graphs/partial_tex_files/candidate_begin.tex >> graphs/graphs.tex
    printf "\n    symbolic x coords={" >> graphs/graphs.tex
    printf "%s," "$2" >> graphs/graphs.tex 
    printf "},\n" >> graphs/graphs.tex
    cat graphs/partial_tex_files/candidate_end.tex >> graphs/graphs.tex


    #create a pgfplots data table to count directly ordered pair of events
    printf "\n\n\n" >> graphs/graphs.tex
    echo "\pgfplotstableread{" >> graphs/graphs.tex
    printf "%s\t" "app" "etrack" "erbase" >> graphs/graphs.tex
    printf "\n" >> graphs/graphs.tex

    echo ""
    echo "Generating the graph comparing number of pairs of events directly ordered by performing VC-JOINS, by EventTrack and ER-baseline for traces from Android app in $dir folder."
    printf "%s\t" "$2" >> graphs/graphs.tex

    file="$dir/eventtrack/stats.log"
    if [ -f "$file" ]
    then
        res=`grep "covering" "$file" | head -1`
        etCandidateSize=$(echo $res | awk '{print $NF}')
        printf "%d\t" $etCandidateSize >> graphs/graphs.tex
    else
        echo "$file not found. It looks like stats collection experiments could not be run successfully on droidracer.log trace of $2 app."
        exit 1
    fi

    file="$dir/er-baseline/stats.log"
    if [ -f "$file" ]
    then
        res=`grep "end to begin edges" "$file" | head -1`
        erbCandidateSize=$(echo $res | awk '{print $NF}')
        printf "%d\t" $erbCandidateSize >> graphs/graphs.tex
    else
        echo "$file not found. It looks like stats collection experiments could not be run successfully on droidracer.log trace of $2 app."
        exit 1
    fi

    printf "\n" >> graphs/graphs.tex

    printf "}{\\%s}\n\n" "orderedeventsdata" >> graphs/graphs.tex
    cat graphs/partial_tex_files/ordered_events_begin.tex >> graphs/graphs.tex
    printf "\n    symbolic x coords={" >> graphs/graphs.tex
    printf "%s," "$2" >> graphs/graphs.tex 
    printf "},\n" >> graphs/graphs.tex
    cat graphs/partial_tex_files/ordered_events_end.tex >> graphs/graphs.tex


elif [ "$1" == "time-graph" ]; then
    #create a pgfplots data table for execution time
    printf "\n\n" >> graphs/graphs.tex
    echo "\pgfplotstableread{" >> graphs/graphs.tex
    printf "%s\t" "app" "etrack" "erbase" "eropt" >> graphs/graphs.tex
    printf "\n" >> graphs/graphs.tex

    echo ""
    echo "Generating the graph comparing time taken by EventTrack, ER-baseline and ER-opt to compute HB relation of traces from Android apps in eventtrack/test_dir folder."
    appCount=0
    for dir in $(find eventtrack/test_dir/* -maxdepth 0 -type d); do
        app[$appCount]="$(echo $dir | awk -F/ '{print $NF}')"
        printf "%s\t" "${app[$appCount]}" >> graphs/graphs.tex
        appCount=$((appCount+1))

        file="$dir/eventtrack/time.log"
        if [ -f "$file" ]
        then
            res=`grep "Time taken for HB computation" "$file" | head -1`
            time=$(echo $res | cut -d' ' -f 6)
            etTime=`echo $time \* 1000 | bc -l`
            printf "%f\t" $etTime >> graphs/graphs.tex
        else
            echo "$file not found. Run experiments to generate this file and then come back to generating graphs."
            exit 1
        fi

        file="$dir/er-baseline/time.log"
        if [ -f "$file" ]
        then
            res=`grep "Time taken for HB computation" "$file" | head -1`
            time=$(echo $res | cut -d' ' -f 6)
            erbTime=`echo $time \* 1000 | bc -l`
            printf "%f\t" $erbTime >> graphs/graphs.tex
        else
            echo "$file not found. Run experiments to generate this file and then come back to generating graphs."
            exit 1
        fi

        file="$dir/er-opt/time.log"
        if [ -f "$file" ]
        then
            res=`grep "Time taken for HB computation" "$file" | head -1`
            time=$(echo $res | cut -d' ' -f 6)
            eroTime=`echo $time \* 1000 | bc -l`
            printf "%f\t" $eroTime >> graphs/graphs.tex
        else
            echo "$file not found. Run experiments to generate this file and then come back to generating graphs."
            exit 1
        fi

        printf "\n" >> graphs/graphs.tex
    done

    printf "}{\\%s}\n\n" "timedata" >> graphs/graphs.tex
    cat graphs/partial_tex_files/time_begin.tex >> graphs/graphs.tex
    printf "\n    symbolic x coords={" >> graphs/graphs.tex
    for i in ${app[@]}; do
        printf "%s," ${i} >> graphs/graphs.tex 
    done
    printf "},\n" >> graphs/graphs.tex
    cat graphs/partial_tex_files/time_end.tex >> graphs/graphs.tex


elif [ "$1" == "candidate-events-graph" ]; then
    #create a pgfplots data table for candidate events size
    printf "\n\n\n" >> graphs/graphs.tex
    echo "\pgfplotstableread{" >> graphs/graphs.tex
    printf "%s\t" "app" "etrack" "erbase" >> graphs/graphs.tex
    printf "\n" >> graphs/graphs.tex

    echo ""
    echo "Generating the graph comparing sizes of candidate events sets computed by EventTrack and ER-baseline for traces from Android apps in eventtrack/test_dir folder."
    appCount=0
    for dir in $(find eventtrack/test_dir/* -maxdepth 0 -type d); do
        app[$appCount]="$(echo $dir | awk -F/ '{print $NF}')"
        printf "%s\t" "${app[$appCount]}" >> graphs/graphs.tex
        appCount=$((appCount+1))

        file="$dir/eventtrack/stats.log"
        if [ -f "$file" ]
        then
            res=`grep "candidate" "$file" | head -1`
            etCandidateSize=$(echo $res | awk '{print $NF}')
            printf "%d\t" $etCandidateSize >> graphs/graphs.tex
        else
            echo "$file not found. Run experiments to generate this file and then come back to generating graphs."
            exit 1
        fi

        file="$dir/er-baseline/stats.log"
        if [ -f "$file" ]
        then
            res=`grep "candidate" "$file" | head -1`
            erbCandidateSize=$(echo $res | awk '{print $NF}')
            printf "%d\t" $erbCandidateSize >> graphs/graphs.tex
        else
            echo "$file not found. Run experiments to generate this file and then come back to generating graphs."
            exit 1
        fi

        printf "\n" >> graphs/graphs.tex
    done

    printf "}{\\%s}\n\n" "candidatedata" >> graphs/graphs.tex
    cat graphs/partial_tex_files/candidate_begin.tex >> graphs/graphs.tex
    printf "\n    symbolic x coords={" >> graphs/graphs.tex
    for i in ${app[@]}; do
        printf "%s," ${i} >> graphs/graphs.tex 
    done
    printf "},\n" >> graphs/graphs.tex
    cat graphs/partial_tex_files/candidate_end.tex >> graphs/graphs.tex


elif [ "$1" == "ordered-events-graph" ]; then
    #create a pgfplots data table to count directly ordered pair of events
    printf "\n\n\n" >> graphs/graphs.tex
    echo "\pgfplotstableread{" >> graphs/graphs.tex
    printf "%s\t" "app" "etrack" "erbase" >> graphs/graphs.tex
    printf "\n" >> graphs/graphs.tex

    echo ""
    echo "Generating the graph comparing number of pairs of events directly ordered by performing VC-JOINS, by EventTrack and ER-baseline for traces from Android apps in eventtrack/test_dir folder."
    appCount=0
    for dir in $(find eventtrack/test_dir/* -maxdepth 0 -type d); do
        app[$appCount]="$(echo $dir | awk -F/ '{print $NF}')"
        printf "%s\t" "${app[$appCount]}" >> graphs/graphs.tex
        appCount=$((appCount+1))

        file="$dir/eventtrack/stats.log"
        if [ -f "$file" ]
        then
            res=`grep "covering" "$file" | head -1`
            etCandidateSize=$(echo $res | awk '{print $NF}')
            printf "%d\t" $etCandidateSize >> graphs/graphs.tex
        else
            echo "$file not found. Run experiments to generate this file and then come back to generating graphs."
            exit 1
        fi

        file="$dir/er-baseline/stats.log"
        if [ -f "$file" ]
        then
            res=`grep "end to begin edges" "$file" | head -1`
            erbCandidateSize=$(echo $res | awk '{print $NF}')
            printf "%d\t" $erbCandidateSize >> graphs/graphs.tex
        else
            echo "$file not found. Run experiments to generate this file and then come back to generating graphs."
            exit 1
        fi

        printf "\n" >> graphs/graphs.tex
    done

    printf "}{\\%s}\n\n" "orderedeventsdata" >> graphs/graphs.tex
    cat graphs/partial_tex_files/ordered_events_begin.tex >> graphs/graphs.tex
    printf "\n    symbolic x coords={" >> graphs/graphs.tex
    for i in ${app[@]}; do
        printf "%s," ${i} >> graphs/graphs.tex 
    done
    printf "},\n" >> graphs/graphs.tex
    cat graphs/partial_tex_files/ordered_events_end.tex >> graphs/graphs.tex


elif [ "$1" == "stats-table" ]; then
    #create table of Android app trace statistics
    printf "\n\n" >> graphs/graphs.tex
    cat graphs/partial_tex_files/stats_table_begin.tex >> graphs/graphs.tex
    printf "\n" >> graphs/graphs.tex
    echo ""
    echo "Generating the Table listing concurrency behaviour related stats of execution traces of Android apps in the eventtrack/test_dir folder."
    appCount=0
    for dir in $(find eventtrack/test_dir/* -maxdepth 0 -type d); do
        app[$appCount]="$(echo $dir | awk -F/ '{print $NF}')"
        printf "%s\t&" "${app[$appCount]}" >> graphs/graphs.tex
        appCount=$((appCount+1))

        file="$dir/eventtrack/stats.log"
        if [ -f "$file" ]
        then
            res=`grep "Trace length after pre-processing" "$file" | head -1`
            ops=$(echo $res | awk '{print $NF}')
            printf "%d\t&" $ops >> graphs/graphs.tex

            res=`grep "No. of threads" "$file" | head -1`
            threads=$(echo $res | awk '{print $NF}')
            printf "%d\t&" $threads >> graphs/graphs.tex

            res=`grep "No. of events" "$file" | head -1`
            events=$(echo $res | awk '{print $NF}')
            printf "%d\t&" $events >> graphs/graphs.tex

            res=`grep "No. of event-loops" "$file" | head -1`
            loops=$(echo $res | awk '{print $NF}')
            printf "%d\t&" $loops >> graphs/graphs.tex

            res=`grep "No. of chains" "$file" | head -1`
            chains=$(echo $res | awk '{print $NF}')
            printf "%d" $chains >> graphs/graphs.tex
        else
            echo "$file not found. Run experiments to generate this file and then come back to generating graphs."
            exit 1
        fi

        printf "\\%s\n" "\\" >> graphs/graphs.tex
    done

    cat graphs/partial_tex_files/stats_table_end.tex >> graphs/graphs.tex


elif [ "$1" == "memory-table" ]; then
    #create table of memory consumed by EventTrack and ER-baseline
    printf "\n\n" >> graphs/graphs.tex
    cat graphs/partial_tex_files/memory_table_begin.tex >> graphs/graphs.tex
    printf "\n" >> graphs/graphs.tex
    echo ""
    echo "Generating the Table reporting memory consumed by EventTrack and ER-baseline when computing HB relation for Android app traces in the eventtrack/test_dir folder."
    appCount=0
    for dir in $(find eventtrack/test_dir/* -maxdepth 0 -type d); do
        app[$appCount]="$(echo $dir | awk -F/ '{print $NF}')"
        printf "%s\t&" "${app[$appCount]}" >> graphs/graphs.tex
        appCount=$((appCount+1))

        file="$dir/eventtrack/memory.log"
        if [ -f "$file" ]
        then
            etUnit=`grep -E 'GB|MB' "$file" | head -1 | awk '{print $NF}'`
            etMem=`grep '^[0-9].*$' "$file" | head -1 | cut -d'^' -f 1`
            printf "%s " $etMem $etUnit >> graphs/graphs.tex
            printf "\t&" >> graphs/graphs.tex
        else
            echo "$file not found. Run experiments to generate this file and then come back to generating graphs."
            exit 1
        fi

        file="$dir/er-baseline/memory.log"
        if [ -f "$file" ]
        then
            erUnit=`grep -E 'GB|MB' "$file" | head -1 | awk '{print $NF}'`
            erMem=`grep '^[0-9].*$' "$file" | head -1 | cut -d'^' -f 1`
            printf "%s " $erMem $erUnit >> graphs/graphs.tex
        else
            echo "$file not found. Run experiments to generate this file and then come back to generating graphs."
            exit 1
        fi

        printf "\\%s\n" "\\" >> graphs/graphs.tex
    done

    cat graphs/partial_tex_files/memory_table_end.tex >> graphs/graphs.tex

else
    echo "Aborting build! First argument passed is invalid."
fi

printf "\n\n" >> graphs/graphs.tex
cat graphs/partial_tex_files/closing.tex >> graphs/graphs.tex


#create graphs.pdf
make -C graphs/ clean
make -C graphs/ 
echo "" ; echo "View the tables and graphs created at graphs/graphs.pdf"
