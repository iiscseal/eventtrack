/*
 * Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <etUtils.h>
#include <erbaseUtils.h>
#include <operationHelper.h>
#include <eventgraph.h>
#include <time.h>


#ifdef EVENTTRACK
    using namespace etrack;
#endif

#ifdef ERBASELINE
    using namespace erbaseline;
#endif

int main(int argc, char* argv[]) {

    if(argc <= 1){
       PERROR("Need to pass path of droidracer.blog file as argument."); 
    }

    OpHelper* oph = new OpHelper();
    EnvModel* appEnv = new EnvModel();
    HbUtils* utilObj = new HbUtils(oph, appEnv);

    utilObj->initializeFunctionPointers();
    utilObj->initializePreProcess(MAIN_TID);


    FILE * fp = fopen(argv[1], "rb" );


    if(fp != NULL){
        clock_t startTime = clock();

        //preprocess the trace
        OpLog oplog;
        while ( fread(&oplog, sizeof(OpLog), 1, fp ) != 0 ) {
            if(oplog.opType >= NUM_TYPES){
                PERROR("Found an operation of undefined type %d", oplog.opType);
            }else{
                utilObj->preProcess(&oplog);
            }
        }

        fclose(fp);

        utilObj->emitEndOpsManually();

        oph->cleanupAfterPreProcess();
        //preprocessing completed

        clock_t preprocessTime = clock();

        //compute HB relation on the preprocessed trace
        utilObj->computeHBForTrace();

        clock_t endTime = clock();
        printf("\nTime taken for preprocessing: %f sec", ((double)(preprocessTime - startTime))/CLOCKS_PER_SEC);
        printf("\nTime taken for HB computation: %f sec\n", ((double)(endTime - preprocessTime))/CLOCKS_PER_SEC);
    }

    return 0;
}
