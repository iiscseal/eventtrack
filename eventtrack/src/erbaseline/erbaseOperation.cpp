/*
 * Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "erbaseOperation.h"

namespace erbaseline{

ErBaseTask::ErBaseTask(int id, int tid, int eventId) : Task(tid, eventId), id(id){
    epoch = 0;
    postOp = NULL;
    endOp = NULL;
}

ErBaseTask::~ErBaseTask(){
    printf("ErBaseTask is being deleted.");
}

std::pair<int,u4> ErBaseTask::getEpoch(){
    return std::make_pair(chain, epoch);
}


ErBaseThread::ErBaseThread(int curTaskId, ErBaseTask* curTask, ForkOp* forkOp)
        : ThreadClass(curTaskId), curTask(curTask), forkOp(forkOp){
    loopOp = NULL;
}

ErBaseThread::~ErBaseThread(){}


// Simple constructor & destructure of ErBaseOperation class

ErBaseOperation::ErBaseOperation(){
    hbSinkOp = NULL;
    task = NULL;
    visitedCount = 0;
}

ErBaseOperation::ErBaseOperation(int tid, int taskId) : Operation(tid, taskId){
    hbSinkOp = NULL;
    task = NULL;
    visitedCount = 0;
}

//ErBaseOperation::~ErBaseOperation(){}

BasePostOp::BasePostOp(){}

BasePostOp::~BasePostOp(){}

PostOp::~PostOp(){}

AtTimePostOp::~AtTimePostOp(){}

BeginOp::~BeginOp(){}

EndOp::~EndOp(){}

EndOp::EndOp(int tid, int taskId, u4 event) : ErBaseOperation(tid, taskId), event(event){
}

AttachqOp::AttachqOp(){}
AttachqOp::~AttachqOp(){}

ForkOp::~ForkOp(){}

JoinOp::~JoinOp(){}

ThreadinitOp::~ThreadinitOp(){}

ThreadexitOp::~ThreadexitOp(){}

LoopOp::~LoopOp(){}

LoopExitOp::~LoopExitOp(){}

EnableEventOp::~EnableEventOp(){}

TriggerEventOp::~TriggerEventOp(){}

EnableActLifecycleOp::~EnableActLifecycleOp(){}

TriggerActLifecycleOp::~TriggerActLifecycleOp(){}

TriggerReceiverOp::~TriggerReceiverOp(){}

TriggerServiceOp::~TriggerServiceOp(){}

InstanceIntentOp::~InstanceIntentOp(){}

EnableWindowFocusOp::~EnableWindowFocusOp(){}

TriggerWindowFocusOp::~TriggerWindowFocusOp(){}

WaitOp::~WaitOp(){}

TimedWaitOp::~TimedWaitOp(){}

NotifyOp::~NotifyOp(){}

IdlePostOp::~IdlePostOp(){}

PostFoqOp::~PostFoqOp(){}

PostNegOp::~PostNegOp(){}

EnableTimerTaskOp::~EnableTimerTaskOp(){}

TriggerTimerTaskOp::~TriggerTimerTaskOp(){}



//more complex constructors

PostOp::PostOp(OpLog* oplog, OpHelper* oph){
    tid = oplog->tid; //maybe -1 if posted by untracked thread such as binder threads.
    event = oplog->arg2;
    dest = oplog->arg3;
    delay = oplog->arg4;
    typeOfPost = DR_POST;

    OpHelper::threadMapIterator thItr = oph->threadMap.find(tid);
    if(thItr != oph->threadMap.end() && thItr->second->curTaskId != -1){
        taskId = thItr->second->curTaskId;
        task = dynamic_cast<ErBaseThread*>(thItr->second)->curTask;
    }else{
        /* this is alright because due to the way we model UI events, posts may be seen outside tasks.
         * Task is NULL for such posts, indicating such posts need not copy parents HB into postsed task.
         * We follow this rule for NATIVE-POSTs too.
         * Also, we model native posts posted by untracked threads as DR_POST.
         */
        taskId = -1;
        task = NULL;
    }

    oph->eventToPostMap.insert(std::make_pair(event,oph->opCount)); 
    
    //destination is set after begin is executed
}

AtTimePostOp::AtTimePostOp(OpLog* oplog, OpHelper* oph){
    tid = oplog->tid; //maybe -1 if posted by untracked thread such as binder threads.
    event = oplog->arg2;
    dest = oplog->arg3;
    delay = oplog->arg4; //currently this is neglected as per rules for AtTimePost in EventRacer paper. We implement the same.
    typeOfPost = DR_AT_TIME_POST;

    OpHelper::threadMapIterator thItr = oph->threadMap.find(tid);
    if(thItr != oph->threadMap.end() && thItr->second->curTaskId != -1){
        taskId = thItr->second->curTaskId;
        task = dynamic_cast<ErBaseThread*>(thItr->second)->curTask;
    }else{
        /* this is alright because due to the way we model UI events, posts may be seen outside tasks.
         * Task is NULL for such posts, indicating such posts need not copy parents HB into postsed task.
         * We follow this rule for NATIVE-POSTs too.
         * Also, we model native posts posted by untracked threads as DR_POST.
         */
        taskId = -1;
        task = NULL;
    }

    oph->eventToPostMap.insert(std::make_pair(event,oph->opCount));

    //destination is set after begin is executed
}

PostFoqOp::PostFoqOp(OpLog* oplog, OpHelper* oph){
    tid = oplog->tid; //maybe -1 if posted by untracked thread such as binder threads.
    event = oplog->arg2;
    dest = oplog->arg3;
    typeOfPost = DR_POST_FOQ;

    OpHelper::threadMapIterator thItr = oph->threadMap.find(tid);
    if(thItr != oph->threadMap.end() && thItr->second->curTaskId != -1){
        taskId = thItr->second->curTaskId;
        task = dynamic_cast<ErBaseThread*>(thItr->second)->curTask;
    }else{
        /* this is alright because due to the way we model UI events, posts may be seen outside tasks.
         * Task is NULL for such posts, indicating such posts need not copy parents HB into postsed task.
         * We follow this rule for NATIVE-POSTs too.
         * Also, we model native FoQ posts posted by untracked threads as POST_FOQ.
         */
        taskId = -1;
        task = NULL;
    }

    oph->eventToPostMap.insert(std::make_pair(event,oph->opCount));

    //destination is set after begin is executed
}

PostNegOp::PostNegOp(OpLog* oplog, OpHelper* oph){
    tid = oplog->tid; //maybe -1 if posted by untracked thread such as binder threads.
    event = oplog->arg2;
    dest = oplog->arg3;
    delay = oplog->arg4;
    typeOfPost = DR_POST_NEG;

    OpHelper::threadMapIterator thItr = oph->threadMap.find(tid);
    if(thItr != oph->threadMap.end() && thItr->second->curTaskId != -1){
        taskId = thItr->second->curTaskId;
        task = dynamic_cast<ErBaseThread*>(thItr->second)->curTask;
    }else{
        /* this is alright because due to the way we model UI events, posts may be seen outside tasks.
         * Task is NULL for such posts, indicating such posts need not copy parents HB into postsed task.
         * We follow this rule for NATIVE-POSTs too.
         * Also, we model native FoQ posts posted by untracked threads as POST_FOQ.
         */
        taskId = -1;
        task = NULL;
    }

    oph->eventToPostMap.insert(std::make_pair(event,oph->opCount));

    //destination is set after begin is executed
}

IdlePostOp::IdlePostOp(OpLog* oplog, OpHelper* oph){
    tid = oplog->tid; //maybe -1 if posted by untracked thread such as binder threads.
    event = oplog->arg2;
    dest = oplog->arg3;
    typeOfPost = DR_IDLE_POST;

    OpHelper::threadMapIterator thItr = oph->threadMap.find(tid);
    if(thItr != oph->threadMap.end() && thItr->second->curTaskId != -1){
        taskId = thItr->second->curTaskId;
        task = dynamic_cast<ErBaseThread*>(thItr->second)->curTask;
    }else{
        /* this is alright because due to the way we model UI events, posts may be seen outside tasks.
         * Task is NULL for such posts, indicating such posts need not copy parents HB into postsed task.
         * We follow this rule for NATIVE-POSTs too.
         * Also, we model native FoQ posts posted by untracked threads as POST_FOQ.
         */
        taskId = -1;
        task = NULL;
    } 

    oph->eventToPostMap.insert(std::make_pair(event,oph->opCount));
    
    //destination is set after begin is executed
}

BeginOp::BeginOp(OpLog* oplog, OpHelper* oph){
    int curOpCount = oph->opCount;

    tid = oplog->tid;
    event = oplog->arg2;
    isOnReceive = false;

    OpHelper::threadMapIterator thItr = oph->threadMap.find(tid);
    if(thItr != oph->threadMap.end() && thItr->second->curTaskId == -1 &&
            thItr->second->loopOpId != -1){
        std::map<u4,int>::iterator it = oph->eventToPostMap.find(event);      
        if(it != oph->eventToPostMap.end()){
            OpHelper::opMapIterator opItr = oph->opMap.find(it->second);
            BasePostOp* postOp = dynamic_cast<BasePostOp*>(opItr->second);
            if(postOp != NULL){
                taskId = oph->taskCount++; //assign a brand new task Id

                ErBaseTask* newTask = new ErBaseTask(taskId, tid, event);
                newTask->postOpId = it->second;
                newTask->beginOpId = curOpCount;
                newTask->postOp = postOp;
                newTask->typeOfPost = postOp->typeOfPost;
                oph->taskMap.insert(std::make_pair(taskId, newTask));

                task = newTask;

                postOp->hbSinkOp = this;

                setHbSink();        
                  
                thItr->second->curTaskId = taskId;
                dynamic_cast<ErBaseThread*>(thItr->second)->curTask = task;

                //if not removed, event is assumed to be removed or not dequeued in the given trace.
                oph->eventToPostMap.erase(it);

                oph->onlyBeginSeenMap.insert(std::make_pair(event, std::make_pair(tid, taskId)));

                //store this taskId in threadClass object
                if(tid != MAIN_TID){
                    //we need this info only if Looper exits. For main thread looper never exits.
                    dynamic_cast<ErBaseThread*>(thItr->second)->taskStack.push(newTask);
                }
 
            }else{
                PERROR("BEGIN of event %d with opid %d has been mapped to an operation of non POST type with id %d", 
                        event, curOpCount, it->second);
            }         
        }else{
            PERROR("Dequeue (BEGIN) of an event %d on thread %d seen prior to its post.", event, tid);
        }
    }else{
        PERROR("Thread %d is either not initialized prior to processing BEGIN, or is still executing prior task" 
               " when event %d is dequeued, or its thread's LOOP operation not logged.",
                 tid, event);
    }
}

EndOp::EndOp(OpLog* oplog, OpHelper* oph){
    tid = oplog->tid;
    event = oplog->arg2;

    OpHelper::threadMapIterator thItr = oph->threadMap.find(tid);
    if(thItr != oph->threadMap.end() && thItr->second->curTaskId != -1){
        taskId = thItr->second->curTaskId;
        task = dynamic_cast<ErBaseThread*>(thItr->second)->curTask;
        task->endOp = this;

        thItr->second->curTaskId = -1;
	dynamic_cast<ErBaseThread*>(thItr->second)->curTask = NULL;
    }else{
        PERROR("Thread %d has performed an operation of type END inside an unknown task.", tid);
    }

    setHbSink();

    oph->onlyBeginSeenMap.erase(event);
}

ThreadinitOp::ThreadinitOp(OpLog* oplog, OpHelper* oph){
    tid = oplog->tid; 

    OpHelper::threadMapIterator thItr = oph->threadMap.find(tid);
    if(thItr != oph->threadMap.end() && thItr->second->curTaskId != -1){
        taskId = thItr->second->curTaskId;
        task = dynamic_cast<ErBaseThread*>(thItr->second)->curTask;
        if(tid != MAIN_TID){
            dynamic_cast<ErBaseThread*>(thItr->second)->forkOp->hbSinkOp = this;
        }
    }else{
        PERROR("Thread %d has performed an operation of type THREADINIT inside an unknown task.", tid);
    }

    setHbSink();
}

ThreadexitOp::ThreadexitOp(OpLog* oplog, OpHelper* oph){
    tid = oplog->tid;

    OpHelper::threadMapIterator thItr = oph->threadMap.find(tid);
    if(thItr != oph->threadMap.end() && thItr->second->curTaskId != -1){
        taskId = thItr->second->curTaskId;
        task = dynamic_cast<ErBaseThread*>(thItr->second)->curTask;
    }else{
        PERROR("Thread %d has performed an operation of type THREADEXIT inside an unknown task.", tid);
    }

    setHbSink();
}

ForkOp::ForkOp(OpLog* oplog, OpHelper* oph){
    tid = oplog->tid;
    childTid = oplog->arg3;

    OpHelper::threadMapIterator thItr = oph->threadMap.find(tid);
    if(thItr != oph->threadMap.end() && thItr->second->curTaskId != -1){
        taskId = thItr->second->curTaskId;
        task = dynamic_cast<ErBaseThread*>(thItr->second)->curTask;
    }else{
        PERROR("Thread %d has performed an operation of type FORK inside an unknown task.", tid);
    }


    thItr = oph->threadMap.find(childTid);
    if(thItr == oph->threadMap.end()){
        int childTaskId = oph->taskCount++;
    
        ErBaseTask* childTask = new ErBaseTask(childTaskId, childTid, 0); //0 indicates no associated event
        oph->taskMap.insert(std::make_pair(childTaskId, childTask));        

        ErBaseThread* thread = new ErBaseThread(childTaskId, childTask, this);
        oph->threadMap.insert(std::make_pair(childTid,thread));
    
    }else{
        PERROR("Thread %d has been initialized before being forked.", childTid);
    }

}

JoinOp::JoinOp(OpLog* oplog, OpHelper* oph){
    //taken care by notify-wait semantics
    return;
}

WaitOp::WaitOp(OpLog* oplog, OpHelper* oph){

    tid = oplog->tid;

    OpHelper::threadMapIterator thItr = oph->threadMap.find(tid);
    if(thItr != oph->threadMap.end() && thItr->second->curTaskId != -1){
        taskId = thItr->second->curTaskId;
        task = dynamic_cast<ErBaseThread*>(thItr->second)->curTask;
    }else{
        PERROR("Thread %d has performed an operation of type WAIT inside an unknown task.", tid);
    }

    std::map<int, int>::iterator it = oph->waitThreadToNotifyMap.find(tid);

    if(it != oph->waitThreadToNotifyMap.end()){
        oph->opsToBeDeleted.erase(it->second);

        OpHelper::opMapIterator opItr = oph->opMap.find(it->second);
        NotifyOp* notifyOp = dynamic_cast<NotifyOp*>(opItr->second);
        if(notifyOp != NULL){
            notifyOp->hbSinkOp = this;
            #ifdef DBGPREPROCESS
                printf("sink of notify: %d\n", oph->opCount);
            #endif
        }else{
            PERROR("a WAIT operation on tid %d has been mapped to an operation of non NOTIFY type with id %d",
                    tid, it->second);
        }

        //erase this so that its not wrongly paired with next WAIT
        oph->waitThreadToNotifyMap.erase(it);

    }else{
        PERROR("WAIT seen on tid %d without corresponding NOTIFY", tid);
    }

    setHbSink();
}

TimedWaitOp::TimedWaitOp(OpLog* oplog, OpHelper* oph){

    tid = oplog->tid;

    OpHelper::threadMapIterator thItr = oph->threadMap.find(tid);
    if(thItr != oph->threadMap.end() && thItr->second->curTaskId != -1){
        taskId = thItr->second->curTaskId;
        task = dynamic_cast<ErBaseThread*>(thItr->second)->curTask;
    }else{
        PERROR("Thread %d has performed an operation of type TIMED-WAIT inside an unknown task.", tid);
    }

    setHbSink();
}

NotifyOp::NotifyOp(OpLog* oplog, OpHelper* oph){

    int curOpCount = oph->opCount;

    tid = oplog->tid;
    notifiedTid = oplog->arg1;

    OpHelper::threadMapIterator thItr = oph->threadMap.find(tid);
    if(thItr != oph->threadMap.end() && thItr->second->curTaskId != -1){
        taskId = thItr->second->curTaskId;
        task = dynamic_cast<ErBaseThread*>(thItr->second)->curTask;
    }else if(tid == -1){ //NATIVE-NOTIFY-WAIT
        taskId = -1;
        task = NULL;
    }else{
        PERROR("A non-native thread %d has performed an operation of type NOTIFY inside an unknown task.", tid);
    }

    //hbSink of notify is set after discovering wait.

    std::map<int, int>::iterator it = oph->waitThreadToNotifyMap.find(notifiedTid);
    if(it == oph->waitThreadToNotifyMap.end()){
        oph->waitThreadToNotifyMap.insert(std::make_pair(notifiedTid, curOpCount));
    }else{
        it->second = curOpCount;
    }

    oph->opsToBeDeleted.insert(curOpCount);
}

LoopOp::LoopOp(OpLog* oplog, OpHelper* oph){
    tid = oplog->tid;
    eventqueue = oplog->arg2;

    OpHelper::threadMapIterator thItr = oph->threadMap.find(tid);
    if(thItr != oph->threadMap.end() && thItr->second->curTaskId != -1){
        ErBaseThread* thread = dynamic_cast<ErBaseThread*>(thItr->second);
        if(thread->loopOpId != -1){
            PERROR("Looper already initialized on tid:%d. Nested looping not handled by our current HB rules.", tid );
        }
        thread->loopOpId = oph->opCount;
        thread->loopOp = this;
        taskId = thread->curTaskId;
        task = thread->curTask;

        thread->curTaskId = -1; //after executing this, the thread waits to dequeue an event        
        thread->curTask = NULL; 
    }else{
        PERROR("Thread %d has performed an operation of type LOOP-On-Q inside an unknown task.", tid);
    }

    setHbSink();
}

LoopExitOp::LoopExitOp(OpLog* oplog, OpHelper* oph){
    tid = oplog->tid;
    eventqueue = oplog->arg2;

    OpHelper::threadMapIterator thItr = oph->threadMap.find(tid);
    if(thItr != oph->threadMap.end() && thItr->second->curTaskId == -1){
        ErBaseThread* thread = dynamic_cast<ErBaseThread*>(thItr->second);
        if(thread->loopOpId == -1){
            PERROR("LOOP-EXIT seen on tid:%d prior to starting of the loop. cannot handle this.", tid);
        }
        thread->loopOpId = -1;
        thread->loopOp = NULL;
        taskId = oph->taskCount++; //assign a new taskId and later take join with the stored LOOP operation's HB info

        ErBaseTask* newTask = new ErBaseTask(taskId, tid, 0); //0 indicates no associated event
        oph->taskMap.insert(std::make_pair(taskId, newTask));         
 
        task = newTask;

        thread->curTaskId = taskId; //after executing loop-exit, this thread can no more process events. back to thread behaviour. 
        thread->curTask = task; 

        while (!thread->taskStack.empty())
        {
            hbSrcList.push_back(thread->taskStack.top());
            thread->taskStack.pop();
        }
    }else{
        PERROR("Thread %d has performed an operation of type LOOP-EXIT inside a valid task. This is not possible.", tid);
    }

    setHbSink();
}

EnableEventOp::EnableEventOp(OpLog* oplog, OpHelper* oph){

    int curOpCount = oph->opCount;

    tid = oplog->tid;
    uiEvent = oplog->arg1;
    view = oplog->arg2;
    shouldStore = false;

    OpHelper::threadMapIterator thItr = oph->threadMap.find(tid);
    if(thItr != oph->threadMap.end() && thItr->second->curTaskId != -1){
        taskId = thItr->second->curTaskId;
        task = dynamic_cast<ErBaseThread*>(thItr->second)->curTask;
    }else{
        PERROR("Thread %d has performed an operation of type ENABLE_EVENT inside an unknown task.", tid);
    }

    setHbSink();


    std::pair<u4, int> viewEventPair = std::make_pair(view, uiEvent);
    std::map<std::pair<u4, int>, int>::iterator it = oph->recentUiEnableEventMap.find(viewEventPair);
    if(it == oph->recentUiEnableEventMap.end()){
        oph->recentUiEnableEventMap.insert(std::make_pair(viewEventPair, curOpCount));
    }else{
        it->second = curOpCount;
    }

    oph->enableEventsToBeDeleted.insert(curOpCount);

}

TriggerEventOp::TriggerEventOp(OpLog* oplog, OpHelper* oph){

    tid = oplog->tid;
    uiEvent = oplog->arg1;
    view = oplog->arg2;
    hbSrcOp = NULL;

    OpHelper::threadMapIterator thItr = oph->threadMap.find(tid);
    if(thItr != oph->threadMap.end() && thItr->second->curTaskId != -1){
        taskId = thItr->second->curTaskId;
        task = dynamic_cast<ErBaseThread*>(thItr->second)->curTask;
    }else{
        PERROR("Thread %d has performed an operation of type TRIGGER_EVENT inside an unknown task.", tid);
    }

    std::pair<u4, int> viewEventPair = std::make_pair(view, uiEvent);
    std::map<std::pair<u4, int>, int>::iterator it = oph->recentUiEnableEventMap.find(viewEventPair);

    if(it != oph->recentUiEnableEventMap.end()){
        OpHelper::opMapIterator opItr = oph->opMap.find(it->second);
        EnableEventOp* enOp = dynamic_cast<EnableEventOp*>(opItr->second);
        if(enOp != NULL){
            if(it->second < task->postOpId){
                enOp->tobeInitializedTasks.insert(taskId);
                oph->enableEventsToBeDeleted.erase(it->second);
                oph->addOpToMap = false;
            }else if(tid == enOp->tid){
                task->envOrderedTasks.insert(enOp->taskId);

                oph->addOpToMap = false;

                #ifdef DBGPREPROCESS
                    task->diagnosticPrint(taskId);
                #endif

            }else{ //enable-trigger on different threads
                enOp->shouldStore = true;
                //enable event operation should not be deleted from operation map
                oph->enableEventsToBeDeleted.erase(it->second);

                oph->addOpToMap = true;
                hbSrcOp = enOp;
                hbSinkOp = this;
                enableOpId = it->second;
            }
        }else{
            PERROR("a trigger ui event %d on view %u has been mapped to an operation of non EnableEvent type with id %d", 
                    uiEvent, view, it->second);
        }
        
    }else{
        PERROR("TriggerEvent seen on view %u and uiEvent %d without corresponding EnableEvent", view, uiEvent);
    }

}

EnableActLifecycleOp::EnableActLifecycleOp(OpLog* oplog, OpHelper* oph){

    tid = oplog->tid;
    state = oplog->arg1;
    instance = oplog->arg2;
    shouldStore = false;

    OpHelper::threadMapIterator thItr = oph->threadMap.find(tid);
    if(thItr != oph->threadMap.end() && thItr->second->curTaskId != -1){
        taskId = thItr->second->curTaskId;
        task = dynamic_cast<ErBaseThread*>(thItr->second)->curTask;
    }else{
        PERROR("Thread %d has performed an operation of type ENABLE_ACYIVITY_LIFECYCLE inside an unknown task.", tid);
    }

    setHbSink();

}

TriggerActLifecycleOp::TriggerActLifecycleOp(OpLog* oplog, OpHelper* oph){

    tid = oplog->tid;
    state = oplog->arg1;
    instance = oplog->arg2;

    OpHelper::threadMapIterator thItr = oph->threadMap.find(tid);
    if(thItr != oph->threadMap.end() && thItr->second->curTaskId != -1){
        taskId = thItr->second->curTaskId;
        task = dynamic_cast<ErBaseThread*>(thItr->second)->curTask;
    }else{
        PERROR("Thread %d has performed an operation of type TRIGGER_ACYIVITY_LIFECYCLE inside an unknown task.", tid);
    }

    setHbSink();

}

TriggerReceiverOp::TriggerReceiverOp(OpLog* oplog, OpHelper* oph){

    tid = oplog->tid;
    state = oplog->arg1;
    identifier = oplog->arg2;
    intentId = oplog->arg3;
    actionKey = oplog->arg5;

    OpHelper::threadMapIterator thItr = oph->threadMap.find(tid);
    if(thItr != oph->threadMap.end() && thItr->second->curTaskId != -1){
        taskId = thItr->second->curTaskId;
        task = dynamic_cast<ErBaseThread*>(thItr->second)->curTask;
    }else{
        PERROR("Thread %d has performed an operation of type TRIGGER_RECEIVER inside an unknown task.", tid);
    }

    setHbSink();

}

TriggerServiceOp::TriggerServiceOp(OpLog* oplog, OpHelper* oph){

    tid = oplog->tid;
    state = oplog->arg1;
    identifier = oplog->arg2;
    serviceClassKey = oplog->arg5;

    OpHelper::threadMapIterator thItr = oph->threadMap.find(tid);
    if(thItr != oph->threadMap.end() && thItr->second->curTaskId != -1){
        taskId = thItr->second->curTaskId;
        task = dynamic_cast<ErBaseThread*>(thItr->second)->curTask;
    }else{
        PERROR("Thread %d has performed an operation of type TRIGGER_SERVICE inside an unknown task.", tid);
    }

    setHbSink();

}

EnableWindowFocusOp::EnableWindowFocusOp(OpLog* oplog, OpHelper* oph){

    int curOpCount = oph->opCount;

    tid = oplog->tid;
    windowId = oplog->arg2;
    shouldStore = false;

    OpHelper::threadMapIterator thItr = oph->threadMap.find(tid);
    if(thItr != oph->threadMap.end() && thItr->second->curTaskId != -1){
        taskId = thItr->second->curTaskId;
        task = dynamic_cast<ErBaseThread*>(thItr->second)->curTask;
    }else{
        PERROR("Thread %d has performed an operation of type ENABLE_WINDOW_FOCUS inside an unknown task.", tid);
    }

    setHbSink();

    std::map<u4, int>::iterator it = oph->recentWindowFocusEnableMap.find(windowId);
    if(it == oph->recentWindowFocusEnableMap.end()){
        oph->recentWindowFocusEnableMap.insert(std::make_pair(windowId, curOpCount));
    }else{
        it->second = curOpCount;
    }

    oph->enableEventsToBeDeleted.insert(curOpCount);

}

TriggerWindowFocusOp::TriggerWindowFocusOp(OpLog* oplog, OpHelper* oph){

    tid = oplog->tid;
    windowId = oplog->arg2;
    enableOpId = -1;
    hbSrcOp = NULL;

    OpHelper::threadMapIterator thItr = oph->threadMap.find(tid);
    if(thItr != oph->threadMap.end() && thItr->second->curTaskId != -1){
        taskId = thItr->second->curTaskId;
        task = dynamic_cast<ErBaseThread*>(thItr->second)->curTask;
    }else{
        PERROR("Thread %d has performed an operation of type TRIGGER_EVENT inside an unknown task.", tid);
    }

    std::map<u4, int>::iterator it = oph->recentWindowFocusEnableMap.find(windowId);
    if(it != oph->recentWindowFocusEnableMap.end()){
        /* if enable and trigger are on the same thread then indicate in trigger's task
         * that it needs to take join with enable's task. In case of window-focus we don't
         * add edge from enable to post of window-focus trigger (due to limitation in accurate instrumentation).
         * Instead we only add an edge from end of enable's task to begin of trigger's task
         * if both are on the same thread. Else will add an edge from enable to trigger.
         * If enable & trigger are on the same thread both enable and trigger can be deleted by pre-processing,
         * as required HB info is already stored.
         */
        OpHelper::opMapIterator opItr = oph->opMap.find(it->second);
        EnableWindowFocusOp* enOp = dynamic_cast<EnableWindowFocusOp*>(opItr->second);
        if(enOp != NULL){
            if(it->second < task->postOpId){
                enOp->tobeInitializedTasks.insert(taskId);
                oph->enableEventsToBeDeleted.erase(it->second);
                oph->addOpToMap = false;
            }else if(tid == enOp->tid){
                task->envOrderedTasks.insert(enOp->taskId);

                oph->addOpToMap = false;

                #ifdef DBGPREPROCESS
                    task->diagnosticPrint(taskId);
                #endif
 
            }else{ //enable-trigger on different threads
                enOp->shouldStore = true;
                //enable event operation should not be deleted from operation map
                oph->enableEventsToBeDeleted.erase(it->second);

                oph->addOpToMap = true;
                hbSrcOp = enOp; 
                hbSinkOp = this;
                enableOpId = it->second;
            }
        }else{
            PERROR("a trigger window focus on window %u has been mapped to an operation of non EnableWindowFocus type with id %d",
                    windowId, it->second);
        }

    }else{
        PERROR("TriggWindowFocus seen on window %u  without corresponding EnableEvent", windowId);
    }
}

EnableTimerTaskOp::EnableTimerTaskOp(OpLog* oplog, OpHelper* oph){

    int curOpCount = oph->opCount;

    tid = oplog->tid;
    instance = oplog->arg2;
    shouldStore = false;

    OpHelper::threadMapIterator thItr = oph->threadMap.find(tid);
    if(thItr != oph->threadMap.end() && thItr->second->curTaskId != -1){
        taskId = thItr->second->curTaskId;
        task = dynamic_cast<ErBaseThread*>(thItr->second)->curTask;
    }else{
        PERROR("Thread %d has performed an operation of type ENABLE_TIMER_TASK inside an unknown task.", tid);
    }

    setHbSink();

    //typically each trigger has only one enable. so we keep things simple
    oph->recentEnableTimerTaskMap.insert(std::make_pair(instance, curOpCount));
}

TriggerTimerTaskOp::TriggerTimerTaskOp(OpLog* oplog, OpHelper* oph){

    tid = oplog->tid;
    instance = oplog->arg2;
    enableOpId = -1;
    hbSrcOp = NULL;

    OpHelper::threadMapIterator thItr = oph->threadMap.find(tid);
    if(thItr != oph->threadMap.end() && thItr->second->curTaskId != -1){
        taskId = thItr->second->curTaskId;
        task = dynamic_cast<ErBaseThread*>(thItr->second)->curTask;
    }else{
        PERROR("Thread %d has performed an operation of type TRIGGER_TIMER_TASK inside an unknown task.", tid);
    }

    std::map<u4, int>::iterator it = oph->recentEnableTimerTaskMap.find(instance);
    if(it != oph->recentEnableTimerTaskMap.end()){
        OpHelper::opMapIterator opItr = oph->opMap.find(it->second);
        EnableTimerTaskOp* enOp = dynamic_cast<EnableTimerTaskOp*>(opItr->second);
        if(enOp != NULL){
            if(tid != enOp->tid){
                enOp->shouldStore = true;

                hbSrcOp = enOp; 
                hbSinkOp = NULL;
                enableOpId = it->second;
            }
        }else{
            PERROR("a trigger timer_task on timer instance %u has been mapped to an operation of non enable timer_task type with id %d",
                    instance, it->second);
        }

        //only one trigger timer_task is mapped to an enable_timer_task
        oph->recentEnableTimerTaskMap.erase(it);

    }else{
        PERROR("TRIGGER_TIMER_TASK seen on timer instance %u  without corresponding EnableEvent", instance);
    }
}


}
