/*
 * Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <erbaseUtils.h>

namespace erbaseline{

HbUtils::HbUtils(OpHelper* oph, EnvModel* appEnv) : oph(oph), appEnv(appEnv) {}

HbUtils::~HbUtils(){
    delete oph;
    delete appEnv;
}



ErBaseOperation* HbUtils::preProcessPostOp(OpLog* oplog) {

    ErBaseOperation* op = new PostOp(oplog, this->oph);

    return op;
}

ErBaseOperation* HbUtils::preProcessAtTimePostOp(OpLog* oplog) {

    ErBaseOperation* op = new AtTimePostOp(oplog, this->oph);

    return op;
}

ErBaseOperation* HbUtils::preProcessBeginOp(OpLog* oplog) {

    ErBaseOperation* op = new BeginOp(oplog, this->oph);

    return op;
}

ErBaseOperation* HbUtils::preProcessEndOp(OpLog* oplog) {

    ErBaseOperation* op = new EndOp(oplog, this->oph);

    return op;
}

ErBaseOperation* HbUtils::preProcessAttachqOp(OpLog* oplog) {
    /* not doing anything as attachQ -> post is not followed in majority of
     * the papers & attachQ -> post is not a real causal ordering and the
     * exception can be caught & avoided
      
    ErBaseOperation* op = new AttachqOp();

    return op;
     */
    return NULL;
}

ErBaseOperation* HbUtils::preProcessForkOp(OpLog* oplog) {

    ErBaseOperation* op = new ForkOp(oplog, this->oph);

    return op;
}

ErBaseOperation* HbUtils::preProcessJoinOp(OpLog* oplog) {

    ErBaseOperation* op = new JoinOp(oplog, this->oph);

    return op;
}

ErBaseOperation* HbUtils::preProcessThreadinitOp(OpLog* oplog) {

    ErBaseOperation* op = new ThreadinitOp(oplog, this->oph);

    return op;
}

ErBaseOperation* HbUtils::preProcessThreadexitOp(OpLog* oplog) {

    ErBaseOperation* op = new ThreadexitOp(oplog, this->oph);

    return op;
}

//   ErBaseOperation* preProcessStartOp();
ErBaseOperation* HbUtils::preProcessRemoveEventOp(OpLog* oplog) {

    /* nothing to be done. Corresponding post will get deleted, if
     * it has not yet been dequeued.
     *
    ErBaseOperation* op = new RemoveEventOp(oplog, this->oph);
    */

    return NULL;
}

ErBaseOperation* HbUtils::preProcessLoopOp(OpLog* oplog) {

    ErBaseOperation* op = new LoopOp(oplog, this->oph);

    return op;
}

ErBaseOperation* HbUtils::preProcessLoopExitOp(OpLog* oplog) {

    ErBaseOperation* op = new LoopExitOp(oplog, this->oph);

    return op;
}

ErBaseOperation* HbUtils::preProcessEnableEventOp(OpLog* oplog) {

    ErBaseOperation* op = new EnableEventOp(oplog, this->oph);

    return op;
}

ErBaseOperation* HbUtils::preProcessTriggerEventOp(OpLog* oplog) {

    ErBaseOperation* op = new TriggerEventOp(oplog, this->oph);

    if(oph->addOpToMap){
        return op;
    }else{
        #ifdef DBGPREPROCESS
            op->diagnosticPrint();
        #endif
        delete op;
        return NULL;
    }
}

ErBaseOperation* HbUtils::preProcessEnableActLifecycleOp(OpLog* oplog) {

    ErBaseOperation* op = new EnableActLifecycleOp(oplog, this->oph);

    appEnv->addEnableLifecycleEventToMap(oph->opCount, oplog);

    //oplog->arg1 = activity state and oplog->arg2 = activity instance
    if(oplog->arg1 == ACT_RESUME && oplog->arg2 == 0){
        appEnv->blankEnableResumeOp = oph->opCount;
    }

    if(oplog->arg1 == APPBIND_DONE){
        appEnv->appBindTaskId = op->taskId;
        #ifdef DBGPREPROCESS
            printf("storing APP-BIND-DONE task %d for future reference\n", appEnv->appBindTaskId);
        #endif
    }

    return op;
}

ErBaseOperation* HbUtils::preProcessTriggerActLifecycleOp(OpLog* oplog) {

    ErBaseOperation* op = new TriggerActLifecycleOp(oplog, this->oph);

    bool shouldAbort = true;
    Task* triggerTask = op->task;

    std::pair<bool, std::set<int> > resultPair = 
        appEnv->connectEnableAndTriggerLifecycleEvents(oph->opCount, oplog, op->taskId, triggerTask, oph);
    if(appEnv->isEventActivityEvent(oplog->arg1)){
        bool hbInfoAdded = appEnv->checkAndUpdateActivityComponentState(oph->opCount, oplog, op->taskId, triggerTask);
        shouldAbort = !resultPair.first && !hbInfoAdded;
    }else{
        shouldAbort = !resultPair.first;
    }

    if(shouldAbort){
        PERROR("Trigger-Lifecycle event seen for component %d and state %d without a "
             "corresponding enable event or mismatch in activity state machine during processing."
             " Aborting processing.", oplog->arg2, oplog->arg1);
    }


    std::set<int>::iterator it = triggerTask->envOrderedEnableOps.begin();
    for(; it != triggerTask->envOrderedEnableOps.end(); it++){
        EnableActLifecycleOp* enOp = dynamic_cast<EnableActLifecycleOp*>(oph->opMap.find(*it)->second);
        if(enOp != NULL){
            enOp->tobeInitializedTasks.insert(op->taskId);
        }else{
            PERROR("TRIGGER_ACT_LIFECYCLE at opId %d found a non ENABLE_ACT_LIFECYCLE op with opId %d "
                   " in its task's envOrderedEnableOps set.", oph->opCount, *it);
        }
    }

    it = triggerTask->envOrderedPosts.begin();
    for(; it != triggerTask->envOrderedPosts.end(); it++){
        BasePostOp* postOp = dynamic_cast<BasePostOp*>(oph->opMap.find(*it)->second);
        if(postOp != NULL){
            postOp->tobeInitializedTasks.insert(op->taskId);
        }else{
            PERROR("TRIGGER_ACT_LIFECYCLE at opId %d found a non POST op with opId %d "
                   " in its task's envOrderedPosts set.", oph->opCount, *it);
        }
    }

    TriggerActLifecycleOp* curOp = dynamic_cast<TriggerActLifecycleOp*>(op);
    it = resultPair.second.begin();
    for(; it != resultPair.second.end(); it++){
        EnableActLifecycleOp* enOp = dynamic_cast<EnableActLifecycleOp*>(oph->opMap.find(*it)->second);
        if(enOp != NULL){
            enOp->shouldStore = true;
            curOp->envOrderedEnableOps.insert(*it); 
        }else{
            PERROR("TRIGGER_ACT_LIFECYCLE at opId %d found a non ENABLE_ACT_LIFECYCLE op with opId %d "
                   " in its envOrderedEnableOps set.", oph->opCount, *it);
        }
    }

    #ifdef DBGPREPROCESS
        triggerTask->diagnosticPrint(op->taskId);
    #endif

    triggerTask->envOrderedEnableOps.clear();
    triggerTask->envOrderedPosts.clear();


    return op;
}

ErBaseOperation* HbUtils::preProcessTriggerReceiverOp(OpLog* oplog) {

    if(oplog->arg1 == REC_TRIGGER_ONRECIEVE_LATER){
        return NULL;
    }

    ErBaseOperation* op = new TriggerReceiverOp(oplog, this->oph);

    ErBaseTask* triggerTask = op->task;

    bool receiverUpdated = appEnv->checkAndUpdateBroadcastState(oph->opCount, oplog, op->taskId, triggerTask);
    bool shouldAbort = !receiverUpdated;

    if(shouldAbort){
        PERROR("TRIGGER_RECEIVER at opId %d with state %d and instance %d "
               "has encountered a state machine error.", oph->opCount, oplog->arg1, oplog->arg2);
    }


    std::set<int>::iterator it = triggerTask->envOrderedEnableOps.begin();
    for(; it != triggerTask->envOrderedEnableOps.end(); it++){
        TriggerReceiverOp* enOp = dynamic_cast<TriggerReceiverOp*>(oph->opMap.find(*it)->second);
        if(enOp != NULL){
            enOp->tobeInitializedTasks.insert(op->taskId);
        }else{
            PERROR("TRIGGER_RECEIVER at opId %d found a non TRIGGER_RECEIVER op with opId %d "
                   " in its task's envOrderedEnableOps set.", oph->opCount, *it);
        }
    }

    it = triggerTask->envOrderedPosts.begin();
    for(; it != triggerTask->envOrderedPosts.end(); it++){
        BasePostOp* postOp = dynamic_cast<BasePostOp*>(oph->opMap.find(*it)->second);
        if(postOp != NULL){
            postOp->tobeInitializedTasks.insert(op->taskId);
        }else{
            PERROR("TRIGGER_RECEIVER at opId %d found a non POST op with opId %d "
                   " in its task's envOrderedPosts set.", oph->opCount, *it);
        }
    }

    std::map<int, int>::iterator recIt = appEnv->onRecTaskToEnableOpMap.find(op->taskId);
    if(recIt != appEnv->onRecTaskToEnableOpMap.end()){
        TriggerReceiverOp* recOp = dynamic_cast<TriggerReceiverOp*>(oph->opMap.find(recIt->second)->second);
        if(recOp->state == REC_SEND_BROADCAST || recOp->state == REC_SEND_STICKY_BROADCAST ||
                recOp->state == REC_REGISTER_RECEIVER){
            recOp->onRecTasks.insert(triggerTask);
            #ifdef DBGPREPROCESS
                printf("TRIGGER-RECEIVER: emitEdge enabled on op %d for onReceive task %d in state %d \n", 
                       recIt->second, triggerTask->id, recOp->state);
            #endif
            appEnv->onRecTaskToEnableOpMap.erase(recIt);
        }else{
            PERROR("A TRIGGER-RECEIVER state of type %d wrongly added to onRecTaskToEnableOpMap.", recOp->state);
        }
    }

    if(oplog->arg1 == REC_TRIGGER_ONRECIEVE){
        BeginOp* beginop = dynamic_cast<BeginOp*>(oph->opMap.find(triggerTask->beginOpId)->second);
        beginop->isOnReceive = true;
    }
    
    #ifdef DBGPREPROCESS
        triggerTask->diagnosticPrint(op->taskId);
    #endif

    triggerTask->envOrderedEnableOps.clear();
    triggerTask->envOrderedPosts.clear();

    return op;
}

ErBaseOperation* HbUtils::preProcessTriggerServiceOp(OpLog* oplog) {

    ErBaseOperation* op = new TriggerServiceOp(oplog, oph);

    Task* triggerTask = op->task;

    bool serviceUpdated = appEnv->checkAndUpdateServiceState(oph->opCount, oplog, op->taskId, triggerTask, this->oph);
    bool shouldAbort = !serviceUpdated;

    if(shouldAbort){
        PERROR("TRIGGER_SERVICE at opId %d with state %d and instance/intent/connection-key %d "
               "has encountered a state machine error.", oph->opCount, oplog->arg1, oplog->arg2);
    }

    std::set<int>::iterator it = triggerTask->envOrderedEnableOps.begin();
    for(; it != triggerTask->envOrderedEnableOps.end(); it++){
        TriggerServiceOp* enOp = dynamic_cast<TriggerServiceOp*>(oph->opMap.find(*it)->second);
        if(enOp != NULL){
            enOp->tobeInitializedTasks.insert(op->taskId);
        }else{
            PERROR("TRIGGER_SERVICE at opId %d found a non TRIGGER_SERVICE op with opId %d "
                   " in its task's envOrderedEnableOps set.", oph->opCount, *it);
        }
    }

    it = triggerTask->envOrderedPosts.begin();
    for(; it != triggerTask->envOrderedPosts.end(); it++){
        BasePostOp* postOp = dynamic_cast<BasePostOp*>(oph->opMap.find(*it)->second);
        if(postOp != NULL){
            postOp->tobeInitializedTasks.insert(op->taskId);
        }else{
            PERROR("TRIGGER_SERVICE at opId %d found a non POST op with opId %d "
                   " in its task's envOrderedPosts set.", oph->opCount, *it);
        }
    }

    #ifdef DBGPREPROCESS
    triggerTask->diagnosticPrint(op->taskId);
    #endif

    triggerTask->envOrderedEnableOps.clear();
    triggerTask->envOrderedPosts.clear();

    return op;
}

ErBaseOperation* HbUtils::preProcessInstanceIntentOp(OpLog* oplog) {

    // change this to return null after plugging in instance for intent in concerned maps
    appEnv->mapInstanceWithIntentId(oplog->arg2, oplog->arg1);

    return NULL;
}

ErBaseOperation* HbUtils::preProcessEnableWindowFocusOp(OpLog* oplog) {

    ErBaseOperation* op = new EnableWindowFocusOp(oplog, this->oph);

    return op;
}

ErBaseOperation* HbUtils::preProcessTriggerWindowFocusOp(OpLog* oplog) {

    //if the enable is on the same thread then after setting up HB taskIds to take join with
    //can remove both these ops. If they are on different threads then indicate that corresponding
    //enable needs to be stored.
    ErBaseOperation* op = new TriggerWindowFocusOp(oplog, this->oph);

    if(oph->addOpToMap){
        return op;
    }else{
        #ifdef DBGPREPROCESS
            op->diagnosticPrint();
        #endif
        delete op;
        return NULL;
    }
}

ErBaseOperation* HbUtils::preProcessWaitOp(OpLog* oplog) {

    ErBaseOperation* op = new WaitOp(oplog, this->oph);

    return op;
}

ErBaseOperation* HbUtils::preProcessTimedWaitOp(OpLog* oplog) {
    //currently TIMED_WAIT is a NOP because it does not result in any HB to be added
    //because it is kind of like sleep. Even if it is woken by a NOTIFY that may not be 
    //the case in some other run.
    return NULL;
}

ErBaseOperation* HbUtils::preProcessNotifyOp(OpLog* oplog) {

    ErBaseOperation* op = new NotifyOp(oplog, this->oph);

    return op;
}

ErBaseOperation* HbUtils::preProcessNativePostOp(OpLog* oplog) {

    /* currently we handle post by native thread similar to post by
     * a tracked app thread. Only difference is in this case we will
     * not know the source task of the operation, which is alright.
     * If this is an environment related post enable - trigger or
     * state machine modeling will plug in the missing HB info.
     * Otherwise, nothing much can be done. But we wont quit
     * because of this.
     */
    oplog->opType = DR_POST;
    ErBaseOperation* op = new PostOp(oplog, this->oph);

    return op;
}

ErBaseOperation* HbUtils::preProcessUiPostOp(OpLog* oplog) {

    ErBaseOperation* op = new UiPostOp();

    return op;
}

ErBaseOperation* HbUtils::preProcessIdlePostOp(OpLog* oplog) {

    ErBaseOperation* op = new IdlePostOp(oplog, this->oph);

    return op;
}

ErBaseOperation* HbUtils::preProcessNativeNotifyOp(OpLog* oplog) {

    /* currently we handle notify executed by any  native thread 
     * to wake up a tracked thread, similar to notify by
     * a tracked app thread. Only difference is in this case we will
     * not know the source task of the operation, which is alright.
     * Also, we allow this only if tid = -1 & not if tid is known
     * but is executed outside a task.
     */
    oplog->opType = DR_NOTIFY;
    ErBaseOperation* op = new NotifyOp(oplog, this->oph);

    return op;
}

ErBaseOperation* HbUtils::preProcessPostFoqOp(OpLog* oplog) {

    ErBaseOperation* op = new PostFoqOp(oplog, this->oph);

    return op;
}

ErBaseOperation* HbUtils::preProcessNativePostFoqOp(OpLog* oplog) {

    /*Handled same as POST_FOQ*/
    ErBaseOperation* op = new PostFoqOp(oplog, this->oph);

    return op;
}

ErBaseOperation* HbUtils::preProcessPostNegOp(OpLog* oplog) {

    ErBaseOperation* op = new PostNegOp(oplog, this->oph);

    return op;
}

ErBaseOperation* HbUtils::preProcessEnableTimerTaskOp(OpLog* oplog) {

    ErBaseOperation* op = new EnableTimerTaskOp(oplog, this->oph);

    return op;
}

ErBaseOperation* HbUtils::preProcessTriggerTimerTaskOp(OpLog* oplog) {

    ErBaseOperation* op = new TriggerTimerTaskOp(oplog, this->oph);

    return op;
}



void HbUtils::initializeFunctionPointers(){
    //initialize preprocessing functions (used in preprocessing pass)
    preFuncPtr[DR_POST] = &HbUtils::preProcessPostOp;
    preFuncPtr[DR_AT_TIME_POST] = &HbUtils::preProcessAtTimePostOp;
    preFuncPtr[DR_BEGIN] = &HbUtils::preProcessBeginOp;
    preFuncPtr[DR_END] = &HbUtils::preProcessEndOp;
    preFuncPtr[DR_ATTACH_Q] = &HbUtils::preProcessAttachqOp;
    preFuncPtr[DR_FORK] = &HbUtils::preProcessForkOp;
    preFuncPtr[DR_JOIN] = &HbUtils::preProcessJoinOp;
    preFuncPtr[DR_THREADINIT] = &HbUtils::preProcessThreadinitOp;
    preFuncPtr[DR_THREADEXIT] = &HbUtils::preProcessThreadexitOp; 
//    preFuncPtr[DR_START] = &HbUtils::preProcessStartOp; 
    preFuncPtr[DR_REMOVE_EVENT] = &HbUtils::preProcessRemoveEventOp; 
    preFuncPtr[DR_LOOP] = &HbUtils::preProcessLoopOp; 
    preFuncPtr[DR_LOOP_EXIT] = &HbUtils::preProcessLoopExitOp; 
    preFuncPtr[DR_ENABLE_EVENT] = &HbUtils::preProcessEnableEventOp; 
    preFuncPtr[DR_TRIGGER_EVENT] = &HbUtils::preProcessTriggerEventOp; 
    preFuncPtr[DR_ENABLE_LIFECYCLE] = &HbUtils::preProcessEnableActLifecycleOp; 
    preFuncPtr[DR_TRIGGER_LIFECYCLE] = &HbUtils::preProcessTriggerActLifecycleOp; 
    preFuncPtr[DR_TRIGGER_RECEIVER] = &HbUtils::preProcessTriggerReceiverOp; 
    preFuncPtr[DR_TRIGGER_SERVICE] = &HbUtils::preProcessTriggerServiceOp; 
    preFuncPtr[DR_INSTANCE_INTENT] = &HbUtils::preProcessInstanceIntentOp; 
    preFuncPtr[DR_ENABLE_WINDOW_FOCUS] = &HbUtils::preProcessEnableWindowFocusOp; 
    preFuncPtr[DR_TRIGGER_WINDOW_FOCUS] = &HbUtils::preProcessTriggerWindowFocusOp; 
    preFuncPtr[DR_WAIT] = &HbUtils::preProcessWaitOp; 
    preFuncPtr[DR_TIMED_WAIT] = &HbUtils::preProcessTimedWaitOp; 
    preFuncPtr[DR_NOTIFY] = &HbUtils::preProcessNotifyOp; 
    preFuncPtr[DR_NATIVE_POST] = &HbUtils::preProcessNativePostOp; 
    preFuncPtr[DR_UI_POST] = &HbUtils::preProcessUiPostOp; 
    preFuncPtr[DR_IDLE_POST] = &HbUtils::preProcessIdlePostOp; 
    preFuncPtr[DR_NATIVE_NOTIFY] = &HbUtils::preProcessNativeNotifyOp; 
    preFuncPtr[DR_POST_FOQ] = &HbUtils::preProcessPostFoqOp; 
    preFuncPtr[DR_NATIVE_POST_FOQ] = &HbUtils::preProcessNativePostFoqOp; 
    preFuncPtr[DR_POST_NEG] = &HbUtils::preProcessPostNegOp; 
    preFuncPtr[DR_ENABLE_TIMER_TASK] = &HbUtils::preProcessEnableTimerTaskOp; 
    preFuncPtr[DR_TRIGGER_TIMER_TASK] = &HbUtils::preProcessTriggerTimerTaskOp; 


  
    //initialize functions used when computing HB

}


void HbUtils::initializePreProcess(int mainThreadId){
    int mainTaskId = oph->taskCount++;

    ErBaseTask* mainTask = new ErBaseTask(mainTaskId, mainThreadId, 0); //0 indicates no associated event
    oph->taskMap.insert(std::make_pair(mainTaskId, mainTask));

    ErBaseThread* thread = new ErBaseThread(mainTaskId, mainTask, NULL);
    oph->threadMap.insert(std::make_pair(mainThreadId,thread));

}


void HbUtils::preProcess(OpLog* oplog){
    oph->addOpToMap = true;
    ErBaseOperation* op = (this->*preFuncPtr[oplog->opType])(oplog);
    if(op != NULL){
        #ifdef DBGPREPROCESS
            printf("\nopid %d  ", oph->opCount);
            op->diagnosticPrint();
        #endif
        oph->opMap.insert(std::make_pair(oph->opCount++,op));
    }
}


void HbUtils::emitEndOpsManually(){
    std::map<u4, std::pair<int, int> >::iterator it = oph->onlyBeginSeenMap.begin();
    for(; it != oph->onlyBeginSeenMap.end(); it++){
        #ifdef DBGPREPROCESS
            printf("emitting END operation for task:%d, tid:%d event:%u", 
                    it->second.second, it->second.first, it->first);
        #endif
        /* tid = it->second.first,  taskId = it->second.second,  event = it->first */
        EndOp* op = new EndOp(it->second.first, it->second.second, it->first);
        op->task = dynamic_cast<ErBaseTask*>(oph->taskMap.find(it->second.second)->second);
        oph->opMap.insert(std::make_pair(oph->opCount++,op));
    }
    oph->onlyBeginSeenMap.clear();
}

void HbUtils::printHbComputationStats(ErBaseline* er){
    printf("\nTrace length after pre-processing: %lu", oph->opMap.size());
    printf("\nNo. of threads: %lu", oph->threadMap.size());
    printf("\nNo. of events: %u", Stats::eventCount);
    printf("\nNo. of event-loops: %u", Stats::eventLoopCount);
    printf("\nNo. of chains: %lu", oph->curClockOnChains->getVCSize());
    printf("\n");
    Stats::dfsCount += Stats::noPreDfsCount + Stats::fifoDfsCount + Stats::fifoFoqDfsCount + Stats::receiverDfsCount + Stats::evAtomicDfsCount;
    printf("\nNo. of nodes visited due to DFS: %u", Stats::dfsCount);
    printf("\nNo. of nodes visited due to NO-PRE evaluated at begin ops: %u", Stats::noPreDfsCount);
    printf("\nNo. of nodes visited due to FIFO evaluated at begin ops: %u", Stats::fifoDfsCount);
    printf("\nNo. of nodes visited due to FIFO-FOQ evaluated at begin ops: %u", Stats::fifoFoqDfsCount);
    printf("\nNo. of nodes visited due to BROADCAST-RECEIVER ordering evaluated at begin ops: %u", Stats::receiverDfsCount);
    printf("\nNo. of nodes visited due to NO-PRE ordering evaluated on adding cross-thread edges: %u", Stats::evAtomicDfsCount);
    printf("\nCumulative size of sets of candidate events found during DFS: %u", Stats::usefulWorkCount);
    printf("\n");
    Stats::joinCandidateCount += Stats::noPreJoinCount + Stats::fifoJoinCount + Stats::fifoFoqJoinCount + Stats::receiverJoinCount;
    printf("\nNo. of event handlers identified to be ordered w.r.t. other handlers, by DFS: %u", Stats::joinCandidateCount);
    printf("\n\t identified due to NO-PRE: %u", Stats::noPreJoinCount);
    printf("\n\t identified due to FIFO: %u", Stats::fifoJoinCount);
    printf("\n\t identified due to FIFO-FOQ: %u", Stats::fifoFoqJoinCount);
    printf("\n\t identified due to RECEIVER ordering: %u", Stats::receiverJoinCount);
    printf("\n\t Note: Ordering from parent to child event on same thread is counted in aggregate but not separately reported.");
    printf("\nNo. of times end to begin edges actually added: %u", Stats::actualEdgesAdded);
    printf("\nTotal number of ER iterations taken to reach fixpoint: %d", er->getErBaselineFixpointIteration());
    printf("\n");
    printf("\nSize related stats:");
    printf("\nTotal number of edges added to HBGraph: %u", Stats::hbParentEdgeCount + Stats::hbChildEdgeCount);
    printf("\nTotal size of HB graph: %lu bytes", (Stats::hbParentEdgeCount + Stats::hbChildEdgeCount) * sizeof(ErBaseOperation *));
    printf("\nTotal memory used by all VCs: %lu bytes", Stats::vcMemory);
    printf("\nMemory allocated for tasks: %lu bytes", oph->taskMap.size() * sizeof(ErBaseTask));
}

void HbUtils::computeHBForTrace(){
    #ifdef DBG_COLLECT_STATS
        Stats::initializeHbComputationStats();
    #endif

    ErBaseline* er = new ErBaseline(this->oph);
    er->computeHB();

    #ifdef DBG_COLLECT_STATS
        printHbComputationStats(er);
    #endif
}

}

