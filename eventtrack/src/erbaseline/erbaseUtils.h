/*
 * Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ERBASEUTILS_H_
#define ERBASEUTILS_H_

#include <erbaseOperation.h>
#include <envModel.h>
#include <erbaseAlgo.h>
#include <erbaseStats.h>


namespace erbaseline{

class HbUtils;

class HbUtils{
public:

   HbUtils(OpHelper* oph, EnvModel* appEnv);

   ~HbUtils();

   void initializeFunctionPointers();

   void initializePreProcess(int mainThreadId);

   void preProcess(OpLog* oplog);

   void emitEndOpsManually();

   void computeHBForTrace();

   OpHelper* oph;
   
   EnvModel* appEnv;

private:

   void printHbComputationStats(ErBaseline* et);

   ErBaseOperation* preProcessPostOp(OpLog* oplog);
   ErBaseOperation* preProcessAtTimePostOp(OpLog* oplog);
   ErBaseOperation* preProcessBeginOp(OpLog* oplog);
   ErBaseOperation* preProcessEndOp(OpLog* oplog);
   ErBaseOperation* preProcessAttachqOp(OpLog* oplog);
   ErBaseOperation* preProcessForkOp(OpLog* oplog);
   ErBaseOperation* preProcessJoinOp(OpLog* oplog);
   ErBaseOperation* preProcessThreadinitOp(OpLog* oplog);
   ErBaseOperation* preProcessThreadexitOp(OpLog* oplog);
//   ErBaseOperation* preProcessStartOp();
   ErBaseOperation* preProcessRemoveEventOp(OpLog* oplog);
   ErBaseOperation* preProcessLoopOp(OpLog* oplog);
   ErBaseOperation* preProcessLoopExitOp(OpLog* oplog);
   ErBaseOperation* preProcessEnableEventOp(OpLog* oplog);
   ErBaseOperation* preProcessTriggerEventOp(OpLog* oplog);
   ErBaseOperation* preProcessEnableActLifecycleOp(OpLog* oplog);
   ErBaseOperation* preProcessTriggerActLifecycleOp(OpLog* oplog);
   ErBaseOperation* preProcessTriggerReceiverOp(OpLog* oplog);
   ErBaseOperation* preProcessTriggerServiceOp(OpLog* oplog);
   ErBaseOperation* preProcessInstanceIntentOp(OpLog* oplog);
   ErBaseOperation* preProcessEnableWindowFocusOp(OpLog* oplog);
   ErBaseOperation* preProcessTriggerWindowFocusOp(OpLog* oplog);
   ErBaseOperation* preProcessWaitOp(OpLog* oplog);
   ErBaseOperation* preProcessTimedWaitOp(OpLog* oplog);
   ErBaseOperation* preProcessNotifyOp(OpLog* oplog);
   ErBaseOperation* preProcessNativePostOp(OpLog* oplog);
   ErBaseOperation* preProcessUiPostOp(OpLog* oplog);
   ErBaseOperation* preProcessIdlePostOp(OpLog* oplog);
   ErBaseOperation* preProcessNativeNotifyOp(OpLog* oplog);
   ErBaseOperation* preProcessPostFoqOp(OpLog* oplog);
   ErBaseOperation* preProcessNativePostFoqOp(OpLog* oplog);
   ErBaseOperation* preProcessPostNegOp(OpLog* oplog);
   ErBaseOperation* preProcessEnableTimerTaskOp(OpLog* oplog);
   ErBaseOperation* preProcessTriggerTimerTaskOp(OpLog* oplog);


   ErBaseOperation* (HbUtils::*preFuncPtr [NUM_TYPES]) (OpLog* oplog);
};

}
#endif

