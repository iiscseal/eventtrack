/*
 * Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ERBASEALGO_H_
#define ERBASEALGO_H_

#include <erbaseOperation.h>
#include <erbaseStats.h>

namespace erbaseline{

#define ER_BASE_ITER 0

class ErBaseIterInfo;
class ErBaseline;

class ErBaseIterInfo{

public:

ErBaseIterInfo(int iter, int index, int prevIterIndex, ErBaseIterInfo* prev);

~ErBaseIterInfo();

const int iter; //iteration ID
int index; //last processed operation
const int prevIterIndex;

std::map<int, VectorClock*> vcMap;

//map to store VC info of ops such as loop-on-q and  enable in case shouldStore = true
std::map<int, VectorClock*> vcStore;

ErBaseIterInfo* next;
ErBaseIterInfo* prev;

};


class ErBaseline{

public:

ErBaseline(OpHelper* oph);

~ErBaseline();

void initializeEvaluateFunctionPointers();

void computeHB();

int getErBaselineFixpointIteration();

typedef std::map<int, VectorClock*>::iterator vcIterator;

private:
   std::list<ErBaseOperation*> isSrcFoqPostedPriorDestPost(int srcTaskId, ErBaseTask* destTask, VectorClock* atomicVC);
   std::list<ErBaseOperation*> isSrcNegPostedPriorDestPost(int srcTaskId, ErBaseTask* destTask, VectorClock* atomicVC);
   std::list<ErBaseOperation*> evaluatePostOp(int opId, ErBaseOperation* op, VectorClock* atomicVC);
   std::list<ErBaseOperation*> evaluateAtTimePostOp(int opId, ErBaseOperation* op, VectorClock* atomicVC);
   std::list<ErBaseOperation*> evaluateBeginOp(int opId, ErBaseOperation* op, VectorClock* atomicVC);
   std::list<ErBaseOperation*> evaluateEndOp(int opId, ErBaseOperation* op, VectorClock* atomicVC);
   std::list<ErBaseOperation*> evaluateForkOp(int opId, ErBaseOperation* op, VectorClock* atomicVC);
   std::list<ErBaseOperation*> evaluateThreadinitOp(int opId, ErBaseOperation* op, VectorClock* atomicVC);
   std::list<ErBaseOperation*> evaluateThreadexitOp(int opId, ErBaseOperation* op, VectorClock* atomicVC);
   std::list<ErBaseOperation*> evaluateLoopOp(int opId, ErBaseOperation* op, VectorClock* atomicVC);
   std::list<ErBaseOperation*> evaluateLoopExitOp(int opId, ErBaseOperation* op, VectorClock* atomicVC);
   std::list<ErBaseOperation*> evaluateEnableEventOp(int opId, ErBaseOperation* op, VectorClock* atomicVC);
   std::list<ErBaseOperation*> evaluateTriggerEventOp(int opId, ErBaseOperation* op, VectorClock* atomicVC);
   std::list<ErBaseOperation*> evaluateEnableActLifecycleOp(int opId, ErBaseOperation* op, VectorClock* atomicVC);
   std::list<ErBaseOperation*> evaluateTriggerActLifecycleOp(int opId, ErBaseOperation* op, VectorClock* atomicVC);
   std::list<ErBaseOperation*> evaluateTriggerReceiverOp(int opId, ErBaseOperation* op, VectorClock* atomicVC);
   std::list<ErBaseOperation*> evaluateTriggerServiceOp(int opId, ErBaseOperation* op, VectorClock* atomicVC);
   std::list<ErBaseOperation*> evaluateEnableWindowFocusOp(int opId, ErBaseOperation* op, VectorClock* atomicVC);
   std::list<ErBaseOperation*> evaluateTriggerWindowFocusOp(int opId, ErBaseOperation* op, VectorClock* atomicVC);
   std::list<ErBaseOperation*> evaluateWaitOp(int opId, ErBaseOperation* op, VectorClock* atomicVC);
   std::list<ErBaseOperation*> evaluateNotifyOp(int opId, ErBaseOperation* op, VectorClock* atomicVC);
   std::list<ErBaseOperation*> evaluateIdlePostOp(int opId, ErBaseOperation* op, VectorClock* atomicVC);
   std::list<ErBaseOperation*> evaluatePostFoqOp(int opId, ErBaseOperation* op, VectorClock* atomicVC);
   std::list<ErBaseOperation*> evaluatePostNegOp(int opId, ErBaseOperation* op, VectorClock* atomicVC);
   std::list<ErBaseOperation*> evaluateEnableTimerTaskOp(int opId, ErBaseOperation* op, VectorClock* atomicVC);
   std::list<ErBaseOperation*> evaluateTriggerTimerTaskOp(int opId, ErBaseOperation* op, VectorClock* atomicVC);

   std::list<ErBaseOperation*> (ErBaseline::*evaluateFuncPtr [NUM_TYPES]) (int opId, ErBaseOperation* op, VectorClock* atomicVC);

   void storeHBInfo(int opId, VectorClock* vc);

   void hbOrder(ErBaseOperation* src, ErBaseOperation* dest);

   void baseVcOrder(ErBaseTask* src, VectorClock* destVc);

   bool areHbOrdered(ErBaseTask* src, VectorClock* destVc);

   bool arePostsHbOrdered(BasePostOp* src, BasePostOp* dest);

   //analysis state components (may not be named same as in paper. Also,
   //some info are stored in Tasks and other datastructures.)
   int fp; //holds highest ER trace processing iteration

   u4 highestVisitedFlag;

   ErBaseIterInfo* curFrame;

   ErBaseIterInfo* baseFrame;

   OpHelper* oph;

   //needed to compute FoQ and Neg related FIFO ordering - another way would be to do DFS which seems overkill as number of FoQ posts are usually not too high
   std::map<u4, VectorClock*> foqNegPostVC;

   std::map<int, std::set<ErBaseOperation*> > reevaluateOps;

   /* if a message is posted as FoQ or with negative delay to
    * a thread t then t is added to foqOrNegPostedThreads set. Since Foq or Neg ordering 
    * check can be expensive for ErBaseline, it is performed when executing 
    * BEGIN of a task on a thread t only when t is found in foqOrNegPostedThreads set.
    */
   std::set<int> foqOrNegPostedThreads;

};

}

#endif //ERBASEALGO_H_
