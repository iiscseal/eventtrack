/*
 * Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <erbaseStats.h>

namespace erbaseline{

u4 Stats::dfsCount;
u4 Stats::noPreDfsCount;
u4 Stats::fifoDfsCount;
u4 Stats::fifoFoqDfsCount;
u4 Stats::receiverDfsCount;
u4 Stats::evAtomicDfsCount;
u4 Stats::usefulWorkCount;
u4 Stats::actualEdgesAdded;
u4 Stats::joinCandidateCount;
u4 Stats::noPreJoinCount;
u4 Stats::fifoJoinCount;
u4 Stats::fifoFoqJoinCount;
u4 Stats::receiverJoinCount;
u4 Stats::hbParentEdgeCount;
u4 Stats::hbChildEdgeCount;
u8 Stats::vcMemory;
u4 Stats::eventCount;
u4 Stats::eventLoopCount;

void Stats::initializeHbComputationStats(){
    dfsCount = 0;
    noPreDfsCount = 0;
    fifoDfsCount = 0;
    fifoFoqDfsCount = 0;
    receiverDfsCount = 0;
    evAtomicDfsCount = 0;
    usefulWorkCount = 0;
    actualEdgesAdded = 0;
    joinCandidateCount = 0;
    noPreJoinCount = 0;
    fifoJoinCount = 0;
    fifoFoqJoinCount = 0;
    receiverJoinCount = 0;
    hbParentEdgeCount = 0;
    hbChildEdgeCount = 0;
    vcMemory = 0;
    eventCount = 0;
    eventLoopCount = 0;

}

}
