/*
 * Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <erbaseAlgo.h>

namespace erbaseline{

ErBaseIterInfo::ErBaseIterInfo(int iter, int index, int prevIterIndex, ErBaseIterInfo* prev) : 
        iter(iter), index(index), prevIterIndex(prevIterIndex), prev(prev){
    next = NULL;
}

ErBaseIterInfo::~ErBaseIterInfo(){
    #ifdef DBG_COMPUTEHB
        printf("Exiting trace processing iteration %d of ER after evaluating "
               "operation at index %d.\n", iter, index);
    #endif
}


ErBaseline::ErBaseline(OpHelper* oph) : oph(oph){
    fp = ER_BASE_ITER;
    highestVisitedFlag = 1;
    curFrame = new ErBaseIterInfo(ER_BASE_ITER, 0, -1, NULL);
    baseFrame = curFrame;
}

ErBaseline::~ErBaseline(){
    delete curFrame;
}

int ErBaseline::getErBaselineFixpointIteration(){
    return fp+1;
}

std::list<ErBaseOperation*> ErBaseline::evaluatePostOp(int opId, ErBaseOperation* op, VectorClock* atomicVC){
    std::list<ErBaseOperation*> crossHbList;

    PostOp* postop = dynamic_cast<PostOp*>(op);
    if(op->task != NULL){
        ErBaseTask* task = op->task;

        if(op->hbSinkOp == NULL){
             PERROR("Sink unknown for POST(%d,%u,%d). Aborting HB computation.", op->tid, postop->event, postop->dest);
        }

        ErBaseline::vcIterator vcIt = curFrame->vcMap.find(task->id);
        VectorClock* vc = NULL;
        if(vcIt != curFrame->vcMap.end()){
            vc = vcIt->second;
        }else{
            PERROR("Missing VC of task %d of POST(%d,%u,%d). "
                   "Aborting as something has been "
                   "wrongly setup.", task->id, op->tid, postop->event, postop->dest);
        }

        //update or initialize VC of posted task (both are possible as some lifecycle related tasks are initialized by enable ops even prior to post)
        ErBaseline::vcIterator eventVcIt = curFrame->vcMap.find(op->hbSinkOp->task->id);
        if(eventVcIt == curFrame->vcMap.end()){
            VectorClock* eventVC = new VectorClock(*vc);
            curFrame->vcMap.insert(std::make_pair(op->hbSinkOp->task->id, eventVC));
        }else{
            eventVcIt->second->vcJoin(vc);
        }

        /*if(curFrame->iter == ER_BASE_ITER){
            hbOrder(op, op->hbSinkOp);
        }*/

        std::set<int>::iterator setIt = postop->tobeInitializedTasks.begin();
        for(; setIt != postop->tobeInitializedTasks.end(); setIt++){
            ErBaseline::vcIterator targetVcIt = curFrame->vcMap.find(*setIt);
            if(targetVcIt == curFrame->vcMap.end()){
                VectorClock* targetVc = new VectorClock(*vc);
                curFrame->vcMap.insert(std::make_pair(*setIt, targetVc));
            }else{
                targetVcIt->second->vcJoin(vc);
            }

            ErBaseTask* targetTask = dynamic_cast<ErBaseTask*>(oph->taskMap.find(*setIt)->second);
            if(curFrame->iter == ER_BASE_ITER){
                hbOrder(op, targetTask->postOp);//in our scenarios targetTask->postOp is an environment post and hence there's no scope for EV-ATOMIC rule to kick in here.
            }
        }


        /* only the initial ER iteration changes the epoch of a task. 
         * Other iterations only transfer HB info between tasks.
         */
        if(curFrame->iter == ER_BASE_ITER){
            int chain = oph->getChain(task, vc);
            int epoch = vc->incrementClock(chain);
            task->epoch = epoch;
            //reflect that the max clock value on 'chain' has changed
            oph->curClockOnChains->incrementClock(chain);
        }

        #ifdef DBG_COMPUTEHB
            printf("\nopid %d  ", opId);
            postop->diagnosticPrint();
            vc->diagnosticPrint();
        #endif

    }else if(op->taskId == -1){
        if(op->hbSinkOp != NULL){
            /*if(curFrame->iter == ER_BASE_ITER){
                hbOrder(op, op->hbSinkOp);
            }*/
            if(!postop->tobeInitializedTasks.empty()){
                std::set<int>::iterator setIt = postop->tobeInitializedTasks.begin();
                for(; setIt != postop->tobeInitializedTasks.end(); setIt++){
                    ErBaseTask* targetTask = dynamic_cast<ErBaseTask*>(oph->taskMap.find(*setIt)->second);
                    if(curFrame->iter == ER_BASE_ITER){
                        hbOrder(op, targetTask->postOp);
                    }
                }
            }//else - nothing to be done
        }else{
            PERROR("missing hbSinkOp when processing native POST(%d,%u,%d).",
            op->tid, postop->event, postop->dest);
        }
        #ifdef DBG_COMPUTEHB
            printf("\nopid %d  ", opId);
            postop->diagnosticPrint();
        #endif
        //we are fine with POST outside tasks, just that we will only be updating posted
        // task's HB info. 
    }else{
        PERROR("missing task found when processing POST(%d,%u,%d).",
                op->tid, postop->event, postop->dest);
    }

    return crossHbList;
}


std::list<ErBaseOperation*> ErBaseline::evaluateAtTimePostOp(int opId, ErBaseOperation* op, VectorClock* atomicVC){
    std::list<ErBaseOperation*> crossHbList;

    AtTimePostOp* postop = dynamic_cast<AtTimePostOp*>(op);
    if(op->task != NULL){
        ErBaseTask* task = op->task;

        if(op->hbSinkOp == NULL){
             PERROR("Sink unknown for AT-TIME-POST(%d,%u,%d). Aborting HB computation.", op->tid, postop->event, postop->dest);
        }

        ErBaseline::vcIterator vcIt = curFrame->vcMap.find(task->id);
        VectorClock* vc = NULL;
        if(vcIt != curFrame->vcMap.end()){
            vc = vcIt->second;
        }else{
            PERROR("Missing VC of task %d of AT-TIME-POST(%d,%u,%d). "
                   "Aborting as something has been "
                   "wrongly setup.", task->id, op->tid, postop->event, postop->dest);
        }

        //update or initialize VC of posted task (both are possible as some lifecycle related tasks are initialized by enable ops even prior to post)
        ErBaseline::vcIterator eventVcIt = curFrame->vcMap.find(op->hbSinkOp->task->id);
        if(eventVcIt == curFrame->vcMap.end()){
            VectorClock* eventVC = new VectorClock(*vc);
            curFrame->vcMap.insert(std::make_pair(op->hbSinkOp->task->id, eventVC));
        }else{
            eventVcIt->second->vcJoin(vc);
        }

        /*if(curFrame->iter == ER_BASE_ITER){
            hbOrder(op, op->hbSinkOp);
        }*/

        std::set<int>::iterator setIt = postop->tobeInitializedTasks.begin();
        for(; setIt != postop->tobeInitializedTasks.end(); setIt++){
            ErBaseline::vcIterator targetVcIt = curFrame->vcMap.find(*setIt);
            if(targetVcIt == curFrame->vcMap.end()){
                VectorClock* targetVc = new VectorClock(*vc);
                curFrame->vcMap.insert(std::make_pair(*setIt, targetVc));
            }else{
                targetVcIt->second->vcJoin(vc);
            }

            ErBaseTask* targetTask = dynamic_cast<ErBaseTask*>(oph->taskMap.find(*setIt)->second);
            if(curFrame->iter == ER_BASE_ITER){
                hbOrder(op, targetTask->postOp);//in our scenarios targetTask->postOp is an environment post and hence there's no scope for EV-ATOMIC rule to kick in here.
            }
        }


        /* only the initial ER iteration changes the epoch of a task. 
         * Other iterations only transfer HB info between tasks.
         */
        if(curFrame->iter == ER_BASE_ITER){
            int chain = oph->getChain(task, vc);
            int epoch = vc->incrementClock(chain);
            task->epoch = epoch;
            //reflect that the max clock value on 'chain' has changed
            oph->curClockOnChains->incrementClock(chain);
        }

        #ifdef DBG_COMPUTEHB
            printf("\nopid %d  ", opId);
            postop->diagnosticPrint();
            vc->diagnosticPrint();
        #endif


    }else if(op->taskId == -1){
        //as per our current instrumentation of environment events this will not hit as none of them do postAtTime.
        if(op->hbSinkOp != NULL){
            /*if(curFrame->iter == ER_BASE_ITER){
                hbOrder(op, op->hbSinkOp);
            }*/
            if(!postop->tobeInitializedTasks.empty()){
                std::set<int>::iterator setIt = postop->tobeInitializedTasks.begin();
                for(; setIt != postop->tobeInitializedTasks.end(); setIt++){
                    ErBaseTask* targetTask = dynamic_cast<ErBaseTask*>(oph->taskMap.find(*setIt)->second);
                    if(curFrame->iter == ER_BASE_ITER){
                        hbOrder(op, targetTask->postOp);
                    }
                }
            }//else - nothing to be done
        }else{
            PERROR("missing hbSinkOp when processing native POST-AT-TIME(%d,%u,%d).",
            op->tid, postop->event, postop->dest);
        }
        #ifdef DBG_COMPUTEHB
            printf("\nopid %d  ", opId);
            postop->diagnosticPrint();
        #endif
        //we are fine with POST outside tasks, just that we will only be updating posted
        // task's HB info. 
    }else{
        PERROR("missing task found when processing POST-AT-TIME(%d,%u,%d).",
                op->tid, postop->event, postop->dest);
    }

    return crossHbList;
}


std::list<ErBaseOperation*> ErBaseline::evaluateBeginOp(int opId, ErBaseOperation* op, VectorClock* atomicVC){
    std::list<ErBaseOperation*> crossHbList;
    BeginOp* beginop = dynamic_cast<BeginOp*>(op);

    if(op->task == NULL){
        PERROR("missing task found when processing BEGIN(%d,%u).",
                op->tid, beginop->event);
    }

    #ifdef DBG_COLLECT_STATS
        Stats::eventCount++;
    #endif

    ErBaseTask* task = op->task;

    ErBaseline::vcIterator vcIt = curFrame->vcMap.find(task->id);
    VectorClock* vc = NULL;
    if(vcIt != curFrame->vcMap.end()){
        vc = vcIt->second;
    }else{
        vc = new VectorClock();
        curFrame->vcMap.insert(std::make_pair(task->id, vc));
    }

    BasePostOp* postOp = task->postOp;
    int typeOfPost = postOp->getTypeId();
    bool edgeAdded = false;
    bool doEvAtomicCheck = false;

    #ifdef ER_ENABLE_EVENT_COVER
        std::map<int, ErBaseTask*> tobeJoinedTaskMap;
    #endif
        
    if(postOp->tid != op->tid && postOp->task != NULL){
        if(curFrame->iter == ER_BASE_ITER){
            hbOrder(postOp, op);
        }
        doEvAtomicCheck = true;
    }else if(postOp->task == NULL){
        //this happens when postOp is an environment post
        if(curFrame->iter == ER_BASE_ITER){
            hbOrder(postOp, op);
        }
        //need to evaluate NO-PRE rule since environment posts can have incoming edge from 
        //enable ops executed on non-environment tasks. Not evaluating NO-PRE for environemnt
        //posts can thus be unsound.
        doEvAtomicCheck = true;
    }else if(postOp->task->eventId > 0){
        //post and begin are on same thread but different tasks 
        if(curFrame->iter == ER_BASE_ITER){
            #ifndef ER_ENABLE_EVENT_COVER
                baseVcOrder(postOp->task, vc);
                hbOrder(postOp->task->endOp, op);
  
                edgeAdded = true;
                #ifdef DBG_COLLECT_STATS
                    Stats::actualEdgesAdded++;
                #endif
            #endif

            #ifdef ER_ENABLE_EVENT_COVER
                tobeJoinedTaskMap.insert(std::make_pair(postOp->task->id, postOp->task));
            #endif

            #ifdef DBG_COLLECT_STATS
                Stats::joinCandidateCount++;
            #endif
        }else{
            std::map<int, std::set<ErBaseOperation*> >::iterator it = reevaluateOps.find(curFrame->iter);
            if(it->second.find(postOp->task->endOp) != it->second.end()){
                #ifndef ER_ENABLE_EVENT_COVER
                    baseVcOrder(postOp->task, vc);

                    edgeAdded = true;
                    #ifdef DBG_COLLECT_STATS
                        Stats::actualEdgesAdded++;
                    #endif
                #endif

                #ifdef ER_ENABLE_EVENT_COVER
                    tobeJoinedTaskMap.insert(std::make_pair(postOp->task->id, postOp->task));
                #endif
            }
        }
    }

    //EV-ATOMIC -- same as NO-PRE rule
    if(doEvAtomicCheck){
        highestVisitedFlag++;
        std::stack<ErBaseOperation*> dfsStack;
        std::set<ErBaseOperation*>::iterator parentIt = postOp->parentOps.begin();
        for(; parentIt != postOp->parentOps.end(); parentIt++){
            if((*parentIt)->visitedCount < highestVisitedFlag){
                (*parentIt)->visitedCount = highestVisitedFlag;
                dfsStack.push(*parentIt);
            }
        }

        while(!dfsStack.empty()){
            #ifdef DBG_COLLECT_STATS
                Stats::noPreDfsCount++;
            #endif

            ErBaseOperation* cur = dfsStack.top();
            dfsStack.pop();

            if(cur->tid == op->tid && cur->task != NULL){
                #ifdef DBG_COLLECT_STATS
                    Stats::usefulWorkCount++;
                #endif
                if(cur->task->eventId > 0){
                    if(curFrame->iter != ER_BASE_ITER || !areHbOrdered(cur->task, vc)){
                        //just a check to avoid repeated joins atleast in the first iteration.
                        //Doing this for other iterations can be unsound.
                        #ifndef ER_ENABLE_EVENT_COVER
                            baseVcOrder(cur->task, vc);
                            hbOrder(cur->task->endOp, op);

                            edgeAdded = true;
                            #ifdef DBG_COLLECT_STATS
                                Stats::actualEdgesAdded++;
                            #endif
                        #endif

                        #ifdef ER_ENABLE_EVENT_COVER
                            tobeJoinedTaskMap.insert(std::make_pair(cur->task->id, cur->task));
                        #endif
                    }//else - already ordered
                    #ifdef DBG_COLLECT_STATS
                        Stats::noPreJoinCount++;
                    #endif
                }//else - reached op prior to LOOP. Hence, can prune.
            }else{
                for(parentIt = cur->parentOps.begin(); parentIt != cur->parentOps.end(); parentIt++){
                    if((*parentIt)->visitedCount < highestVisitedFlag){
                        (*parentIt)->visitedCount = highestVisitedFlag;
                        dfsStack.push(*parentIt);
                    }
                }
            }
        }
    }

    std::stack<ErBaseOperation*> dfsStack;
    //FIFO computation 
    highestVisitedFlag++;
    std::set<ErBaseOperation*>::iterator parentIt;
    if(typeOfPost != DR_POST_FOQ && typeOfPost != DR_POST_NEG){
        parentIt = postOp->parentOps.begin();
        for(; parentIt != postOp->parentOps.end(); parentIt++){
            if((*parentIt)->visitedCount < highestVisitedFlag){
                (*parentIt)->visitedCount = highestVisitedFlag;
                dfsStack.push(*parentIt);
            }
        }
    }

    while(!dfsStack.empty()){
        #ifdef DBG_COLLECT_STATS
            Stats::fifoDfsCount++;
        #endif
        ErBaseOperation* cur = dfsStack.top();
        dfsStack.pop();
        BasePostOp* curPostOp = dynamic_cast<BasePostOp*>(cur);
        if(curPostOp != NULL && curPostOp->dest == op->tid){
            #ifdef DBG_COLLECT_STATS
                Stats::usefulWorkCount++;
            #endif
             
            if(curPostOp->isFifoOrdered(postOp, typeOfPost)){
               if( curFrame->iter != ER_BASE_ITER || !areHbOrdered(curPostOp->hbSinkOp->task, vc) ){
                    #ifndef ER_ENABLE_EVENT_COVER
                        baseVcOrder(curPostOp->hbSinkOp->task, vc);    
                        hbOrder(curPostOp->hbSinkOp->task->endOp, op);    

                        edgeAdded = true;
                        #ifdef DBG_COLLECT_STATS
                            Stats::actualEdgesAdded++;
                        #endif
                    #endif
 
                    #ifdef ER_ENABLE_EVENT_COVER
                        tobeJoinedTaskMap.insert(std::make_pair(curPostOp->hbSinkOp->task->id, curPostOp->hbSinkOp->task));
                    #endif
                }
                #ifdef DBG_COLLECT_STATS
                    Stats::fifoJoinCount++;
                #endif
            }

            if(typeOfPost == DR_POST){
                if(cur->getTypeId() == DR_POST && dynamic_cast<PostOp*>(cur)->delay == dynamic_cast<PostOp*>(postOp)->delay){
                    //make begin of curPostOp's task visited --- this is also part of pruning
                    if(cur->task != NULL && cur->task->eventId > 0){
                        cur->task->postOp->hbSinkOp->visitedCount = highestVisitedFlag;
                    } 
                    continue;
                }
            }else if(typeOfPost == DR_IDLE_POST && cur->getTypeId() == DR_IDLE_POST){
                //make begin of curPostOp's task visited --- this is also part of pruning
                if(cur->task != NULL && cur->task->eventId > 0){
                        cur->task->postOp->hbSinkOp->visitedCount = highestVisitedFlag;
                }
                continue;
            }
        }

        for(parentIt = cur->parentOps.begin(); parentIt != cur->parentOps.end(); parentIt++){
            if((*parentIt)->visitedCount < highestVisitedFlag){
                (*parentIt)->visitedCount = highestVisitedFlag;
                dfsStack.push(*parentIt);
            }
        }
    }

    if(beginop->isOnReceive){
        //analogous to ordering due to KNOWN_SEND in EventTrack
        highestVisitedFlag++;

        //note that if an onReceive neither has corresponding register or send broadcast the below code will
        //completely avoid unnecessary DFS.
        for(std::set<ErBaseOperation*>::iterator postParentIt = postOp->parentOps.begin();
                postParentIt != postOp->parentOps.end(); postParentIt++){
            for(parentIt = (*postParentIt)->parentOps.begin(); parentIt != (*postParentIt)->parentOps.end();
                   parentIt++){
                if((*parentIt)->visitedCount < highestVisitedFlag){
                    (*parentIt)->visitedCount = highestVisitedFlag;
                    dfsStack.push(*parentIt);
                }
            }
        }

        while(!dfsStack.empty()){
            #ifdef DBG_COLLECT_STATS
                Stats::receiverDfsCount++;
            #endif

            ErBaseOperation* cur = dfsStack.top();
            dfsStack.pop();
            TriggerReceiverOp* triggerRecOp = dynamic_cast<TriggerReceiverOp*>(cur);
            if(triggerRecOp != NULL && !triggerRecOp->onRecTasks.empty() &&
                    triggerRecOp->onRecTasks.find(task) == triggerRecOp->onRecTasks.end()){
                #ifdef DBG_COLLECT_STATS
                    Stats::usefulWorkCount++;
                #endif

                for(std::set<ErBaseTask*>::iterator recTaskIt = triggerRecOp->onRecTasks.begin();
                        recTaskIt != triggerRecOp->onRecTasks.end(); recTaskIt++){
                    if(curFrame->iter != ER_BASE_ITER || !areHbOrdered((*recTaskIt), vc)){
                        #ifndef ER_ENABLE_EVENT_COVER
                            baseVcOrder((*recTaskIt), vc);   
                            hbOrder((*recTaskIt)->endOp, op);   

                            edgeAdded = true;
                            #ifdef DBG_COLLECT_STATS
                                Stats::actualEdgesAdded++;
                            #endif
                        #endif

                        #ifdef ER_ENABLE_EVENT_COVER
                            tobeJoinedTaskMap.insert(std::make_pair((*recTaskIt)->id, *recTaskIt));
                        #endif
                    }
                    #ifdef DBG_COLLECT_STATS
                        Stats::receiverJoinCount++;
                    #endif
                }

                if(cur->task->eventId > 0){//we dont collect traces where native threads send broadcast. Hence task is non-null
                    cur->task->postOp->hbSinkOp->visitedCount = highestVisitedFlag;
                }
            }else{
                for(parentIt = cur->parentOps.begin(); parentIt != cur->parentOps.end(); parentIt++){
                    if((*parentIt)->visitedCount < highestVisitedFlag){
                        (*parentIt)->visitedCount = highestVisitedFlag;
                        dfsStack.push(*parentIt);
                    }
                }
            }
        }
    }

    //hb-vc order environment ordered tasks
    std::set<int>::iterator envIt = task->envOrderedTasks.begin();
    for(; envIt != task->envOrderedTasks.end(); envIt++){
        ErBaseTask* envOrderedTask = dynamic_cast<ErBaseTask*>(oph->taskMap.find(*envIt)->second);
        if(curFrame->iter == ER_BASE_ITER){
            if(!areHbOrdered(envOrderedTask, vc)){
                #ifndef ER_ENABLE_EVENT_COVER
                    baseVcOrder(envOrderedTask, vc);
                    hbOrder(envOrderedTask->endOp, op);
 
                    edgeAdded = true;
                    #ifdef DBG_COLLECT_STATS
                        Stats::actualEdgesAdded++;
                    #endif
                #endif

                #ifdef ER_ENABLE_EVENT_COVER
                    tobeJoinedTaskMap.insert(std::make_pair(envOrderedTask->id, envOrderedTask));    
                #endif
            }
            #ifdef DBG_COLLECT_STATS
                Stats::joinCandidateCount++;
            #endif
        }else{
            std::map<int, std::set<ErBaseOperation*> >::iterator it = reevaluateOps.find(curFrame->iter);
            if(it->second.find(envOrderedTask->endOp) != it->second.end()){
                #ifndef ER_ENABLE_EVENT_COVER
                    baseVcOrder(envOrderedTask, vc);

                    edgeAdded = true;
                    #ifdef DBG_COLLECT_STATS
                        Stats::actualEdgesAdded++;
                    #endif
                #endif

                #ifdef ER_ENABLE_EVENT_COVER
                    tobeJoinedTaskMap.insert(std::make_pair(envOrderedTask->id, envOrderedTask));
                #endif
            }
        }
    }

    #ifdef ER_ENABLE_EVENT_COVER
        //compute VC-join to order events
        std::map<int, ErBaseTask*>::reverse_iterator tobeJoinedTaskIt = tobeJoinedTaskMap.rbegin();
        for(; tobeJoinedTaskIt != tobeJoinedTaskMap.rend(); tobeJoinedTaskIt++){
            if(!areHbOrdered(tobeJoinedTaskIt->second, vc)){
                baseVcOrder(tobeJoinedTaskIt->second, vc);
                hbOrder(tobeJoinedTaskIt->second->endOp, op);

                #ifdef DBG_COLLECT_STATS
                    Stats::actualEdgesAdded++;
                #endif
                edgeAdded = true;
            }
        }
        tobeJoinedTaskMap.clear();
    #endif


    //only if any edges were added by other rules and an foq event has been dequeued, fixpoint computation kicks in
    if(edgeAdded && foqOrNegPostedThreads.find(op->tid) != foqOrNegPostedThreads.end()){
        //fixpoint computation due to FIFO-FoQ
        bool exitLoop = true;
        highestVisitedFlag++;
        do{
            exitLoop = true;

            std::set<ErBaseOperation*>::iterator parentIt = op->parentOps.begin();
            for(; parentIt != op->parentOps.end(); parentIt++){
                if((*parentIt)->visitedCount < highestVisitedFlag){
                    (*parentIt)->visitedCount = highestVisitedFlag;
                    dfsStack.push(*parentIt);
                }
            }

            bool doPrune = false;

            while(!dfsStack.empty()){
                #ifdef DBG_COLLECT_STATS
                    Stats::fifoFoqDfsCount++;
                #endif

                ErBaseOperation* cur = dfsStack.top();
                dfsStack.pop();

                if(cur == postOp){
                    doPrune = true;//post-foqs and post-negs that can be reached via beginop's post are already taken care by FIFO computation
                }else if(cur->getTypeId() == DR_POST_FOQ || cur->getTypeId() == DR_POST_NEG){
                    BasePostOp* curPostOp = dynamic_cast<BasePostOp*>(cur);

                    if(curPostOp->dest == op->tid){
                        #ifdef DBG_COLLECT_STATS
                            Stats::usefulWorkCount++;
                        #endif
                        if(typeOfPost == DR_POST || typeOfPost == DR_AT_TIME_POST || typeOfPost == DR_IDLE_POST){ 
                            if(curFrame->iter != ER_BASE_ITER || !areHbOrdered(curPostOp->hbSinkOp->task, vc)){
                                #ifndef ER_ENABLE_EVENT_COVER
                                    baseVcOrder(curPostOp->hbSinkOp->task, vc);
                                    hbOrder(curPostOp->hbSinkOp->task->endOp, op);

                                    exitLoop = false;
                                    #ifdef DBG_COLLECT_STATS
                                        Stats::actualEdgesAdded++;
                                    #endif
                                #endif

                                #ifdef ER_ENABLE_EVENT_COVER
                                    tobeJoinedTaskMap.insert(std::make_pair(curPostOp->hbSinkOp->task->id, curPostOp->hbSinkOp->task));
                                #endif
                            }
                            #ifdef DBG_COLLECT_STATS
                                Stats::fifoFoqJoinCount++;
                            #endif
                        }else if(typeOfPost == DR_POST_FOQ){ 
                            if(cur == postOp){
                                doPrune = true;
                            }else if(arePostsHbOrdered(postOp, dynamic_cast<BasePostOp*>(cur))){
                                if(curFrame->iter != ER_BASE_ITER || !areHbOrdered(curPostOp->hbSinkOp->task, vc)){
                                    #ifndef ER_ENABLE_EVENT_COVER
                                        baseVcOrder(curPostOp->hbSinkOp->task, vc);
                                        hbOrder(curPostOp->hbSinkOp->task->endOp, op);

                                        exitLoop = false;
                                        #ifdef DBG_COLLECT_STATS
                                            Stats::actualEdgesAdded++;
                                        #endif
                                    #endif

                                    #ifdef ER_ENABLE_EVENT_COVER
                                        tobeJoinedTaskMap.insert(std::make_pair(curPostOp->hbSinkOp->task->id, curPostOp->hbSinkOp->task));
                                    #endif
                                }
                                #ifdef DBG_COLLECT_STATS
                                    Stats::fifoFoqJoinCount++;
                                #endif
                            }
                        }else if(typeOfPost == DR_POST_NEG && cur->getTypeId() == DR_POST_FOQ){
                            if(cur == postOp){
                                doPrune = true;
                            }else if(arePostsHbOrdered(postOp, dynamic_cast<BasePostOp*>(cur))){
                                if(curFrame->iter != ER_BASE_ITER || !areHbOrdered(curPostOp->hbSinkOp->task, vc)){
                                    #ifndef ER_ENABLE_EVENT_COVER
                                        baseVcOrder(curPostOp->hbSinkOp->task, vc);
                                        hbOrder(curPostOp->hbSinkOp->task->endOp, op);

                                        exitLoop = false;
                                        #ifdef DBG_COLLECT_STATS
                                            Stats::actualEdgesAdded++;
                                        #endif
                                    #endif
 
                                    #ifdef ER_ENABLE_EVENT_COVER
                                        tobeJoinedTaskMap.insert(std::make_pair(curPostOp->hbSinkOp->task->id, curPostOp->hbSinkOp->task));
                                    #endif
                                }
                                #ifdef DBG_COLLECT_STATS
                                    Stats::fifoFoqJoinCount++;
                                #endif
                            }
                        }
                    }
                }
                /*else if(cur == postOp){
                    doPrune = true;//post-foqs and post-negs that can be reached via beginop's post are already taken care by FIFO computation
                }*/

                if(doPrune){
                    if(cur->task != NULL && cur->task->eventId > 0){
                        cur->task->postOp->hbSinkOp->visitedCount = highestVisitedFlag;
                    }
                    doPrune = false;
                }else{
                    for(parentIt = cur->parentOps.begin(); parentIt != cur->parentOps.end(); parentIt++){
                        if((*parentIt)->visitedCount < highestVisitedFlag){
                            (*parentIt)->visitedCount = highestVisitedFlag;
                            dfsStack.push(*parentIt);
                        }
                    }
                }
            }

            #ifdef ER_ENABLE_EVENT_COVER
                //compute VC-join to order events
                std::map<int, ErBaseTask*>::reverse_iterator tobeJoinedTaskIt = tobeJoinedTaskMap.rbegin();
                for(; tobeJoinedTaskIt != tobeJoinedTaskMap.rend(); tobeJoinedTaskIt++){
                    if(!areHbOrdered(tobeJoinedTaskIt->second, vc)){
                        baseVcOrder(tobeJoinedTaskIt->second, vc);
                        hbOrder(tobeJoinedTaskIt->second->endOp, op);

                        #ifdef DBG_COLLECT_STATS
                            Stats::actualEdgesAdded++;
                        #endif
                        exitLoop = false;
                    }
                }
                tobeJoinedTaskMap.clear();
            #endif


            if(!exitLoop){
                edgeAdded = true;
            }
        }while(!exitLoop);
    }

    if(!edgeAdded){
        OpHelper::threadMapIterator threadIt = oph->threadMap.find(op->tid); 
        if(threadIt != oph->threadMap.end()){
            ErBaseline::vcIterator evcIt = curFrame->vcStore.find(threadIt->second->loopOpId);
            if(evcIt != curFrame->vcStore.end()){
                LoopOp* loopop = dynamic_cast<ErBaseThread*>(threadIt->second)->loopOp;
                if(curFrame->iter == ER_BASE_ITER){
                    vc->vcJoin(evcIt->second);
                    hbOrder(loopop, op);
                }else{
                    vc->vcJoin(evcIt->second);
                }
            } 
        }else{
            PERROR("Missing thread object with ID %d in threadMap found when processing BEGIN(%d,%u).", 
                    op->tid, op->tid, beginop->event);
        }
    }

    if(typeOfPost == DR_POST_FOQ || typeOfPost == DR_POST_NEG){
        foqOrNegPostedThreads.insert(op->tid);
    }

    /* only the initial ER iteration changes the epoch of a task. 
     * Other iterations only transfer HB info between tasks.
     */
    if(curFrame->iter == ER_BASE_ITER){
        int chain = oph->getChain(task, vc);
        int epoch = vc->incrementClock(chain);
        task->epoch = epoch;
        //reflect that the max clock value on 'chain' has changed
        oph->curClockOnChains->incrementClock(chain);
    }

    #ifdef DBG_COMPUTEHB
        printf("\nopid %d  ", opId);
        beginop->diagnosticPrint();
        vc->diagnosticPrint();
    #endif

    return crossHbList;
}


std::list<ErBaseOperation*> ErBaseline::evaluateEndOp(int opId, ErBaseOperation* op, VectorClock* atomicVC){
    std::list<ErBaseOperation*> crossHbList;
    if(curFrame->iter != ER_BASE_ITER){
        ErBaseline::vcIterator curVcIt = curFrame->vcMap.find(op->task->id);
        if(curVcIt != curFrame->vcMap.end()){
            VectorClock* baseVc = baseFrame->vcMap.find(op->task->id)->second;
            baseVc->vcJoin(curVcIt->second);
        }else{
            PERROR("missing VC of END(%d, %u) in iteration %d.", op->tid, dynamic_cast<EndOp*>(op)->event, curFrame->iter);
        }
    }

    //returning false since END does not trigger any interesting functionality
    return crossHbList;
}

std::list<ErBaseOperation*> ErBaseline::evaluateForkOp(int opId, ErBaseOperation* op, VectorClock* atomicVC){
    std::list<ErBaseOperation*> crossHbList;
    ForkOp* forkop = dynamic_cast<ForkOp*>(op);

    if(op->task != NULL){
        ErBaseTask* task = op->task;

        ErBaseline::vcIterator vcIt = curFrame->vcMap.find(task->id);
        VectorClock* vc = NULL;
        if(vcIt != curFrame->vcMap.end()){
            vc = vcIt->second;
        }else{
            PERROR("Missing VC for task %d of FORK(%d,%d).  Aborting as something has been "
                   "wrongly setup.", task->id, op->tid, forkop->childTid);
        }
         if(op->hbSinkOp == NULL ){
            PERROR("Sink unknown for FORK(%d,%d). Aborting HB computation.", op->tid, forkop->childTid);
        }

        //initialize VC of child thread
        VectorClock* childVC = new VectorClock(*vc);
        curFrame->vcMap.insert(std::make_pair(op->hbSinkOp->task->id, childVC));

        if(curFrame->iter == ER_BASE_ITER){
            hbOrder(op, op->hbSinkOp);
        }

        /* only the initial ER iteration changes the epoch of a task. 
         * Other iterations only transfer HB info between tasks.
         */
        if(curFrame->iter == ER_BASE_ITER){
            int chain = oph->getChain(task, vc);
            int epoch = vc->incrementClock(chain);
            task->epoch = epoch;
            //reflect that the max clock value on 'chain' has changed
            oph->curClockOnChains->incrementClock(chain);
        }

        #ifdef DBG_COMPUTEHB
            printf("\nopid %d  ", opId);
            forkop->diagnosticPrint();
            vc->diagnosticPrint();
        #endif
    }else{
        PERROR("missing task found when processing FORK(%d,%d).",
                op->tid, forkop->childTid);
    }

    return crossHbList;
}

std::list<ErBaseOperation*> ErBaseline::evaluateLoopOp(int opId, ErBaseOperation* op, VectorClock* atomicVC){
    std::list<ErBaseOperation*> crossHbList;
    LoopOp* loopop = dynamic_cast<LoopOp*>(op);

    if(op->task != NULL){
        #ifdef DBG_COLLECT_STATS
            Stats::eventLoopCount++;
        #endif

        ErBaseTask* task = op->task;

        ErBaseline::vcIterator vcIt = curFrame->vcMap.find(task->id);
        VectorClock* vc = NULL;
        if(vcIt != curFrame->vcMap.end()){
            vc = vcIt->second;
        }else{
            PERROR("Missing VC of LOOP(%u).  Aborting as something has been "
                   "wrongly setup.", loopop->eventqueue);
        }

        /* only the initial ER iteration changes the epoch of a task. 
         * Other iterations only transfer HB info between tasks.
         */
        if(curFrame->iter == ER_BASE_ITER){
            int chain = oph->getChain(task, vc);
            int epoch = vc->incrementClock(chain);
            task->epoch = epoch; 
            //reflect that the max clock value on 'chain' has changed
            oph->curClockOnChains->incrementClock(chain);
        }

        /* LOOP is the last operation executed by thread prior to processing events.
         * store LOOP's HB info into vcStore as all begin ops on this thread will take join with it.
         */
        storeHBInfo(opId, vc);
        #ifdef DBG_COMPUTEHB
            printf("\nopid %d  ", opId);
            loopop->diagnosticPrint();
            vc->diagnosticPrint();
        #endif

        OpHelper::threadMapIterator threadItr = oph->threadMap.find(task->tid);
        if(threadItr != oph->threadMap.end()){
            threadItr->second->loopOpId = opId;
            (dynamic_cast<ErBaseThread*>(threadItr->second))->loopOp = dynamic_cast<LoopOp*>(op);
        }else{
            PERROR("Missing entry for thread %d in threadMap, found when processing LOOP(%u).",
                        op->tid, loopop->eventqueue);
        }
    }else{
        PERROR("missing task found when processing LOOP(%u).",
                loopop->eventqueue);
    }

    return crossHbList;
}

std::list<ErBaseOperation*> ErBaseline::evaluateLoopExitOp(int opId, ErBaseOperation* op, VectorClock* atomicVC){
    std::list<ErBaseOperation*> crossHbList;
    LoopExitOp* loopexitop = dynamic_cast<LoopExitOp*>(op);

    if(op->task != NULL){
        ErBaseTask* task = op->task;
       
        VectorClock* vc = new VectorClock();
        curFrame->vcMap.insert(std::make_pair(task->id, vc));

        bool hbInitialized = false;

        OpHelper::threadMapIterator threadIt = oph->threadMap.find(op->tid);
        if(threadIt != oph->threadMap.end()){
            std::list<ErBaseTask*>::iterator listIt = loopexitop->hbSrcList.begin();
            for(; listIt != loopexitop->hbSrcList.end(); listIt++){
                if(curFrame->iter == ER_BASE_ITER){
                    if(!areHbOrdered(*listIt, vc)){
                        baseVcOrder(*listIt, vc);
                        hbOrder((*listIt)->endOp, op);
                    }
                }else{
                    std::map<int, std::set<ErBaseOperation*> >::iterator it = reevaluateOps.find(curFrame->iter);
                    if(it->second.find((*listIt)->endOp) != it->second.end()){
                        vc->vcJoin(curFrame->vcMap.find((*listIt)->id)->second);
                    }
                }
                hbInitialized = true;
            }
        }else{
            PERROR("Missing thread object entry threadMap for tid:%d found when evaluating LOOPEXIT(%u).",
                    op->tid, loopexitop->eventqueue);
        }

        if(!hbInitialized){            
            if(threadIt->second->loopOpId != -1){
                std::map<int, VectorClock*>::iterator evcIt =
                        curFrame->vcStore.find(threadIt->second->loopOpId);
                if(evcIt != curFrame->vcStore.end()){
                    vc->vcJoin(evcIt->second);
                    if(curFrame->iter == ER_BASE_ITER){
                        hbOrder(dynamic_cast<ErBaseThread*>(threadIt->second)->loopOp, op);
                    }
                }

            }else{
                PERROR("Missing entry for thread %d in threadMap, found when processing LOOP-EXIT(%u)..",
                        op->tid, loopexitop->eventqueue);
            }
        }

        threadIt->second->loopOpId = -1;
        dynamic_cast<ErBaseThread*>(threadIt->second)->loopOp = NULL;

        if(curFrame->iter == ER_BASE_ITER){
            int chain = oph->getChain(task, vc);
            int epoch = vc->incrementClock(chain);
            task->epoch = epoch; 
            //reflect that the max clock value on 'chain' has changed
            oph->curClockOnChains->incrementClock(chain);
        }

        #ifdef DBG_COMPUTEHB
           printf("\nopid %d  ", opId);
           loopexitop->diagnosticPrint();
           vc->diagnosticPrint();
        #endif

    }else{
        PERROR("missing task found when processing LOOP-EXIT(%u).",
                loopexitop->eventqueue);
    }

    return crossHbList;
}

std::list<ErBaseOperation*> ErBaseline::evaluateThreadexitOp(int opId, ErBaseOperation* op, VectorClock* atomicVC){
    std::list<ErBaseOperation*> crossHbList;
    
    //returning false since threadexit does not trigger any interesting functionality
    return crossHbList;
}

std::list<ErBaseOperation*> ErBaseline::evaluateThreadinitOp(int opId, ErBaseOperation* op, VectorClock* atomicVC){
    std::list<ErBaseOperation*> crossHbList;

    if(op->task != NULL){
        ErBaseTask* task = op->task;

        ErBaseline::vcIterator vcIt = curFrame->vcMap.find(task->id);
        VectorClock* vc = NULL;
        if(vcIt != curFrame->vcMap.end()){
            vc = vcIt->second;
        }else{
            vc = new VectorClock();
            curFrame->vcMap.insert(std::make_pair(task->id, vc));
        }

        /* only the initial ER iteration changes the epoch of a task. 
         * Other iterations only transfer HB info between tasks.
         */
        if(curFrame->iter == ER_BASE_ITER){
            int chain = oph->getChain(task, vc);
            int epoch = vc->incrementClock(chain);
            task->epoch = epoch;
            //reflect that the max clock value on 'chain' has changed
            oph->curClockOnChains->incrementClock(chain);
        }

        //sinkModified = true; - we need this only for ops that result in cross-thread middle-of-task HB

        #ifdef DBG_COMPUTEHB
            printf("\nopid %d  ", opId);
            (dynamic_cast<ThreadinitOp*>(op))->diagnosticPrint();
            vc->diagnosticPrint();
        #endif
    }else{
        PERROR("missing task found when processing THREADINIT of tid %d.",
                op->tid);
    }

    return crossHbList;
}

std::list<ErBaseOperation*> ErBaseline::evaluateEnableEventOp(int opId, ErBaseOperation* op, VectorClock* atomicVC){
    std::list<ErBaseOperation*> crossHbList;

    bool sinkModified = false;
    EnableEventOp* enableop = dynamic_cast<EnableEventOp*>(op);
    if(!enableop->tobeInitializedTasks.empty() || enableop->shouldStore){
        if(op->task != NULL){
            ErBaseTask* task = op->task;
    
            ErBaseline::vcIterator vcIt = curFrame->vcMap.find(task->id);
            VectorClock* vc = NULL;
            if(vcIt != curFrame->vcMap.end()){
                vc = vcIt->second;
            }else{
                PERROR("Missing VC for task %d of ENABLE_EVENT(%u,%d). Aborting as something has been "
                       "wrongly setup.", task->id, enableop->view, enableop->uiEvent);
            }
    
            if(enableop->shouldStore){
                storeHBInfo(opId, vc);
            }
    
            std::set<int>::iterator sinkTaskIt = enableop->tobeInitializedTasks.begin();
            for(; sinkTaskIt != enableop->tobeInitializedTasks.end(); sinkTaskIt++){
                //task of each trigger_event is initialized by exactly one enable_event. 
                ErBaseTask* sinkTask = dynamic_cast<ErBaseTask*>(oph->taskMap.find(*sinkTaskIt)->second);
                 VectorClock* sinkVc = NULL;
                if(task->tid != sinkTask->tid || task->eventId <= 0){
                    sinkVc = new VectorClock(*vc);
                }else{
                    //VC info can be obtained when join is taken during processing of sinkTask's begin
                    sinkVc = new VectorClock();
                }

                if(curFrame->iter == ER_BASE_ITER){
                    hbOrder(op, sinkTask->postOp);
                }
            }

            /* only the initial ER iteration changes the epoch of a task. 
             * Other iterations only transfer HB info between tasks.
             */
            if(curFrame->iter == ER_BASE_ITER){
                int chain = oph->getChain(task, vc);
                int epoch = vc->incrementClock(chain);
                task->epoch = epoch;
                //reflect that the max clock value on 'chain' has changed
                oph->curClockOnChains->incrementClock(chain);
            }

            #ifdef DBG_COMPUTEHB
                printf("\nopid %d  ", opId);
                enableop->diagnosticPrint();
                vc->diagnosticPrint();
            #endif
        }else{
            PERROR("missing task found when processing ENABLE_EVENT(%u,%d).",
                    enableop->view, enableop->uiEvent);
        }
    }else{
        PERROR("The preprocessor has not deleted ENABLE_EVENT(%u,%d) even though it is not initializing tasks of any"
               " trigger event handlers.", enableop->view, enableop->uiEvent);
    }

    return crossHbList;
}

std::list<ErBaseOperation*> ErBaseline::evaluateTriggerEventOp(int opId, ErBaseOperation* op, VectorClock* atomicVC){
    std::list<ErBaseOperation*> crossHbList;
    TriggerEventOp* triggerop = dynamic_cast<TriggerEventOp*>(op);

    if(triggerop->enableOpId > 0){
        std::map<int, VectorClock*>::iterator evcIt = curFrame->vcStore.find(triggerop->enableOpId);
        if(evcIt != curFrame->vcStore.end()){ 
            /* TriggerOp's VC timestamp gets modified only if enable has been updated
             * in this iteration. It does not matter whether trigger's task itself has been 
             * updated by this iteration yet.
             * Also, we evaluate TRIGGER_EVENT only if ENABLE_EVENT and TRIGGER_EVENT are on
             * different threads, else erbaseOperation.cpp does not store this op.
             */
            if(op->task != NULL){
                ErBaseTask* task = op->task;

                ErBaseline::vcIterator vcIt = curFrame->vcMap.find(task->id);
                VectorClock* vc = NULL;
                if(vcIt != curFrame->vcMap.end()){
                    vc = vcIt->second;
                }else{
                    vc = new VectorClock();
                    curFrame->vcMap.insert(std::make_pair(task->id, vc));
                }

                //to aid in evaluation of NO-PRE rule
                ErBaseIterInfo* tempFrame = baseFrame;
                while(tempFrame != NULL){
                    vcIt = tempFrame->vcMap.find(task->id); 
                    if(vcIt != tempFrame->vcMap.end()){
                        atomicVC->vcJoin(vcIt->second);
                    }
                    tempFrame = tempFrame->next;
                }

                vc->vcJoin(evcIt->second); //take join with enable's stored VC timestamp

                if(curFrame->iter == ER_BASE_ITER){
                    hbOrder(triggerop->hbSrcOp, op);
                }
                
                crossHbList.push_back(triggerop->hbSrcOp);

                #ifdef DBG_COMPUTEHB
                    printf("\nopid %d  ", opId);
                    triggerop->diagnosticPrint();
                    vc->diagnosticPrint();
                #endif
            }else{
                PERROR("missing task found when processing TRIGGER_EVENT(%u,%d).", 
                        triggerop->view, triggerop->uiEvent);
            }
        }
    }else{
        PERROR("Missing enableOpId corresponding to TRIGGER_EVENT(%u,%d).", triggerop->view, triggerop->uiEvent);
    }

    return crossHbList;
}

std::list<ErBaseOperation*> ErBaseline::evaluateEnableActLifecycleOp(int opId, ErBaseOperation* op, VectorClock* atomicVC){
    std::list<ErBaseOperation*> crossHbList;
    EnableActLifecycleOp* enableop = dynamic_cast<EnableActLifecycleOp*>(op);

    if(!enableop->tobeInitializedTasks.empty() || enableop->shouldStore){ //only if this enable initializes trigger event's tasks process it, otherwiise there's nothing interesting
        if(op->task != NULL){
            ErBaseTask* task = op->task;

            ErBaseline::vcIterator vcIt = curFrame->vcMap.find(task->id);
            VectorClock* vc = NULL;
            if(vcIt != curFrame->vcMap.end()){
                vc = vcIt->second;
            }else{
                PERROR("Missing VC for task %d of ENABLE_ACT_LIFECYCLE(%u,%d). Aborting as something has been "
                       "wrongly setup.", task->id, enableop->instance, enableop->state);
            }
    
            if(enableop->shouldStore){
                storeHBInfo(opId, vc);
            }
    
            std::set<int>::iterator setIt = enableop->tobeInitializedTasks.begin();
            for(; setIt != enableop->tobeInitializedTasks.end(); setIt++){
                //task of each trigger_event is initialized by exactly one enable_event. 
                ErBaseTask* sinkTask = dynamic_cast<ErBaseTask*>(oph->taskMap.find(*setIt)->second);
                ErBaseline::vcIterator sinkVcIt = curFrame->vcMap.find(*setIt);
                if(sinkVcIt == curFrame->vcMap.end()){
                    VectorClock* sinkVc = NULL;
                    if(task->tid != sinkTask->tid || task->eventId <= 0){
                        sinkVc = new VectorClock(*vc);
                    }else{
                        //VC info of current task will be obtained when sinkTask's BEGIN joins with current task
                        sinkVc = new VectorClock();
                    }
                    curFrame->vcMap.insert(std::make_pair(*setIt, sinkVc));
                }else{
                    if(task->tid != sinkTask->tid || task->eventId <= 0){
                        sinkVcIt->second->vcJoin(vc);
                    }
                }

                if(curFrame->iter == ER_BASE_ITER){
                    hbOrder(op, sinkTask->postOp);
                }
            }

            /* only the initial ER iteration changes the epoch of a task. 
             * Other iterations only transfer HB info between tasks.
             */
            if(curFrame->iter == ER_BASE_ITER){
                int chain = oph->getChain(task, vc);
                int epoch = vc->incrementClock(chain);
                task->epoch = epoch;
                //reflect that the max clock value on 'chain' has changed
                oph->curClockOnChains->incrementClock(chain);
            }
    
            #ifdef DBG_COMPUTEHB
                printf("\nopid %d  ", opId);
                enableop->diagnosticPrint();
                vc->diagnosticPrint();
            #endif
        }else{
            PERROR("missing task found when processing ENABLE_ACT_LIFECYCLE(%u,%d).",
                    enableop->instance, enableop->state);
        }

    }else{
        #ifdef DBG_COMPUTEHB
        printf("Found an ENABLE_ACT_LIFECYCLE(%u,%d) which is neither stored nor initializes any new tasks.\n", 
                 enableop->instance, enableop->state);
        #endif
    }

    return crossHbList;
}

std::list<ErBaseOperation*> ErBaseline::evaluateTriggerActLifecycleOp(int opId, ErBaseOperation* op, VectorClock* atomicVC){
    std::list<ErBaseOperation*> crossHbList;
    TriggerActLifecycleOp* triggerop = dynamic_cast<TriggerActLifecycleOp*>(op);

    if(!triggerop->envOrderedEnableOps.empty()){
        if(op->task != NULL){
            ErBaseTask* task = op->task;
            
            ErBaseline::vcIterator vcIt = curFrame->vcMap.find(task->id);
            VectorClock* vc = NULL;
            if(vcIt != curFrame->vcMap.end()){
                vc = vcIt->second;
            }

            std::set<int>::iterator enSetIt = triggerop->envOrderedEnableOps.begin();
            for(; enSetIt != triggerop->envOrderedEnableOps.end(); enSetIt++){
                std::map<int, VectorClock*>::iterator evcIt = curFrame->vcStore.find(*enSetIt);
                if(evcIt != curFrame->vcStore.end()){ 
                    /* TriggerOp's VC timestamp gets modified only if enable has been updated
                     * in this iteration. It does not matter whether trigger's task itself has been 
                     * updated by this iteration yet.
                     */
                    if(vc == NULL){
                        vc = new VectorClock(*evcIt->second);
                        curFrame->vcMap.insert(std::make_pair(task->id, vc));
                    }else{
                        vc->vcJoin(evcIt->second);
                    }

                    ErBaseOperation* enableOp = dynamic_cast<ErBaseOperation*>(oph->opMap.find(*enSetIt)->second);
                    if(curFrame->iter == ER_BASE_ITER){
                        hbOrder(enableOp, op);
                    }
                    crossHbList.push_back(enableOp);
                }
            }
            
            #ifdef DBG_COMPUTEHB
                printf("\nopid %d  ", opId);
                triggerop->diagnosticPrint();
                vc->diagnosticPrint();
            #endif
        }else{
            PERROR("missing task found when processing TRIGGER_ACT_LIFECYCLE(%u, %d).", 
                    triggerop->instance, triggerop->state);
        }
    }

    return crossHbList;
}

//for BroadcastReceiver and Servicecomponents we do not emit ENABLE as TRIGGER captures that info too.
//Here the state field of the operation was used to identify enabling operations etc.
std::list<ErBaseOperation*> ErBaseline::evaluateTriggerReceiverOp(int opId, ErBaseOperation* op, VectorClock* atomicVC){
    std::list<ErBaseOperation*> crossHbList;
    TriggerReceiverOp* recop = dynamic_cast<TriggerReceiverOp*>(op);

    if(!recop->tobeInitializedTasks.empty()){ //only if this operation initializes trigger event's tasks process it, otherwiise there's nothing interesting
        if(op->task != NULL){
            ErBaseTask* task = op->task;
    
            ErBaseline::vcIterator vcIt = curFrame->vcMap.find(task->id);
            VectorClock* vc = NULL;
            if(vcIt != curFrame->vcMap.end()){
                vc = vcIt->second;
            }else{
                PERROR("Missing VC for task %d of TRIGGER_RECEIVER with action-key %d, receiver instance %u, intentId %d and state %d. " 
                       "Aborting as something has been "
                       "wrongly setup.", task->id, recop->actionKey, recop->identifier, 
                       recop->intentId, recop->state);
            }
    
            std::set<int>::iterator setIt = recop->tobeInitializedTasks.begin();
            for(; setIt != recop->tobeInitializedTasks.end(); setIt++){
                ErBaseTask* sinkTask = dynamic_cast<ErBaseTask*>(oph->taskMap.find(*setIt)->second);
                 ErBaseline::vcIterator sinkVcIt = curFrame->vcMap.find(*setIt);
                if(sinkVcIt == curFrame->vcMap.end()){
                    VectorClock* sinkVc = NULL;
                    if(task->tid != sinkTask->tid || task->eventId <= 0){
                        sinkVc = new VectorClock(*vc);
                    }else{
                        sinkVc = new VectorClock();
                    }
                    curFrame->vcMap.insert(std::make_pair(*setIt, sinkVc));
                }else{
                    if(task->tid != sinkTask->tid || task->eventId <= 0){
                        sinkVcIt->second->vcJoin(vc);
                    }
                }
                
                if(curFrame->iter == ER_BASE_ITER){
                   hbOrder(op, sinkTask->postOp);
                }
            }

            /* only the initial ER iteration changes the epoch of a task. 
             * Other iterations only transfer HB info between tasks.
             */
            if(curFrame->iter == ER_BASE_ITER){
                int chain = oph->getChain(task, vc);
                int epoch = vc->incrementClock(chain);
                task->epoch = epoch;
                //reflect that the max clock value on 'chain' has changed
                oph->curClockOnChains->incrementClock(chain);
            }

            #ifdef DBG_COMPUTEHB
                printf("\nopid %d  ", opId);
                recop->diagnosticPrint();
                vc->diagnosticPrint();
            #endif
        }else{
            PERROR("missing task found when processing TRIGGER_RECEIVER with "
                   "action-key %d, receiver instance %u, intentId %d and state %d ",
                    recop->actionKey, recop->identifier, recop->intentId, recop->state);
        }

    }else{
        #ifdef DBG_COMPUTEHB
        printf("Found a TRIGGER_RECEIVER with action-key %d, receiver instance %u, intentId %d and state %d " 
               "which does not initializes any new tasks.\n", recop->actionKey, 
               recop->identifier, recop->intentId, recop->state);
        #endif
    }

    return crossHbList;
}


//for BroadcastReceiver and Servicecomponents we do not emit ENABLE as TRIGGER captures that info too.
//Here the state field of the operation was used to identify enabling operations etc.
std::list<ErBaseOperation*> ErBaseline::evaluateTriggerServiceOp(int opId, ErBaseOperation* op, VectorClock* atomicVC){
    std::list<ErBaseOperation*> crossHbList;
    TriggerServiceOp* serop = dynamic_cast<TriggerServiceOp*>(op);

    if(!serop->tobeInitializedTasks.empty()){ //only if this operation initializes trigger event's tasks process it, otherwiise there's nothing interesting
        if(op->task != NULL){
            ErBaseTask* task = op->task;
    
            ErBaseline::vcIterator vcIt = curFrame->vcMap.find(task->id);
            VectorClock* vc = NULL;
            if(vcIt != curFrame->vcMap.end()){
                vc = vcIt->second;
            }else{
                PERROR("Missing VC for task %d of TRIGGER_SERVICE with class-key %d, identifier %u and state %d. " 
                       "Aborting as something has been "
                       "wrongly setup.", task->id, serop->serviceClassKey, serop->identifier, 
                       serop->state);
            }
    
            std::set<int>::iterator setIt = serop->tobeInitializedTasks.begin();
            for(; setIt != serop->tobeInitializedTasks.end(); setIt++){
                ErBaseTask* sinkTask = dynamic_cast<ErBaseTask*>(oph->taskMap.find(*setIt)->second);
                ErBaseline::vcIterator sinkVcIt = curFrame->vcMap.find(*setIt);
                if(sinkVcIt == curFrame->vcMap.end()){
                    VectorClock* sinkVc = NULL;
                    if(task->tid != sinkTask->tid || task->eventId <= 0){
                        sinkVc = new VectorClock(*vc);
                    }else{
                        sinkVc = new VectorClock();
                    }
                    curFrame->vcMap.insert(std::make_pair(*setIt, sinkVc));
                }else{
                    if(task->tid != sinkTask->tid || task->eventId <= 0){
                        sinkVcIt->second->vcJoin(vc);
                    }
                }

                if(curFrame->iter == ER_BASE_ITER){                
                    hbOrder(op, sinkTask->postOp);
                }
            }

            /* only the initial ER iteration changes the epoch of a task. 
             * Other iterations only transfer HB info between tasks.
             */
            if(curFrame->iter == ER_BASE_ITER){
                int chain = oph->getChain(task, vc);
                int epoch = vc->incrementClock(chain);
                task->epoch = epoch;
                //reflect that the max clock value on 'chain' has changed
                oph->curClockOnChains->incrementClock(chain);
            }

            #ifdef DBG_COMPUTEHB
                printf("\nopid %d  ", opId);
                serop->diagnosticPrint();
                vc->diagnosticPrint();
            #endif
        }else{
            PERROR("missing task found when processing TRIGGER_SERVICE with "
                   "class-key %d, identifier %u and state %d ",
                    serop->serviceClassKey, serop->identifier, serop->state);
        }

    }else{
        #ifdef DBG_COMPUTEHB
        printf("Found a TRIGGER_SERVICE with class-key %d, identifier %u and state %d " 
               "which does not initializes any new tasks.\n", serop->serviceClassKey, serop->identifier,
               serop->state);
        #endif
    }

    return crossHbList;
}

std::list<ErBaseOperation*> ErBaseline::evaluateEnableWindowFocusOp(int opId, ErBaseOperation* op, VectorClock* atomicVC){
    std::list<ErBaseOperation*> crossHbList;
    EnableWindowFocusOp* enableop = dynamic_cast<EnableWindowFocusOp*>(op);

    if(enableop->shouldStore || !enableop->tobeInitializedTasks.empty()){ 
        if(op->task != NULL){
            ErBaseTask* task = op->task;
    
            ErBaseline::vcIterator vcIt = curFrame->vcMap.find(task->id);
            VectorClock* vc = NULL;
            if(vcIt != curFrame->vcMap.end()){
                vc = vcIt->second;
            }else{
                PERROR("Missing VC for task %d of ENABLE_WINDOW_FOCUS(%u). Aborting as something has been "
                       "wrongly setup.", task->id, enableop->windowId);
            }
    
            if(enableop->shouldStore){
                storeHBInfo(opId, vc);
            }

            std::set<int>::iterator sinkTaskIt = enableop->tobeInitializedTasks.begin();
            for(; sinkTaskIt != enableop->tobeInitializedTasks.end(); sinkTaskIt++){
                //task of each trigger_window_focus is initialized by exactly one enable_event. 
                ErBaseTask* sinkTask = dynamic_cast<ErBaseTask*>(oph->taskMap.find(*sinkTaskIt)->second);
                VectorClock* sinkVc = NULL;
                if(task->tid != sinkTask->tid || task->eventId <= 0){
                    sinkVc = new VectorClock(*vc);
                }else{
                    //VC info can be obtained when join is taken during processing of sinkTask's begin
                    sinkVc = new VectorClock();
                }

                curFrame->vcMap.insert(std::make_pair(*sinkTaskIt, sinkVc));

                if(curFrame->iter == ER_BASE_ITER){
                    hbOrder(op, sinkTask->postOp);
                }
            }

            /* only the initial ER iteration changes the epoch of a task. 
             * Other iterations only transfer HB info between tasks.
             */
            if(curFrame->iter == ER_BASE_ITER){
                int chain = oph->getChain(task, vc);
                int epoch = vc->incrementClock(chain);
                task->epoch = epoch;
                //reflect that the max clock value on 'chain' has changed
                oph->curClockOnChains->incrementClock(chain);
            }
    
            #ifdef DBG_COMPUTEHB
                printf("\nopid %d  ", opId);
                enableop->diagnosticPrint();
                vc->diagnosticPrint();
            #endif
        }else{
            PERROR("missing task found when processing ENABLE_WINDOW_FOCUS(%u).",
                    enableop->windowId);
        }

    }else{
        PERROR("The preprocessor has stored ENABLE_WINDOW_FOCUS(%u) even though its shouldStore is false "
               "and is not initializing any task.", enableop->windowId);
    }

    return crossHbList;
}

std::list<ErBaseOperation*> ErBaseline::evaluateTriggerWindowFocusOp(int opId, ErBaseOperation* op, VectorClock* atomicVC){
    std::list<ErBaseOperation*> crossHbList;
    TriggerWindowFocusOp* triggerop = dynamic_cast<TriggerWindowFocusOp*>(op);

    if(triggerop->enableOpId > 0){
        std::map<int, VectorClock*>::iterator evcIt = curFrame->vcStore.find(triggerop->enableOpId);
        if(evcIt != curFrame->vcStore.end()){ 
            /* TriggerOp's VC timestamp gets modified only if enable has been updated
             * in this iteration. It does not matter whether trigger's task itself has been 
             * updated by this iteration yet.
             */
            if(op->task != NULL){
                ErBaseTask* task = op->task;

                ErBaseline::vcIterator vcIt = curFrame->vcMap.find(task->id);
                VectorClock* vc = NULL;
                if(vcIt != curFrame->vcMap.end()){
                    vc = vcIt->second;
                }else{
                    vc = new VectorClock();
                    curFrame->vcMap.insert(std::make_pair(task->id, vc));
                }

                //to aid in evaluation of NO-PRE rule
                ErBaseIterInfo* tempFrame = baseFrame;
                while(tempFrame != NULL){
                    vcIt = tempFrame->vcMap.find(task->id);
                    if(vcIt != tempFrame->vcMap.end()){
                        atomicVC->vcJoin(vcIt->second);
                    }
                    tempFrame = tempFrame->next;
                }

                vc->vcJoin(evcIt->second); //take join with enable's stored VC timestamp
                if(curFrame->iter == ER_BASE_ITER){
                    hbOrder(triggerop->hbSrcOp, op);
                }

                crossHbList.push_back(triggerop->hbSrcOp);

                #ifdef DBG_COMPUTEHB
                    printf("\nopid %d  ", opId);
                    triggerop->diagnosticPrint();
                    vc->diagnosticPrint();
                #endif

            }else{
                PERROR("missing task found when processing TRIGGER_WINDOW_FOCUS(%u).", 
                        triggerop->windowId);
            }
        }

    }else{
        PERROR("Missing enableOpId corresponding to TRIGGER_WINDOW_FOCUS(%u).", triggerop->windowId);
    }

    return crossHbList;
}

std::list<ErBaseOperation*> ErBaseline::evaluateWaitOp(int opId, ErBaseOperation* op, VectorClock* atomicVC){
    std::list<ErBaseOperation*> crossHbList;
    
    //returning false since WAIT does not trigger any interesting functionality
    //Notify HB Wait is handled at Notify itself
    return crossHbList;
}

std::list<ErBaseOperation*> ErBaseline::evaluateNotifyOp(int opId, ErBaseOperation* op, VectorClock* atomicVC){
    std::list<ErBaseOperation*> crossHbList;
    NotifyOp* notifyop = dynamic_cast<NotifyOp*>(op);
    if(op->task != NULL){
        ErBaseTask* task = op->task;

        if(op->hbSinkOp == NULL){
            PERROR("Sink unknown for NOTIFY(%d) on tid %d. Aborting HB computation.", notifyop->notifiedTid, op->tid);
        }

        ErBaseline::vcIterator vcIt = curFrame->vcMap.find(task->id);
        VectorClock* vc = NULL;
        if(vcIt != curFrame->vcMap.end()){
            vc = vcIt->second;
        }else{
            PERROR("Missing VC for task %d of NOTIFY(%d). Aborting as something has been "
                   "wrongly setup.", task->id, notifyop->notifiedTid);
        }

        //update or initialize VC of waiting thread (both are possible due to incremental reprocessing of trace)
        VectorClock* waitVC = NULL;
        ErBaseline::vcIterator waitvcIt = curFrame->vcMap.find(op->hbSinkOp->task->id);
        if(waitvcIt == curFrame->vcMap.end()){
            waitVC = new VectorClock();
            curFrame->vcMap.insert(std::make_pair(op->hbSinkOp->task->id, waitVC));
        }else{
            waitVC = waitvcIt->second;
        }

        //to aid in evaluation of NO-PRE rule
        ErBaseIterInfo* tempFrame = baseFrame;
        while(tempFrame != NULL){
            vcIt = tempFrame->vcMap.find(op->hbSinkOp->task->id);
            if(vcIt != tempFrame->vcMap.end()){
                atomicVC->vcJoin(vcIt->second);
            }
            tempFrame = tempFrame->next;
        }

        waitVC->vcJoin(vc);

        if(curFrame->iter == ER_BASE_ITER){
            hbOrder(op, op->hbSinkOp);
        }

        crossHbList.push_back(op);

        /* only the initial ER iteration changes the epoch of a task. 
         * Other iterations only transfer HB info between tasks.
         */
        if(curFrame->iter == ER_BASE_ITER){
            int chain = oph->getChain(task, vc);
            int epoch = vc->incrementClock(chain);
            task->epoch = epoch;
            //reflect that the max clock value on 'chain' has changed
            oph->curClockOnChains->incrementClock(chain);
        }

        #ifdef DBG_COMPUTEHB
            printf("\nopid %d  ", opId);
            notifyop->diagnosticPrint();
            vc->diagnosticPrint();
        #endif
    }else if(op->taskId == -1){
        #ifdef DBG_COMPUTEHB
            printf("found a NATIVE NOTIFY(%d). We will not be updating any HB info, but continue the"
               " HB computation.\n", notifyop->notifiedTid);
        #endif
    }else{
        PERROR("missing task found when processing NOTIFY(%d).",
                notifyop->notifiedTid);
    }

    return crossHbList;
}

std::list<ErBaseOperation*> ErBaseline::evaluateIdlePostOp(int opId, ErBaseOperation* op, VectorClock* atomicVC){
    std::list<ErBaseOperation*> crossHbList;
    IdlePostOp* postop = dynamic_cast<IdlePostOp*>(op);
    if(op->task != NULL){
        ErBaseTask* task = op->task;

        if(op->hbSinkOp == NULL){
             PERROR("Sink unknown for IDLE-POST(%d,%u,%d). Aborting HB computation.", op->tid, postop->event, postop->dest);
        }

        ErBaseline::vcIterator vcIt = curFrame->vcMap.find(task->id);
        VectorClock* vc = NULL;
        if(vcIt != curFrame->vcMap.end()){
            vc = vcIt->second;
        }else{
            PERROR("Missing VC of task %d of IDLE-POST(%d,%u,%d). "
                   "Aborting as something has been "
                   "wrongly setup.", task->id, op->tid, postop->event, postop->dest);
        }

        //update or initialize VC of posted task (both are possible as some lifecycle related tasks are initialized by enable ops even prior to post)
        ErBaseline::vcIterator eventVcIt = curFrame->vcMap.find(op->hbSinkOp->task->id);
        if(eventVcIt == curFrame->vcMap.end()){
            VectorClock* eventVC = new VectorClock(*vc);
            curFrame->vcMap.insert(std::make_pair(op->hbSinkOp->task->id, eventVC));
        }else{
            eventVcIt->second->vcJoin(vc);
        }

        /*if(curFrame->iter == ER_BASE_ITER){
            hbOrder(op, op->hbSinkOp);
        }*/

        std::set<int>::iterator setIt = postop->tobeInitializedTasks.begin();
        for(; setIt != postop->tobeInitializedTasks.end(); setIt++){
            ErBaseline::vcIterator targetVcIt = curFrame->vcMap.find(*setIt);
            if(targetVcIt == curFrame->vcMap.end()){
                VectorClock* targetVc = new VectorClock(*vc);
                curFrame->vcMap.insert(std::make_pair(*setIt, targetVc));
            }else{
                targetVcIt->second->vcJoin(vc);
            }

            ErBaseTask* targetTask = dynamic_cast<ErBaseTask*>(oph->taskMap.find(*setIt)->second);
            if(curFrame->iter == ER_BASE_ITER){
                hbOrder(op, targetTask->postOp);//in our scenarios targetTask->postOp is an environment post and hence there's no scope for NO-PRE rule to kick in here.
            }
        }

        /* only the initial ER iteration changes the epoch of a task. 
         * Other iterations only transfer HB info between tasks.
         */
        if(curFrame->iter == ER_BASE_ITER){
            int chain = oph->getChain(task, vc);
            int epoch = vc->incrementClock(chain);
            task->epoch = epoch;
            //reflect that the max clock value on 'chain' has changed
            oph->curClockOnChains->incrementClock(chain);
        }

        #ifdef DBG_COMPUTEHB
            printf("\nopid %d  ", opId);
            postop->diagnosticPrint();
            vc->diagnosticPrint();
        #endif
    }else{
        PERROR("missing task found when processing IDLE-POST(%d,%u,%d).",
                op->tid, postop->event, postop->dest);
    }

    return crossHbList;
}

std::list<ErBaseOperation*> ErBaseline::evaluatePostFoqOp(int opId, ErBaseOperation* op, VectorClock* atomicVC){
    std::list<ErBaseOperation*> crossHbList;
    PostFoqOp* postop = dynamic_cast<PostFoqOp*>(op);

    if(op->task != NULL){
        ErBaseTask* task = op->task;
        if(op->hbSinkOp == NULL){
             PERROR("Sink unknown for POST-FOQ(%d,%u,%d). Aborting HB computation.", op->tid, postop->event, postop->dest);
        }

        ErBaseline::vcIterator vcIt = curFrame->vcMap.find(task->id);
        VectorClock* vc = NULL;
        if(vcIt != curFrame->vcMap.end()){
            vc = vcIt->second;
        }else{
            PERROR("Missing VC of task %d of POST-FOQ(%d,%u,%d). "
                   "Aborting as something has been "
                   "wrongly setup.", task->id, op->tid, postop->event, postop->dest);
        }

        //update or initialize VC of posted task (both are possible as some lifecycle related tasks are initialized by enable ops even prior to post)
        ErBaseline::vcIterator eventVcIt = curFrame->vcMap.find(op->hbSinkOp->task->id);
        if(eventVcIt == curFrame->vcMap.end()){
            VectorClock* eventVC = new VectorClock(*vc);
            curFrame->vcMap.insert(std::make_pair(op->hbSinkOp->task->id, eventVC));
        }else{
            eventVcIt->second->vcJoin(vc);
        }

        /*if(curFrame->iter == ER_BASE_ITER){
            hbOrder(op, op->hbSinkOp);
        }*/

        std::set<int>::iterator setIt = postop->tobeInitializedTasks.begin();
        for(; setIt != postop->tobeInitializedTasks.end(); setIt++){
            ErBaseline::vcIterator targetVcIt = curFrame->vcMap.find(*setIt);
            if(targetVcIt == curFrame->vcMap.end()){
                VectorClock* targetVc = new VectorClock(*vc);
                curFrame->vcMap.insert(std::make_pair(*setIt, targetVc));
            }else{
                targetVcIt->second->vcJoin(vc);
            }

            ErBaseTask* targetTask = dynamic_cast<ErBaseTask*>(oph->taskMap.find(*setIt)->second);
            if(curFrame->iter == ER_BASE_ITER){
                hbOrder(op, targetTask->postOp);//in our scenarios targetTask->postOp is an environment post and hence there's no scope for NO-PRE rule to kick in here.
            }
        }

        //store the VC timestamp so as to evaluate FIFO-FOQ rule
        if(curFrame->iter == ER_BASE_ITER){
            VectorClock* tmpVc = new VectorClock(*vc);
            foqNegPostVC.insert(std::make_pair(postop->event, tmpVc));
        }else{
            foqNegPostVC.find(postop->event)->second->vcJoin(vc);
        }

        /* only the initial ER iteration changes the epoch of a task. 
         * Other iterations only transfer HB info between tasks.
         */
        if(curFrame->iter == ER_BASE_ITER){
            int chain = oph->getChain(task, vc);
            int epoch = vc->incrementClock(chain);
            task->epoch = epoch;
            //reflect that the max clock value on 'chain' has changed
            oph->curClockOnChains->incrementClock(chain);
        }

        #ifdef DBG_COMPUTEHB
            printf("\nopid %d  ", opId);
            postop->diagnosticPrint();
            vc->diagnosticPrint();
        #endif

    }else if(op->taskId == -1){
        #ifdef DBG_COMPUTEHB
            printf("found a NATIVE-POST-FOQ(%d,%u,%d). We will not be updating any HB info, but continue the"
               " HB computation.\n", op->tid, postop->event, postop->dest);
        #endif
    }else{
        PERROR("missing task found when processing POST-FOQ(%d,%u,%d).",
                op->tid, postop->event, postop->dest);
    }

    return crossHbList;
}

std::list<ErBaseOperation*> ErBaseline::evaluatePostNegOp(int opId, ErBaseOperation* op, VectorClock* atomicVC){
    std::list<ErBaseOperation*> crossHbList;
    PostNegOp* postop = dynamic_cast<PostNegOp*>(op);

    if(op->task != NULL){
        ErBaseTask* task = op->task;

        if(op->hbSinkOp == NULL){
             PERROR("Sink unknown for POST-NEG(%d,%u,%d). Aborting HB computation.", op->tid, postop->event, postop->dest);
        }

        ErBaseline::vcIterator vcIt = curFrame->vcMap.find(task->id);
        VectorClock* vc = NULL;
        if(vcIt != curFrame->vcMap.end()){
            vc = vcIt->second;
        }else{
            PERROR("Missing VC of task %d of POST-NEG(%d,%u,%d). "
                   "Aborting as something has been "
                   "wrongly setup.", task->id, op->tid, postop->event, postop->dest);
        }

        //update or initialize VC of posted task (both are possible as some lifecycle related tasks are initialized by enable ops even prior to post)
        ErBaseline::vcIterator eventVcIt = curFrame->vcMap.find(op->hbSinkOp->task->id);
        if(eventVcIt == curFrame->vcMap.end()){
            VectorClock* eventVC = new VectorClock(*vc);
            curFrame->vcMap.insert(std::make_pair(op->hbSinkOp->task->id, eventVC));
        }else{
            eventVcIt->second->vcJoin(vc);
        }

        /*if(curFrame->iter == ER_BASE_ITER){
            hbOrder(op, op->hbSinkOp);
        }*/

        std::set<int>::iterator setIt = postop->tobeInitializedTasks.begin();
        for(; setIt != postop->tobeInitializedTasks.end(); setIt++){
            ErBaseline::vcIterator targetVcIt = curFrame->vcMap.find(*setIt);
            if(targetVcIt == curFrame->vcMap.end()){
                VectorClock* targetVc = new VectorClock(*vc);
                curFrame->vcMap.insert(std::make_pair(*setIt, targetVc));
            }else{
                targetVcIt->second->vcJoin(vc);
            }

            ErBaseTask* targetTask = dynamic_cast<ErBaseTask*>(oph->taskMap.find(*setIt)->second);
            if(curFrame->iter == ER_BASE_ITER){
                hbOrder(op, targetTask->postOp);//in our scenarios targetTask->postOp is an environment post and hence there's no scope for NO-PRE rule to kick in here.
            }
        }

        //store the VC timestamp so as to evaluate FIFO-FOQ rule
        if(curFrame->iter == ER_BASE_ITER){
            VectorClock* tmpVc = new VectorClock(*vc);
            foqNegPostVC.insert(std::make_pair(postop->event, tmpVc));
        }else{
            foqNegPostVC.find(postop->event)->second->vcJoin(vc);
        }

        /* only the initial ER iteration changes the epoch of a task. 
         * Other iterations only transfer HB info between tasks.
         */
        if(curFrame->iter == ER_BASE_ITER){
            int chain = oph->getChain(task, vc);
            int epoch = vc->incrementClock(chain);
            task->epoch = epoch;
            //reflect that the max clock value on 'chain' has changed
            oph->curClockOnChains->incrementClock(chain);
        }

        #ifdef DBG_COMPUTEHB
            printf("\nopid %d  ", opId);
            postop->diagnosticPrint();
            vc->diagnosticPrint();
        #endif



    }else{
        PERROR("missing task found when processing POST-NEG(%d,%u,%d).",
                op->tid, postop->event, postop->dest);
    }

    return crossHbList;
}

std::list<ErBaseOperation*> ErBaseline::evaluateEnableTimerTaskOp(int opId, ErBaseOperation* op, VectorClock* atomicVC){
    std::list<ErBaseOperation*> crossHbList;
    EnableTimerTaskOp* enableop = dynamic_cast<EnableTimerTaskOp*>(op);

    if(enableop->shouldStore){ //only if this enable op is indicated to be stored process it, else skip.

        if(op->task != NULL){
            ErBaseTask* task = op->task;
    
            ErBaseline::vcIterator vcIt = curFrame->vcMap.find(task->id);
            VectorClock* vc = NULL;
            if(vcIt != curFrame->vcMap.end()){
                vc = vcIt->second;
            }else{
                PERROR("Missing VC for task %d of ENABLE_TIMER_TASK(%u). Aborting as something has been "
                       "wrongly setup.", task->id, enableop->instance);
            }
    
            storeHBInfo(opId, vc);
    
            /* only the initial ER iteration changes the epoch of a task. 
             * Other iterations only transfer HB info between tasks.
             */
            if(curFrame->iter == ER_BASE_ITER){
                int chain = oph->getChain(task, vc);
                int epoch = vc->incrementClock(chain);
                task->epoch = epoch;
                //reflect that the max clock value on 'chain' has changed
                oph->curClockOnChains->incrementClock(chain);
            }
    
            #ifdef DBG_COMPUTEHB
                printf("\nopid %d  ", opId);
                enableop->diagnosticPrint();
                vc->diagnosticPrint();
            #endif
        }else{
            PERROR("missing task found when processing ENABLE_TIMER_TASK(%u).",
                    enableop->instance);
        }

    }else{
        //not a serious error but worth informing
        #ifdef DBG_COMPUTEHB
        printf("The preprocessor has stored ENABLE_TIMER_TASK(%u) with opId %d even though its shouldStore is false.", 
                enableop->instance, opId);
        #endif
    }

    return crossHbList;
}

std::list<ErBaseOperation*> ErBaseline::evaluateTriggerTimerTaskOp(int opId, ErBaseOperation* op, VectorClock* atomicVC){
    std::list<ErBaseOperation*> crossHbList;
    TriggerTimerTaskOp* triggerop = dynamic_cast<TriggerTimerTaskOp*>(op);

    if(triggerop->enableOpId > 0){
        std::map<int, VectorClock* >::iterator evcIt =
                curFrame->vcStore.find(triggerop->enableOpId);
        if(evcIt != curFrame->vcStore.end()){ 
            /* TriggerOp's VC timestamp gets modified only if enable has been updated
             * in this iteration. It does not matter whether trigger's task itself has been 
             * updated by this iteration yet.
             */
            if(op->task != NULL){
                ErBaseTask* task = op->task;

                ErBaseline::vcIterator vcIt = curFrame->vcMap.find(task->id);
                VectorClock* vc = NULL;
                if(vcIt != curFrame->vcMap.end()){
                    vc = vcIt->second;
                }else{
                    vc = new VectorClock();
                    curFrame->vcMap.insert(std::make_pair(task->id, vc));
                }

                //to aid in evaluation of NO-PRE rule
                ErBaseIterInfo* tempFrame = baseFrame;
                while(tempFrame != NULL){
                    vcIt = tempFrame->vcMap.find(task->id);
                    if(vcIt != tempFrame->vcMap.end()){
                        atomicVC->vcJoin(vcIt->second);
                    }
                    tempFrame = tempFrame->next;
                }

                vc->vcJoin(evcIt->second); //take join with enable's stored VC timestamp
                if(curFrame->iter == ER_BASE_ITER){
                    hbOrder(triggerop->hbSrcOp, op);
                }

                crossHbList.push_back(triggerop->hbSrcOp);

                #ifdef DBG_COMPUTEHB
                    printf("\nopid %d  ", opId);
                    triggerop->diagnosticPrint();
                    vc->diagnosticPrint();
                #endif

            }else{
                PERROR("missing task found when processing TRIGGER_TIMER_TASK(%u).", 
                        triggerop->instance);
            }
        }
    }else{
            //we dont consider this a very serious error but worth informing.
            //Also this happens when enable and trigger timer task are on the same thread.
            #ifdef DBG_COMPUTEHB
                printf("Missing enableOpId corresponding to TRIGGER_TIMER_TASK(%u).", triggerop->instance);
            #endif
    }

    return crossHbList;
}

void ErBaseline::storeHBInfo(int opId, VectorClock* vc){
    VectorClock* vcToStore = new VectorClock(*vc);
    curFrame->vcStore.insert(std::make_pair(opId, vcToStore));
}

void ErBaseline::hbOrder(ErBaseOperation* src, ErBaseOperation* dest){
    dest->parentOps.insert(src);
    src->childrenOps.insert(dest);
}

//join baseFrame's VC with dest VC
void ErBaseline::baseVcOrder(ErBaseTask* src, VectorClock* destVc){
    VectorClock* srcVc = baseFrame->vcMap.find(src->id)->second;
    destVc->vcJoin(srcVc);
}

bool ErBaseline::areHbOrdered(ErBaseTask* src, VectorClock* destVc){
    bool areOrdered = false;
    std::pair<int, u4> chainClockPair = src->getEpoch();
    if(destVc->doesSubsumeClock(chainClockPair.first, chainClockPair.second)){
        areOrdered = true;
    }

    return areOrdered;
}

bool ErBaseline::arePostsHbOrdered(BasePostOp* src, BasePostOp* dest){
    bool areOrdered = false;
    if(src->task == NULL){
        return false;
    }
    std::map<u4, VectorClock*>::iterator srcIt = foqNegPostVC.find(src->event);
    std::map<u4, VectorClock*>::iterator destIt = foqNegPostVC.find(dest->event);
    if(srcIt != foqNegPostVC.end() && destIt != foqNegPostVC.end()){
        VectorClock* srcVc = srcIt->second;
        VectorClock* destVc = destIt->second;    
        if(destVc->doesSubsumeClock(src->task->chain, srcVc->getClockValue(src->task->chain))){
            areOrdered = true;
        }
    }else{
        PERROR("missing Front of Queue or Neg Post Vector Clock -- detected when performing FIFO computation");
    }

    return areOrdered;
}


void ErBaseline::initializeEvaluateFunctionPointers(){

    evaluateFuncPtr[DR_POST] = &ErBaseline::evaluatePostOp;
    evaluateFuncPtr[DR_AT_TIME_POST] = &ErBaseline::evaluateAtTimePostOp;
    evaluateFuncPtr[DR_BEGIN] = &ErBaseline::evaluateBeginOp;
    evaluateFuncPtr[DR_END] = &ErBaseline::evaluateEndOp;
    evaluateFuncPtr[DR_FORK] = &ErBaseline::evaluateForkOp;
    evaluateFuncPtr[DR_THREADINIT] = &ErBaseline::evaluateThreadinitOp;
    evaluateFuncPtr[DR_THREADEXIT] = &ErBaseline::evaluateThreadexitOp;
    evaluateFuncPtr[DR_LOOP] = &ErBaseline::evaluateLoopOp;
    evaluateFuncPtr[DR_LOOP_EXIT] = &ErBaseline::evaluateLoopExitOp;
    evaluateFuncPtr[DR_ENABLE_EVENT] = &ErBaseline::evaluateEnableEventOp;
    evaluateFuncPtr[DR_TRIGGER_EVENT] = &ErBaseline::evaluateTriggerEventOp;
    evaluateFuncPtr[DR_ENABLE_LIFECYCLE] = &ErBaseline::evaluateEnableActLifecycleOp;
    evaluateFuncPtr[DR_TRIGGER_LIFECYCLE] = &ErBaseline::evaluateTriggerActLifecycleOp;
    evaluateFuncPtr[DR_TRIGGER_RECEIVER] = &ErBaseline::evaluateTriggerReceiverOp;
    evaluateFuncPtr[DR_TRIGGER_SERVICE] = &ErBaseline::evaluateTriggerServiceOp;
    evaluateFuncPtr[DR_ENABLE_WINDOW_FOCUS] = &ErBaseline::evaluateEnableWindowFocusOp;
    evaluateFuncPtr[DR_TRIGGER_WINDOW_FOCUS] = &ErBaseline::evaluateTriggerWindowFocusOp;
    evaluateFuncPtr[DR_WAIT] = &ErBaseline::evaluateWaitOp;
    evaluateFuncPtr[DR_NOTIFY] = &ErBaseline::evaluateNotifyOp;
    evaluateFuncPtr[DR_IDLE_POST] = &ErBaseline::evaluateIdlePostOp;
    evaluateFuncPtr[DR_POST_FOQ] = &ErBaseline::evaluatePostFoqOp;
    evaluateFuncPtr[DR_POST_NEG] = &ErBaseline::evaluatePostNegOp;
    evaluateFuncPtr[DR_ENABLE_TIMER_TASK] = &ErBaseline::evaluateEnableTimerTaskOp;
    evaluateFuncPtr[DR_TRIGGER_TIMER_TASK] = &ErBaseline::evaluateTriggerTimerTaskOp;

}


void ErBaseline::computeHB(){
    
    OpHelper::opMapIterator opIt = oph->opMap.begin();
    if(oph->opMap.empty()){
        PERROR("Operations map is empty. Hence, no trace available to compute HB. Aborting HB computation");
    }

    initializeEvaluateFunctionPointers();

    std::map<int, ErBaseOperation*> taskPrevOpMap;
    std::list<ErBaseOperation*> crossHbList;
    VectorClock* atomicVC = new VectorClock(); // this VC gets initialized to operation's current VC in case crossHB edges get added
    
    while(opIt != oph->opMap.end()){
        crossHbList.clear();
        atomicVC->resetVector();
        curFrame->index = opIt->first;
        ErBaseOperation* op = dynamic_cast<ErBaseOperation*>(opIt->second);

        if(curFrame->iter == ER_BASE_ITER && op->task != NULL){
            std::map<int, ErBaseOperation*>::iterator prevOpIt = taskPrevOpMap.find(op->task->id);
            if(prevOpIt != taskPrevOpMap.end()){
                hbOrder(prevOpIt->second, op);
                prevOpIt->second = op;
            }else{
                taskPrevOpMap.insert(std::make_pair(op->task->id, op));
            }
        }
        
        if(curFrame->iter == ER_BASE_ITER || 
                reevaluateOps.find(curFrame->iter)->second.find(op) != reevaluateOps.find(curFrame->iter)->second.end()){
            //evaluate operation
            crossHbList = (this->*evaluateFuncPtr[op->getTypeId()])(curFrame->index, op, atomicVC);
        }

        if(curFrame->index == curFrame->prevIterIndex){ //this will never hold for curFrame = baseFrame
            ErBaseIterInfo* tempFrame = curFrame->prev; 

            ErBaseline::vcIterator srcVcIt = curFrame->vcMap.begin();
            ErBaseline::vcIterator destVcIt;

            for(; srcVcIt != curFrame->vcMap.end(); srcVcIt++){
                VectorClock* destVc = NULL;
                destVcIt = tempFrame->vcMap.find(srcVcIt->first);
                if(destVcIt != tempFrame->vcMap.end()){
                    destVcIt->second->vcJoin(srcVcIt->second);
                    destVc = destVcIt->second;
                }else{
                    destVc = new VectorClock();
                    destVc->vcJoin(srcVcIt->second);
                    tempFrame->vcMap.insert(std::make_pair(srcVcIt->first, destVc));
                }
            }

            srcVcIt = curFrame->vcStore.begin();
            for(; srcVcIt != curFrame->vcStore.end(); ){
                VectorClock* curVc = srcVcIt->second;
                
                destVcIt = tempFrame->vcStore.find(srcVcIt->first);
                if(destVcIt != tempFrame->vcStore.end()){
                    //update VC of destination map
                    destVcIt->second->vcJoin(curVc);
                    delete curVc; //we dont need this anymore
                    
                }else{
                    srcVcIt->second = NULL;
                    tempFrame->vcStore.insert(std::make_pair(srcVcIt->first, curVc));
                }

                curFrame->vcStore.erase(srcVcIt++);
            }
            
            reevaluateOps.erase(curFrame->iter);

            delete curFrame;
            curFrame = tempFrame;
            curFrame->next = NULL;
        }

        if(!crossHbList.empty()){
            ErBaseOperation* sinkOp = NULL;
            
            if(op->getTypeId() == DR_NOTIFY){
                if(op->hbSinkOp != NULL){
                    sinkOp = op->hbSinkOp;
                }else{
                    PERROR("Notify operation with opid %d on tid %d is missing a sink task ID.", 
                            curFrame->index, op->tid);
                }
            }else if(op->getTypeId() == DR_TRIGGER_WINDOW_FOCUS){
                sinkOp = op;
            }else if(op->getTypeId() == DR_TRIGGER_LIFECYCLE){
                TriggerActLifecycleOp* actOp = dynamic_cast<TriggerActLifecycleOp*>(op);
                if(!actOp->envOrderedEnableOps.empty()){
                    sinkOp = op;
                }
            }else if(op->getTypeId() == DR_TRIGGER_TIMER_TASK){
                sinkOp = op;
            }

            if(sinkOp != NULL && sinkOp->task != NULL && sinkOp->task->eventId > 0){
                bool shouldReEvaluate = false;
                highestVisitedFlag++;
                std::list< ErBaseOperation* >::iterator chIt = crossHbList.begin();
                VectorClock* newVC = NULL;
                for(; chIt != crossHbList.end(); chIt++){
                    ErBaseOperation* src = *chIt;

                    //perform iterative DFS to check if NO-PRE rule can be applied
                    std::stack<ErBaseOperation*> dfsStack;
                    std::set<ErBaseOperation*>::iterator parentIt = src->parentOps.begin();
                    for(; parentIt != src->parentOps.end(); parentIt++){
                        if((*parentIt)->visitedCount < highestVisitedFlag){
                            (*parentIt)->visitedCount = highestVisitedFlag;
                            dfsStack.push(*parentIt);
                        }
                    }                
    
                    while(!dfsStack.empty()){
                        #ifdef DBG_COLLECT_STATS
                            Stats::evAtomicDfsCount++;
                        #endif
    
                        ErBaseOperation* cur = dfsStack.top();  
                        dfsStack.pop();
    
                        if(cur->tid == sinkOp->tid && cur->task != NULL){
                            if(cur->task != sinkOp->task && cur->task->eventId > 0){
                                
                                #ifdef DBG_COLLECT_STATS
                                    Stats::usefulWorkCount++;
                                #endif

                                std::pair<int, u4> chainClockPair = cur->task->getEpoch();
                                if(!atomicVC->doesSubsumeClock(chainClockPair.first, chainClockPair.second)){
                                    hbOrder(cur->task->endOp, sinkOp->task->postOp->hbSinkOp);

                                    if(newVC == NULL){
                                        newVC = new VectorClock(*(baseFrame->vcMap.find(cur->task->id)->second));
                                    }else{
                                        newVC->vcJoin(baseFrame->vcMap.find(cur->task->id)->second);
                                    }

                                    shouldReEvaluate = true;
                                    #ifdef DBG_COLLECT_STATS
                                        Stats::actualEdgesAdded++;
                                    #endif
                                }//else - already ordered

                                #ifdef DBG_COLLECT_STATS
                                    Stats::joinCandidateCount++;
                                #endif
                            }//else - reached sinkOp in same task or prior to LOOP. Hence, can prune.
                        }else{
                            for(parentIt = cur->parentOps.begin(); parentIt != cur->parentOps.end(); parentIt++){
                                if((*parentIt)->visitedCount < highestVisitedFlag){
                                    (*parentIt)->visitedCount = highestVisitedFlag;
                                    dfsStack.push(*parentIt);
                                }
                            }
                        }
                    }                
                }

                if(shouldReEvaluate){
                    std::set<ErBaseOperation*> destOpSet;
                    std::map<int, VectorClock*> newVcMap;

                    highestVisitedFlag++;

                    //new edges added from end of some task to sinkOp->task->begin. Reevaluate affected ops from sinkOp->task->begin to sinkOp
                    //Even though DFS may add ops even beyond sinkop (if iter > 2) our logic wont process ceyond current op and hence not do redundant computation.
                    std::stack<ErBaseOperation*> dfsStack;
                    ErBaseOperation* beginop = sinkOp->task->postOp->hbSinkOp;
                    std::set<ErBaseOperation*>::iterator childIt = beginop->childrenOps.begin();
                    for(; childIt != beginop->childrenOps.end(); childIt++){
                        dfsStack.push(*childIt);
                    }
    
                    while(!dfsStack.empty()){
                        ErBaseOperation* cur = dfsStack.top();
                        dfsStack.pop();
    
                        if(cur->visitedCount < highestVisitedFlag){
                            cur->visitedCount = highestVisitedFlag;
                            destOpSet.insert(cur);
                        
                            if(cur->getTypeId() == DR_BEGIN){
                                /* A begin op bop maybe ordered w.r.t. an end op eop due to EV-ATOMIC. However, when processing
                                 * bop we do DFS only via its post and not through any end ops directly connected to bop.
                                 * If newVC could be passed only through eop ordered via EV-ATOMIC then DFS done through bop's
                                 * post may not hit any ops in eop's task. Also, our current EV-ATOMIC check done above too
                                 * will not initate any reprocessing as eop's baseFrame epoch is already known to bop. Hence,
                                 * we pass newVc to all begin operations that are HB ordered w.r.t sinkOp here itself.
                                 */
                                VectorClock* tempBeginVc = new VectorClock(*newVC);
                                newVcMap.insert(std::make_pair(cur->task->id, tempBeginVc));
                            }
    
                            for(childIt = cur->childrenOps.begin(); childIt != cur->childrenOps.end(); childIt++){
                                if((*childIt)->visitedCount < highestVisitedFlag){
                                    dfsStack.push(*childIt);
                                }
                            }
                        }
                    }
              
                    //We have computed ops whose VC should be updated
                    if(!destOpSet.empty()){
                        int newIter = fp++;
                        reevaluateOps.insert(std::make_pair(newIter, destOpSet));
                        newVcMap.insert(std::make_pair(sinkOp->task->id, newVC));
                  
                        opIt = oph->opMap.find(sinkOp->task->beginOpId);
                            if(opIt == oph->opMap.end()){
                            PERROR("missing entry in opMap corresponding to BEGIN operation %d detected when initializing" 
                               " a new ER iteration %d.", sinkOp->task->beginOpId, fp-1);
                        }
                        opIt++; //since we have already computed new VC of begin we can initiate new iteration's computation fromthe next operation in the opMap
                   
                        ErBaseIterInfo* newIterInfo = new ErBaseIterInfo(++fp, opIt->first, curFrame->index, curFrame);
                        newIterInfo->vcMap = newVcMap;
                        curFrame->next = newIterInfo;
                        curFrame = newIterInfo;

                        continue;//we do not want to change opIt
                    }else{
                        delete newVC; //since shouldReEvaluate is true newVC is definitely non null
                    }
                }
            }
        }

        opIt++;
    }

    #ifdef DBG_COLLECT_STATS
        if(curFrame->iter == ER_BASE_ITER){
            OpHelper::taskMapIterator taskIt = oph->taskMap.begin();
            for(; taskIt != oph->taskMap.end(); taskIt++){
                ErBaseline::vcIterator vcIt = curFrame->vcMap.find((dynamic_cast<ErBaseTask*>(taskIt->second))->id);
                if(vcIt != curFrame->vcMap.end()){
                    Stats::vcMemory += sizeof(VectorClock) + (vcIt->second->getVCSize() * sizeof(u4));
                }else{
                    PERROR("Task %d has no VC assigned!", (dynamic_cast<ErBaseTask*>(taskIt->second))->id);
                }
            }

            std::map<int, VectorClock*>::iterator evcIt = curFrame->vcStore.begin();
            for(; evcIt != curFrame->vcStore.end(); evcIt++){
                Stats::vcMemory += sizeof(VectorClock) + (evcIt->second->getVCSize() * sizeof(u4));
            }

            std::map<u4, VectorClock*>::iterator foqIt = foqNegPostVC.begin();
            for(; foqIt != foqNegPostVC.end(); foqIt++){
                Stats::vcMemory += sizeof(VectorClock) + (foqIt->second->getVCSize() * sizeof(u4));
            }

            OpHelper::opMapIterator tmpOpIt = oph->opMap.begin();
            for(; tmpOpIt != oph->opMap.end(); tmpOpIt++){
                ErBaseOperation* tmpErOp = dynamic_cast<ErBaseOperation*>(tmpOpIt->second);
                Stats::hbParentEdgeCount += tmpErOp->parentOps.size();
                Stats::hbChildEdgeCount += tmpErOp->childrenOps.size();
            }
        }
    #endif
}

}
