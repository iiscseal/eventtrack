/*
 * Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ERBASEOPERATION_H_
#define ERBASEOPERATION_H_

#include <operation.h>
#include <operationHelper.h>

namespace erbaseline{

class ErBaseTask;
class ErBaseThread;
class ErBaseTask;
class ErBaseOperation;
class BasePostOp; 
class PostOp;
class AtTimePostOp;
class BeginOp;
class EndOp;
class AttachqOp;
class ForkOp;
class JoinOp;
class ThreadinitOp;
class ThreadexitOp;
class RemoveEventOp;
class LoopOp;
class LoopExitOp;
class EnableEventOp;
class TriggerEventOp;
class EnableActLifecycleOp;
class TriggerActLifecycleOp;
class TriggerReceiverOp;
class TriggerServiceOp;
class InstanceIntentOp;
class EnableWindowFocusOp;
class TriggerWindowFocusOp;
class WaitOp;
class TimedWaitOp;
class NotifyOp;
class NativePostOp;
class UiPostOp;
class IdlePostOp;
class NativeNotifyOp;
class PostFoqOp;
class NativePostFoqOp;
class PostNegOp;
class EnableTimerTaskOp;
class TriggerTimerTaskOp;


class ErBaseTask : public Task{

public:

ErBaseTask(int id, int tid, int eventId);

~ErBaseTask();

std::pair<int,u4> getEpoch();

/* list of tasks on the same thread w.r.t. which this task has already been ordered
 * due to HB rule.
 */
std::set<ErBaseTask*> joinedWrtTasks;

int id; //taskId - key used in taskMap

BasePostOp* postOp;

EndOp* endOp;

/* updated only in the first iteration of algo.
 * Reflects current clock value of the task.
 */
u4 epoch;

};


class ErBaseThread : public ThreadClass{

public:

ErBaseThread(int curTaskId, ErBaseTask* curTask, ForkOp* forkOp);
virtual ~ErBaseThread();

ErBaseTask* curTask;

ForkOp* forkOp;

LoopOp* loopOp;

std::stack<ErBaseTask*> taskStack;

};

class ErBaseOperation : public Operation{

public:

ErBaseOperation();

ErBaseOperation(int tid, int taskId);

virtual ~ErBaseOperation(){}

virtual void diagnosticPrint(){
    Operation::diagnosticPrint();
    printf("this:%p, hbSink:%p, visitedCount:%d \n", this, hbSinkOp, visitedCount);
    printf("parent HB nodes:\n");
    std::set<ErBaseOperation*>::iterator it = parentOps.begin();
    for(; it != parentOps.end(); it++){
        printf("%p,  ", (*it));
    }
    printf("\n");
    printf("children HB nodes:\n");
    it = childrenOps.begin();
    for(; it != childrenOps.end(); it++){
        printf("%p,  ", (*it));
    }
    printf("\n");
}

virtual void setHbSink(){
    hbSinkOp = this; //trivial assignment in case there is no other sink
}

virtual int getTypeId(){
    return DR_HB_OP;
}

ErBaseTask* task;

std::set<ErBaseOperation*> parentOps;
std::set<ErBaseOperation*> childrenOps;

u4 visitedCount; //used as integer flag for DFS. Using bool would be inefficient as ErBaseline does multiple DFS

ErBaseOperation* hbSinkOp;

};


//base class for all kinds of post operation
class BasePostOp : public ErBaseOperation{

public:

BasePostOp();
virtual ~BasePostOp();

virtual int getTypeId(){
    return DR_BASE_POST;
}

virtual bool isFifoOrdered(BasePostOp* postOp, int typeOfPost){
    return false;
}

u4 event;
int dest; //destination threadId
int typeOfPost;

//only contains tasks posted by native threads
std::set<int> tobeInitializedTasks;

virtual void diagnosticPrint(){
    ErBaseOperation::diagnosticPrint();
    printf("msg:%u, destination thread:%d, typeOfPost:%d \n", event, dest, typeOfPost);
}

};

class PostOp : public BasePostOp {

public:

PostOp(OpLog* oplog, OpHelper* oph);

~PostOp();

virtual int getTypeId(){
    return DR_POST;
}

virtual bool isFifoOrdered(BasePostOp* postOp, int typeOfPost){
    bool res = false;
    if(typeOfPost == DR_POST){
        if(delay <= dynamic_cast<PostOp*>(postOp)->delay){
            res = true;
        }
    }else if(typeOfPost == DR_IDLE_POST){
        if(delay == 0){
            res = true;
        }
    }
    return res;
}

virtual void diagnosticPrint(){
    printf("POST: \n");
    BasePostOp::diagnosticPrint();
    printf("delay:%u\n", delay);
}

int delay;

};

class AtTimePostOp : public BasePostOp {

public:

AtTimePostOp(OpLog* oplog, OpHelper* oph);

~AtTimePostOp();

virtual int getTypeId(){
    return DR_AT_TIME_POST;
}

virtual bool isFifoOrdered(BasePostOp* postOp, int typeOfPost){
    return false;
}

virtual void diagnosticPrint(){
    printf("POST-AT-TIME: \n");
    BasePostOp::diagnosticPrint();
    printf("delay:%u\n", delay);
}

int delay;

};

class BeginOp : public ErBaseOperation{

public:

BeginOp(OpLog* oplog, OpHelper* oph);

~BeginOp();

virtual int getTypeId(){
    return DR_BEGIN;
}

virtual void diagnosticPrint(){
    printf("BEGIN:\n");
    ErBaseOperation::diagnosticPrint();
    printf("event:%u \n", event);
}

/* taskId should be sufficient. If you need event uncomment this and remove
 * declaration of event in preProcessBegin.
 */
u4 event;

bool isOnReceive;

};

class EndOp : public ErBaseOperation{

public:

EndOp(OpLog* oplog, OpHelper* oph);

EndOp(int tid, int taskId, u4 event);

virtual int getTypeId(){
    return DR_END;
}

virtual void diagnosticPrint(){
    printf("END:\n");
    ErBaseOperation::diagnosticPrint();
    printf("event:%u \n", event);
}

~EndOp();

//taskId should be sufficient
u4 event;

};

class AttachqOp : public ErBaseOperation{

public:
AttachqOp();
~AttachqOp();

virtual int getTypeId(){
    return DR_ATTACH_Q;
}

};

class ForkOp : public ErBaseOperation{

public:

ForkOp(OpLog* oplog, OpHelper* oph);

~ForkOp();

virtual int getTypeId(){
    return DR_FORK;
}

virtual void diagnosticPrint(){
    printf("FORK:\n");
    ErBaseOperation::diagnosticPrint();
    printf("childTid:%d \n", childTid);
}

int childTid;

};

class JoinOp : public ErBaseOperation{

public:

//We currently do not log Join as it is taken care by NOTIFY-WAIT
//Join is implemented internally using notify-wait
JoinOp(OpLog* oplog, OpHelper* oph);

~JoinOp();

virtual int getTypeId(){
    return DR_JOIN;
}

virtual void diagnosticPrint(){
    printf("JOIN:\n");
    ErBaseOperation::diagnosticPrint();
    printf("joinThread:%d \n", joinThread);
}

int joinThread; //thread for which current thread is waiting to finish

};

class ThreadinitOp : public ErBaseOperation{

public:

ThreadinitOp(OpLog* oplog, OpHelper* oph);

~ThreadinitOp();

virtual int getTypeId(){
    return DR_THREADINIT;
}

virtual void diagnosticPrint(){
    printf("THREADINIT:\n");
    ErBaseOperation::diagnosticPrint();
}

};

class ThreadexitOp : public ErBaseOperation{

public: 

ThreadexitOp(OpLog* oplog, OpHelper* oph);

~ThreadexitOp();

virtual int getTypeId(){
    return DR_THREADEXIT;
}

virtual void diagnosticPrint(){
    printf("THREADEXIT:\n");
    ErBaseOperation::diagnosticPrint();
}

};

class RemoveEventOp : public ErBaseOperation{

};

class LoopOp : public ErBaseOperation{

public:

LoopOp(OpLog* oplog, OpHelper* oph);

~LoopOp();

virtual int getTypeId(){
    return DR_LOOP;
}

virtual void diagnosticPrint(){
    printf("LOOP-On-Q:\n");
    ErBaseOperation::diagnosticPrint();
    printf("event-queue:%u \n", eventqueue);
}

u4 eventqueue;

};


class LoopExitOp : public ErBaseOperation{

public:

LoopExitOp(OpLog* oplog, OpHelper* oph);

~LoopExitOp();

virtual int getTypeId(){
    return DR_LOOP_EXIT;
}

virtual void diagnosticPrint(){
    printf("LOOP-EXIT:\n");
    ErBaseOperation::diagnosticPrint();
    printf("event-queue:%u \n", eventqueue);
     printf("Tasks executed on this thread prior to this loop exit:\n");
    for(std::list<ErBaseTask*>::iterator listIt = hbSrcList.begin(); listIt != hbSrcList.end(); listIt++){
        printf("%d,  ", (*listIt)->id);
    }
    printf("\n");
}

u4 eventqueue;
std::list<ErBaseTask*> hbSrcList;

};


class EnableEventOp : public ErBaseOperation{

public:

EnableEventOp(OpLog* oplog, OpHelper* oph);

~EnableEventOp();

virtual int getTypeId(){
    return DR_ENABLE_EVENT;
}

virtual void diagnosticPrint(){
    printf("ENABLE_EVENT:\n");
    ErBaseOperation::diagnosticPrint();
    printf("view:%u, uiEvent:%d \n", view, uiEvent);
}


int uiEvent; //stores event code which will be used for matching
u4 view; //hashcode of UI view on which event will be triggered

/* below field is true if this operation's HB info should be 
 * stored for a future op to use. 
 */
bool shouldStore;

/* a set of tasks whose post will have HB with this enable operation.
 * Hence, HB info of these tasks get initialized when processing this operation.
 * This is done due to the way DroidRacer models lifecycle callbacks and environment events.
 * Only contains tasks posted by native threads or outside event handlers
 */
std::set<int> tobeInitializedTasks;

};

class TriggerEventOp : public ErBaseOperation{

public:

TriggerEventOp(OpLog* oplog, OpHelper* oph);

~TriggerEventOp();

virtual int getTypeId(){
    return DR_TRIGGER_EVENT;
}

virtual void diagnosticPrint(){
    printf("TRIGGER_EVENT:\n");
    ErBaseOperation::diagnosticPrint();
    printf("view:%u, uiEvent:%d, enableOpId:%d \n", view, uiEvent, enableOpId);
}

int uiEvent; //stores event code which will be used for matching
u4 view; //hashcode of UI view on which event will be triggered
int enableOpId;
ErBaseOperation* hbSrcOp;

};

class EnableActLifecycleOp : public ErBaseOperation{

public:

EnableActLifecycleOp(OpLog* olog, OpHelper* oph);

~EnableActLifecycleOp();

virtual int getTypeId(){
    return DR_ENABLE_LIFECYCLE;
}

virtual void diagnosticPrint(){
    printf("ENABLE_LIFECYCLE:\n");
    ErBaseOperation::diagnosticPrint();
    printf("activity instance:%u, state:%d \n", instance, state);
    printf("Tasks which will be initialized by this operation:\n");
    std::set<int>::iterator it = tobeInitializedTasks.begin();
    for(; it != tobeInitializedTasks.end(); it++){
        printf(" %d,", *it);
    }
    printf("\n");
}

int state;
u4 instance;

/* below field is true if this operation's HB info should be 
 * stored for a future op to use. 
 */
bool shouldStore;

std::set<int> tobeInitializedTasks;

};

class TriggerActLifecycleOp : public ErBaseOperation{

public:

TriggerActLifecycleOp(OpLog* oplog, OpHelper* oph);

~TriggerActLifecycleOp();

virtual int getTypeId(){
    return DR_TRIGGER_LIFECYCLE;
}

virtual void diagnosticPrint(){
    printf("TRIGGER_LIFECYCLE:\n");
    ErBaseOperation::diagnosticPrint();
    printf("activity instance:%u, state:%d \n", instance, state);
    printf("ENABLE_LIFECYCLE <operation, task> which have direct HB with this operation:\n");
    std::set<int>::iterator it = envOrderedEnableOps.begin();
    for(; it != envOrderedEnableOps.end(); it++){ 
        printf(" %d, ", *it);
    }
    printf("\n");

}

/* need to add edge from these enable ops to this trigger operation directly.
 * Hence you will have to set shouldStore of these operations to TRUE.
 */
std::set<int> envOrderedEnableOps;

int state;
u4 instance;

};

class TriggerReceiverOp : public ErBaseOperation{

public:

TriggerReceiverOp(OpLog* oplog, OpHelper* oph);

~TriggerReceiverOp();

virtual int getTypeId(){
    return DR_TRIGGER_RECEIVER;
}

virtual void diagnosticPrint(){
    printf("TRIGGER_RECEIVER:\n");
    ErBaseOperation::diagnosticPrint();
    printf("broadcast reciever actionKey:%d, identifier:%u, intent:%d, state:%d \n", actionKey, identifier, intentId, state);
    printf("Tasks of ON_RECEIVE event handler which will be initialzed by this operation:\n");
    std::set<int>::iterator it = tobeInitializedTasks.begin();
    for(; it != tobeInitializedTasks.end(); it++){
        printf(" %d,", *it);
    }
    printf("\n");
}

//only contains tasks posted by native threads
std::set<int> tobeInitializedTasks;

int state;
u4 identifier;
int intentId;
int actionKey;
std::set<ErBaseTask*> onRecTasks; 

};

class TriggerServiceOp : public ErBaseOperation{

public:

TriggerServiceOp(OpLog* oplog, OpHelper* oph);

~TriggerServiceOp();

virtual int getTypeId(){
    return DR_TRIGGER_SERVICE;
}

virtual void diagnosticPrint(){
    printf("TRIGGER_SERVICE:\n");
    ErBaseOperation::diagnosticPrint();
    printf("service classKey:%d, identifier:%u, state:%d \n", serviceClassKey, identifier, state);
    printf("Tasks of TRIGGER_SERVICE operations which will be initialzed by this operation:\n");
    std::set<int>::iterator it = tobeInitializedTasks.begin();
    for(; it != tobeInitializedTasks.end(); it++){ 
        printf(" %d,", *it);
    }
    printf("\n");
}

//only contains tasks posted by native threads
std::set<int> tobeInitializedTasks;

int state;
u4 identifier;
int serviceClassKey;

};

class InstanceIntentOp : public ErBaseOperation{

public:

~InstanceIntentOp();

virtual int getTypeId(){
    return DR_INSTANCE_INTENT;
}

};

class EnableWindowFocusOp : public ErBaseOperation{

public:

EnableWindowFocusOp(OpLog* oplog, OpHelper* oph);

~EnableWindowFocusOp();

virtual int getTypeId(){
    return DR_ENABLE_WINDOW_FOCUS;
}

virtual void diagnosticPrint(){
    printf("ENABLE_WINDOW_FOCUS:\n");
    ErBaseOperation::diagnosticPrint();
    printf("window:%u \n", windowId);
    printf("Trigger-window event handlers initialized by this enable operation:\n");
    std::set<int>::iterator it = tobeInitializedTasks.begin();
    for(; it != tobeInitializedTasks.end(); it++){
        printf(" %d,", *it);
    }
    printf("\n");

}

u4 windowId; //window whose focus will be changed

/* below field is true if this operation's HB info should be 
 * stored for a future op to use. 
 */
bool shouldStore;

//this is empty if post of trigger-window focus is executed prior to enable-window focus
std::set<int> tobeInitializedTasks;

};

class TriggerWindowFocusOp : public ErBaseOperation{

public:

TriggerWindowFocusOp(OpLog* oplog, OpHelper* oph);

~TriggerWindowFocusOp();

virtual int getTypeId(){
    return DR_TRIGGER_WINDOW_FOCUS;
}

virtual void diagnosticPrint(){
    printf("TRIGGER_WINDOW_FOCUS:\n");
    ErBaseOperation::diagnosticPrint();
    printf("window:%u, enableOpId:%d \n", windowId, enableOpId);
}

int enableOpId; //this is set only if enable & trigger are on different threads

u4 windowId; //window whose focus is changed

ErBaseOperation* hbSrcOp;

};

class WaitOp : public ErBaseOperation{

public:

WaitOp(OpLog* oplog, OpHelper* oph);

~WaitOp();

virtual int getTypeId(){
    return DR_WAIT;
}

virtual void diagnosticPrint(){
    printf("WAIT:\n");
    ErBaseOperation::diagnosticPrint();
}
/* in a future implementation we will track object on which
 * wait is performed, so as to more accurately match with notify.
 * However, have not seen any problem with our current way of tracking.
 */

};

class TimedWaitOp : public ErBaseOperation{

public:

TimedWaitOp(OpLog* oplog, OpHelper* oph);

~TimedWaitOp();

virtual int getTypeId(){
    return DR_TIMED_WAIT;
}

virtual void diagnosticPrint(){
    printf("TIMED_WAIT:\n");
    ErBaseOperation::diagnosticPrint();
}

};

class NotifyOp : public ErBaseOperation{

public:

NotifyOp(OpLog* oplog, OpHelper* oph);

~NotifyOp();

virtual int getTypeId(){
    return DR_NOTIFY;
}

int notifiedTid;

virtual void diagnosticPrint(){
    printf("NOTIFY:\n");
    ErBaseOperation::diagnosticPrint();
    printf("notifiedTid: %d\n", notifiedTid);
}

};

class NativePostOp : public ErBaseOperation{

};

class UiPostOp : public ErBaseOperation{

};

class IdlePostOp : public BasePostOp{

public:

IdlePostOp(OpLog* oplog, OpHelper* oph);

~IdlePostOp();

virtual int getTypeId(){
    return DR_IDLE_POST;
}

virtual bool isFifoOrdered(BasePostOp* postOp, int typeOfPost){
    if(typeOfPost == DR_IDLE_POST){
        return true;
    }

    return false;
}

virtual void diagnosticPrint(){
    printf("IDLE-POST:\n");
    BasePostOp::diagnosticPrint();
}

};

class NativeNotifyOp : public ErBaseOperation{

};

class PostFoqOp : public BasePostOp{

public:

PostFoqOp(OpLog* oplog, OpHelper* oph);

~PostFoqOp();

virtual int getTypeId(){
    return DR_POST_FOQ;
}

virtual bool isFifoOrdered(BasePostOp* postOp, int typeOfPost){
    bool res = false;
    if(typeOfPost == DR_POST || typeOfPost == DR_AT_TIME_POST || typeOfPost == DR_IDLE_POST){
        res = true;
    }
    return res;
}

virtual void diagnosticPrint(){
    printf("POST-FOQ:\n");
    BasePostOp::diagnosticPrint();
    printf("\n");
}

};

class NativePostFoqOp : public BasePostOp{

};

class PostNegOp : public BasePostOp{

public:

PostNegOp(OpLog* oplog, OpHelper* oph);

~PostNegOp();

virtual int getTypeId(){
    return DR_POST_NEG;
}

virtual bool isFifoOrdered(BasePostOp* postOp, int typeOfPost){
    bool res = false;
    if(typeOfPost == DR_POST || typeOfPost == DR_AT_TIME_POST || typeOfPost == DR_IDLE_POST){
        res = true;
    }
    return res;
}

virtual void diagnosticPrint(){
    printf("NEG-POST:\n");
    BasePostOp::diagnosticPrint();
    printf("delay:%d \n", delay);
}

/* we do not really use this field. This will be useful if we do observe
 * postAtTime posts with NEG time specified. Then we can use this field to
 * apply more fine grained event ordering rules.
 */
int delay;

};

class EnableTimerTaskOp : public ErBaseOperation{

public:

EnableTimerTaskOp(OpLog* oplog, OpHelper* oph);

~EnableTimerTaskOp();

virtual int getTypeId(){
    return DR_ENABLE_TIMER_TASK;
}

virtual void diagnosticPrint(){
    printf("ENABLE_TIMER_TASK:\n");
    ErBaseOperation::diagnosticPrint();
    printf("instance:%u \n", instance);
}

/* below field is true if this operation's HB info should be 
 * stored for a future op to use. 
 */
bool shouldStore;

u4 instance;

};

class TriggerTimerTaskOp : public ErBaseOperation{

public:

TriggerTimerTaskOp(OpLog* oplog, OpHelper* oph);

~TriggerTimerTaskOp();

virtual int getTypeId(){
    return DR_TRIGGER_TIMER_TASK;
}

virtual void diagnosticPrint(){
    printf("TRIGGER_TIMER_TASK:\n");
    ErBaseOperation::diagnosticPrint();
    printf("instance:%u, enableOpId:%d \n", instance, enableOpId);
}

u4 instance;

int enableOpId; 

ErBaseOperation* hbSrcOp;

};

}
#endif  // ERBASEOPERATION_H_

