/*
 * Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ERBASESTATS_H_
#define ERBASESTATS_H_

#include <common.h>

namespace erbaseline{

class Stats;

class Stats{

public:

static void initializeHbComputationStats();

static u4 dfsCount;

static u4 noPreDfsCount;

static u4 fifoDfsCount;

static u4 fifoFoqDfsCount;

static u4 receiverDfsCount;

static u4 evAtomicDfsCount;

static u4 usefulWorkCount;

static u4 actualEdgesAdded;

static u4 joinCandidateCount;

static u4 noPreJoinCount;

static u4 fifoJoinCount;

static u4 fifoFoqJoinCount;

static u4 receiverJoinCount;

static u4 hbParentEdgeCount;

static u4 hbChildEdgeCount;
/* Total space occupied by VCs */
static u8 vcMemory;

static u4 eventCount;

static u4 eventLoopCount;

};

}

#endif

