/*
 * Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef EVENTGRAPH_H_
#define EVENTGRAPH_H_

#include <vectorclock.h>
#include <operationHelper.h>
#include <etCommon.h>
#include <etOperation.h>
#include <etStats.h>

namespace etrack{

class EtList;
class EventGraphNode;
class EventAwareVC;

class EtList{

public:

EtList();

EtList(EtTask* task, edgeFlagType lbl);

EtList(const EtList& srcList);

~EtList();

EtList* insert(EtList* etlist, bool modifyInput);

EtList* optimizedInsert(EtList* etlist, bool modifyInput);

void diagnosticPrint();


//int id; //typically this list may be sorted based on this ID
EtTask* task; //EtList is sorted based on task->id
EtList* next;
EtList* prev;
edgeFlagType lbl;

};


class EventGraphNode{

public:

EventGraphNode();

~EventGraphNode();

EventGraphNode(const EventGraphNode& srcEgn);

//returns NULL if no incoming edges from tasks on input thread.
EtList* getIncomingEdgesForThread(int tid);

EtList* getCopyOfTypedIncomingEdgesForThread(int tid, std::set<edgeFlagType> lblSet);

EtList* getCopyOfTypedIncomingEdgesForThread(int tid, edgeFlagType lbl);

void insertEdgeUnmodified(int tid, EtList* src);

void insertEdge(int tid, EtList* src);

void optimizedInsertEdge(int tid, EtList* src);

void insertEdgesAcrossThreads(EventGraphNode* srcEgNode);

//operator uplus defined in the paper
void sparseJoinAcrossThreads(EventGraphNode* egNode, VectorClock* vc,
        bool modifyInput, int selfNodeId, OpHelper* oph);

void diagnosticPrint();

private:
 
bool sparseJoinEdges(int tid, EtList* srcEdges, VectorClock* vc, bool modifySrc, 
        int selfNodeId, OpHelper* oph);

EtList* sparseJoinRecursive(EtList* srcEdges, EtList* destEdges, VectorClock* vc,
        bool modifySrc, int selfNodeId, OpHelper* oph);

/* map of threadId to sorted list of sources (taskIds) of incoming edges
 * such that sources are tasks on that thread.
 */
std::map<int, EtList*> incomingEdges;

};


class EventAwareVC{

public:

EventAwareVC();
~EventAwareVC();

void joinEVCWith(EventAwareVC* srcEvc, OpHelper* oph, bool modifyInputEg);

std::map<int, VectorClock*> vcMap;
std::map<int, EventGraphNode*> eventGraph;

typedef std::map<int, VectorClock*>::iterator vcIterator;
typedef std::map<int, EventGraphNode*>::iterator egIterator;

};


}

#endif
