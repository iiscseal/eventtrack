/*
 * Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ETUTILS_H_
#define ETUTILS_H_

#include <etOperation.h>
#include <envModel.h>
#include <etalgo.h>
#include <etStats.h>


namespace etrack{

class HbUtils;

class HbUtils{
public:

   HbUtils(OpHelper* oph, EnvModel* appEnv);

   ~HbUtils();

   void initializeFunctionPointers();

   void initializePreProcess(int mainThreadId);

   void preProcess(OpLog* oplog);

   void emitEndOpsManually();

   void computeHBForTrace();

   OpHelper* oph;
   
   EnvModel* appEnv;

private:

   void printHbComputationStats(EventTrack* et);

   EtOperation* preProcessPostOp(OpLog* oplog);
   EtOperation* preProcessAtTimePostOp(OpLog* oplog);
   EtOperation* preProcessBeginOp(OpLog* oplog);
   EtOperation* preProcessEndOp(OpLog* oplog);
   EtOperation* preProcessAttachqOp(OpLog* oplog);
   EtOperation* preProcessForkOp(OpLog* oplog);
   EtOperation* preProcessJoinOp(OpLog* oplog);
   EtOperation* preProcessThreadinitOp(OpLog* oplog);
   EtOperation* preProcessThreadexitOp(OpLog* oplog);
//   EtOperation* preProcessStartOp();
   EtOperation* preProcessRemoveEventOp(OpLog* oplog);
   EtOperation* preProcessLoopOp(OpLog* oplog);
   EtOperation* preProcessLoopExitOp(OpLog* oplog);
   EtOperation* preProcessEnableEventOp(OpLog* oplog);
   EtOperation* preProcessTriggerEventOp(OpLog* oplog);
   EtOperation* preProcessEnableActLifecycleOp(OpLog* oplog);
   EtOperation* preProcessTriggerActLifecycleOp(OpLog* oplog);
   EtOperation* preProcessTriggerReceiverOp(OpLog* oplog);
   EtOperation* preProcessTriggerServiceOp(OpLog* oplog);
   EtOperation* preProcessInstanceIntentOp(OpLog* oplog);
   EtOperation* preProcessEnableWindowFocusOp(OpLog* oplog);
   EtOperation* preProcessTriggerWindowFocusOp(OpLog* oplog);
   EtOperation* preProcessWaitOp(OpLog* oplog);
   EtOperation* preProcessTimedWaitOp(OpLog* oplog);
   EtOperation* preProcessNotifyOp(OpLog* oplog);
   EtOperation* preProcessNativePostOp(OpLog* oplog);
   EtOperation* preProcessUiPostOp(OpLog* oplog);
   EtOperation* preProcessIdlePostOp(OpLog* oplog);
   EtOperation* preProcessNativeNotifyOp(OpLog* oplog);
   EtOperation* preProcessPostFoqOp(OpLog* oplog);
   EtOperation* preProcessNativePostFoqOp(OpLog* oplog);
   EtOperation* preProcessPostNegOp(OpLog* oplog);
   EtOperation* preProcessEnableTimerTaskOp(OpLog* oplog);
   EtOperation* preProcessTriggerTimerTaskOp(OpLog* oplog);


   EtOperation* (HbUtils::*preFuncPtr [NUM_TYPES]) (OpLog* oplog);
};

}
#endif

