/*
 * Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ETALGO_H_
#define ETALGO_H_

#include <etOperation.h>
#include <eventgraph.h>
#include <etStats.h>

namespace etrack{

#define ET_BASE_ITER 0

class EtIterInfo;
class EventTrack;

class EtIterInfo{

public:

EtIterInfo(int iter, int index, int prevIterIndex, EtIterInfo* prev);

~EtIterInfo();

const int iter; //iteration ID
int index; //last processed operation
const int prevIterIndex;

EventAwareVC* evc;

//map to store VC and EG node of ops such as loop-on-q and  enable in case shouldStore = true
std::map<int, std::pair<VectorClock*, EventGraphNode*> > evcStore;

EtIterInfo* next;
EtIterInfo* prev;

};


class EventTrack{

public:

EventTrack(OpHelper* oph);

~EventTrack();

void initializeEvaluateFunctionPointers();

void computeHB();

int getEventTrackFixpointIteration();


private:
   bool isSrcFoqPostedPriorDestPost(int srcTaskId, EtTask* destTask);
   bool isSrcNegPostedPriorDestPost(int srcTaskId, EtTask* destTask);

   bool evaluatePostOp(int opId, EtOperation* op);
   bool evaluateAtTimePostOp(int opId, EtOperation* op);
   bool evaluateBeginOp(int opId, EtOperation* op);
   bool evaluateEndOp(int opId, EtOperation* op);
//   void evaluateAttachqOp(int opId, EtOperation* op);
   bool evaluateForkOp(int opId, EtOperation* op);
//   void evaluateJoinOp(EtOperation* op); //handledint opId,  by NOtify - Wait
   bool evaluateThreadinitOp(int opId, EtOperation* op);
   bool evaluateThreadexitOp(int opId, EtOperation* op);
//   void evaluateRemoveEventOp(int opId, EtOperation* op);
   bool evaluateLoopOp(int opId, EtOperation* op);
   bool evaluateLoopExitOp(int opId, EtOperation* op);
   bool evaluateEnableEventOp(int opId, EtOperation* op);
   bool evaluateTriggerEventOp(int opId, EtOperation* op);
   bool evaluateEnableActLifecycleOp(int opId, EtOperation* op);
   bool evaluateTriggerActLifecycleOp(int opId, EtOperation* op);
   bool evaluateTriggerReceiverOp(int opId, EtOperation* op);
   bool evaluateTriggerServiceOp(int opId, EtOperation* op);
//   void evaluateInstanceIntentOp(int opId, EtOperation* op);
   bool evaluateEnableWindowFocusOp(int opId, EtOperation* op);
   bool evaluateTriggerWindowFocusOp(int opId, EtOperation* op);
   bool evaluateWaitOp(int opId, EtOperation* op);
   bool evaluateNotifyOp(int opId, EtOperation* op);
//   void evaluateNativePostOp(int opId, EtOperation* op);
//   void evaluateUiPostOp(int opId, EtOperation* op);
   bool evaluateIdlePostOp(int opId, EtOperation* op);
//   void evaluateNativeNotifyOp(int opId, EtOperation* op);
   bool evaluatePostFoqOp(int opId, EtOperation* op);
//   void evaluateNativePostFoqOp(int opId, EtOperation* op);
   bool evaluatePostNegOp(int opId, EtOperation* op);
   bool evaluateEnableTimerTaskOp(int opId, EtOperation* op);
   bool evaluateTriggerTimerTaskOp(int opId, EtOperation* op);


   bool (EventTrack::*evaluateFuncPtr [NUM_TYPES]) (int opId, EtOperation* op);

   void storeHBInfo(int opId, VectorClock* vc, EventGraphNode* egn);

   //analysis state components (may not be named same as in paper. Also,
   //some info are stored in Tasks and other datastructures.)
   int fp; //holds highest ET trace processing iteration

   EtIterInfo* curFrame;

   EtIterInfo* baseFrame;

   OpHelper* oph;

   /* if a message is posted as FoQ or with negative delay to
    * a thread t then t is added to foqOrNegPostedThreads set.
    * Only if t is found in foqOrNegPostedThreads set inference rule that evaluates
    * FoQ rule should be applied. We use this simple check for all HB computation algorithms.
    */
   std::set<int> foqOrNegPostedThreads;


};

}

#endif //ETALGO_H_
