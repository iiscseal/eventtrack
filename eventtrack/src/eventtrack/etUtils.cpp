/*
 * Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <etUtils.h>

namespace etrack{

HbUtils::HbUtils(OpHelper* oph, EnvModel* appEnv) : oph(oph), appEnv(appEnv) {}

HbUtils::~HbUtils(){
    delete oph;
    delete appEnv;
}



EtOperation* HbUtils::preProcessPostOp(OpLog* oplog) {

    EtOperation* op = new PostOp(oplog, this->oph);

    return op;
}

EtOperation* HbUtils::preProcessAtTimePostOp(OpLog* oplog) {

    EtOperation* op = new AtTimePostOp(oplog, this->oph);

    return op;
}

EtOperation* HbUtils::preProcessBeginOp(OpLog* oplog) {

    EtOperation* op = new BeginOp(oplog, this->oph);

    return op;
}

EtOperation* HbUtils::preProcessEndOp(OpLog* oplog) {

    EtOperation* op = new EndOp(oplog, this->oph);

    return op;
}

EtOperation* HbUtils::preProcessAttachqOp(OpLog* oplog) {
    /* not doing anything as attachQ -> post is not followed in majority of
     * the papers & attachQ -> post is not a real causal ordering and the
     * exception can be caught & avoided
      
    EtOperation* op = new AttachqOp();

    return op;
     */
    return NULL;
}

EtOperation* HbUtils::preProcessForkOp(OpLog* oplog) {

    EtOperation* op = new ForkOp(oplog, this->oph);

    return op;
}

EtOperation* HbUtils::preProcessJoinOp(OpLog* oplog) {

    EtOperation* op = new JoinOp(oplog, this->oph);

    return op;
}

EtOperation* HbUtils::preProcessThreadinitOp(OpLog* oplog) {

    EtOperation* op = new ThreadinitOp(oplog, this->oph);

    return op;
}

EtOperation* HbUtils::preProcessThreadexitOp(OpLog* oplog) {

    EtOperation* op = new ThreadexitOp(oplog, this->oph);

    return op;
}

//   EtOperation* preProcessStartOp();
EtOperation* HbUtils::preProcessRemoveEventOp(OpLog* oplog) {

    /* nothing to be done. Corresponding post will get deleted, if
     * it has not yet been dequeued.
     *
    EtOperation* op = new RemoveEventOp(oplog, this->oph);
    */

    return NULL;
}

EtOperation* HbUtils::preProcessLoopOp(OpLog* oplog) {

    EtOperation* op = new LoopOp(oplog, this->oph);

    return op;
}

EtOperation* HbUtils::preProcessLoopExitOp(OpLog* oplog) {

    EtOperation* op = new LoopExitOp(oplog, this->oph);

    return op;
}

EtOperation* HbUtils::preProcessEnableEventOp(OpLog* oplog) {

    EtOperation* op = new EnableEventOp(oplog, this->oph);

    return op;
}

EtOperation* HbUtils::preProcessTriggerEventOp(OpLog* oplog) {

    EtOperation* op = new TriggerEventOp(oplog, this->oph);

    if(oph->addOpToMap){
        return op;
    }else{
        #ifdef DBGPREPROCESS
            op->diagnosticPrint();
        #endif
        delete op;
        return NULL;
    }
}

EtOperation* HbUtils::preProcessEnableActLifecycleOp(OpLog* oplog) {

    EtOperation* op = new EnableActLifecycleOp(oplog, this->oph);

    appEnv->addEnableLifecycleEventToMap(oph->opCount, oplog);

    //oplog->arg1 = activity state and oplog->arg2 = activity instance
    if(oplog->arg1 == ACT_RESUME && oplog->arg2 == 0){
        appEnv->blankEnableResumeOp = oph->opCount;
    }

    if(oplog->arg1 == APPBIND_DONE){
        appEnv->appBindTaskId = op->taskId;
        #ifdef DBGPREPROCESS
            printf("storing APP-BIND-DONE task %d for future reference\n", appEnv->appBindTaskId);
        #endif
    }

    return op;
}

EtOperation* HbUtils::preProcessTriggerActLifecycleOp(OpLog* oplog) {

    EtOperation* op = new TriggerActLifecycleOp(oplog, this->oph);

    bool shouldAbort = true;
    Task* triggerTask = op->task;

    std::pair<bool, std::set<int> > resultPair = 
        appEnv->connectEnableAndTriggerLifecycleEvents(oph->opCount, oplog, op->taskId, triggerTask, oph);
    if(appEnv->isEventActivityEvent(oplog->arg1)){
        bool hbInfoAdded = appEnv->checkAndUpdateActivityComponentState(oph->opCount, oplog, op->taskId, triggerTask);
        shouldAbort = !resultPair.first && !hbInfoAdded;
    }else{
        shouldAbort = !resultPair.first;
    }

    if(shouldAbort){
        PERROR("Trigger-Lifecycle event seen for component %d and state %d without a "
             "corresponding enable event or mismatch in activity state machine during processing."
             " Aborting processing.", oplog->arg2, oplog->arg1);
    }


    std::set<int>::iterator it = triggerTask->envOrderedEnableOps.begin();
    for(; it != triggerTask->envOrderedEnableOps.end(); it++){
        EnableActLifecycleOp* enOp = dynamic_cast<EnableActLifecycleOp*>(oph->opMap.find(*it)->second);
        if(enOp != NULL){
            enOp->tobeInitializedTasks.insert(op->taskId);
        }else{
            PERROR("TRIGGER_ACT_LIFECYCLE at opId %d found a non ENABLE_ACT_LIFECYCLE op with opId %d "
                   " in its task's envOrderedEnableOps set.", oph->opCount, *it);
        }
    }

    it = triggerTask->envOrderedPosts.begin();
    for(; it != triggerTask->envOrderedPosts.end(); it++){
        BasePostOp* postOp = dynamic_cast<BasePostOp*>(oph->opMap.find(*it)->second);
        if(postOp != NULL){
            postOp->tobeInitializedTasks.insert(op->taskId);
        }else{
            PERROR("TRIGGER_ACT_LIFECYCLE at opId %d found a non POST op with opId %d "
                   " in its task's envOrderedPosts set.", oph->opCount, *it);
        }
    }

    TriggerActLifecycleOp* curOp = dynamic_cast<TriggerActLifecycleOp*>(op);
    it = resultPair.second.begin();
    for(; it != resultPair.second.end(); it++){
        EnableActLifecycleOp* enOp = dynamic_cast<EnableActLifecycleOp*>(oph->opMap.find(*it)->second);
        if(enOp != NULL){
            enOp->shouldStore = true;
            curOp->envOrderedEnableOps.insert(std::make_pair(*it, enOp->taskId)); 
        }else{
            PERROR("TRIGGER_ACT_LIFECYCLE at opId %d found a non ENABLE_ACT_LIFECYCLE op with opId %d "
                   " in its envOrderedEnableOps set.", oph->opCount, *it);
        }
    }

    #ifdef DBGPREPROCESS
        triggerTask->diagnosticPrint(op->taskId);
    #endif

    triggerTask->envOrderedEnableOps.clear();
    triggerTask->envOrderedPosts.clear();


    return op;
}

EtOperation* HbUtils::preProcessTriggerReceiverOp(OpLog* oplog) {

    if(oplog->arg1 == REC_TRIGGER_ONRECIEVE_LATER){
        return NULL;
    }

    EtOperation* op = new TriggerReceiverOp(oplog, this->oph);

    EtTask* triggerTask = op->task;

    bool receiverUpdated = appEnv->checkAndUpdateBroadcastState(oph->opCount, oplog, op->taskId, triggerTask);
    bool shouldAbort = !receiverUpdated;

    if(shouldAbort){
        PERROR("TRIGGER_RECEIVER at opId %d with state %d and instance %d "
               "has encountered a state machine error.", oph->opCount, oplog->arg1, oplog->arg2);
    }


    std::set<int>::iterator it = triggerTask->envOrderedEnableOps.begin();
    for(; it != triggerTask->envOrderedEnableOps.end(); it++){
        TriggerReceiverOp* enOp = dynamic_cast<TriggerReceiverOp*>(oph->opMap.find(*it)->second);
        if(enOp != NULL){
            enOp->tobeInitializedTasks.insert(op->taskId);
        }else{
            PERROR("TRIGGER_RECEIVER at opId %d found a non TRIGGER_RECEIVER op with opId %d "
                   " in its task's envOrderedEnableOps set.", oph->opCount, *it);
        }
    }

    it = triggerTask->envOrderedPosts.begin();
    for(; it != triggerTask->envOrderedPosts.end(); it++){
        BasePostOp* postOp = dynamic_cast<BasePostOp*>(oph->opMap.find(*it)->second);
        if(postOp != NULL){
            postOp->tobeInitializedTasks.insert(op->taskId);
        }else{
            PERROR("TRIGGER_RECEIVER at opId %d found a non POST op with opId %d "
                   " in its task's envOrderedPosts set.", oph->opCount, *it);
        }
    }

    std::map<int, int>::iterator recIt = appEnv->onRecTaskToEnableOpMap.find(op->taskId);
    if(recIt != appEnv->onRecTaskToEnableOpMap.end()){
        TriggerReceiverOp* recOp = dynamic_cast<TriggerReceiverOp*>(oph->opMap.find(recIt->second)->second);
        if(recOp->state == REC_SEND_BROADCAST || recOp->state == REC_SEND_STICKY_BROADCAST ||
                recOp->state == REC_REGISTER_RECEIVER){
            recOp->emitEdges.insert(triggerTask->id);
            #ifdef DBGPREPROCESS
                printf("TRIGGER-RECEIVER: emitEdge enabled on op %d for onReceive task %d in state %d \n", 
                       recIt->second, triggerTask->id, recOp->state);
            #endif
            appEnv->onRecTaskToEnableOpMap.erase(recIt);
        }else{
            PERROR("A TRIGGER-RECEIVER state of type %d wrongly added to onRecTaskToEnableOpMap.", recOp->state);
        }
    }

    if(oplog->arg1 == REC_TRIGGER_ONRECIEVE){
        BeginOp* beginop = dynamic_cast<BeginOp*>(oph->opMap.find(triggerTask->beginOpId)->second);
        beginop->isOnReceive = true;
    }
    
    #ifdef DBGPREPROCESS
        triggerTask->diagnosticPrint(op->taskId);
    #endif

    triggerTask->envOrderedEnableOps.clear();
    triggerTask->envOrderedPosts.clear();

    return op;
}

EtOperation* HbUtils::preProcessTriggerServiceOp(OpLog* oplog) {

    EtOperation* op = new TriggerServiceOp(oplog, oph);

    Task* triggerTask = op->task;

    bool serviceUpdated = appEnv->checkAndUpdateServiceState(oph->opCount, oplog, op->taskId, triggerTask, this->oph);
    bool shouldAbort = !serviceUpdated;

    if(shouldAbort){
        PERROR("TRIGGER_SERVICE at opId %d with state %d and instance/intent/connection-key %d "
               "has encountered a state machine error.", oph->opCount, oplog->arg1, oplog->arg2);
    }

    std::set<int>::iterator it = triggerTask->envOrderedEnableOps.begin();
    for(; it != triggerTask->envOrderedEnableOps.end(); it++){
        TriggerServiceOp* enOp = dynamic_cast<TriggerServiceOp*>(oph->opMap.find(*it)->second);
        if(enOp != NULL){
            enOp->tobeInitializedTasks.insert(op->taskId);
        }else{
            PERROR("TRIGGER_SERVICE at opId %d found a non TRIGGER_SERVICE op with opId %d "
                   " in its task's envOrderedEnableOps set.", oph->opCount, *it);
        }
    }

    it = triggerTask->envOrderedPosts.begin();
    for(; it != triggerTask->envOrderedPosts.end(); it++){
        BasePostOp* postOp = dynamic_cast<BasePostOp*>(oph->opMap.find(*it)->second);
        if(postOp != NULL){
            postOp->tobeInitializedTasks.insert(op->taskId);
        }else{
            PERROR("TRIGGER_SERVICE at opId %d found a non POST op with opId %d "
                   " in its task's envOrderedPosts set.", oph->opCount, *it);
        }
    }

    #ifdef DBGPREPROCESS
    triggerTask->diagnosticPrint(op->taskId);
    #endif

    triggerTask->envOrderedEnableOps.clear();
    triggerTask->envOrderedPosts.clear();

    return op;
}

EtOperation* HbUtils::preProcessInstanceIntentOp(OpLog* oplog) {

    // change this to return null after plugging in instance for intent in concerned maps
    appEnv->mapInstanceWithIntentId(oplog->arg2, oplog->arg1);

    return NULL;
}

EtOperation* HbUtils::preProcessEnableWindowFocusOp(OpLog* oplog) {

    EtOperation* op = new EnableWindowFocusOp(oplog, this->oph);

    return op;
}

EtOperation* HbUtils::preProcessTriggerWindowFocusOp(OpLog* oplog) {

    //if the enable is on the same thread then after setting up HB taskIds to take join with
    //can remove both these ops. If they are on different threads then indicate that corresponding
    //enable needs to be stored.
    EtOperation* op = new TriggerWindowFocusOp(oplog, this->oph);

    if(oph->addOpToMap){
        return op;
    }else{
        #ifdef DBGPREPROCESS
            op->diagnosticPrint();
        #endif
        delete op;
        return NULL;
    }
}

EtOperation* HbUtils::preProcessWaitOp(OpLog* oplog) {

    EtOperation* op = new WaitOp(oplog, this->oph);

    return op;
}

EtOperation* HbUtils::preProcessTimedWaitOp(OpLog* oplog) {
    //currently TIMED_WAIT is a NOP because it does not result in any HB to be added
    //because it is kind of like sleep. Even if it is woken by a NOTIFY that may not be 
    //the case in some other run.
    return NULL;
}

EtOperation* HbUtils::preProcessNotifyOp(OpLog* oplog) {

    EtOperation* op = new NotifyOp(oplog, this->oph);

    return op;
}

EtOperation* HbUtils::preProcessNativePostOp(OpLog* oplog) {

    /* currently we handle post by native thread similar to post by
     * a tracked app thread. Only difference is in this case we will
     * not know the source task of the operation, which is alright.
     * If this is an environment related post enable - trigger or
     * state machine modeling will plug in the missing HB info.
     * Otherwise, nothing much can be done. But we wont quit
     * because of this.
     */
    oplog->opType = DR_POST;
    EtOperation* op = new PostOp(oplog, this->oph);

    return op;
}

EtOperation* HbUtils::preProcessUiPostOp(OpLog* oplog) {

    EtOperation* op = new UiPostOp();

    return op;
}

EtOperation* HbUtils::preProcessIdlePostOp(OpLog* oplog) {

    EtOperation* op = new IdlePostOp(oplog, this->oph);

    return op;
}

EtOperation* HbUtils::preProcessNativeNotifyOp(OpLog* oplog) {

    /* currently we handle notify executed by any  native thread 
     * to wake up a tracked thread, similar to notify by
     * a tracked app thread. Only difference is in this case we will
     * not know the source task of the operation, which is alright.
     * Also, we allow this only if tid = -1 & not if tid is known
     * but is executed outside a task.
     */
    oplog->opType = DR_NOTIFY;
    EtOperation* op = new NotifyOp(oplog, this->oph);

    return op;
}

EtOperation* HbUtils::preProcessPostFoqOp(OpLog* oplog) {

    EtOperation* op = new PostFoqOp(oplog, this->oph);

    return op;
}

EtOperation* HbUtils::preProcessNativePostFoqOp(OpLog* oplog) {

    /*Handled same as POST_FOQ*/
    EtOperation* op = new PostFoqOp(oplog, this->oph);

    return op;
}

EtOperation* HbUtils::preProcessPostNegOp(OpLog* oplog) {

    EtOperation* op = new PostNegOp(oplog, this->oph);

    return op;
}

EtOperation* HbUtils::preProcessEnableTimerTaskOp(OpLog* oplog) {

    EtOperation* op = new EnableTimerTaskOp(oplog, this->oph);

    return op;
}

EtOperation* HbUtils::preProcessTriggerTimerTaskOp(OpLog* oplog) {

    EtOperation* op = new TriggerTimerTaskOp(oplog, this->oph);

    return op;
}



void HbUtils::initializeFunctionPointers(){
    //initialize preprocessing functions (used in preprocessing pass)
    preFuncPtr[DR_POST] = &HbUtils::preProcessPostOp;
    preFuncPtr[DR_AT_TIME_POST] = &HbUtils::preProcessAtTimePostOp;
    preFuncPtr[DR_BEGIN] = &HbUtils::preProcessBeginOp;
    preFuncPtr[DR_END] = &HbUtils::preProcessEndOp;
    preFuncPtr[DR_ATTACH_Q] = &HbUtils::preProcessAttachqOp;
    preFuncPtr[DR_FORK] = &HbUtils::preProcessForkOp;
    preFuncPtr[DR_JOIN] = &HbUtils::preProcessJoinOp;
    preFuncPtr[DR_THREADINIT] = &HbUtils::preProcessThreadinitOp;
    preFuncPtr[DR_THREADEXIT] = &HbUtils::preProcessThreadexitOp; 
//    preFuncPtr[DR_START] = &HbUtils::preProcessStartOp; 
    preFuncPtr[DR_REMOVE_EVENT] = &HbUtils::preProcessRemoveEventOp; 
    preFuncPtr[DR_LOOP] = &HbUtils::preProcessLoopOp; 
    preFuncPtr[DR_LOOP_EXIT] = &HbUtils::preProcessLoopExitOp; 
    preFuncPtr[DR_ENABLE_EVENT] = &HbUtils::preProcessEnableEventOp; 
    preFuncPtr[DR_TRIGGER_EVENT] = &HbUtils::preProcessTriggerEventOp; 
    preFuncPtr[DR_ENABLE_LIFECYCLE] = &HbUtils::preProcessEnableActLifecycleOp; 
    preFuncPtr[DR_TRIGGER_LIFECYCLE] = &HbUtils::preProcessTriggerActLifecycleOp; 
    preFuncPtr[DR_TRIGGER_RECEIVER] = &HbUtils::preProcessTriggerReceiverOp; 
    preFuncPtr[DR_TRIGGER_SERVICE] = &HbUtils::preProcessTriggerServiceOp; 
    preFuncPtr[DR_INSTANCE_INTENT] = &HbUtils::preProcessInstanceIntentOp; 
    preFuncPtr[DR_ENABLE_WINDOW_FOCUS] = &HbUtils::preProcessEnableWindowFocusOp; 
    preFuncPtr[DR_TRIGGER_WINDOW_FOCUS] = &HbUtils::preProcessTriggerWindowFocusOp; 
    preFuncPtr[DR_WAIT] = &HbUtils::preProcessWaitOp; 
    preFuncPtr[DR_TIMED_WAIT] = &HbUtils::preProcessTimedWaitOp; 
    preFuncPtr[DR_NOTIFY] = &HbUtils::preProcessNotifyOp; 
    preFuncPtr[DR_NATIVE_POST] = &HbUtils::preProcessNativePostOp; 
    preFuncPtr[DR_UI_POST] = &HbUtils::preProcessUiPostOp; 
    preFuncPtr[DR_IDLE_POST] = &HbUtils::preProcessIdlePostOp; 
    preFuncPtr[DR_NATIVE_NOTIFY] = &HbUtils::preProcessNativeNotifyOp; 
    preFuncPtr[DR_POST_FOQ] = &HbUtils::preProcessPostFoqOp; 
    preFuncPtr[DR_NATIVE_POST_FOQ] = &HbUtils::preProcessNativePostFoqOp; 
    preFuncPtr[DR_POST_NEG] = &HbUtils::preProcessPostNegOp; 
    preFuncPtr[DR_ENABLE_TIMER_TASK] = &HbUtils::preProcessEnableTimerTaskOp; 
    preFuncPtr[DR_TRIGGER_TIMER_TASK] = &HbUtils::preProcessTriggerTimerTaskOp; 


  
    //initialize functions used when computing HB

}


void HbUtils::initializePreProcess(int mainThreadId){
    int mainTaskId = oph->taskCount++;

    EtTask* mainTask = new EtTask(mainTaskId, mainThreadId, 0); //0 indicates no associated event
    oph->taskMap.insert(std::make_pair(mainTaskId, mainTask));

    EtThread* thread = new EtThread(mainTaskId, mainTask);
    oph->threadMap.insert(std::make_pair(mainThreadId,thread));

}


void HbUtils::preProcess(OpLog* oplog){
    oph->addOpToMap = true;
    EtOperation* op = (this->*preFuncPtr[oplog->opType])(oplog);
    if(op != NULL){
        #ifdef DBGPREPROCESS
            printf("\nopid %d  ", oph->opCount);
            op->diagnosticPrint();
        #endif
        oph->opMap.insert(std::make_pair(oph->opCount++,op));
    }
}


void HbUtils::emitEndOpsManually(){
    std::map<u4, std::pair<int, int> >::iterator it = oph->onlyBeginSeenMap.begin();
    for(; it != oph->onlyBeginSeenMap.end(); it++){
        #ifdef DBGPREPROCESS
            printf("emitting END operation for task:%d, tid:%d event:%u", 
                    it->second.second, it->second.first, it->first);
        #endif
        /* tid = it->second.first,  taskId = it->second.second,  event = it->first */
        EndOp* op = new EndOp(it->second.first, it->second.second, it->first);
        op->task = dynamic_cast<EtTask*>(oph->taskMap.find(it->second.second)->second);
        oph->opMap.insert(std::make_pair(oph->opCount++,op));
    }
    oph->onlyBeginSeenMap.clear();
}

void HbUtils::printHbComputationStats(EventTrack* et){
    printf("\nTrace length after pre-processing: %lu", oph->opMap.size());
    printf("\nNo. of threads: %lu", oph->threadMap.size());
    printf("\nNo. of events: %u", Stats::eventCount);
    printf("\nNo. of event-loops: %u", Stats::eventLoopCount);
    printf("\nNo. of chains: %lu", oph->curClockOnChains->getVCSize());
    printf("\n\n#EventGraph accessed/manipulations performed: %u", Stats::graphComputationCount);
    printf("\n#incoming edges from the same thread across all tasks (sets of candidate events): %u", Stats::incomingEdgesCount);
    printf("\n#incoming edges (events) identified for ordering across all tasks: %u", Stats::tobeOrderedIncomingEdgesCount);
    printf("\n#Events actually ordered (event-covering sets): %u", Stats::sparseJoinedTaskCount);
    printf("\n#EventTrack iterations taken to reach fixpoint: %d", et->getEventTrackFixpointIteration());
    printf("\n");
    printf("\nSize related stats:");
    printf("\n#edges added to EventGraph: %u", Stats::egEdgeCount);
    printf("\n#edges deleted from EventGraph: %u", Stats::egDeletedEdgeCount);
    printf("\nEstimated maximum size of EventGraph if edges were never deletd: %lu bytes", Stats::egEdgeCount * sizeof(EtList));
    printf("\nTotal memory used by all VCs: %lu bytes", Stats::vcMemory);
    printf("\nMemory allocated for tasks: %lu bytes", oph->taskMap.size() * sizeof(EtTask));
    
 /*   #ifdef DBG_COLLECT_STATS
        u8 msgSquare = 0;
        for(OpHelper::threadMapIterator it = oph->threadMap.begin(); it != oph->threadMap.end(); it++){
            u4 msgCount = dynamic_cast<EtThread*>(it->second)->msgCount;
            msgSquare += (msgCount * (msgCount-1))/2;
        }
        printf("\nO(msg^2): %lu", msgSquare);
        printf("\nO(msg^2)/egCost: %lu", msgSquare/Stats::graphComputationCount);
    #endif*/
}

void HbUtils::computeHBForTrace(){
    #ifdef DBG_COLLECT_STATS
        Stats::initializeHbComputationStats();
    #endif

    EventTrack* et = new EventTrack(this->oph);
    et->computeHB();

    #ifdef DBG_COLLECT_STATS
        printHbComputationStats(et);
    #endif
}

}

