/*
 * Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ETOPERATION_H_
#define ETOPERATION_H_

//#include "../commonHeaders/operation.h"
#include <operation.h>
#include <operationHelper.h>

namespace etrack{

class EtTask;
class EtThread;
class EtTask;
class EtOperation;
class BasePostOp; 
class PostOp;
class AtTimePostOp;
class BeginOp;
class EndOp;
class AttachqOp;
class ForkOp;
class JoinOp;
class ThreadinitOp;
class ThreadexitOp;
//class StartOp;
class RemoveEventOp;
class LoopOp;
class LoopExitOp;
class EnableEventOp;
class TriggerEventOp;
class EnableActLifecycleOp;
class TriggerActLifecycleOp;
class TriggerReceiverOp;
class TriggerServiceOp;
class InstanceIntentOp;
class EnableWindowFocusOp;
class TriggerWindowFocusOp;
class WaitOp;
class TimedWaitOp;
class NotifyOp;
class NativePostOp;
class UiPostOp;
class IdlePostOp;
class NativeNotifyOp;
class PostFoqOp;
class NativePostFoqOp;
class PostNegOp;
class EnableTimerTaskOp;
class TriggerTimerTaskOp;


class EtTask : public Task{

public:

EtTask(int id, int tid, int eventId);

~EtTask();

std::pair<int,u4> getEpoch();

/* list of tasks on the same thread w.r.t. which this task has already been ordered
 * due to HB rule.
 */
std::set<EtTask*> joinedWrtTasks;

int id; //taskId - key used in taskMap

BasePostOp* postOp;

/* updated only in the first iteration of algo.
 * Reflects current clock value of the task.
 */
u4 epoch;
int mod; //most recent EventTrack iteration which modified this task

};


class EtThread : public ThreadClass{

public:

EtThread(int curTaskId, EtTask* curTask);
virtual ~EtThread();

EtTask* curTask;

#ifdef DBG_COLLECT_STATS
u4 msgCount;
#endif

};

class EtOperation : public Operation{

public:

EtOperation();

EtOperation(int tid, int taskId);

virtual ~EtOperation(){}

virtual void diagnosticPrint(){
    Operation::diagnosticPrint();
    printf("hbSink:%p \n", hbSinkTask);
}

virtual void setHbSourceSink(){
    hbSinkTask = task;
}

virtual int getTypeId(){
    return DR_HB_OP;
}

EtTask* task;

/* source and sink as given in the Algorithm EventTrack.
 * These can be computed in the HB computation pass itself. However, we
 * fill this info in pre-processing run as we are anyways iterating over
 * the entire trace in pre-processing run, to identify environment modeling
 * related HB info.
 */
EtTask* hbSinkTask;

};


//base class for all kinds of post operation
class BasePostOp : public EtOperation{

public:

BasePostOp();
virtual ~BasePostOp();

virtual int getTypeId(){
    return DR_BASE_POST;
}

u4 event;
int dest; //destination threadId
int typeOfPost;

//only contains tasks posted by native threads
std::set<int> tobeInitializedTasks;

virtual void diagnosticPrint(){
    EtOperation::diagnosticPrint();
    printf("msg:%u, destination thread:%d, typeOfPost:%d \n", event, dest, typeOfPost);
}

};

class PostOp : public BasePostOp {

public:

PostOp(OpLog* oplog, OpHelper* oph);

~PostOp();

virtual int getTypeId(){
    return DR_POST;
}

virtual void diagnosticPrint(){
    printf("POST: \n");
    BasePostOp::diagnosticPrint();
    printf("delay:%u\n", delay);
}

int delay;

};

class AtTimePostOp : public BasePostOp {

public:

AtTimePostOp(OpLog* oplog, OpHelper* oph);

~AtTimePostOp();

virtual int getTypeId(){
    return DR_AT_TIME_POST;
}

virtual void diagnosticPrint(){
    printf("POST-AT-TIME: \n");
    BasePostOp::diagnosticPrint();
    printf("delay:%u\n", delay);
}

int delay;

};

class BeginOp : public EtOperation{

public:

BeginOp(OpLog* oplog, OpHelper* oph);

~BeginOp();

virtual int getTypeId(){
    return DR_BEGIN;
}

virtual void diagnosticPrint(){
    printf("BEGIN:\n");
    EtOperation::diagnosticPrint();
    printf("event:%u \n", event);
}

/* taskId should be sufficient. If you need event uncomment this and remove
 * declaration of event in preProcessBegin.
 */
u4 event;

bool isOnReceive;

};

class EndOp : public EtOperation{

public:

EndOp(OpLog* oplog, OpHelper* oph);

EndOp(int tid, int taskId, u4 event);

virtual int getTypeId(){
    return DR_END;
}

virtual void diagnosticPrint(){
    printf("END:\n");
    EtOperation::diagnosticPrint();
    printf("event:%u \n", event);
}

~EndOp();

//taskId should be sufficient
u4 event;

};

class AttachqOp : public EtOperation{

public:
AttachqOp();
~AttachqOp();

virtual int getTypeId(){
    return DR_ATTACH_Q;
}

};

class ForkOp : public EtOperation{

public:

ForkOp(OpLog* oplog, OpHelper* oph);

~ForkOp();

virtual int getTypeId(){
    return DR_FORK;
}

virtual void diagnosticPrint(){
    printf("FORK:\n");
    EtOperation::diagnosticPrint();
    printf("childTid:%d \n", childTid);
}

int childTid;

};

class JoinOp : public EtOperation{

public:

//We currently do not log Join as it is taken care by NOTIFY-WAIT
//Join is implemented internally using notify-wait
JoinOp(OpLog* oplog, OpHelper* oph);

~JoinOp();

virtual int getTypeId(){
    return DR_JOIN;
}

virtual void diagnosticPrint(){
    printf("JOIN:\n");
    EtOperation::diagnosticPrint();
    printf("joinThread:%d \n", joinThread);
}

int joinThread; //thread for which current thread is waiting to finish

};

class ThreadinitOp : public EtOperation{

public:

ThreadinitOp(OpLog* oplog, OpHelper* oph);

~ThreadinitOp();

virtual int getTypeId(){
    return DR_THREADINIT;
}

virtual void diagnosticPrint(){
    printf("THREADINIT:\n");
    EtOperation::diagnosticPrint();
}

};

class ThreadexitOp : public EtOperation{

public: 

ThreadexitOp(OpLog* oplog, OpHelper* oph);

~ThreadexitOp();

virtual int getTypeId(){
    return DR_THREADEXIT;
}

virtual void diagnosticPrint(){
    printf("THREADEXIT:\n");
    EtOperation::diagnosticPrint();
}

};

class RemoveEventOp : public EtOperation{

};

class LoopOp : public EtOperation{

public:

LoopOp(OpLog* oplog, OpHelper* oph);

~LoopOp();

virtual int getTypeId(){
    return DR_LOOP;
}

virtual void diagnosticPrint(){
    printf("LOOP-On-Q:\n");
    EtOperation::diagnosticPrint();
    printf("event-queue:%u \n", eventqueue);
}

u4 eventqueue;

};


class LoopExitOp : public EtOperation{

public:

LoopExitOp(OpLog* oplog, OpHelper* oph);

~LoopExitOp();

virtual int getTypeId(){
    return DR_LOOP_EXIT;
}

virtual void diagnosticPrint(){
    printf("LOOP-EXIT:\n");
    EtOperation::diagnosticPrint();
    printf("event-queue:%u \n", eventqueue);
    printf("Tasks executed on this thread prior to this loop exit:\n");
    for(std::set<int>::iterator setIt = hbSrcSet.begin(); setIt != hbSrcSet.end(); setIt++){
        printf("%d,  ", (*setIt));
    }
    printf("\n");
}

u4 eventqueue;
std::set<int> hbSrcSet;

};


class EnableEventOp : public EtOperation{

public:

EnableEventOp(OpLog* oplog, OpHelper* oph);

~EnableEventOp();

virtual int getTypeId(){
    return DR_ENABLE_EVENT;
}

virtual void diagnosticPrint(){
    printf("ENABLE_EVENT:\n");
    EtOperation::diagnosticPrint();
    printf("view:%u, uiEvent:%d \n", view, uiEvent);
}


int uiEvent; //stores event code which will be used for matching
u4 view; //hashcode of UI view on which event will be triggered
/* below field is true if this operation's HB info should be 
 * stored for a future op to use. 
 */
bool shouldStore;

/* a set of tasks whose post will have HB with this enable operation.
 * Hence, HB info of these tasks get initialized when processing this operation.
 * This is done due to the way DroidRacer models lifecycle callbacks and environment events.
 * Only contains tasks posted by native threads or outside event handlers
 */
std::set<int> tobeInitializedTasks;

};

class TriggerEventOp : public EtOperation{

public:

TriggerEventOp(OpLog* oplog, OpHelper* oph);

~TriggerEventOp();

virtual int getTypeId(){
    return DR_TRIGGER_EVENT;
}

virtual void diagnosticPrint(){
    printf("TRIGGER_EVENT:\n");
    EtOperation::diagnosticPrint();
    printf("view:%u, uiEvent:%d \n", view, uiEvent);
}

int uiEvent; //stores event code which will be used for matching
u4 view; //hashcode of UI view on which event will be triggered
int enableOpId;
EtTask* hbSrcTask;

};

class EnableActLifecycleOp : public EtOperation{

public:

EnableActLifecycleOp(OpLog* olog, OpHelper* oph);

~EnableActLifecycleOp();

virtual int getTypeId(){
    return DR_ENABLE_LIFECYCLE;
}

virtual void diagnosticPrint(){
    printf("ENABLE_LIFECYCLE:\n");
    EtOperation::diagnosticPrint();
    printf("activity instance:%u, state:%d \n", instance, state);
    printf("Tasks which will be initialized by this operation:\n");
    std::set<int>::iterator it = tobeInitializedTasks.begin();
    for(; it != tobeInitializedTasks.end(); it++){
        printf(" %d,", *it);
    }
    printf("\n");
}

int state;
u4 instance;
/* below field is true if this operation's HB info should be 
 * stored for a future op to use. 
 */
bool shouldStore;

std::set<int> tobeInitializedTasks;

};

class TriggerActLifecycleOp : public EtOperation{

public:

TriggerActLifecycleOp(OpLog* oplog, OpHelper* oph);

~TriggerActLifecycleOp();

virtual int getTypeId(){
    return DR_TRIGGER_LIFECYCLE;
}

virtual void diagnosticPrint(){
    printf("TRIGGER_LIFECYCLE:\n");
    EtOperation::diagnosticPrint();
    printf("activity instance:%u, state:%d \n", instance, state);
    printf("ENABLE_LIFECYCLE <operation, task> which have direct HB with this operation:\n");
    std::map<int,int>::iterator it = envOrderedEnableOps.begin();
    for(; it != envOrderedEnableOps.end(); it++){ 
        printf(" <%d, %d>,", it->first, it->second);
    }
    printf("\n");

}

/* map<enableOpId, enableOp taskId> 
 * need to add edge from these enable ops to this trigger operation directly.
 * Hence you will have to set shouldStore of these operations to TRUE.
 * We need enableOp's taskId to add edges into EventGraph.
 */
std::map<int,int> envOrderedEnableOps;

int state;
u4 instance;

};

class TriggerReceiverOp : public EtOperation{

public:

TriggerReceiverOp(OpLog* oplog, OpHelper* oph);

~TriggerReceiverOp();

virtual int getTypeId(){
    return DR_TRIGGER_RECEIVER;
}

virtual void diagnosticPrint(){
    printf("TRIGGER_RECEIVER:\n");
    EtOperation::diagnosticPrint();
    printf("broadcast reciever actionKey:%d, identifier:%u, intent:%d, state:%d \n", actionKey, identifier, intentId, state);
    printf("Tasks of ON_RECEIVE event handler which will be initialzed by this operation:\n");
    std::set<int>::iterator it = tobeInitializedTasks.begin();
    for(; it != tobeInitializedTasks.end(); it++){
        printf(" %d,", *it);
    }
    printf("\n");
}

//only contains tasks posted by native threads
std::set<int> tobeInitializedTasks;

int state;
u4 identifier;
int intentId;
int actionKey;
/* indicates if an EventGraph edge needs to be emitted on processing this operation with TAG
 * KNOWN_BROADCAST so that derived HB rules related to Receivers can be applied.
 * holds set of taskIds of onReceiver handler which will be the source node of the KNOWN_BROADCAST edge.
 * We store taskIds instead of tasks since we will be inserting edges sorted based on task IDs.
 */
std::set<int> emitEdges; 

};

class TriggerServiceOp : public EtOperation{

public:

TriggerServiceOp(OpLog* oplog, OpHelper* oph);

~TriggerServiceOp();

virtual int getTypeId(){
    return DR_TRIGGER_SERVICE;
}

virtual void diagnosticPrint(){
    printf("TRIGGER_SERVICE:\n");
    EtOperation::diagnosticPrint();
    printf("service classKey:%d, identifier:%u, state:%d \n", serviceClassKey, identifier, state);
    printf("Tasks of TRIGGER_SERVICE operations which will be initialzed by this operation:\n");
    std::set<int>::iterator it = tobeInitializedTasks.begin();
    for(; it != tobeInitializedTasks.end(); it++){ 
        printf(" %d,", *it);
    }
    printf("\n");
}

//only contains tasks posted by native threads
std::set<int> tobeInitializedTasks;

int state;
u4 identifier;
int serviceClassKey;

};

class InstanceIntentOp : public EtOperation{

public:

~InstanceIntentOp();

virtual int getTypeId(){
    return DR_INSTANCE_INTENT;
}

};

class EnableWindowFocusOp : public EtOperation{

public:

EnableWindowFocusOp(OpLog* oplog, OpHelper* oph);

~EnableWindowFocusOp();

virtual int getTypeId(){
    return DR_ENABLE_WINDOW_FOCUS;
}

virtual void diagnosticPrint(){
    printf("ENABLE_WINDOW_FOCUS:\n");
    EtOperation::diagnosticPrint();
    printf("window:%u \n", windowId);
    printf("Trigger-window event handlers initialized by this enable operation:\n");
    std::set<int>::iterator it = tobeInitializedTasks.begin();
    for(; it != tobeInitializedTasks.end(); it++){
        printf(" %d,", *it);
    }
    printf("\n");

}

u4 windowId; //window whose focus will be changed
/* below field is true if this operation's HB info should be 
 * stored for a future op to use. 
 */
bool shouldStore;

//this is empty if post of trigger-window focus is executed prior to enable-window focus
std::set<int> tobeInitializedTasks;

};

class TriggerWindowFocusOp : public EtOperation{

public:

TriggerWindowFocusOp(OpLog* oplog, OpHelper* oph);

~TriggerWindowFocusOp();

virtual int getTypeId(){
    return DR_TRIGGER_WINDOW_FOCUS;
}

virtual void diagnosticPrint(){
    printf("TRIGGER_WINDOW_FOCUS:\n");
    EtOperation::diagnosticPrint();
    printf("window:%u, enableOpId:%d \n", windowId, enableOpId);
}

int enableOpId; //this is set only if enable & trigger are on different threads

u4 windowId; //window whose focus is changed

EtTask* hbSrcTask;

};

class WaitOp : public EtOperation{

public:

WaitOp(OpLog* oplog, OpHelper* oph);

~WaitOp();

virtual int getTypeId(){
    return DR_WAIT;
}

virtual void diagnosticPrint(){
    printf("WAIT:\n");
    EtOperation::diagnosticPrint();
}
/* in a future implementation we will track object on which
 * wait is performed, so as to more accurately match with notify.
 * However, have not seen any problem with our current way of tracking.
 */

};

class TimedWaitOp : public EtOperation{

public:

TimedWaitOp(OpLog* oplog, OpHelper* oph);

~TimedWaitOp();

virtual int getTypeId(){
    return DR_TIMED_WAIT;
}

virtual void diagnosticPrint(){
    printf("TIMED_WAIT:\n");
    EtOperation::diagnosticPrint();
}

};

class NotifyOp : public EtOperation{

public:

NotifyOp(OpLog* oplog, OpHelper* oph);

~NotifyOp();

virtual int getTypeId(){
    return DR_NOTIFY;
}

int notifiedTid;

virtual void diagnosticPrint(){
    printf("NOTIFY:\n");
    EtOperation::diagnosticPrint();
    printf("notifiedTid: %d\n", notifiedTid);
}

};

class NativePostOp : public EtOperation{

};

class UiPostOp : public EtOperation{

};

class IdlePostOp : public BasePostOp{

public:

IdlePostOp(OpLog* oplog, OpHelper* oph);

~IdlePostOp();

virtual int getTypeId(){
    return DR_IDLE_POST;
}

virtual void diagnosticPrint(){
    printf("IDLE-POST:\n");
    BasePostOp::diagnosticPrint();
}

};

class NativeNotifyOp : public EtOperation{

};

class PostFoqOp : public BasePostOp{

public:

PostFoqOp(OpLog* oplog, OpHelper* oph);

~PostFoqOp();

virtual int getTypeId(){
    return DR_POST_FOQ;
}

virtual void diagnosticPrint(){
    printf("POST-FOQ:\n");
    BasePostOp::diagnosticPrint();
    printf("Direct Causal FoQ-Posts: ");
    std::set<int>::iterator it = causalFoQPosts.begin();
    for(; it != causalFoQPosts.end(); it++){
        printf("%d, ",*it);
    }
    printf("\n");
    printf("Direct Causal Neg-Posts: ");
    it = causalNegPosts.begin();
    for(; it != causalNegPosts.end(); it++){
        printf("%d, ",*it);
    }
    printf("\n");
}

/* operatin IDs of other FoQ or Neg posts to the same thread
 * which this FoQ post is aware of. EventTrack only stores
 * those posts about which the current operation knows that
 * the events have not yet been dequeued. Refer Algorithm.
 */
std::set<int> causalFoQPosts;
std::set<int> causalNegPosts;

};

class NativePostFoqOp : public BasePostOp{

};

class PostNegOp : public BasePostOp{

public:

PostNegOp(OpLog* oplog, OpHelper* oph);

~PostNegOp();

virtual int getTypeId(){
    return DR_POST_NEG;
}

virtual void diagnosticPrint(){
    printf("NEG-POST:\n");
    BasePostOp::diagnosticPrint();
    printf("delay:%d \n", delay);
    printf("Direct Causal FoQ-Posts: ");
    std::set<int>::iterator it = causalFoQPosts.begin();
    for(; it != causalFoQPosts.end(); it++){
        printf("%d, ",*it);
    }
    printf("\n");
    printf("Direct Causal Neg-Posts: ");
    it = causalNegPosts.begin();
    for(; it != causalNegPosts.end(); it++){
        printf("%d, ",*it);
    }
    printf("\n");
}


/* we do not really use this field. This will be useful if we do observe
 * postAtTime posts with NEG time specified. Then we can use this field to
 * apply more fine grained event ordering rules.
 */
int delay;

/* operatin IDs of other FoQ or Neg posts to the same thread
 * which this Neg post is aware of. EventTrack only stores
 * those posts about which the current operation knows that
 * the events have not yet been dequeued. Refer Algorithm.
 */
std::set<int> causalFoQPosts;
std::set<int> causalNegPosts;

};

class EnableTimerTaskOp : public EtOperation{

public:

EnableTimerTaskOp(OpLog* oplog, OpHelper* oph);

~EnableTimerTaskOp();

virtual int getTypeId(){
    return DR_ENABLE_TIMER_TASK;
}

virtual void diagnosticPrint(){
    printf("ENABLE_TIMER_TASK:\n");
    EtOperation::diagnosticPrint();
    printf("instance:%u \n", instance);
}

u4 instance;
/* below field is true if this operation's HB info should be 
 * stored for a future op to use. 
 */
bool shouldStore;

};

class TriggerTimerTaskOp : public EtOperation{

public:

TriggerTimerTaskOp(OpLog* oplog, OpHelper* oph);

~TriggerTimerTaskOp();

virtual int getTypeId(){
    return DR_TRIGGER_TIMER_TASK;
}

virtual void diagnosticPrint(){
    printf("TRIGGER_TIMER_TASK:\n");
    EtOperation::diagnosticPrint();
    printf("instance:%u, enableOpId:%d \n", instance, enableOpId);
}

u4 instance;

int enableOpId; 

EtTask* hbSrcTask;

};



}
#endif  // ETOPERATION_H_

