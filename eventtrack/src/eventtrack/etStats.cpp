/*
 * Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <etStats.h>

namespace etrack{

u4 Stats::incomingEdgesCount;

u4 Stats::tobeOrderedIncomingEdgesCount;

u4 Stats::sparseJoinedTaskCount;

u4 Stats::graphComputationCount;

u4 Stats::egEdgeCount;

u4 Stats::egDeletedEdgeCount;

u8 Stats::vcMemory;

u4 Stats::eventCount;

u4 Stats::eventLoopCount;


void Stats::initializeHbComputationStats(){
    incomingEdgesCount = 0;
    tobeOrderedIncomingEdgesCount = 0;
    sparseJoinedTaskCount = 0;
    graphComputationCount = 0;
    egEdgeCount = 0;
    egDeletedEdgeCount = 0;
    vcMemory = 0;
    eventCount = 0;
    eventLoopCount = 0;

}


}
