/*
 * Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <etalgo.h>

namespace etrack{

EtIterInfo::EtIterInfo(int iter, int index, int prevIterIndex, EtIterInfo* prev) : 
        iter(iter), index(index), prevIterIndex(prevIterIndex), prev(prev){

    next = NULL;
    evc  = new EventAwareVC();
}

EtIterInfo::~EtIterInfo(){
    #ifdef DBG_COMPUTEHB
        printf("Exiting trace processing iteration %d of EventTrack after evaluating "
               "operation at index %d.\n", iter, index);
    #endif
    delete evc;
}


EventTrack::EventTrack(OpHelper* oph) : oph(oph){
    fp = ET_BASE_ITER;
    curFrame = new EtIterInfo(ET_BASE_ITER, 0, -1, NULL);
    baseFrame = curFrame;
}

EventTrack::~EventTrack(){
    delete curFrame;
}

int EventTrack::getEventTrackFixpointIteration(){
    return fp+1;
}

//takes src and dest taskIds of an FoQ post and FoQ/Neg post respectively
bool EventTrack::isSrcFoqPostedPriorDestPost(int srcTaskId, EtTask* destTask){
    Operation* op = destTask->postOp;
    if(destTask->typeOfPost == DR_POST_FOQ){
        PostFoqOp* postop = dynamic_cast<PostFoqOp*>(op);
        if(postop->causalFoQPosts.find(srcTaskId) != postop->causalFoQPosts.end()){
            return true;
        }else{
            return false;
        }
    }else if(destTask->typeOfPost == DR_POST_NEG){
        PostNegOp* postop = dynamic_cast<PostNegOp*>(op);
        if(postop->causalFoQPosts.find(srcTaskId) != postop->causalFoQPosts.end()){
            return true;
        }else{
            return false;
        }
    }

    return false;
}

//takes src and dest taskIds of a Neg post and FoQ/Neg post respectively
bool EventTrack::isSrcNegPostedPriorDestPost(int srcTaskId, EtTask* destTask){
    Operation* op = destTask->postOp;
    if(destTask->typeOfPost == DR_POST_FOQ){
        PostFoqOp* postop = dynamic_cast<PostFoqOp*>(op);
        if(postop->causalNegPosts.find(srcTaskId) != postop->causalNegPosts.end()){
            return true;
        }else{
            return false;
        }
    }else if(destTask->typeOfPost == DR_POST_NEG){
        PostNegOp* postop = dynamic_cast<PostNegOp*>(op);
        if(postop->causalNegPosts.find(srcTaskId) != postop->causalNegPosts.end()){
            return true;
        }else{
            return false;
        }
    }

    return false;
}


bool EventTrack::evaluatePostOp(int opId, EtOperation* op){

    bool sinkModified = false;
    PostOp* postop = dynamic_cast<PostOp*>(op);
    if(op->task != NULL){
        EtTask* task = op->task;

        if(task->mod >= curFrame->iter){
            if(op->hbSinkTask == NULL){
                PERROR("Sink unknown for POST(%d,%u,%d). Aborting HB computation.", op->tid, postop->event, postop->dest);
            }

            EventAwareVC::vcIterator vcIt = curFrame->evc->vcMap.find(op->taskId);
            VectorClock* vc = NULL;
            if(vcIt != curFrame->evc->vcMap.end()){
                vc = vcIt->second;
            }else{
                PERROR("Since task %d of POST(%d,%u,%d) is modified by current or a higher iteration, its VC "
                       "should already have been initialized. Aborting as something has been "
                       "wrongly setup.", op->taskId, op->tid, postop->event, postop->dest);
            }

            //update or initialize VC of posted task (both are possible as some lifecycle related tasks are initialized by enable ops even prior to post)
            VectorClock* eventVC = NULL;
            EventAwareVC::vcIterator eventVcIt = curFrame->evc->vcMap.find(op->hbSinkTask->id);
            if(eventVcIt == curFrame->evc->vcMap.end()){
                eventVC = new VectorClock(*vc);
                curFrame->evc->vcMap.insert(std::make_pair(op->hbSinkTask->id, eventVC));
            }else{
                eventVC = eventVcIt->second;
                eventVC->vcJoin(vc);
            }

            EventAwareVC::egIterator egIt = curFrame->evc->eventGraph.find(op->taskId);
            EventGraphNode* egn = NULL;
           
            if(egIt == curFrame->evc->eventGraph.end()){
                PERROR("Since task %d of POST(%d,%u,%d) is modified by current or a higher iteration, its EventGraphNode "
                       "should already have been initialized. Aborting as something has been "
                       "wrongly setup.", op->taskId, op->tid, postop->event, postop->dest);
            }
                
            egn = egIt->second;
            
            //update or initialize EventGraph node for posted task
            EventGraphNode* eventEgn = NULL;
            EventAwareVC::egIterator eventEgIt = curFrame->evc->eventGraph.find(op->hbSinkTask->id);
            if(eventEgIt == curFrame->evc->eventGraph.end()){
                if(task->eventId > 0){
                    eventEgn = new EventGraphNode();
                    EtList* parentNode = new EtList(task, KNOWN_BEGIN);
                    eventEgn->insertEdge(op->tid, parentNode);
                    eventEgn->insertEdgesAcrossThreads(egn);
                }else{
                    eventEgn = new EventGraphNode(*egn);
                }
                curFrame->evc->eventGraph.insert(std::make_pair(op->hbSinkTask->id, eventEgn));
            }else{
                eventEgn = eventEgIt->second;
                eventEgn->sparseJoinAcrossThreads(egn, eventVC, false, op->hbSinkTask->id, oph);
                if(task-> eventId > 0){
                    EtList* parentNode = new EtList(task, KNOWN_BEGIN);
                    eventEgn->insertEdge(op->tid, parentNode);
                }
            }

            EtList* sinkNode = new EtList(op->hbSinkTask, KNOWN_POST);
            #ifdef ET_ENABLE_OPT_INSERT
                egn->optimizedInsertEdge(postop->dest,sinkNode);
            #endif
            #ifndef ET_ENABLE_OPT_INSERT
                egn->insertEdge(postop->dest,sinkNode);
            #endif

            std::set<int>::iterator setIt = postop->tobeInitializedTasks.begin();
            for(; setIt != postop->tobeInitializedTasks.end(); setIt++){
                EventAwareVC::vcIterator targetVcIt = curFrame->evc->vcMap.find(*setIt);
                VectorClock* targetVc = NULL;
                if(targetVcIt == curFrame->evc->vcMap.end()){
                    targetVc = new VectorClock(*vc);
                    curFrame->evc->vcMap.insert(std::make_pair(*setIt, targetVc));
                }else{
                    targetVcIt->second->vcJoin(vc);
                    targetVc = targetVcIt->second;
                }

                EventAwareVC::egIterator targetEgIt = curFrame->evc->eventGraph.find(*setIt);
                EventGraphNode* targetEgn = NULL;
                if(targetEgIt == curFrame->evc->eventGraph.end()){
                    targetEgn = new EventGraphNode(*egn);
                    curFrame->evc->eventGraph.insert(std::make_pair(*setIt, targetEgn));
                }else{
                    targetEgn = targetEgIt->second;
                    targetEgn->sparseJoinAcrossThreads(egn, targetVc, false, *setIt, oph);
                }

                EtList* hbSrcNode = new EtList(op->hbSinkTask, KNOWN_POST);
                targetEgn->insertEdge(postop->dest, hbSrcNode);

                if(task->eventId > 0){
                    hbSrcNode = new EtList(task, KNOWN_BEGIN);
                    targetEgn->insertEdge(op->tid, hbSrcNode);
                }

                if(curFrame->iter != ET_BASE_ITER){
                    EtTask* targetTask = dynamic_cast<EtTask*>(oph->taskMap.find(*setIt)->second);
                    targetTask->mod = curFrame->iter;
                }
            }


            /* only the initial ET iteration changes the epoch of a task. 
             * Other iterations only transfer HB info between tasks.
             */
            if(curFrame->iter == ET_BASE_ITER){
                int chain = oph->getChain(task, vc);
                int epoch = vc->incrementClock(chain);
                task->epoch = epoch;
                //reflect that the max clock value on 'chain' has changed
                oph->curClockOnChains->incrementClock(chain);
            }else{
                //indicate the iteration to most recntly modify sinkTask's HB info
                op->hbSinkTask->mod = curFrame->iter;
            }

            //sinkModified = true; - we need this only for ops that result in cross-thread middle-of-task HB

            #ifdef DBG_COMPUTEHB
                printf("\nopid %d  ", opId);
                postop->diagnosticPrint();
                vc->diagnosticPrint();
                egn->diagnosticPrint();
            #endif

        }
    }else if(op->taskId == -1){
        if(!postop->tobeInitializedTasks.empty()){
            if(op->hbSinkTask != NULL){
                EtTask* task = op->hbSinkTask;
                if(task->mod >= curFrame->iter){
                    EventAwareVC::egIterator egIt = curFrame->evc->eventGraph.find(task->id);
                    EventGraphNode* egn = NULL;
                    if(egIt != curFrame->evc->eventGraph.end()){
                        egn = egIt->second;
                    }

                    std::set<int>::iterator setIt = postop->tobeInitializedTasks.begin();
                    for(; setIt != postop->tobeInitializedTasks.end(); setIt++){
                        EventAwareVC::vcIterator targetVcIt = curFrame->evc->vcMap.find(*setIt);
                        VectorClock* targetVc = NULL;
                        if(targetVcIt == curFrame->evc->vcMap.end()){
                            //we do post-to-post HB initialization only for posts posting tasks to same thread
                            //& which are environment posts whose delay is 0. Else we need to do delay check
                            //& in presence of delay we should pass on VC info too. In the current scenario
                            //VC info is obtained when targetTask's BEGIN takes join with END of posted task.
                            targetVc = new VectorClock();
                            curFrame->evc->vcMap.insert(std::make_pair(*setIt, targetVc));
                        }else{
                            targetVc = targetVcIt->second;
                        }
        
                        EventAwareVC::egIterator targetEgIt = curFrame->evc->eventGraph.find(*setIt);
                        EventGraphNode* targetEgn = NULL;
                        if(targetEgIt == curFrame->evc->eventGraph.end()){
                            if(egn != NULL){
                                targetEgn = new EventGraphNode(*egn);
                            }else{
                                targetEgn = new EventGraphNode();
                            }
                            curFrame->evc->eventGraph.insert(std::make_pair(*setIt, targetEgn));
                        }else{
                            targetEgn = targetEgIt->second;
                            if(egn != NULL){
                                targetEgn->sparseJoinAcrossThreads(egn, targetVc, false, *setIt, oph);
                            }
                        }
        
                        EtList* hbSrcNode = new EtList(task, KNOWN_POST);
                        targetEgn->insertEdge(postop->dest, hbSrcNode);
        
                        //we will not change mod of target task. When begin of target gets processed,
                        //if hbSinkTask is seen to be updated then target task too will get updated.
                        //Edge form end -> begin will definitely get added from hbSinkTask to targetTask
                        //in the base iteration, after which hbSinkTask will move to joinedWrtTasks set of
                        //targetTask.
                    }
                }
            }else{
                PERROR("missing hbSinkTask when processing native POST(%d,%u,%d).",
                op->tid, postop->event, postop->dest);
            }
        }//else - nothing to be done
        #ifdef DBG_COMPUTEHB
            printf("\nopid %d  ", opId);
            postop->diagnosticPrint();
        #endif
        //we are fine with POST outside tasks, just that we will only be updating posted
        // task's HB info. 
    }else{
        PERROR("missing task found when processing POST(%d,%u,%d).",
                op->tid, postop->event, postop->dest);
    }

    return sinkModified;

}


bool EventTrack::evaluateAtTimePostOp(int opId, EtOperation* op){

    bool sinkModified = false;
    AtTimePostOp* postop = dynamic_cast<AtTimePostOp*>(op);
    if(op->task != NULL){
        EtTask* task = op->task;

        if(task->mod >= curFrame->iter){
            if(op->hbSinkTask == NULL){
                PERROR("Sink unknown for POST-AT-TIME(%d,%u,%d). Aborting HB computation.", op->tid, postop->event, postop->dest);
            }

            EventAwareVC::vcIterator vcIt = curFrame->evc->vcMap.find(op->taskId);
            VectorClock* vc = NULL;
            if(vcIt != curFrame->evc->vcMap.end()){
                vc = vcIt->second;
            }else{
                PERROR("Since task %d of POST-AT-TIME(%d,%u,%d) is modified by current or a higher iteration, its VC "
                       "should already have been initialized. Aborting as something has been "
                       "wrongly setup.", op->taskId, op->tid, postop->event, postop->dest);
            }

            //update or initialize VC of posted task (both are possible as some lifecycle related tasks are initialized by enable ops even prior to post)
            VectorClock* eventVC = NULL;
            EventAwareVC::vcIterator eventVcIt = curFrame->evc->vcMap.find(op->hbSinkTask->id);
            if(eventVcIt == curFrame->evc->vcMap.end()){
                eventVC = new VectorClock(*vc);
                curFrame->evc->vcMap.insert(std::make_pair(op->hbSinkTask->id, eventVC));
            }else{
                eventVC = eventVcIt->second;
                eventVC->vcJoin(vc);
            }

            EventAwareVC::egIterator egIt = curFrame->evc->eventGraph.find(op->taskId);
            EventGraphNode* egn = NULL;
           
            if(egIt == curFrame->evc->eventGraph.end()){
                PERROR("Since task %d of POST-AT-TIME(%d,%u,%d) is modified by current or a higher iteration, its EventGraphNode "
                       "should already have been initialized. Aborting as something has been "
                       "wrongly setup.", op->taskId, op->tid, postop->event, postop->dest);
            }
                
            egn = egIt->second;

            //update or initialize EventGraph node for posted task
            EventGraphNode* eventEgn = NULL;
            EventAwareVC::egIterator eventEgIt = curFrame->evc->eventGraph.find(op->hbSinkTask->id);
            if(eventEgIt == curFrame->evc->eventGraph.end()){
                if(task->eventId > 0){
                    eventEgn = new EventGraphNode();
                    EtList* parentNode = new EtList(task, KNOWN_BEGIN);
                    eventEgn->insertEdge(op->tid, parentNode);
                    eventEgn->insertEdgesAcrossThreads(egn);
                }else{
                    eventEgn = new EventGraphNode(*egn);
                }
                curFrame->evc->eventGraph.insert(std::make_pair(op->hbSinkTask->id, eventEgn));
            }else{
                eventEgn = eventEgIt->second;
                eventEgn->sparseJoinAcrossThreads(egn, eventVC, false, op->hbSinkTask->id, oph);
                if(task-> eventId > 0){
                    EtList* parentNode = new EtList(task, KNOWN_BEGIN);
                    eventEgn->insertEdge(op->tid, parentNode);
                }
            }

            /* Task of AT_TIME_POST does not happen-before other tasks posted after it due to FIFO - see 
             * EventRacer OOPSLA paper. Hence we dont have to track it.
             */

            std::set<int>::iterator setIt = postop->tobeInitializedTasks.begin();
            for(; setIt != postop->tobeInitializedTasks.end(); setIt++){
                EventAwareVC::vcIterator targetVcIt = curFrame->evc->vcMap.find(*setIt);
                VectorClock* targetVc = NULL;
                if(targetVcIt == curFrame->evc->vcMap.end()){
                    targetVc = new VectorClock(*vc);
                    curFrame->evc->vcMap.insert(std::make_pair(*setIt, targetVc));
                }else{
                    targetVcIt->second->vcJoin(vc);
                    targetVc = targetVcIt->second;
                }

                EventAwareVC::egIterator targetEgIt = curFrame->evc->eventGraph.find(*setIt);
                EventGraphNode* targetEgn = NULL;
                if(targetEgIt == curFrame->evc->eventGraph.end()){
                    targetEgn = new EventGraphNode(*egn);
                    curFrame->evc->eventGraph.insert(std::make_pair(*setIt, targetEgn));
                }else{
                    targetEgn = targetEgIt->second;
                    targetEgn->sparseJoinAcrossThreads(egn, targetVc, false, *setIt, oph);
                }

                /* Task of AT_TIME_POST is not ordered w.r.t other tasks posted after it - see EventRacer OOPSLA paper.
                EtList* hbSrcNode = new EtList(op->hbSinkTask, KNOWN_POST);
                targetEgn->insertEdge(postop->dest, hbSrcNode);
                */

                if(task->eventId > 0){
                    EtList* hbSrcNode = new EtList(task, KNOWN_BEGIN);
                    targetEgn->insertEdge(op->tid, hbSrcNode);
                }

                if(curFrame->iter != ET_BASE_ITER){
                    EtTask* targetTask = dynamic_cast<EtTask*>(oph->taskMap.find(*setIt)->second);
                    targetTask->mod = curFrame->iter;
                }
            }


            /* only the initial ET iteration changes the epoch of a task. 
             * Other iterations only transfer HB info between tasks.
             */
            if(curFrame->iter == ET_BASE_ITER){
                int chain = oph->getChain(task, vc);
                int epoch = vc->incrementClock(chain);
                task->epoch = epoch;
                //reflect that the max clock value on 'chain' has changed
                oph->curClockOnChains->incrementClock(chain);
            }else{
                //indicate the iteration to most recntly modify sinkTask's HB info
                op->hbSinkTask->mod = curFrame->iter;
            }

            //sinkModified = true; - we need this only for ops that result in cross-thread middle-of-task HB

            #ifdef DBG_COMPUTEHB
                printf("\nopid %d  ", opId);
                postop->diagnosticPrint();
                vc->diagnosticPrint();
                egn->diagnosticPrint();
            #endif

        }
    }else if(op->taskId == -1){
        if(!postop->tobeInitializedTasks.empty()){
            //as per our current instrumentation of environment events this will not hit as none of them do postAtTime.
            if(op->hbSinkTask != NULL){
                EtTask* task = op->hbSinkTask;
                if(task->mod >= curFrame->iter){
                    EventAwareVC::egIterator egIt = curFrame->evc->eventGraph.find(task->id);
                    EventGraphNode* egn = NULL;
                    if(egIt != curFrame->evc->eventGraph.end()){
                        egn = egIt->second;
                    }

                    std::set<int>::iterator setIt = postop->tobeInitializedTasks.begin();
                    for(; setIt != postop->tobeInitializedTasks.end(); setIt++){
                        EventAwareVC::vcIterator targetVcIt = curFrame->evc->vcMap.find(*setIt);
                        VectorClock* targetVc = NULL;
                        if(targetVcIt == curFrame->evc->vcMap.end()){
                            //we do post-to-post HB initialization only for posts posting tasks to same thread
                            //& which are environment posts whose delay is 0. Else we need to do delay check
                            //& in presence of delay we should pass on VC info too. In the current scenario
                            //VC info is obtained when targetTask's BEGIN takes join with END of posted task.
                            targetVc = new VectorClock();
                            curFrame->evc->vcMap.insert(std::make_pair(*setIt, targetVc));
                        }else{
                            targetVc = targetVcIt->second;
                        }
        
                        EventAwareVC::egIterator targetEgIt = curFrame->evc->eventGraph.find(*setIt);
                        EventGraphNode* targetEgn = NULL;
                        if(targetEgIt == curFrame->evc->eventGraph.end()){
                            if(egn != NULL){
                                targetEgn = new EventGraphNode(*egn);
                            }else{
                                targetEgn = new EventGraphNode();
                            }
                            curFrame->evc->eventGraph.insert(std::make_pair(*setIt, targetEgn));
                        }else{
                            targetEgn = targetEgIt->second;
                            if(egn != NULL){
                                targetEgn->sparseJoinAcrossThreads(egn, targetVc, false, *setIt, oph);
                            }
                        }
        
                        /* Task of AT_TIME_POST is not ordered w.r.t other tasks posted after it - see EventRacer OOPSLA paper.
                        EtList* hbSrcNode = new EtList(op->hbSinkTask, KNOWN_POST);
                        targetEgn->insertEdge(postop->dest, hbSrcNode);
                        */
        
                        //we will not change mod of target task. When begin of target gets processed,
                        //if hbSinkTask is seen to be updated then target task too will get updated.
                        //Edge form end -> begin will definitely get added from hbSinkTask to targetTask
                        //in the base iteration, after which hbSinkTask will move to joinedWrtTasks set of
                        //targetTask.
                    }
                }
            }else{
                PERROR("missing hbSinkTask when processing native POST-AT-TIME(%d,%u,%d).",
                op->tid, postop->event, postop->dest);
            }
        }//else - nothing to be done
        #ifdef DBG_COMPUTEHB
            printf("\nopid %d  ", opId);
            postop->diagnosticPrint();
        #endif
        //we are fine with POST outside tasks, just that we will only be updating posted
        // task's HB info. 
    }else{
        PERROR("missing task found when processing POST-AT-TIME(%d,%u,%d).",
                op->tid, postop->event, postop->dest);
    }

    return sinkModified;

}


bool EventTrack::evaluateBeginOp(int opId, EtOperation* op){

    bool sinkModified = false;
    BeginOp* beginop = dynamic_cast<BeginOp*>(op);
    if(op->task != NULL){
        EtTask* task = op->task;

        #ifdef DBG_COLLECT_STATS
            dynamic_cast<EtThread*>(oph->threadMap.find(op->tid)->second)->msgCount++;
            Stats::eventCount++;
        #endif

        //even if below constraint does not hold need to process begin since tasks with which
        //it was previously ordered may have been updated.

        bool isCurVcInMap = false;
        bool isCurEgnInMap = false;
        bool updateTaskMod = false;

        EventAwareVC::vcIterator vcIt = curFrame->evc->vcMap.find(op->taskId);
        VectorClock* vc = NULL;
        if(vcIt != curFrame->evc->vcMap.end()){
            vc = vcIt->second;
            isCurVcInMap = true;
        }else{
            vc = new VectorClock();
        }

        EventAwareVC::egIterator egIt = curFrame->evc->eventGraph.find(op->taskId);
        EventGraphNode* egn = NULL;
        if(egIt != curFrame->evc->eventGraph.end()){
            egn = egIt->second;
            isCurEgnInMap = true;
        }else{
            egn = new EventGraphNode();
        }
     
        BasePostOp* postop = task->postOp;
        int delay = 0;
        if(task->typeOfPost == DR_POST){
            delay = (dynamic_cast<PostOp*>(postop))->delay;
        } 

        EtList* incEdges = NULL;
        EtList* newNode = NULL;
        EtList* newHead = NULL;
        
        if(curFrame->iter == ET_BASE_ITER && !task->envOrderedTasks.empty()){
            /* in base iteration all these tasks' HB info are joined with current task,
             * they are moved to joinedWrtTasks set and hence accounted via joinedWrtTasks in other iterations.
             */
            std::set<int>::iterator envIt = task->envOrderedTasks.begin();
            incEdges = new EtList(dynamic_cast<EtTask*>(oph->taskMap.find(*envIt)->second), KNOWN_BEGIN);
            envIt++;
            for(; envIt != task->envOrderedTasks.end(); envIt++){
                newNode = new EtList(dynamic_cast<EtTask*>(oph->taskMap.find(*envIt)->second), KNOWN_BEGIN); 
                newHead = incEdges->insert(newNode, true);
                if(newHead != NULL){
                    incEdges = newHead;
                }
            }

            //remove below stats collection if you increment counter inside constructor of EtList
            #ifdef DBG_COLLECT_STATS
                Stats::graphComputationCount += task->envOrderedTasks.size();
            #endif
        }
            

        EtList* tempList = egn->getIncomingEdgesForThread(op->tid);
        if(tempList != NULL){
            if(incEdges == NULL){
                incEdges = new EtList(*tempList);
            }else{
                newHead = incEdges->insert(tempList, false);
                if(newHead != NULL){
                    incEdges = newHead;
                }
            }
        }

        if(curFrame->iter != ET_BASE_ITER && incEdges != NULL){
            //remove incoming edges already in joinedWrtTasks set. see algo in the paper.
            EtList* cur = incEdges;
            while(cur != NULL){
                #ifdef DBG_COLLECT_STATS
                    Stats::graphComputationCount++;
                #endif

                if(task->joinedWrtTasks.find(cur->task) != task->joinedWrtTasks.end()){
                    if(cur->prev != NULL){
                        cur->prev->next = cur->next;
                    }
                    if(cur->next != NULL){
                        cur->next->prev = cur->prev;
                    }
                    EtList* delNode = cur;
                    cur = cur->next;
                    delNode->next = NULL;
                    delete delNode;
                }else{
                    cur = cur->next;
                }
            }
        }

        std::set<EtTask*>::iterator joinIt = task->joinedWrtTasks.begin();
        for(; joinIt != task->joinedWrtTasks.end(); joinIt++){
            EtTask* joinedTask = *joinIt;
            if(joinedTask->mod >= curFrame->iter){
                newNode = new EtList(joinedTask, KNOWN_BEGIN);
                if(incEdges != NULL){
                    newHead = incEdges->insert(newNode, true);
                    if(newHead != NULL){
                        incEdges = newHead;
                    }
                }else{
                    incEdges = newNode;
                }
            }
        }

        #ifdef DBG_COLLECT_STATS
            Stats::graphComputationCount += task->joinedWrtTasks.size();
        #endif


        EtTask* inTask = NULL;
        PostOp* inPostop = NULL;
        EventAwareVC::vcIterator srcVcIt;
        EventAwareVC::egIterator srcEgIt;

        bool shouldJoin = false;
        bool edgeAdded = false;


        while(incEdges != NULL){
            shouldJoin = false;
            #ifdef DBG_COLLECT_STATS
                Stats::graphComputationCount++;
                if(curFrame->iter == ET_BASE_ITER){
                    Stats::incomingEdgesCount++;
                }
            #endif

            inTask = incEdges->task;

            if(task->typeOfPost == DR_POST){
                switch(incEdges->lbl){
                    case KNOWN_POST: 
                        inPostop = dynamic_cast<PostOp*>(inTask->postOp);
                        if(inPostop->delay <= delay){
                            shouldJoin = true;
                        }
                        break;
                    case KNOWN_BEGIN:
                    case KNOWN_POST_FOQ:
                    case KNOWN_NEG_POST:
                        shouldJoin = true;
                        break;
                    case KNOWN_BROADCAST:
                        if(beginop->isOnReceive){
                            shouldJoin = true;
                        }
                        break;
                    default: shouldJoin = false;
                }
            }else if(task->typeOfPost == DR_AT_TIME_POST){
                switch(incEdges->lbl){
                    case KNOWN_BEGIN: 
                    case KNOWN_POST_FOQ:
                    case KNOWN_NEG_POST:
                        shouldJoin = true;
                        break;
                    default: shouldJoin = false;
                }
            }else if(task->typeOfPost == DR_POST_FOQ){
                switch(incEdges->lbl){
                    case KNOWN_BEGIN: 
                        shouldJoin = true;
                        break;
                    default: shouldJoin = false;
                }
            }else if(task->typeOfPost == DR_POST_NEG){
                switch(incEdges->lbl){
                    case KNOWN_BEGIN: 
                        shouldJoin = true;
                        break;
                    default: shouldJoin = false;
                }
            }else if(task->typeOfPost == DR_IDLE_POST){
                switch(incEdges->lbl){
                    case KNOWN_POST: 
                        inPostop = dynamic_cast<PostOp*>(inTask->postOp);
                        if(inPostop->delay == 0){
                            shouldJoin = true;
                        }
                        break;
                    case KNOWN_IDLE_POST:
                    case KNOWN_BEGIN:
                    case KNOWN_POST_FOQ:
                    case KNOWN_NEG_POST:
                        shouldJoin = true;
                        break;
                    default: shouldJoin = false;
                }
            }

            if(shouldJoin){
                #ifdef DBG_COLLECT_STATS
                    if(curFrame->iter == ET_BASE_ITER){
                        Stats::tobeOrderedIncomingEdgesCount++;
                    }
                #endif

                //Check if the HB info of this task already knows everything about the inTask
                std::pair<int, u4> chainClockPair = inTask->getEpoch();
                if(!vc->doesSubsumeClock(chainClockPair.first, chainClockPair.second)){
                    #ifdef DBG_COLLECT_STATS
                        if(curFrame->iter == ET_BASE_ITER){
                            Stats::sparseJoinedTaskCount++;
                        }
                    #endif

                    //Hb order identified task w.r.t current begin, as described in paper
                    srcVcIt = curFrame->evc->vcMap.find(incEdges->task->id);
                    if(srcVcIt != curFrame->evc->vcMap.end()){
                        vc->vcJoin(srcVcIt->second);
                    }

                    if(curFrame != baseFrame && task->joinedWrtTasks.find(incEdges->task) == task->joinedWrtTasks.end()){
                        //we are taking join with this task for the first time
                        EtIterInfo* tempFrame = baseFrame;
                        while(tempFrame != curFrame && tempFrame->iter <= inTask->mod){
                            srcVcIt = tempFrame->evc->vcMap.find(incEdges->task->id);
                            if(srcVcIt != tempFrame->evc->vcMap.end()){
                                vc->vcJoin(srcVcIt->second);
                            }
                        
                            tempFrame = tempFrame->next;
                        }
                    }

                    srcEgIt = curFrame->evc->eventGraph.find(incEdges->task->id);
                    if(srcEgIt != curFrame->evc->eventGraph.end()){
                         egn->sparseJoinAcrossThreads(srcEgIt->second, vc, false, op->taskId, oph);        
                    }

                    if(curFrame != baseFrame && task->joinedWrtTasks.find(incEdges->task) == task->joinedWrtTasks.end()){
                        //we are taking join with this task for the first time
                        EtIterInfo* tempFrame = baseFrame;
                        while(tempFrame != curFrame && tempFrame->iter <= inTask->mod){
                            srcEgIt = tempFrame->evc->eventGraph.find(incEdges->task->id);
                            if(srcEgIt != tempFrame->evc->eventGraph.end()){
                                egn->sparseJoinAcrossThreads(srcEgIt->second, vc, false, op->taskId, oph);        
                            }

                            tempFrame = tempFrame->next;
                        }
                    }

                    edgeAdded = true;
                }

                //even if you did not take direct join incEdges->id should be indicated as joined with this task. 
                //Logic for mod-check based reprocessing, in case reprocessing of trace happens. However, have never seen any
                //trace resulting in reprocessing.
                task->joinedWrtTasks.insert(incEdges->task);
            }
            incEdges = incEdges->next;
        }//end of while

        
        if(edgeAdded){


        delete incEdges;

        updateTaskMod = true;

        if(!isCurVcInMap){
            curFrame->evc->vcMap.insert(std::make_pair(op->taskId, vc));    
        }
        if(!isCurEgnInMap){
            curFrame->evc->eventGraph.insert(std::make_pair(op->taskId, egn));
        }


        //only if any edges were added in previous iteration and an foq event has been dequeued, fixpoint computation kicks in
        if(foqOrNegPostedThreads.find(op->tid) != foqOrNegPostedThreads.end()){

        bool edgeAddedFoqNeg = false;

        //fixpoint computation to apply FoQ related rules, immediately after processing begin.
        std::set<edgeFlagType> edgeFlagSet;
        edgeFlagSet.insert(KNOWN_POST_FOQ);
        edgeFlagSet.insert(KNOWN_NEG_POST);

        while((tempList = egn->getCopyOfTypedIncomingEdgesForThread(op->tid, edgeFlagSet)) != NULL){
            edgeAddedFoqNeg = false;

            incEdges = new EtList(*tempList);

            while(incEdges != NULL){
                //We dont have to collect stats for incomingEdgeCount here as it has already been counted
                //in getCopyOfTypedIncomingEdgesForThread.

                shouldJoin = false;

                if(task->joinedWrtTasks.find(incEdges->task) != task->joinedWrtTasks.end()){
                    //such tasks are already handled above.
                    incEdges = incEdges->next;
                    continue;
                }
               
                if(task->typeOfPost == DR_POST){
                    shouldJoin = true;
                }if(task->typeOfPost == DR_AT_TIME_POST){
                    shouldJoin = true;
                }else if(task->typeOfPost == DR_POST_FOQ){
                    switch(incEdges->lbl){
                        case KNOWN_POST_FOQ: 
                        case KNOWN_NEG_POST: 
                            if(isSrcFoqPostedPriorDestPost(op->taskId, incEdges->task)){
                                shouldJoin = true;
                            }
                            break;
                        default: shouldJoin = false;
                    }
                }else if(task->typeOfPost == DR_POST_NEG){
                    switch(incEdges->lbl){
                        case KNOWN_POST_FOQ: 
                            if(isSrcNegPostedPriorDestPost(op->taskId, incEdges->task)){
                                shouldJoin = true;
                            }
                            break;
                        default: shouldJoin = false;
                    }
                }else if(task->typeOfPost == DR_IDLE_POST){
                    shouldJoin = true;
                }

                if(shouldJoin){
                    #ifdef DBG_COLLECT_STATS
                        if(curFrame->iter == ET_BASE_ITER){
                            Stats::tobeOrderedIncomingEdgesCount++;
                        }
                    #endif

                    inTask = incEdges->task;
                    //Check if the HB info of this task already knows everything about the inTask
                    std::pair<int, u4> chainClockPair = inTask->getEpoch();
                    if(!vc->doesSubsumeClock(chainClockPair.first, chainClockPair.second)){
                        #ifdef DBG_COLLECT_STATS
                            if(curFrame->iter == ET_BASE_ITER){
                                Stats::sparseJoinedTaskCount++;
                            }
                        #endif


                        //Hb order identified task w.r.t current begin, as described in paper
                        srcVcIt = curFrame->evc->vcMap.find(incEdges->task->id);
                        if(srcVcIt != curFrame->evc->vcMap.end()){
                            vc->vcJoin(srcVcIt->second);
                        }

                        if(curFrame != baseFrame && task->joinedWrtTasks.find(incEdges->task) == task->joinedWrtTasks.end()){
                            //we are taking join with this task for the first time
                            EtIterInfo* tempFrame = baseFrame;
                            while(tempFrame != curFrame && tempFrame->iter <= inTask->mod){
                                srcVcIt = tempFrame->evc->vcMap.find(incEdges->task->id);
                                if(srcVcIt != tempFrame->evc->vcMap.end()){
                                    vc->vcJoin(srcVcIt->second);
                                }
                        
                                tempFrame = tempFrame->next;
                            }
                        }

                        srcEgIt = curFrame->evc->eventGraph.find(incEdges->task->id);
                        if(srcEgIt != curFrame->evc->eventGraph.end()){
                             egn->sparseJoinAcrossThreads(srcEgIt->second, vc, false, op->taskId, oph);        
                        }

                        if(curFrame != baseFrame && task->joinedWrtTasks.find(incEdges->task) == task->joinedWrtTasks.end()){
                            //we are taking join with this task for the first time
                            EtIterInfo* tempFrame = baseFrame;
                            while(tempFrame != curFrame && tempFrame->iter <= inTask->mod){
                                srcEgIt = tempFrame->evc->eventGraph.find(incEdges->task->id);
                                if(srcEgIt != tempFrame->evc->eventGraph.end()){
                                    egn->sparseJoinAcrossThreads(srcEgIt->second, vc, false, op->taskId, oph);        
                                }

                                tempFrame = tempFrame->next;
                            }
                        }

                        edgeAddedFoqNeg = true;
                    }

                    //even if you did not take direct join incEdges->id should be indicated as joined with this task. See algo in paper.
                    task->joinedWrtTasks.insert(incEdges->task);
                }

                incEdges = incEdges->next;
            }//end of while

            /* if no edges were added then we have reached fixpoint and no more FoQPosted or NegPosted 
             * tasks can be ordered w.r.t. this BEGIN operation.
             */
            if(!edgeAddedFoqNeg){
                break;
            }
 
        }

        } //close foqOrNegPostedThreads set check

        }else{
            if(incEdges != NULL){
                delete incEdges;
            }

            /* since this task has not been ordered w.r.t. any other task on task->tid,
             * need to take join with LOOP-ON-Q directly. Otherwise this info would have
             * been obtained via other tasks transitively.
             */
            OpHelper::threadMapIterator threadIt = oph->threadMap.find(op->tid);
            if(threadIt != oph->threadMap.end() && threadIt->second->loopOpId != -1){
                std::map<int, std::pair<VectorClock*, EventGraphNode*> >::iterator evcIt =
                        curFrame->evcStore.find(threadIt->second->loopOpId);
                if(evcIt != curFrame->evcStore.end()){
                    vc->vcJoin(evcIt->second.first);
                    egn->sparseJoinAcrossThreads(evcIt->second.second, vc, false, op->taskId, oph);

                    updateTaskMod = true;
                
                    if(!isCurVcInMap){
                        curFrame->evc->vcMap.insert(std::make_pair(op->taskId, vc));    
                    }
                    if(!isCurEgnInMap){
                        curFrame->evc->eventGraph.insert(std::make_pair(op->taskId, egn));
                    }
                }

            }else{
                PERROR("Missing entry for thread %d in threadMap, found when processing BEGIN(%d,%u) or missing loopOpId.",
                        op->tid, op->tid, beginop->event);
            }
        }

        if(task->typeOfPost == DR_POST_FOQ || task->typeOfPost == DR_POST_NEG){
            foqOrNegPostedThreads.insert(op->tid);
        }

        /* only the initial ET iteration changes the epoch of a task. 
         * Other iterations only transfer HB info between tasks.
         */
        if(curFrame->iter == ET_BASE_ITER){
            int chain = oph->getChain(task, vc);
            int epoch = vc->incrementClock(chain);
            task->epoch = epoch;
            //reflect that the max clock value on 'chain' has changed
            oph->curClockOnChains->incrementClock(chain);
        }else if(updateTaskMod){
            task->mod = curFrame->iter;
        }

        //sinkModified = true; - we need this only for ops that result in cross-thread middle-of-task HB

        #ifdef DBG_COMPUTEHB
            printf("\nopid %d  ", opId);
            beginop->diagnosticPrint();
            vc->diagnosticPrint();
            egn->diagnosticPrint();
        #endif

    }else{
        PERROR("missing task found when processing BEGIN(%d,%u).",
                op->tid, beginop->event);
    }

    return sinkModified;

}


bool EventTrack::evaluateEndOp(int opId, EtOperation* op){
    //bool sinkModified = true;

    //returning false since END does not trigger any interesting functionality
    return false;
}

bool EventTrack::evaluateForkOp(int opId, EtOperation* op){
    bool sinkModified = false;
    ForkOp* forkop = dynamic_cast<ForkOp*>(op);
    if(op->task != NULL){
        EtTask* task = op->task;

        if(task->mod >= curFrame->iter){
            EventAwareVC::vcIterator vcIt = curFrame->evc->vcMap.find(op->taskId);
            VectorClock* vc = NULL;
            if(vcIt != curFrame->evc->vcMap.end()){
                vc = vcIt->second;
            }else{
                PERROR("Since task %d of FORK(%d,%d) is modified by current or a higher iteration, its VC "
                       "should already have been initialized. Aborting as something has been "
                       "wrongly setup.", op->taskId, op->tid, forkop->childTid);
            }

            if(op->hbSinkTask == NULL ){
                PERROR("Sink unknown for FORK(%d,%d). Aborting HB computation.", op->tid, forkop->childTid);
            }

            //initialize VC of child thread
            VectorClock* childVC = new VectorClock(*vc);
            curFrame->evc->vcMap.insert(std::make_pair(op->hbSinkTask->id, childVC));

            EventAwareVC::egIterator egIt = curFrame->evc->eventGraph.find(op->taskId);
            if(egIt == curFrame->evc->eventGraph.end()){
                PERROR("Since task %d of FORK(%d,%d) is modified by current or a higher iteration, its EventGraphNode "
                       "should already have been initialized. Aborting as something has been "
                       "wrongly setup.", op->taskId, op->tid, forkop->childTid);

            }
            
            //initialize EventGraph node for child thread
            EventGraphNode* childEgn = NULL;
            if(task-> eventId > 0){
                childEgn = new EventGraphNode();
                EtList* parentNode = new EtList(task, KNOWN_BEGIN);
                childEgn->insertEdge(op->tid, parentNode);
                childEgn->insertEdgesAcrossThreads(egIt->second);
            }else{
                childEgn = new EventGraphNode(*egIt->second);
            }
            curFrame->evc->eventGraph.insert(std::make_pair(op->hbSinkTask->id, childEgn));


            /* only the initial ET iteration changes the epoch of a task. 
             * Other iterations only transfer HB info between tasks.
             */
            if(curFrame->iter == ET_BASE_ITER){
                int chain = oph->getChain(task, vc);
                int epoch = vc->incrementClock(chain);
                task->epoch = epoch;
                //reflect that the max clock value on 'chain' has changed
                oph->curClockOnChains->incrementClock(chain);
            }else{
                //indicate the iteration to most recntly modify sinkTask's HB info
                op->hbSinkTask->mod = curFrame->iter;
            }

            //sinkModified = true; - we need this only for ops that result in cross-thread middle-of-task HB

            #ifdef DBG_COMPUTEHB
                printf("\nopid %d  ", opId);
                forkop->diagnosticPrint();
                vc->diagnosticPrint();
                egIt->second->diagnosticPrint();
            #endif
        }
    }else{
        PERROR("missing task found when processing FORK(%d,%d).",
                op->tid, forkop->childTid);
    }

    return sinkModified;
}

bool EventTrack::evaluateLoopOp(int opId, EtOperation* op){
    bool sinkModified = false;
    LoopOp* loopop = dynamic_cast<LoopOp*>(op);
    if(op->task != NULL){
        #ifdef DBG_COLLECT_STATS
            Stats::eventLoopCount++;
        #endif

        EtTask* task = op->task;

        if(task->mod >= curFrame->iter){
            EventAwareVC::vcIterator vcIt = curFrame->evc->vcMap.find(op->taskId);
            VectorClock* vc = NULL;
            if(vcIt != curFrame->evc->vcMap.end()){
                vc = vcIt->second;
            }else{
                PERROR("Since task %d of LOOP(%u) is modified by current or a higher iteration, its VectorClock "
                       "should already have been initialized. Aborting as something has been "
                       "wrongly setup.", op->taskId, loopop->eventqueue);
            }

            EventAwareVC::egIterator egIt = curFrame->evc->eventGraph.find(op->taskId);
            EventGraphNode* egn = NULL;
            if(egIt != curFrame->evc->eventGraph.end()){
                egn = egIt->second;
            }else{
                PERROR("Since task %d of LOOP(%u) is modified by current or a higher iteration, its EventGraphNode "
                       "should already have been initialized. Aborting as something has been "
                       "wrongly setup.", op->taskId, loopop->eventqueue);
            }

            /* only the initial ET iteration changes the epoch of a task. 
             * Other iterations only transfer HB info between tasks.
             */
            if(curFrame->iter == ET_BASE_ITER){
                int chain = oph->getChain(task, vc);
                int epoch = vc->incrementClock(chain);
                task->epoch = epoch; 
                //reflect that the max clock value on 'chain' has changed
                oph->curClockOnChains->incrementClock(chain);
            }

            /* LOOP is the last operation executed by thread prior to processing events.
             * store LOOP's HB info into evcStore as all begin ops on this thread will take join with it.
             */
            storeHBInfo(opId, vc, egn);

            //sinkModified = true; - we need this only for ops that result in cross-thread middle-of-task HB

            #ifdef DBG_COMPUTEHB
                printf("\nopid %d  ", opId);
                loopop->diagnosticPrint();
                vc->diagnosticPrint();
                egn->diagnosticPrint();
            #endif
        }

        OpHelper::threadMapIterator threadItr = oph->threadMap.find(task->tid);
        if(threadItr != oph->threadMap.end()){
            threadItr->second->loopOpId = opId;
        }else{
            PERROR("Missing entry for thread %d in threadMap, found when processing LOOP(%u).",
                        op->tid, loopop->eventqueue);
        }
    }else{
        PERROR("missing task found when processing LOOP(%u).",
                loopop->eventqueue);
    }

    return sinkModified;
}

bool EventTrack::evaluateLoopExitOp(int opId, EtOperation* op){
    bool sinkModified = false;
    LoopExitOp* loopexitop = dynamic_cast<LoopExitOp*>(op);
    if(op->task != NULL){
        EtTask* task = op->task;
       
        VectorClock* vc = new VectorClock();
        EventGraphNode* egn = new EventGraphNode();

        bool hbInitialized = false;
        EventAwareVC::vcIterator srcVcItr;
        EventAwareVC::egIterator srcEgnItr;

        std::set<int>::reverse_iterator setIt = loopexitop->hbSrcSet.rbegin();
        for(; setIt != loopexitop->hbSrcSet.rend(); setIt++){
            OpHelper::taskMapIterator srcTaskItr = oph->taskMap.find(*setIt);
            if(srcTaskItr == oph->taskMap.end()){
                PERROR("missing task with ID %d found when processing LOOP-EXIT(%u).",
                        *setIt, loopexitop->eventqueue);
            }

            EtTask* srcTask = dynamic_cast<EtTask*>(srcTaskItr->second);
            if(srcTask->mod >= curFrame->iter){
                std::pair<int, u4> chainClockPair = srcTask->getEpoch();
                if(!vc->doesSubsumeClock(chainClockPair.first, chainClockPair.second)){
                    srcVcItr = curFrame->evc->vcMap.find(*setIt);
                    if(srcVcItr != curFrame->evc->vcMap.end()){
                        vc->vcJoin(srcVcItr->second);
                    }else{
                        PERROR("Since HB src task %d of LOOP-EXIT(%u) is modified by current or a higher iteration, its VC "
                               "should already have been initialized. Aborting as something has been "
                               "wrongly setup.", *setIt, loopexitop->eventqueue);
                    }
    
                    srcEgnItr = curFrame->evc->eventGraph.find(*setIt);
                    if(srcEgnItr != curFrame->evc->eventGraph.end()){
                        egn->sparseJoinAcrossThreads(srcEgnItr->second, vc, false, op->taskId, oph);
                    }else{
                        PERROR("Since HB src task %d of LOOP-EXIT(%u) is modified by current or a higher iteration, its EventGraph "
                               "should already have been initialized. Aborting as something has been "
                               "wrongly setup.", *setIt, loopexitop->eventqueue);
                    }

                    hbInitialized = true;
                }
            }
        }

        OpHelper::threadMapIterator threadIt = oph->threadMap.find(task->tid);

        if(!hbInitialized){            
            if(threadIt != oph->threadMap.end() && threadIt->second->loopOpId != -1){
                std::map<int, std::pair<VectorClock*, EventGraphNode*> >::iterator evcIt =
                        curFrame->evcStore.find(threadIt->second->loopOpId);
                if(evcIt != curFrame->evcStore.end()){
                    vc->vcJoin(evcIt->second.first);
                    egn->sparseJoinAcrossThreads(evcIt->second.second, vc, false, op->taskId, oph);

                    curFrame->evc->vcMap.insert(std::make_pair(op->taskId, vc));
                    curFrame->evc->eventGraph.insert(std::make_pair(op->taskId, egn));

                    hbInitialized = true;
                }

            }else{
                PERROR("Missing entry for thread %d in threadMap, found when processing LOOP-EXIT(%u)..",
                        op->tid, loopexitop->eventqueue);
            }
        }else{
            //no need to take join with LOOP opId as this is already subsumed when join is performed wiht a task.
            curFrame->evc->vcMap.insert(std::make_pair(op->taskId, vc));
            curFrame->evc->eventGraph.insert(std::make_pair(op->taskId, egn));
        }
        

        if(threadIt != oph->threadMap.end()){
            threadIt->second->loopOpId = -1;
        }else{
            PERROR("Missing entry for thread %d in threadMap, found when processing LOOP-EXIT(%u).",
                        op->tid, loopexitop->eventqueue);
        }

        if(curFrame->iter == ET_BASE_ITER){
            int chain = oph->getChain(task, vc);
            int epoch = vc->incrementClock(chain);
            task->epoch = epoch; 
            //reflect that the max clock value on 'chain' has changed
            oph->curClockOnChains->incrementClock(chain);
        }else if(hbInitialized){
            task->mod = curFrame->iter;
        }

        #ifdef DBG_COMPUTEHB
           printf("\nopid %d  ", opId);
           loopexitop->diagnosticPrint();
           vc->diagnosticPrint();
           egn->diagnosticPrint();
        #endif

    }else{
        PERROR("missing task found when processing LOOP-EXIT(%u).",
                loopexitop->eventqueue);
    }

    return sinkModified;
}

bool EventTrack::evaluateThreadexitOp(int opId, EtOperation* op){
    //bool sinkModified = true;
    
    //returning false since threadexit does not trigger any interesting functionality
    return false;
}

bool EventTrack::evaluateThreadinitOp(int opId, EtOperation* op){

    bool sinkModified = false;
    if(op->task != NULL){
        EtTask* task = op->task;

        if(task->mod >= curFrame->iter){
            EventAwareVC::vcIterator vcIt = curFrame->evc->vcMap.find(op->taskId);
            VectorClock* vc = NULL;
            if(vcIt != curFrame->evc->vcMap.end()){
                vc = vcIt->second;
            }else{
                vc = new VectorClock();
                curFrame->evc->vcMap.insert(std::make_pair(op->taskId, vc));
            }

            EventAwareVC::egIterator egIt = curFrame->evc->eventGraph.find(op->taskId);
            if(egIt == curFrame->evc->eventGraph.end()){
                EventGraphNode* egn = new EventGraphNode();
                curFrame->evc->eventGraph.insert(std::make_pair(op->taskId, egn));
            }

            /* only the initial ET iteration changes the epoch of a task. 
             * Other iterations only transfer HB info between tasks.
             */
            if(curFrame->iter == ET_BASE_ITER){
                int chain = oph->getChain(task, vc);
                int epoch = vc->incrementClock(chain);
                task->epoch = epoch;
                //reflect that the max clock value on 'chain' has changed
                oph->curClockOnChains->incrementClock(chain);
            }

            //sinkModified = true; - we need this only for ops that result in cross-thread middle-of-task HB

            #ifdef DBG_COMPUTEHB
                printf("\nopid %d  ", opId);
                (dynamic_cast<ThreadinitOp*>(op))->diagnosticPrint();
                vc->diagnosticPrint();
                if(egIt != curFrame->evc->eventGraph.end()){
                    egIt->second->diagnosticPrint();
                }
            #endif
        }
    }else{
        PERROR("missing task found when processing THREADINIT of tid %d.",
                op->tid);
    }

    return sinkModified;

}

bool EventTrack::evaluateEnableEventOp(int opId, EtOperation* op){

    bool sinkModified = false;
    EnableEventOp* enableop = dynamic_cast<EnableEventOp*>(op);
    if(!enableop->tobeInitializedTasks.empty() || enableop->shouldStore){

        if(op->task != NULL){
            EtTask* task = op->task;
    
            if(task->mod >= curFrame->iter){
                EventAwareVC::vcIterator vcIt = curFrame->evc->vcMap.find(op->taskId);
                VectorClock* vc = NULL;
                if(vcIt != curFrame->evc->vcMap.end()){
                    vc = vcIt->second;
                }else{
                    PERROR("Since task %d of ENABLE_EVENT(%u,%d) is modified by current or a higher iteration, its VC "
                           "should already have been initialized. Aborting as something has been "
                           "wrongly setup.", op->taskId, enableop->view, enableop->uiEvent);
                }
    
                EventAwareVC::egIterator egIt = curFrame->evc->eventGraph.find(op->taskId);
                EventGraphNode* egn = NULL;
                if(egIt != curFrame->evc->eventGraph.end()){
                    egn = egIt->second;
                }else{
                    PERROR("Since task %d of ENABLE_EVENT(%u,%d) is modified by current or a higher iteration, its EventGraph "
                           "should already have been initialized. Aborting as something has been "
                           "wrongly setup.", op->taskId, enableop->view, enableop->uiEvent);
                }

                if(enableop->shouldStore){
                    storeHBInfo(opId, vc, egn);
                }
    
                std::set<int>::iterator sinkTaskIt = enableop->tobeInitializedTasks.begin();
                for(; sinkTaskIt != enableop->tobeInitializedTasks.end(); sinkTaskIt++){
                    //task of each trigger_event is initialized by exactly one enable_event. 
                    EtTask* sinkTask = dynamic_cast<EtTask*>(oph->taskMap.find(*sinkTaskIt)->second);
                    sinkTask->mod = curFrame->iter;

                    VectorClock* sinkVc = NULL;
                    if(task->tid != sinkTask->tid || task->eventId <= 0){
                        sinkVc = new VectorClock(*vc);
                    }else{
                        //VC info can be obtained when join is taken during processing of sinkTask's begin
                        sinkVc = new VectorClock();
                    }
                    EventGraphNode* sinkEgn = new EventGraphNode(*egn);
                    curFrame->evc->vcMap.insert(std::make_pair(*sinkTaskIt, sinkVc));
                    curFrame->evc->eventGraph.insert(std::make_pair(*sinkTaskIt, sinkEgn));
  
                    if(task->eventId > 0){
                        EtList* parentNode = new EtList(task, KNOWN_BEGIN);
                        sinkEgn->insertEdge(op->tid, parentNode);
                    }
                }
    
                /* only the initial ET iteration changes the epoch of a task. 
                 * Other iterations only transfer HB info between tasks.
                 */
                if(curFrame->iter == ET_BASE_ITER){
                    int chain = oph->getChain(task, vc);
                    int epoch = vc->incrementClock(chain);
                    task->epoch = epoch;
                    //reflect that the max clock value on 'chain' has changed
                    oph->curClockOnChains->incrementClock(chain);
                }
    
                //sinkModified = true; - we need this only for ops that result in cross-thread middle-of-task HB

                #ifdef DBG_COMPUTEHB
                    printf("\nopid %d  ", opId);
                    enableop->diagnosticPrint();
                    vc->diagnosticPrint();
                    egn->diagnosticPrint();
                #endif
            }
        }else{
            PERROR("missing task found when processing ENABLE_EVENT(%u,%d).",
                    enableop->view, enableop->uiEvent);
        }

    }else{
        PERROR("The preprocessor has not deleted ENABLE_EVENT(%u,%d) even though it is not initializing tasks of any"
               " trigger event handlers.", enableop->view, enableop->uiEvent);
    }

    return sinkModified;

}

bool EventTrack::evaluateTriggerEventOp(int opId, EtOperation* op){

    bool sinkModified = false;
    TriggerEventOp* triggerop = dynamic_cast<TriggerEventOp*>(op);

    if(triggerop->enableOpId > 0){
        std::map<int, std::pair<VectorClock*, EventGraphNode*> >::iterator evcIt =
                curFrame->evcStore.find(triggerop->enableOpId);
        if(evcIt != curFrame->evcStore.end()){ 
            /* TriggerOp's VC timestamp gets modified only if enable has been updated
             * in this iteration. It does not matter whether trigger's task itself has been 
             * updated by this iteration yet.
             * Also, we evaluate TRIGGER_EVENT only if ENABLE_EVENT and TRIGGER_EVENT are on
             * different threads, else etOperation.cpp does not store this op.
             */
            if(op->task != NULL){
                EtTask* task = op->task;

                EventAwareVC::vcIterator vcIt = curFrame->evc->vcMap.find(op->taskId);
                VectorClock* vc = NULL;
                if(vcIt != curFrame->evc->vcMap.end()){
                    vc = vcIt->second;
                }else{
                    vc = new VectorClock();
                    curFrame->evc->vcMap.insert(std::make_pair(op->taskId, vc));
                }
                vc->vcJoin(evcIt->second.first); //take join with enable's stored VC timestamp
    
                EventAwareVC::egIterator egIt = curFrame->evc->eventGraph.find(op->taskId);
                EventGraphNode* egn = NULL;
                if(egIt == curFrame->evc->eventGraph.end()){
                    egn = new EventGraphNode();
                    curFrame->evc->eventGraph.insert(std::make_pair(op->taskId, egn));
                }else{
                    egn = egIt->second;   
                }
                egn->sparseJoinAcrossThreads(evcIt->second.second, vc, false, op->taskId, oph);
                
                if(triggerop->hbSrcTask != NULL){
                    if(triggerop->hbSrcTask->eventId > 0){
                        EtList* parentNode = new EtList(triggerop->hbSrcTask, KNOWN_BEGIN);
                        egn->insertEdge(triggerop->hbSrcTask->tid, parentNode);
                    }
                }else{
                    PERROR("missing taskId for hbSrc (enable's task) of TRIGGER_EVENT(%u,%d).", triggerop->view, triggerop->uiEvent);
                }
    
                //TRIGGER does not result in change of epoch of a task
 
                task->mod = curFrame->iter; //this is essential in case this is the first op in this task to be updated by current iteration
                sinkModified = true;

                #ifdef DBG_COMPUTEHB
                    printf("\nopid %d  ", opId);
                    triggerop->diagnosticPrint();
                    vc->diagnosticPrint();
                    egn->diagnosticPrint();
                #endif

            }else{
                PERROR("missing task found when processing TRIGGER_EVENT(%u,%d).", 
                        triggerop->view, triggerop->uiEvent);
            }
        }

    }else{
        PERROR("Missing enableOpId corresponding to TRIGGER_EVENT(%u,%d).", triggerop->view, triggerop->uiEvent);
    }

    return sinkModified;
}

bool EventTrack::evaluateEnableActLifecycleOp(int opId, EtOperation* op){

    bool sinkModified = false;
    EnableActLifecycleOp* enableop = dynamic_cast<EnableActLifecycleOp*>(op);
    if(!enableop->tobeInitializedTasks.empty() || enableop->shouldStore){ //only if this enable initializes trigger event's tasks process it, otherwiise there's nothing interesting

        if(op->task != NULL){
            EtTask* task = op->task;
    
            if(task->mod >= curFrame->iter){
                EventAwareVC::vcIterator vcIt = curFrame->evc->vcMap.find(op->taskId);
                VectorClock* vc = NULL;
                if(vcIt != curFrame->evc->vcMap.end()){
                    vc = vcIt->second;
                }else{
                    PERROR("Since task %d of ENABLE_ACT_LIFECYCLE(%u,%d) is modified by current or a higher iteration, its VC "
                           "should already have been initialized. Aborting as something has been "
                           "wrongly setup.", op->taskId, enableop->instance, enableop->state);
                }
    
                EventAwareVC::egIterator egIt = curFrame->evc->eventGraph.find(op->taskId);
                EventGraphNode* egn = NULL;
                if(egIt != curFrame->evc->eventGraph.end()){
                    egn = egIt->second;
                }else{
                    PERROR("Since task %d of ENABLE_ACT_LIFECYCLE(%u,%d) is modified by current or a higher iteration, its EventGraph "
                           "should already have been initialized. Aborting as something has been "
                           "wrongly setup.", op->taskId, enableop->instance, enableop->state);
                }
    
                std::set<int>::iterator setIt = enableop->tobeInitializedTasks.begin();
                for(; setIt != enableop->tobeInitializedTasks.end(); setIt++){
                    EtTask* sinkTask = dynamic_cast<EtTask*>(oph->taskMap.find(*setIt)->second);
                    sinkTask->mod = curFrame->iter;

                    EventAwareVC::vcIterator sinkVcIt = curFrame->evc->vcMap.find(*setIt);
                    VectorClock* sinkVc = NULL;
                    if(sinkVcIt == curFrame->evc->vcMap.end()){
                        if(task->tid != sinkTask->tid || task->eventId <= 0){
                            sinkVc = new VectorClock(*vc);
                        }else{
                            //VC info of current task will be obtained when sinkTask's BEGIN joins with current task
                            sinkVc = new VectorClock();
                        }
                        curFrame->evc->vcMap.insert(std::make_pair(*setIt, sinkVc));
                    }else{
                        if(task->tid != sinkTask->tid || task->eventId <= 0){
                            sinkVcIt->second->vcJoin(vc);
                        }
                        sinkVc = sinkVcIt->second;
                    }

                    EventAwareVC::egIterator sinkEgIt = curFrame->evc->eventGraph.find(*setIt);
                    EventGraphNode* sinkEgn = NULL;
                    if(sinkEgIt == curFrame->evc->eventGraph.end()){
                        sinkEgn = new EventGraphNode(*egn);
                        curFrame->evc->eventGraph.insert(std::make_pair(*setIt, sinkEgn));
                    }else{
                        sinkEgn = sinkEgIt->second;
                        sinkEgn->sparseJoinAcrossThreads(egn, sinkVc, false, *setIt, oph);
                    }
                     
                    if(task->eventId > 0){
                        EtList* parentNode = new EtList(task, KNOWN_BEGIN);
                        sinkEgn->insertEdge(op->tid, parentNode);
                    }
                }

                if(enableop->shouldStore){
                    storeHBInfo(opId, vc, egn);
                }
    
                /* only the initial ET iteration changes the epoch of a task. 
                 * Other iterations only transfer HB info between tasks.
                 */
                if(curFrame->iter == ET_BASE_ITER){
                    int chain = oph->getChain(task, vc);
                    int epoch = vc->incrementClock(chain);
                    task->epoch = epoch;
                    //reflect that the max clock value on 'chain' has changed
                    oph->curClockOnChains->incrementClock(chain);
                }
    
                //sinkModified = true;

                #ifdef DBG_COMPUTEHB
                    printf("\nopid %d  ", opId);
                    enableop->diagnosticPrint();
                    vc->diagnosticPrint();
                    egn->diagnosticPrint();
                #endif
            }
        }else{
            PERROR("missing task found when processing ENABLE_ACT_LIFECYCLE(%u,%d).",
                    enableop->instance, enableop->state);
        }

    }else{
        #ifdef DBG_COMPUTEHB
        printf("Found an ENABLE_ACT_LIFECYCLE(%u,%d) which is neither stored nor initializes any new tasks.\n", 
                 enableop->instance, enableop->state);
        #endif
    }

    return sinkModified;

}

bool EventTrack::evaluateTriggerActLifecycleOp(int opId, EtOperation* op){

    bool sinkModified = false;
    TriggerActLifecycleOp* triggerop = dynamic_cast<TriggerActLifecycleOp*>(op);

    if(!triggerop->envOrderedEnableOps.empty()){
        if(op->task != NULL){
            EtTask* task = op->task;

            EventAwareVC::vcIterator vcIt = curFrame->evc->vcMap.find(task->id);
            VectorClock* vc = NULL;
            if(vcIt != curFrame->evc->vcMap.end()){
                vc = vcIt->second;
            }

            EventAwareVC::egIterator egIt = curFrame->evc->eventGraph.find(task->id);
            EventGraphNode* egn = NULL;
            if(egIt != curFrame->evc->eventGraph.end()){
                egn = egIt->second;
            }

            std::map<int,int>::iterator enMapIt = triggerop->envOrderedEnableOps.begin();
            for(; enMapIt != triggerop->envOrderedEnableOps.end(); enMapIt++){
                std::map<int, std::pair<VectorClock*, EventGraphNode*> >::iterator evcIt =
                        curFrame->evcStore.find(enMapIt->first);
                if(evcIt != curFrame->evcStore.end()){ 
                    /* TriggerOp's VC timestamp gets modified only if enable has been updated
                     * in this iteration. It does not matter whether trigger's task itself has been 
                     * updated by this iteration yet.
                     */
    
                    if(vc != NULL){
                        vc->vcJoin(evcIt->second.first); //take join with enable's stored VC timestamp
                    }else{
                        vc = new VectorClock(*evcIt->second.first);
                        curFrame->evc->vcMap.insert(std::make_pair(op->taskId, vc));
                    }
        
                    if(egn == NULL){
                        egn = new EventGraphNode(*evcIt->second.second);
                        curFrame->evc->eventGraph.insert(std::make_pair(op->taskId, egn));
                    }else{
                        egn->sparseJoinAcrossThreads(evcIt->second.second, vc, false, op->taskId, oph);
                    }
                    
                    if(enMapIt->second > 0){
                        EtTask* enableTask = dynamic_cast<EtTask*>(oph->taskMap.find(enMapIt->second)->second);
                        if(enableTask->eventId > 0){
                            EtList* parentNode = new EtList(enableTask, KNOWN_BEGIN);
                            egn->insertEdge(enableTask->tid, parentNode);
                        }
                    }else{
                        PERROR("missing taskId for hbSrc (enable's task) of TRIGGER_ACT_LIFECYCLE(%u, %d).", 
                            triggerop->instance, triggerop->state);
                    }
        
                    //TRIGGER does not result in change of epoch of a task
     
                    task->mod = curFrame->iter; //this is essential in case this is the first op in this task to be updated by current iteration
                    sinkModified = true;

                }

            }

            #ifdef DBG_COMPUTEHB
                 printf("\nopid %d  ", opId);
                 triggerop->diagnosticPrint();
                 vc->diagnosticPrint();
                 egn->diagnosticPrint();
            #endif
        }else{
            PERROR("missing task found when processing TRIGGER_ACT_LIFECYCLE(%u, %d).", 
                    triggerop->instance, triggerop->state);
        }

    }

    return sinkModified;

}

//for BroadcastReceiver and Servicecomponents we do not emit ENABLE as TRIGGER captures that info too.
//Here the state field of the operation was used to identify enabling operations etc.
bool EventTrack::evaluateTriggerReceiverOp(int opId, EtOperation* op){

    bool sinkModified = false;
    TriggerReceiverOp* recop = dynamic_cast<TriggerReceiverOp*>(op);
    if(!recop->tobeInitializedTasks.empty()){ //only if this operation initializes trigger event's tasks process it, otherwiise there's nothing interesting

        if(op->task != NULL){
            EtTask* task = op->task;
    
            if(task->mod >= curFrame->iter){ 
                EventAwareVC::vcIterator vcIt = curFrame->evc->vcMap.find(op->taskId);
                VectorClock* vc = NULL;
                if(vcIt != curFrame->evc->vcMap.end()){
                    vc = vcIt->second;
                }else{
                    PERROR("Since task %d of TRIGGER_RECEIVER with action-key %d, receiver instance %u, intentId %d and state %d " 
                            "is modified by current or a higher iteration, its VC "
                           "should already have been initialized. Aborting as something has been "
                           "wrongly setup.", op->taskId, recop->actionKey, recop->identifier, 
                           recop->intentId, recop->state);
                }
    
                EventAwareVC::egIterator egIt = curFrame->evc->eventGraph.find(op->taskId);
                EventGraphNode* egn = NULL;
                if(egIt != curFrame->evc->eventGraph.end()){
                    egn = egIt->second;
                }else{
                    PERROR("Since task %d of TRIGGER_RECEIVER with action-key %d, receiver instance %u, intentId %d and state %d " 
                            "is modified by current or a higher iteration, its EventGraph "
                           "should already have been initialized. Aborting as something has been "
                           "wrongly setup.", op->taskId, recop->actionKey, recop->identifier, 
                           recop->intentId, recop->state);
                }

                std::map<int, EtTask*> tmpTaskMap;
    
                std::set<int>::iterator setIt = recop->tobeInitializedTasks.begin();
                for(; setIt != recop->tobeInitializedTasks.end(); setIt++){
                    EtTask* sinkTask = dynamic_cast<EtTask*>(oph->taskMap.find(*setIt)->second);
                    sinkTask->mod = curFrame->iter;

                    EventAwareVC::vcIterator sinkVcIt = curFrame->evc->vcMap.find(*setIt);
                    VectorClock* sinkVc = NULL;
                    if(sinkVcIt == curFrame->evc->vcMap.end()){
                        if(task->tid != sinkTask->tid || task->eventId <= 0){
                            sinkVc = new VectorClock(*vc);
                        }else{
                            sinkVc = new VectorClock();
                        }
                        curFrame->evc->vcMap.insert(std::make_pair(*setIt, sinkVc));
                    }else{
                        if(task->tid != sinkTask->tid || task->eventId <= 0){
                            sinkVcIt->second->vcJoin(vc);
                        }
                        sinkVc = sinkVcIt->second;
                    }

                    EventAwareVC::egIterator sinkEgIt = curFrame->evc->eventGraph.find(*setIt);
                    EventGraphNode* sinkEgn = NULL;
                    if(sinkEgIt == curFrame->evc->eventGraph.end()){
                        sinkEgn = new EventGraphNode(*egn);
                        curFrame->evc->eventGraph.insert(std::make_pair(*setIt, sinkEgn));
                    }else{
                        sinkEgn = sinkEgIt->second;
                        sinkEgn->sparseJoinAcrossThreads(egn, sinkVc, false, *setIt, oph);
                    }
                     
                    if(task->eventId > 0){
                        EtList* srcNode = new EtList(task, KNOWN_BEGIN);
                        sinkEgn->insertEdge(op->tid, srcNode);
                    }

                    //check if edge labelled KNOWN_BROADCAST should be added as incoming edge of op's task
                    /*if(recop->emitEdge == sinkTask){
                        EtList* srcNode = new EtList(recop->emitEdge, KNOWN_BROADCAST);
                        #ifdef ET_ENABLE_OPT_INSERT
                            egn->optimizedInsertEdge(sinkTask->tid, srcNode);
                        #endif
                        #ifndef ET_ENABLE_OPT_INSERT
                            egn->insertEdge(sinkTask->tid, srcNode);
                        #endif
                    }*/

                    std::set<int>::iterator emitIt = recop->emitEdges.find(sinkTask->id);
                    if(emitIt != recop->emitEdges.end()){
                        tmpTaskMap.insert(std::make_pair(sinkTask->id, sinkTask));
                    }
                }

                for(std::set<int>::iterator emitIt = recop->emitEdges.begin(); emitIt != recop->emitEdges.end(); emitIt++){
                    EtTask* emitTask = tmpTaskMap.find(*emitIt)->second;
                    EtList* emitNode = new EtList(emitTask, KNOWN_BROADCAST);
                    if(emitIt == recop->emitEdges.begin()){
                        //only the lowest task id in the set should be inserted optimally and other normally,
                        //as there is no HB between sends of tasks in this set and they should not be replacing each other.
                        #ifdef ET_ENABLE_OPT_INSERT
                            egn->optimizedInsertEdge(emitTask->tid, emitNode);
                        #endif
                        #ifndef ET_ENABLE_OPT_INSERT
                            egn->insertEdge(emitTask->tid, emitNode);
                        #endif
                    }else{
                        egn->insertEdge(emitTask->tid, emitNode);
                    }
                }
                tmpTaskMap.clear();

    
                /* only the initial ET iteration changes the epoch of a task. 
                 * Other iterations only transfer HB info between tasks.
                 */
                if(curFrame->iter == ET_BASE_ITER){
                    int chain = oph->getChain(task, vc);
                    int epoch = vc->incrementClock(chain);
                    task->epoch = epoch;
                    //reflect that the max clock value on 'chain' has changed
                    oph->curClockOnChains->incrementClock(chain);
                }
    
                //sinkModified = true;

                #ifdef DBG_COMPUTEHB
                    printf("\nopid %d  ", opId);
                    recop->diagnosticPrint();
                    vc->diagnosticPrint();
                    egn->diagnosticPrint();
                #endif
            }
        }else{
            PERROR("missing task found when processing TRIGGER_RECEIVER with "
                   "action-key %d, receiver instance %u, intentId %d and state %d ",
                    recop->actionKey, recop->identifier, recop->intentId, recop->state);
        }

    }else{
        #ifdef DBG_COMPUTEHB
        printf("Found a TRIGGER_RECEIVER with action-key %d, receiver instance %u, intentId %d and state %d " 
               "which does not initializes any new tasks.\n", recop->actionKey, 
               recop->identifier, recop->intentId, recop->state);
        #endif
    }

    return sinkModified;
}


//for BroadcastReceiver and Servicecomponents we do not emit ENABLE as TRIGGER captures that info too.
//Here the state field of the operation was used to identify enabling operations etc.
bool EventTrack::evaluateTriggerServiceOp(int opId, EtOperation* op){

    bool sinkModified = false;
    TriggerServiceOp* serop = dynamic_cast<TriggerServiceOp*>(op);
    if(!serop->tobeInitializedTasks.empty()){ //only if this operation initializes trigger event's tasks process it, otherwiise there's nothing interesting

        if(op->task != NULL){
            EtTask* task = op->task;
    
            if(task->mod >= curFrame->iter){ 
                EventAwareVC::vcIterator vcIt = curFrame->evc->vcMap.find(op->taskId);
                VectorClock* vc = NULL;
                if(vcIt != curFrame->evc->vcMap.end()){
                    vc = vcIt->second;
                }else{
                    PERROR("Since task %d of TRIGGER_SERVICE with class-key %d, identifier %u and state %d " 
                            "is modified by current or a higher iteration, its VC "
                           "should already have been initialized. Aborting as something has been "
                           "wrongly setup.", op->taskId, serop->serviceClassKey, serop->identifier, 
                           serop->state);
                }
    
                EventAwareVC::egIterator egIt = curFrame->evc->eventGraph.find(op->taskId);
                EventGraphNode* egn = NULL;
                if(egIt != curFrame->evc->eventGraph.end()){
                    egn = egIt->second;
                }else{
                    PERROR("Since task %d of TRIGGER_SERVICE with class-key %d, identifier %u and state %d " 
                            "is modified by current or a higher iteration, its EventGraph "
                           "should already have been initialized. Aborting as something has been "
                           "wrongly setup.", op->taskId, serop->serviceClassKey, serop->identifier, 
                           serop->state);
                }
    
                std::set<int>::iterator setIt = serop->tobeInitializedTasks.begin();
                for(; setIt != serop->tobeInitializedTasks.end(); setIt++){
                    EtTask* sinkTask = dynamic_cast<EtTask*>(oph->taskMap.find(*setIt)->second);
                    sinkTask->mod = curFrame->iter;

                    EventAwareVC::vcIterator sinkVcIt = curFrame->evc->vcMap.find(*setIt);
                    VectorClock* sinkVc = NULL;
                    if(sinkVcIt == curFrame->evc->vcMap.end()){
                        if(task->tid != sinkTask->tid || task->eventId <= 0){
                            sinkVc = new VectorClock(*vc);
                        }else{
                            sinkVc = new VectorClock();
                        }
                        curFrame->evc->vcMap.insert(std::make_pair(*setIt, sinkVc));
                    }else{
                        if(task->tid != sinkTask->tid || task->eventId <= 0){
                            sinkVcIt->second->vcJoin(vc);
                        }
                        sinkVc = sinkVcIt->second;
                    }

                    EventAwareVC::egIterator sinkEgIt = curFrame->evc->eventGraph.find(*setIt);
                    EventGraphNode* sinkEgn = NULL;
                    if(sinkEgIt == curFrame->evc->eventGraph.end()){
                        sinkEgn = new EventGraphNode(*egn);
                        curFrame->evc->eventGraph.insert(std::make_pair(*setIt, sinkEgn));
                    }else{
                        sinkEgn = sinkEgIt->second;
                        sinkEgn->sparseJoinAcrossThreads(egn, sinkVc, false, *setIt, oph);
                    }
                     
                    if(task->eventId > 0){
                        EtList* srcNode = new EtList(task, KNOWN_BEGIN);
                        sinkEgn->insertEdge(op->tid, srcNode);
                    }
                }

                
                /* only the initial ET iteration changes the epoch of a task. 
                 * Other iterations only transfer HB info between tasks.
                 */
                if(curFrame->iter == ET_BASE_ITER){
                    int chain = oph->getChain(task, vc);
                    int epoch = vc->incrementClock(chain);
                    task->epoch = epoch;
                    //reflect that the max clock value on 'chain' has changed
                    oph->curClockOnChains->incrementClock(chain);
                }
    
                //sinkModified = true;

                #ifdef DBG_COMPUTEHB
                    printf("\nopid %d  ", opId);
                    serop->diagnosticPrint();
                    vc->diagnosticPrint();
                    egn->diagnosticPrint();
                #endif
            }
        }else{
            PERROR("missing task found when processing TRIGGER_SERVICE with "
                   "class-key %d, identifier %u and state %d ",
                    serop->serviceClassKey, serop->identifier, serop->state);
        }

    }else{
        #ifdef DBG_COMPUTEHB
        printf("Found a TRIGGER_SERVICE with class-key %d, identifier %u and state %d " 
               "which does not initializes any new tasks.\n", serop->serviceClassKey, serop->identifier,
               serop->state);
        #endif
    }

    return sinkModified;

}

bool EventTrack::evaluateEnableWindowFocusOp(int opId, EtOperation* op){
    
    bool sinkModified = false;
    EnableWindowFocusOp* enableop = dynamic_cast<EnableWindowFocusOp*>(op);
    if(enableop->shouldStore || !enableop->tobeInitializedTasks.empty()){ 

        if(op->task != NULL){
            EtTask* task = op->task;
    
            if(task->mod >= curFrame->iter){
                EventAwareVC::vcIterator vcIt = curFrame->evc->vcMap.find(op->taskId);
                VectorClock* vc = NULL;
                if(vcIt != curFrame->evc->vcMap.end()){
                    vc = vcIt->second;
                }else{
                    PERROR("Since task %d of ENABLE_WINDOW_FOCUS(%u) is modified by current or a higher iteration, its VC "
                           "should already have been initialized. Aborting as something has been "
                           "wrongly setup.", op->taskId, enableop->windowId);
                }
    
                EventAwareVC::egIterator egIt = curFrame->evc->eventGraph.find(op->taskId);
                EventGraphNode* egn = NULL;
                if(egIt != curFrame->evc->eventGraph.end()){
                    egn = egIt->second;
                }else{
                    PERROR("Since task %d of ENABLE_WINDOW_FOCUS(%u) is modified by current or a higher iteration, its EventGraph "
                           "should already have been initialized. Aborting as something has been "
                           "wrongly setup.", op->taskId, enableop->windowId);
                }
    
                if(enableop->shouldStore){
                    storeHBInfo(opId, vc, egn);
                }

                std::set<int>::iterator sinkTaskIt = enableop->tobeInitializedTasks.begin();
                for(; sinkTaskIt != enableop->tobeInitializedTasks.end(); sinkTaskIt++){
                    //task of each trigger_window_focus is initialized by exactly one enable_event. 
                    EtTask* sinkTask = dynamic_cast<EtTask*>(oph->taskMap.find(*sinkTaskIt)->second);
                    VectorClock* sinkVc = NULL;
                    if(task->tid != sinkTask->tid || task->eventId <= 0){
                        sinkVc = new VectorClock(*vc);
                    }else{
                        //VC info can be obtained when join is taken during processing of sinkTask's begin
                        sinkVc = new VectorClock();
                    }

                    EventGraphNode* sinkEgn = new EventGraphNode(*egn);
                    curFrame->evc->vcMap.insert(std::make_pair(*sinkTaskIt, sinkVc));
                    curFrame->evc->eventGraph.insert(std::make_pair(*sinkTaskIt, sinkEgn));

                    if(task->eventId > 0){
                        EtList* parentNode = new EtList(task, KNOWN_BEGIN);
                        sinkEgn->insertEdge(op->tid, parentNode);
                    }

                    sinkTask->mod = curFrame->iter;
                }

                /* only the initial ET iteration changes the epoch of a task. 
                 * Other iterations only transfer HB info between tasks.
                 */
                if(curFrame->iter == ET_BASE_ITER){
                    int chain = oph->getChain(task, vc);
                    int epoch = vc->incrementClock(chain);
                    task->epoch = epoch;
                    //reflect that the max clock value on 'chain' has changed
                    oph->curClockOnChains->incrementClock(chain);
                }
    
                //sinkModified = true; - we need this only for ops that result in cross-thread middle-of-task HB

                #ifdef DBG_COMPUTEHB
                    printf("\nopid %d  ", opId);
                    enableop->diagnosticPrint();
                    vc->diagnosticPrint();
                    egn->diagnosticPrint();
                #endif
            }
        }else{
            PERROR("missing task found when processing ENABLE_WINDOW_FOCUS(%u).",
                    enableop->windowId);
        }

    }else{
        PERROR("The preprocessor has stored ENABLE_WINDOW_FOCUS(%u) even though its shouldStore is false "
               "and is not initializing any task.", enableop->windowId);
    }

    return sinkModified;

}

bool EventTrack::evaluateTriggerWindowFocusOp(int opId, EtOperation* op){

    bool sinkModified = false;
    TriggerWindowFocusOp* triggerop = dynamic_cast<TriggerWindowFocusOp*>(op);

    if(triggerop->enableOpId > 0){
        std::map<int, std::pair<VectorClock*, EventGraphNode*> >::iterator evcIt =
                curFrame->evcStore.find(triggerop->enableOpId);
        if(evcIt != curFrame->evcStore.end()){ 
            /* TriggerOp's VC timestamp gets modified only if enable has been updated
             * in this iteration. It does not matter whether trigger's task itself has been 
             * updated by this iteration yet.
             */
            if(op->task != NULL){
                EtTask* task = op->task;

                EventAwareVC::vcIterator vcIt = curFrame->evc->vcMap.find(op->taskId);
                VectorClock* vc = NULL;
                if(vcIt != curFrame->evc->vcMap.end()){
                    vc = vcIt->second;
                }else{
                    vc = new VectorClock();
                    curFrame->evc->vcMap.insert(std::make_pair(op->taskId, vc));
                }
                vc->vcJoin(evcIt->second.first); //take join with enable's stored VC timestamp
    
                EventAwareVC::egIterator egIt = curFrame->evc->eventGraph.find(op->taskId);
                EventGraphNode* egn = NULL;
                if(egIt == curFrame->evc->eventGraph.end()){
                    egn = new EventGraphNode();
                    curFrame->evc->eventGraph.insert(std::make_pair(op->taskId, egn));
                }else{
                    egn = egIt->second;   
                }
                egn->sparseJoinAcrossThreads(evcIt->second.second, vc, false, op->taskId, oph);
                
                if(triggerop->hbSrcTask->id > 0){
                    if(triggerop->hbSrcTask->eventId > 0){
                        EtList* parentNode = new EtList(triggerop->hbSrcTask, KNOWN_BEGIN);
                        egn->insertEdge(triggerop->hbSrcTask->tid, parentNode);
                    }
                }else{
                    PERROR("missing taskId for hbSrc (enable's task) of TRIGGER_WINDOW_FOCUS(%u).", triggerop->windowId);
                }
    
                //TRIGGER does not result in change of epoch of a task
 
                task->mod = curFrame->iter; //this is essential in case this is the first op in this task to be updated by current iteration
                sinkModified = true;

                #ifdef DBG_COMPUTEHB
                    printf("\nopid %d  ", opId);
                    triggerop->diagnosticPrint();
                    vc->diagnosticPrint();
                    egn->diagnosticPrint();
                #endif

            }else{
                PERROR("missing task found when processing TRIGGER_WINDOW_FOCUS(%u).", 
                        triggerop->windowId);
            }
        }

    }else{
        PERROR("Missing enableOpId corresponding to TRIGGER_WINDOW_FOCUS(%u).", triggerop->windowId);
    }

    return sinkModified;

}

bool EventTrack::evaluateWaitOp(int opId, EtOperation* op){
    //bool sinkModified = true;
    
    //returning false since WAIT does not trigger any interesting functionality
    //Notify HB Wait is handled at Notify itself
    return false;
}

bool EventTrack::evaluateNotifyOp(int opId, EtOperation* op){

    bool sinkModified = false;
    NotifyOp* notifyop = dynamic_cast<NotifyOp*>(op);
    if(op->task != NULL){
        EtTask* task = op->task;

        if(task->mod >= curFrame->iter){
            if(op->hbSinkTask == NULL){
                PERROR("Sink unknown for NOTIFY(%d) on tid %d. Aborting HB computation.", notifyop->notifiedTid, op->tid);
            }

            EventAwareVC::vcIterator vcIt = curFrame->evc->vcMap.find(op->taskId);
            VectorClock* vc = NULL;
            if(vcIt != curFrame->evc->vcMap.end()){
                vc = vcIt->second;
            }else{
                PERROR("Since task %d of NOTIFY(%d) is modified by current or a higher iteration, its VC "
                       "should already have been initialized. Aborting as something has been "
                       "wrongly setup.", op->taskId, notifyop->notifiedTid);
            }

            //update or initialize VC of waiting thread (both are possible due to incremental reprocessing of trace)
            VectorClock* waitVC = NULL;
            EventAwareVC::vcIterator waitvcIt = curFrame->evc->vcMap.find(op->hbSinkTask->id);
            if(waitvcIt == curFrame->evc->vcMap.end()){
                waitVC = new VectorClock(*vc);
                curFrame->evc->vcMap.insert(std::make_pair(op->hbSinkTask->id, waitVC));
            }else{
                waitVC = waitvcIt->second;
                waitVC->vcJoin(vc);
            }

            EventAwareVC::egIterator egIt = curFrame->evc->eventGraph.find(op->taskId);
            if(egIt == curFrame->evc->eventGraph.end()){
                PERROR("Since task %d of  NOTIFY(%d) is modified by current or a higher iteration, its EventGraphNode "
                       "should already have been initialized. Aborting as something has been "
                       "wrongly setup.", op->taskId, notifyop->notifiedTid);

            }
            
            //update or initialize EventGraph node for wait thread
            EventGraphNode* waitEgn = NULL;
            EventAwareVC::egIterator waitegIt = curFrame->evc->eventGraph.find(op->hbSinkTask->id);
            if(waitegIt == curFrame->evc->eventGraph.end()){
                waitEgn = new EventGraphNode(*egIt->second);
                curFrame->evc->eventGraph.insert(std::make_pair(op->hbSinkTask->id, waitEgn));
            }else{
                waitEgn = waitegIt->second;
                waitEgn->sparseJoinAcrossThreads(egIt->second, waitVC, false, op->hbSinkTask->id, oph);
            }

            if(task->eventId > 0){
                EtList* parentNode = new EtList(task, KNOWN_BEGIN);
                waitEgn->insertEdge(op->tid, parentNode);
            }

            /* only the initial ET iteration changes the epoch of a task. 
             * Other iterations only transfer HB info between tasks.
             */
            if(curFrame->iter == ET_BASE_ITER){
                int chain = oph->getChain(task, vc);
                int epoch = vc->incrementClock(chain);
                task->epoch = epoch;
                //reflect that the max clock value on 'chain' has changed
                oph->curClockOnChains->incrementClock(chain);
            }else{
                //indicate the iteration to most recntly modify sinkTask's HB info
                op->hbSinkTask->mod = curFrame->iter;
            }

            sinkModified = true;

            #ifdef DBG_COMPUTEHB
                printf("\nopid %d  ", opId);
                notifyop->diagnosticPrint();
                vc->diagnosticPrint();
                egIt->second->diagnosticPrint();
            #endif
        }
    }else if(op->taskId == -1){
        #ifdef DBG_COMPUTEHB
            printf("found a NATIVE NOTIFY(%d). We will not be updating any HB info, but continue the"
               " HB computation.\n", notifyop->notifiedTid);
        #endif
        //we are fine with NOTIFY from native threads, just that we will only be updating notified
        // task's HB info. Hence nootifyop->hbSinkTask->mod will not change.
    }else{
        PERROR("missing task found when processing NOTIFY(%d).",
                notifyop->notifiedTid);
    }

    return sinkModified;

}

bool EventTrack::evaluateIdlePostOp(int opId, EtOperation* op){

    bool sinkModified = false;
    IdlePostOp* postop = dynamic_cast<IdlePostOp*>(op);
    if(op->task != NULL){
        EtTask* task = op->task;

        if(task->mod >= curFrame->iter){
            if(op->hbSinkTask == NULL){
                PERROR("Sink unknown for IDLE-POST(%d,%u,%d). Aborting HB computation.", op->tid, postop->event, postop->dest);
            }

            EventAwareVC::vcIterator vcIt = curFrame->evc->vcMap.find(op->taskId);
            VectorClock* vc = NULL;
            if(vcIt != curFrame->evc->vcMap.end()){
                vc = vcIt->second;
            }else{
                PERROR("Since task %d of IDLE-POST(%d,%u,%d) is modified by current or a higher iteration, its VC "
                       "should already have been initialized. Aborting as something has been "
                       "wrongly setup.", op->taskId, op->tid, postop->event, postop->dest);
            }

            //update or initialize VC of waiting thread (both are possible as some lifecycle related tasks are initialized by enable ops even prior to post)
            VectorClock* eventVC = NULL;
            EventAwareVC::vcIterator eventVcIt = curFrame->evc->vcMap.find(op->hbSinkTask->id);
            if(eventVcIt == curFrame->evc->vcMap.end()){
                eventVC = new VectorClock(*vc);
                curFrame->evc->vcMap.insert(std::make_pair(op->hbSinkTask->id, eventVC));
            }else{
                eventVC = eventVcIt->second;
                eventVC->vcJoin(vc);
            }

            EventAwareVC::egIterator egIt = curFrame->evc->eventGraph.find(op->taskId);
            EventGraphNode* egn = NULL;
           
            if(egIt == curFrame->evc->eventGraph.end()){
                PERROR("Since task %d of IDLE-POST(%d,%u,%d) is modified by current or a higher iteration, its EventGraphNode "
                       "should already have been initialized. Aborting as something has been "
                       "wrongly setup.", op->taskId, op->tid, postop->event, postop->dest);
            }
                
            egn = egIt->second;
            
            //update or initialize EventGraph node for wait thread
            EventGraphNode* eventEgn = NULL;
            EventAwareVC::egIterator eventEgIt = curFrame->evc->eventGraph.find(op->hbSinkTask->id);
            if(eventEgIt == curFrame->evc->eventGraph.end()){
                if(task-> eventId > 0){
                    eventEgn = new EventGraphNode();
                    EtList* parentNode = new EtList(task, KNOWN_BEGIN);
                    eventEgn->insertEdge(op->tid, parentNode);
                    eventEgn->insertEdgesAcrossThreads(egn);
                }else{
                    eventEgn = new EventGraphNode(*egn);
                }

                curFrame->evc->eventGraph.insert(std::make_pair(op->hbSinkTask->id, eventEgn));
            }else{
                eventEgn->sparseJoinAcrossThreads(egn, eventVC, false, op->hbSinkTask->id, oph);
                eventEgn = eventEgIt->second;
                if(task-> eventId > 0){
                    EtList* parentNode = new EtList(task, KNOWN_BEGIN);
                    eventEgn->insertEdge(op->tid, parentNode);
                }
            }

            EtList* sinkNode = new EtList(op->hbSinkTask, KNOWN_IDLE_POST);
            #ifdef ET_ENABLE_OPT_INSERT
                egn->optimizedInsertEdge(postop->dest,sinkNode);
            #endif
            #ifndef ET_ENABLE_OPT_INSERT
                egn->insertEdge(postop->dest,sinkNode);
            #endif


            /* only the initial ET iteration changes the epoch of a task. 
             * Other iterations only transfer HB info between tasks.
             */
            if(curFrame->iter == ET_BASE_ITER){
                int chain = oph->getChain(task, vc);
                int epoch = vc->incrementClock(chain);
                task->epoch = epoch;
                //reflect that the max clock value on 'chain' has changed
                oph->curClockOnChains->incrementClock(chain);
            }else{
                //indicate the iteration to most recntly modify sinkTask's HB info
                op->hbSinkTask->mod = curFrame->iter;
            }

            //sinkModified = true; - we need this only for ops that result in cross-thread middle-of-task HB

            #ifdef DBG_COMPUTEHB
                printf("\nopid %d  ", opId);
                postop->diagnosticPrint();
                vc->diagnosticPrint();
                egIt->second->diagnosticPrint();
            #endif
        }
    }else{
        PERROR("missing task found when processing IDLE-POST(%d,%u,%d).",
                op->tid, postop->event, postop->dest);
    }

    return sinkModified;

}

bool EventTrack::evaluatePostFoqOp(int opId, EtOperation* op){

    bool sinkModified = false;
    PostFoqOp* postop = dynamic_cast<PostFoqOp*>(op);
    if(op->task != NULL){
        EtTask* task = op->task;

        if(task->mod >= curFrame->iter){
            if(op->hbSinkTask == NULL){
                PERROR("Sink unknown for POST-FOQ(%d,%u,%d). Aborting HB computation.", op->tid, postop->event, postop->dest);
            }

            EventAwareVC::vcIterator vcIt = curFrame->evc->vcMap.find(op->taskId);
            VectorClock* vc = NULL;
            if(vcIt != curFrame->evc->vcMap.end()){
                vc = vcIt->second;
            }else{
                PERROR("Since task %d of POST-FOQ(%d,%u,%d) is modified by current or a higher iteration, its VC "
                       "should already have been initialized. Aborting as something has been "
                       "wrongly setup.", op->taskId, op->tid, postop->event, postop->dest);
            }

            //update or initialize VC of waiting thread (both are possible as some lifecycle related tasks are initialized by enable ops even prior to post)
            VectorClock* eventVC = NULL;
            EventAwareVC::vcIterator eventVcIt = curFrame->evc->vcMap.find(op->hbSinkTask->id);
            if(eventVcIt == curFrame->evc->vcMap.end()){
                eventVC = new VectorClock(*vc);
                curFrame->evc->vcMap.insert(std::make_pair(op->hbSinkTask->id, eventVC));
            }else{
                eventVC = eventVcIt->second;
                eventVC->vcJoin(vc);
            }

            EventAwareVC::egIterator egIt = curFrame->evc->eventGraph.find(op->taskId);
            EventGraphNode* egn = NULL;
           
            if(egIt == curFrame->evc->eventGraph.end()){
                PERROR("Since task %d of POST-FOQ(%d,%u,%d) is modified by current or a higher iteration, its EventGraphNode "
                       "should already have been initialized. Aborting as something has been "
                       "wrongly setup.", op->taskId, op->tid, postop->event, postop->dest);
            }
                
            egn = egIt->second;
            
            //update or initialize EventGraph node for wait thread
            EventGraphNode* eventEgn = NULL;
            EventAwareVC::egIterator eventEgIt = curFrame->evc->eventGraph.find(op->hbSinkTask->id);
            if(eventEgIt == curFrame->evc->eventGraph.end()){
                if(task-> eventId > 0){
                    eventEgn = new EventGraphNode();
                    EtList* parentNode = new EtList(task, KNOWN_BEGIN);
                    eventEgn->insertEdge(op->tid, parentNode);
                    eventEgn->insertEdgesAcrossThreads(egn);
                }else{
                    eventEgn = new EventGraphNode(*egn);
                }

                curFrame->evc->eventGraph.insert(std::make_pair(op->hbSinkTask->id, eventEgn));
            }else{
                eventEgn = eventEgIt->second;
                eventEgn->sparseJoinAcrossThreads(egn, eventVC, false, op->hbSinkTask->id, oph);
                if(task-> eventId > 0){
                    EtList* parentNode = new EtList(task, KNOWN_BEGIN);
                    eventEgn->insertEdge(op->tid, parentNode);
                }
            }

            EtList* sinkNode = new EtList(op->hbSinkTask, KNOWN_POST_FOQ);
            egn->insertEdge(postop->dest,sinkNode);

            //store taskIDs of event handlers posted using postFoQ or postNeg operations, and about whose
            //post the current operaiton is aware of but not about their dequeue.
            EtList* postFoqList = eventEgn->getCopyOfTypedIncomingEdgesForThread(postop->dest, KNOWN_POST_FOQ);
            while(postFoqList != NULL){
                postop->causalFoQPosts.insert(postFoqList->task->id);
                postFoqList = postFoqList->next;
            }

            EtList* postNegList = eventEgn->getCopyOfTypedIncomingEdgesForThread(postop->dest, KNOWN_NEG_POST);
            while(postNegList != NULL){
                postop->causalNegPosts.insert(postNegList->task->id);
                postNegList = postNegList->next;
            }
            //the above two types of posts are nt common. Hence evaluate PostFoq itself may not hit often.
            //Hence we are fine with not so optimized traversal of list to run the above two queries. 


            //postFoQ and postNeg may not be used to make environmental posts and hence may not feature in
            //any component lifecycle related logic. Keeping below code just for completeness.
            std::set<int>::iterator setIt = postop->tobeInitializedTasks.begin();
            for(; setIt != postop->tobeInitializedTasks.end(); setIt++){
                EventAwareVC::vcIterator targetVcIt = curFrame->evc->vcMap.find(*setIt);
                VectorClock* targetVc = NULL;
                if(targetVcIt == curFrame->evc->vcMap.end()){
                    targetVc = new VectorClock(*vc);
                    curFrame->evc->vcMap.insert(std::make_pair(*setIt, targetVc));
                }else{
                    targetVcIt->second->vcJoin(vc);
                    targetVc = targetVcIt->second;
                }

                EventAwareVC::egIterator targetEgIt = curFrame->evc->eventGraph.find(*setIt);
                EventGraphNode* targetEgn = NULL;
                if(targetEgIt == curFrame->evc->eventGraph.end()){
                    targetEgn = new EventGraphNode(*egn);
                    curFrame->evc->eventGraph.insert(std::make_pair(*setIt, targetEgn));
                }else{
                    targetEgn = targetEgIt->second;
                    targetEgn->sparseJoinAcrossThreads(egn, targetVc, false, *setIt, oph);
                }

                EtList* hbSrcNode = new EtList(op->hbSinkTask, KNOWN_POST_FOQ);
                targetEgn->insertEdge(postop->dest, hbSrcNode);

                if(task->eventId > 0){
                    hbSrcNode = new EtList(task, KNOWN_BEGIN);
                    targetEgn->insertEdge(op->tid, hbSrcNode);
                }

                if(curFrame->iter != ET_BASE_ITER){
                    EtTask* targetTask = dynamic_cast<EtTask*>(oph->taskMap.find(*setIt)->second);
                    targetTask->mod = curFrame->iter;
                }
            }


            /* only the initial ET iteration changes the epoch of a task. 
             * Other iterations only transfer HB info between tasks.
             */
            if(curFrame->iter == ET_BASE_ITER){
                int chain = oph->getChain(task, vc);
                int epoch = vc->incrementClock(chain);
                task->epoch = epoch;
                //reflect that the max clock value on 'chain' has changed
                oph->curClockOnChains->incrementClock(chain);
            }else{
                //indicate the iteration to most recntly modify sinkTask's HB info
                op->hbSinkTask->mod = curFrame->iter;
            }

            //sinkModified = true; - we need this only for ops that result in cross-thread middle-of-task HB

            #ifdef DBG_COMPUTEHB
                printf("\nopid %d  ", opId);
                postop->diagnosticPrint();
                vc->diagnosticPrint();
                egIt->second->diagnosticPrint();
            #endif
        }
    }else if(op->taskId == -1){
        #ifdef DBG_COMPUTEHB
            printf("found a NATIVE-POST-FOQ(%d,%u,%d). We will not be updating any HB info, but continue the"
               " HB computation.\n", op->tid, postop->event, postop->dest);
        #endif
        //we are fine with POST outside tasks, just that we will only be updating notified
        // task's HB info. Hence postop->hbSinkTask->mod will not change.
    }else{
        PERROR("missing task found when processing POST-FOQ(%d,%u,%d).",
                op->tid, postop->event, postop->dest);
    }

    return sinkModified;

}

bool EventTrack::evaluatePostNegOp(int opId, EtOperation* op){

    bool sinkModified = false;
    PostNegOp* postop = dynamic_cast<PostNegOp*>(op);
    if(op->task != NULL){
        EtTask* task = op->task;

        if(task->mod >= curFrame->iter){
            if(op->hbSinkTask == NULL){
                PERROR("Sink unknown for POST-NEG(%d,%u,%d). Aborting HB computation.", op->tid, postop->event, postop->dest);
            }

            EventAwareVC::vcIterator vcIt = curFrame->evc->vcMap.find(op->taskId);
            VectorClock* vc = NULL;
            if(vcIt != curFrame->evc->vcMap.end()){
                vc = vcIt->second;
            }else{
                PERROR("Since task %d of POST-NEG(%d,%u,%d) is modified by current or a higher iteration, its VC "
                       "should already have been initialized. Aborting as something has been "
                       "wrongly setup.", op->taskId, op->tid, postop->event, postop->dest);
            }

            //update or initialize VC of waiting thread (both are possible as some lifecycle related tasks are initialized by enable ops even prior to post)
            VectorClock* eventVC = NULL;
            EventAwareVC::vcIterator eventVcIt = curFrame->evc->vcMap.find(op->hbSinkTask->id);
            if(eventVcIt == curFrame->evc->vcMap.end()){
                eventVC = new VectorClock(*vc);
                curFrame->evc->vcMap.insert(std::make_pair(op->hbSinkTask->id, eventVC));
            }else{
                eventVC = eventVcIt->second;
                eventVC->vcJoin(vc);
            }

            EventAwareVC::egIterator egIt = curFrame->evc->eventGraph.find(op->taskId);
            EventGraphNode* egn = NULL;
           
            if(egIt == curFrame->evc->eventGraph.end()){
                PERROR("Since task %d of POST-NEG(%d,%u,%d) is modified by current or a higher iteration, its EventGraphNode "
                       "should already have been initialized. Aborting as something has been "
                       "wrongly setup.", op->taskId, op->tid, postop->event, postop->dest);
            }
                
            egn = egIt->second;
            
            //update or initialize EventGraph node for wait thread
            EventGraphNode* eventEgn = NULL;
            EventAwareVC::egIterator eventEgIt = curFrame->evc->eventGraph.find(op->hbSinkTask->id);
            if(eventEgIt == curFrame->evc->eventGraph.end()){
                if(task-> eventId > 0){
                    eventEgn = new EventGraphNode();
                    EtList* parentNode = new EtList(task, KNOWN_BEGIN);
                    eventEgn->insertEdge(op->tid, parentNode);
                    eventEgn->insertEdgesAcrossThreads(egn);
                }else{
                    eventEgn = new EventGraphNode(*egn);
                }

                curFrame->evc->eventGraph.insert(std::make_pair(op->hbSinkTask->id, eventEgn));
            }else{
                eventEgn = eventEgIt->second;
                eventEgn->sparseJoinAcrossThreads(egn, eventVC, false, op->hbSinkTask->id, oph);
                if(task-> eventId > 0){
                    EtList* parentNode = new EtList(task, KNOWN_BEGIN);
                    eventEgn->insertEdge(op->tid, parentNode);
                }
            }

            EtList* sinkNode = new EtList(op->hbSinkTask, KNOWN_NEG_POST);
            egn->insertEdge(postop->dest,sinkNode);

            //store taskIDs of event handlers posted using postFoQ or postNeg operations, and about whose
            //post the current operaiton is aware of but not about their dequeue.
            EtList* postFoqList = eventEgn->getCopyOfTypedIncomingEdgesForThread(postop->dest, KNOWN_POST_FOQ);
            while(postFoqList != NULL){
                postop->causalFoQPosts.insert(postFoqList->task->id);
                postFoqList = postFoqList->next;
            }

            EtList* postNegList = eventEgn->getCopyOfTypedIncomingEdgesForThread(postop->dest, KNOWN_NEG_POST);
            while(postNegList != NULL){
                postop->causalNegPosts.insert(postNegList->task->id);
                postNegList = postNegList->next;
            }


            //postFoQ and postNeg may not be used to make environmental posts and hence may not feature in
            //any component lifecycle related logic. Keeping below code just for completeness.
            std::set<int>::iterator setIt = postop->tobeInitializedTasks.begin();
            for(; setIt != postop->tobeInitializedTasks.end(); setIt++){
                EventAwareVC::vcIterator targetVcIt = curFrame->evc->vcMap.find(*setIt);
                VectorClock* targetVc = NULL;
                if(targetVcIt == curFrame->evc->vcMap.end()){
                    targetVc = new VectorClock(*vc);
                    curFrame->evc->vcMap.insert(std::make_pair(*setIt, targetVc));
                }else{
                    targetVcIt->second->vcJoin(vc);
                    targetVc = targetVcIt->second;
                }

                EventAwareVC::egIterator targetEgIt = curFrame->evc->eventGraph.find(*setIt);
                EventGraphNode* targetEgn = NULL;
                if(targetEgIt == curFrame->evc->eventGraph.end()){
                    targetEgn = new EventGraphNode(*egn);
                    curFrame->evc->eventGraph.insert(std::make_pair(*setIt, targetEgn));
                }else{
                    targetEgn = targetEgIt->second;
                    targetEgn->sparseJoinAcrossThreads(egn, targetVc, false, *setIt, oph);
                }

                EtList* hbSrcNode = new EtList(op->hbSinkTask, KNOWN_NEG_POST);
                targetEgn->insertEdge(postop->dest, hbSrcNode);

                if(task->eventId > 0){
                    hbSrcNode = new EtList(task, KNOWN_BEGIN);
                    targetEgn->insertEdge(op->tid, hbSrcNode);
                }

                if(curFrame->iter != ET_BASE_ITER){
                    EtTask* targetTask = dynamic_cast<EtTask*>(oph->taskMap.find(*setIt)->second);
                    targetTask->mod = curFrame->iter;
                }
            }


            /* only the initial ET iteration changes the epoch of a task. 
             * Other iterations only transfer HB info between tasks.
             */
            if(curFrame->iter == ET_BASE_ITER){
                int chain = oph->getChain(task, vc);
                int epoch = vc->incrementClock(chain);
                task->epoch = epoch;
                //reflect that the max clock value on 'chain' has changed
                oph->curClockOnChains->incrementClock(chain);
            }else{
                //indicate the iteration to most recntly modify sinkTask's HB info
                op->hbSinkTask->mod = curFrame->iter;
            }

            //sinkModified = true; - we need this only for ops that result in cross-thread middle-of-task HB

            #ifdef DBG_COMPUTEHB
                printf("\nopid %d  ", opId);
                postop->diagnosticPrint();
                vc->diagnosticPrint();
                egIt->second->diagnosticPrint();
            #endif
        }
    }else{
        PERROR("missing task found when processing POST-NEG(%d,%u,%d).",
                op->tid, postop->event, postop->dest);
    }

    return sinkModified;

}

bool EventTrack::evaluateEnableTimerTaskOp(int opId, EtOperation* op){

    bool sinkModified = false;
    EnableTimerTaskOp* enableop = dynamic_cast<EnableTimerTaskOp*>(op);
    if(enableop->shouldStore){ //only if this enable op is indicated to be stored process it, else skip.

        if(op->task != NULL){
            EtTask* task = op->task;
    
            if(task->mod >= curFrame->iter){
                EventAwareVC::vcIterator vcIt = curFrame->evc->vcMap.find(op->taskId);
                VectorClock* vc = NULL;
                if(vcIt != curFrame->evc->vcMap.end()){
                    vc = vcIt->second;
                }else{
                    PERROR("Since task %d of ENABLE_TIMER_TASK(%u) is modified by current or a higher iteration, its VC "
                           "should already have been initialized. Aborting as something has been "
                           "wrongly setup.", op->taskId, enableop->instance);
                }
    
                EventAwareVC::egIterator egIt = curFrame->evc->eventGraph.find(op->taskId);
                EventGraphNode* egn = NULL;
                if(egIt != curFrame->evc->eventGraph.end()){
                    egn = egIt->second;
                }else{
                    PERROR("Since task %d of ENABLE_TIMER_TASK(%u) is modified by current or a higher iteration, its EventGraph "
                           "should already have been initialized. Aborting as something has been "
                           "wrongly setup.", op->taskId, enableop->instance);
                }
    
                storeHBInfo(opId, vc, egn);
    
                /* only the initial ET iteration changes the epoch of a task. 
                 * Other iterations only transfer HB info between tasks.
                 */
                if(curFrame->iter == ET_BASE_ITER){
                    int chain = oph->getChain(task, vc);
                    int epoch = vc->incrementClock(chain);
                    task->epoch = epoch;
                    //reflect that the max clock value on 'chain' has changed
                    oph->curClockOnChains->incrementClock(chain);
                }
    
                //sinkModified = true; - we need this only for ops that result in cross-thread middle-of-task HB

                #ifdef DBG_COMPUTEHB
                    printf("\nopid %d  ", opId);
                    enableop->diagnosticPrint();
                    vc->diagnosticPrint();
                    egIt->second->diagnosticPrint();
                #endif
            }
        }else{
            PERROR("missing task found when processing ENABLE_TIMER_TASK(%u).",
                    enableop->instance);
        }

    }else{
        //not a serious error but worth informing
        #ifdef DBG_COMPUTEHB
        printf("The preprocessor has stored ENABLE_TIMER_TASK(%u) with opId %d even though its shouldStore is false.", 
                enableop->instance, opId);
        #endif
    }

    return sinkModified;

}

bool EventTrack::evaluateTriggerTimerTaskOp(int opId, EtOperation* op){

    bool sinkModified = false;
    TriggerTimerTaskOp* triggerop = dynamic_cast<TriggerTimerTaskOp*>(op);

    if(triggerop->enableOpId > 0){
        std::map<int, std::pair<VectorClock*, EventGraphNode*> >::iterator evcIt =
                curFrame->evcStore.find(triggerop->enableOpId);
        if(evcIt != curFrame->evcStore.end()){ 
            /* TriggerOp's VC timestamp gets modified only if enable has been updated
             * in this iteration. It does not matter whether trigger's task itself has been 
             * updated by this iteration yet.
             */
            if(op->task != NULL){
                EtTask* task = op->task;

                EventAwareVC::vcIterator vcIt = curFrame->evc->vcMap.find(op->taskId);
                VectorClock* vc = NULL;
                if(vcIt != curFrame->evc->vcMap.end()){
                    vc = vcIt->second;
                }else{
                    vc = new VectorClock();
                    curFrame->evc->vcMap.insert(std::make_pair(op->taskId, vc));
                }
                vc->vcJoin(evcIt->second.first); //take join with enable's stored VC timestamp
    
                EventAwareVC::egIterator egIt = curFrame->evc->eventGraph.find(op->taskId);
                EventGraphNode* egn = NULL;
                if(egIt == curFrame->evc->eventGraph.end()){
                    egn = new EventGraphNode();
                    curFrame->evc->eventGraph.insert(std::make_pair(op->taskId, egn));
                }else{
                    egn = egIt->second;   
                }
                egn->sparseJoinAcrossThreads(evcIt->second.second, vc, false, op->taskId, oph);
                
                if(triggerop->hbSrcTask->id > 0){
                    if(triggerop->hbSrcTask->eventId > 0){
                        EtList* parentNode = new EtList(triggerop->hbSrcTask, KNOWN_BEGIN);
                        egn->insertEdge(triggerop->hbSrcTask->tid, parentNode);
                    }
                }else{
                    PERROR("missing taskId for hbSrc (enable's task) of TRIGGER_TIMER_TASK(%u).", triggerop->instance);
                }
    
                //TRIGGER does not result in change of epoch of a task
 
                task->mod = curFrame->iter; //this is essential in case this is the first op in this task to be updated by current iteration
                sinkModified = true;

                #ifdef DBG_COMPUTEHB
                    printf("\nopid %d  ", opId);
                    triggerop->diagnosticPrint();
                    vc->diagnosticPrint();
                    egIt->second->diagnosticPrint();
                #endif

            }else{
                PERROR("missing task found when processing TRIGGER_TIMER_TASK(%u).", 
                        triggerop->instance);
            }
        }

    }else{
            //we dont consider this a very serious error but worth informing.
            //Also this happens when enable and trigger timer task are on the same thread.
            #ifdef DBG_COMPUTEHB
                printf("Missing enableOpId corresponding to TRIGGER_TIMER_TASK(%u).", triggerop->instance);
            #endif
    }

    return sinkModified;

}

void EventTrack::storeHBInfo(int opId, VectorClock* vc, EventGraphNode* egn){
    VectorClock* vcToStore = new VectorClock(*vc);
    EventGraphNode* egnToStore = new EventGraphNode(*egn);
    curFrame->evcStore.insert(std::make_pair(opId, std::make_pair(vcToStore, egnToStore)));
}



void EventTrack::initializeEvaluateFunctionPointers(){

    evaluateFuncPtr[DR_POST] = &EventTrack::evaluatePostOp;
    evaluateFuncPtr[DR_AT_TIME_POST] = &EventTrack::evaluateAtTimePostOp;
    evaluateFuncPtr[DR_BEGIN] = &EventTrack::evaluateBeginOp;
    evaluateFuncPtr[DR_END] = &EventTrack::evaluateEndOp;
//    evaluateFuncPtr[DR_ATTACH_Q] = &EventTrack::evaluateAttachqOp;
    evaluateFuncPtr[DR_FORK] = &EventTrack::evaluateForkOp;
//    evaluateFuncPtr[DR_JOIN] = &EventTrack::evaluateJoinOp; //handled by notify - wait
    evaluateFuncPtr[DR_THREADINIT] = &EventTrack::evaluateThreadinitOp;
    evaluateFuncPtr[DR_THREADEXIT] = &EventTrack::evaluateThreadexitOp;
//    evaluateFuncPtr[DR_REMOVE_EVENT] = &EventTrack::evaluateRemoveEventOp;
    evaluateFuncPtr[DR_LOOP] = &EventTrack::evaluateLoopOp;
    evaluateFuncPtr[DR_LOOP_EXIT] = &EventTrack::evaluateLoopExitOp;
    evaluateFuncPtr[DR_ENABLE_EVENT] = &EventTrack::evaluateEnableEventOp;
    evaluateFuncPtr[DR_TRIGGER_EVENT] = &EventTrack::evaluateTriggerEventOp;
    evaluateFuncPtr[DR_ENABLE_LIFECYCLE] = &EventTrack::evaluateEnableActLifecycleOp;
    evaluateFuncPtr[DR_TRIGGER_LIFECYCLE] = &EventTrack::evaluateTriggerActLifecycleOp;
    evaluateFuncPtr[DR_TRIGGER_RECEIVER] = &EventTrack::evaluateTriggerReceiverOp;
    evaluateFuncPtr[DR_TRIGGER_SERVICE] = &EventTrack::evaluateTriggerServiceOp;
//    evaluateFuncPtr[DR_INSTANCE_INTENT] = &EventTrack::evaluateInstanceIntentOp;
    evaluateFuncPtr[DR_ENABLE_WINDOW_FOCUS] = &EventTrack::evaluateEnableWindowFocusOp;
    evaluateFuncPtr[DR_TRIGGER_WINDOW_FOCUS] = &EventTrack::evaluateTriggerWindowFocusOp;
    evaluateFuncPtr[DR_WAIT] = &EventTrack::evaluateWaitOp;
    evaluateFuncPtr[DR_NOTIFY] = &EventTrack::evaluateNotifyOp;
//    evaluateFuncPtr[DR_NATIVE_POST] = &EventTrack::evaluateNativePostOp; //handled as DR_POST
//    evaluateFuncPtr[DR_UI_POST] = &EventTrack::evaluateUiPostOp; //handled as DR_POST
    evaluateFuncPtr[DR_IDLE_POST] = &EventTrack::evaluateIdlePostOp;
//    evaluateFuncPtr[DR_NATIVE_NOTIFY] = &EventTrack::evaluateNativeNotifyOp; //handled as NOTIFY
    evaluateFuncPtr[DR_POST_FOQ] = &EventTrack::evaluatePostFoqOp;
//    evaluateFuncPtr[DR_NATIVE_POST_FOQ] = &EventTrack::evaluateNativePostFoqOp; //handled as POST_FOQ
    evaluateFuncPtr[DR_POST_NEG] = &EventTrack::evaluatePostNegOp;
    evaluateFuncPtr[DR_ENABLE_TIMER_TASK] = &EventTrack::evaluateEnableTimerTaskOp;
    evaluateFuncPtr[DR_TRIGGER_TIMER_TASK] = &EventTrack::evaluateTriggerTimerTaskOp;

}


void EventTrack::computeHB(){
    
    OpHelper::opMapIterator opIt = oph->opMap.begin();
    if(oph->opMap.empty()){
        PERROR("Operations map is empty. Hence, no trace available to compute HB. Aborting HB computation");
    }

    initializeEvaluateFunctionPointers();

    
    while(opIt != oph->opMap.end()){
        curFrame->index = opIt->first;
        EtOperation* op = dynamic_cast<EtOperation*>(opIt->second);

        //evaluate operation
        bool sinkModified = (this->*evaluateFuncPtr[op->getTypeId()])(curFrame->index, op);

        if(curFrame->index == curFrame->prevIterIndex){ //this will never hold for curFrame = baseFrame
            EtIterInfo* tempFrame = curFrame->prev; 
            tempFrame->evc->joinEVCWith(curFrame->evc, oph, true);

            std::map<int, std::pair<VectorClock*, EventGraphNode*> >::iterator srcEvcIt = 
                    curFrame->evcStore.begin();
            for(; srcEvcIt != curFrame->evcStore.end(); ){
                VectorClock* curVc = srcEvcIt->second.first;
                EventGraphNode* curEgn = srcEvcIt->second.second;
                
                std::map<int, std::pair<VectorClock*, EventGraphNode*> >::iterator destEvcIt =
                    tempFrame->evcStore.find(srcEvcIt->first);
                if(destEvcIt != tempFrame->evcStore.end()){
                    //update VC of destination map
                    destEvcIt->second.first->vcJoin(curVc);
                    delete curVc; //we dont need this anymore
                    
                    destEvcIt->second.second->sparseJoinAcrossThreads(curEgn, destEvcIt->second.first, 
                            true, srcEvcIt->first, oph); //reuse the srcGraphNodes as the source wont need it anymore
                }else{
                    srcEvcIt->second.first = NULL;
                    srcEvcIt->second.second = NULL; 

                    tempFrame->evcStore.insert(std::make_pair(srcEvcIt->first, std::make_pair(curVc, curEgn)));
                }

                curFrame->evcStore.erase(srcEvcIt++);
                 
            }

            delete curFrame;
            curFrame = tempFrame;
            curFrame->next = NULL;
        }

        if(sinkModified){
            EtTask* sinkTask = NULL;
            
            if(op->getTypeId() == DR_NOTIFY){
                if(op->hbSinkTask != NULL){
                    sinkTask = op->hbSinkTask;
                }else{
                    PERROR("Notify operation with opid %d on tid %d is missing a sink task ID.", 
                            curFrame->index, op->tid);
                }
            }else if(op->getTypeId() == DR_TRIGGER_WINDOW_FOCUS){
                sinkTask = op->task;
            }else if(op->getTypeId() == DR_TRIGGER_LIFECYCLE){
                TriggerActLifecycleOp* actOp = dynamic_cast<TriggerActLifecycleOp*>(op);
                if(!actOp->envOrderedEnableOps.empty()){
                    sinkTask = op->task;
                }
            }else if(op->getTypeId() == DR_TRIGGER_TIMER_TASK){
                sinkTask = op->task;
            }

            if(sinkTask != NULL && sinkTask->eventId > 0){
                    EventAwareVC::egIterator egIt = curFrame->evc->eventGraph.find(sinkTask->id);            
                    if(egIt != curFrame->evc->eventGraph.end()){
                        //get incoming edges whose dequeue is known - find candidates which can trigger EV-ATOMIC rule
                        EtList* incEdges = egIt->second->getCopyOfTypedIncomingEdgesForThread(sinkTask->tid, KNOWN_BEGIN);
                        if(incEdges != NULL && curFrame != baseFrame){
                            //We dont have to collect stats for incomingEdgeCount here as it has already been counted
                            //in getCopyOfTypedIncomingEdgesForThread.

                            std::map<int, std::pair<int, u4> > taskIdToEpochMap;
                            std::map<int, std::pair<int, u4> >::iterator taskEpochIt;
                            EtList* tempList = incEdges;
                            while(tempList != NULL){
                                taskIdToEpochMap.insert(std::make_pair(tempList->task->id, tempList->task->getEpoch()));
                                
                                tempList = tempList->next;
                            }                         

                            EtIterInfo* tempItrInfo = baseFrame;
                            while(tempItrInfo != curFrame && !taskIdToEpochMap.empty()){

                                EventAwareVC::vcIterator vcIt = tempItrInfo->evc->vcMap.find(sinkTask->id); 
                                if(vcIt != tempItrInfo->evc->vcMap.end()){
                                    for(taskEpochIt = taskIdToEpochMap.begin(); taskEpochIt != taskIdToEpochMap.end(); ){
                                        if(vcIt->second->doesSubsumeClock(taskEpochIt->second.first, taskEpochIt->second.second)){
                                            taskIdToEpochMap.erase(taskEpochIt++);
                                        }else{
                                            taskEpochIt++;
                                        }
                                    } 
                                }

                                tempItrInfo = tempItrInfo->next;
                            }

                            tempList = incEdges;
                            while(tempList != NULL){
                                if(taskIdToEpochMap.find(tempList->task->id) == taskIdToEpochMap.end()){
                                    EtList* toDeleteNode = tempList;
                                    if(tempList->prev != NULL){
                                        tempList->prev->next = tempList->next;
                                    }else{
                                        incEdges = tempList->next;
                                    }

                                    if(tempList->next != NULL){
                                        tempList->next->prev = tempList->prev;
                                    }

                                    tempList = tempList->next;
                                    toDeleteNode->next = NULL;
                                    delete toDeleteNode;
                                }else{
                                    tempList = tempList->next;
                                }
                            }
                        }

                        //At this point incEdges finally represents the set h in EventTrack algorithm
                        if(incEdges != NULL){
                            EtIterInfo* newIterInfo = new EtIterInfo(++fp, sinkTask->beginOpId, curFrame->index, curFrame);

                            //need to reprocess from begin of sinkTask. Reset iterator.
                            opIt = oph->opMap.find(sinkTask->beginOpId);
                            if(opIt == oph->opMap.end()){
                                PERROR("missing entry in opMap corresponding to BEGIN operation %d detected when initializing" 
                                       " a new EventTrack iteration %d.", sinkTask->beginOpId, fp-1);
                            }
                            
                            /* For this iteration we can forget all we have computed in the past & only need to
                             * provide info to facilitate application of EV-ATOMIC rule on sinkTask, so as to
                             * trigger reprocessing.
                             */
                            EventGraphNode* initialEgNode = new EventGraphNode();
                            initialEgNode->insertEdge(sinkTask->tid, incEdges);
                            newIterInfo->evc->eventGraph.insert(std::make_pair(sinkTask->id, initialEgNode));
                            VectorClock* initialVC = new VectorClock();
                            newIterInfo->evc->vcMap.insert(std::make_pair(sinkTask->id, initialVC));
                            
                            curFrame->next = newIterInfo;
                            curFrame = newIterInfo;

                            /* also indicate that sinkTask has been updated by the current iteration, as its HB info have 
                             * been initialized for this iteration.
                             */
                            sinkTask->mod = curFrame->iter;

                            continue; //we do not want to change oIt info
                        }
                    }else{
                        PERROR("missing eventgraph node for task %d which is indicated as modified by opId %d "
                               "in EventTrack iteration %d", sinkTask->id, curFrame->index, curFrame->iter);
                    }
            }

        }

        opIt++;
    }

    #ifdef DBG_COLLECT_STATS
        if(curFrame->iter == ET_BASE_ITER){
            OpHelper::taskMapIterator taskIt = oph->taskMap.begin();
            for(; taskIt != oph->taskMap.end(); taskIt++){
                EventAwareVC::vcIterator vcIt = curFrame->evc->vcMap.find((dynamic_cast<EtTask*>(taskIt->second))->id);
                if(vcIt != curFrame->evc->vcMap.end()){
                    Stats::vcMemory += sizeof(VectorClock) + (vcIt->second->getVCSize() * sizeof(u4));
                }else{
                    PERROR("Task %d has no VC assigned!", (dynamic_cast<EtTask*>(taskIt->second))->id);
                }
            }

            std::map<int, std::pair<VectorClock*, EventGraphNode*> >::iterator evcIt = curFrame->evcStore.begin();
            for(; evcIt != curFrame->evcStore.end(); evcIt++){
                Stats::vcMemory += sizeof(VectorClock) + (evcIt->second.first->getVCSize() * sizeof(u4));
            }
        }
    #endif
}


}
