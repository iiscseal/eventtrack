/*
 * Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <eventgraph.h>

namespace etrack{

EtList::EtList(){}

EtList::EtList(EtTask* task, edgeFlagType lbl) : task(task), lbl(lbl) {
    prev = NULL;
    next = NULL;
  
   /* #ifdef DBG_COLLECT_STATS
        Stats::egEdgeCount++;
    #endif*/

}

EtList::EtList(const EtList& srcList) : task(srcList.task), lbl(srcList.lbl){
    #ifdef DBG_COLLECT_STATS
        Stats::graphComputationCount++;
        Stats::egEdgeCount++;
    #endif

    if(srcList.next == NULL){
        next = NULL;
    }else{
        next = new EtList(*srcList.next);
        next->prev = this;
    }

    if(srcList.prev == NULL){
        prev = NULL;
    }
}

EtList::~EtList(){
    #ifdef DBG_COLLECT_STATS
        Stats::egDeletedEdgeCount++;
    #endif
}


/* assume the current list and etlist is sorted (descending order of task->id)
 * etlist is assumed to be the head of the list i.e. we dont care about its prev
 * returns NULL if head of current list is not modifed.
 * returns new HEAD if it is modified.
 * Arg modifyInput indicates whether we can modify input etlist or not. 
 * If not, have to create new nodes.
 *
 */
EtList* EtList::insert(EtList* etlist, bool modifyInput){
    if(etlist != NULL){
       #ifdef DBG_COLLECT_STATS
           Stats::graphComputationCount++;
       #endif

       if(etlist->task->id == task->id){
            if(etlist->lbl > lbl){
                lbl = etlist->lbl;
            }
            if(modifyInput){
                EtList* temp = etlist;
                etlist = etlist->next;
                temp->next = NULL;
                delete temp;
                this->insert(etlist,modifyInput);
            }else{
                this->insert(etlist->next, modifyInput);
            }
            return NULL; //since etlist is sorted, etlist->next->id <= this->id
        }else if(etlist->task->id > task->id){
            if(prev != NULL){
                if(modifyInput){
                    prev->next = etlist;
                    etlist->prev = prev;
                    EtList* temp = etlist->next;
                    etlist->next = this;
                    prev = etlist;
                    this->insert(temp, modifyInput);
                }else{ //create new node and insert it
                    #ifdef DBG_COLLECT_STATS
                        Stats::egEdgeCount++;
                    #endif
                    EtList* newNode = new EtList(etlist->task, etlist->lbl);
                    prev->next = newNode;
                    newNode->prev = prev;
                    newNode->next = this;
                    prev = newNode;
                    this->insert(etlist->next, modifyInput);
                }
                return NULL; //this is a dummy return which returns only after children have returned.
            }else{ //we will be modifying HEAD of current list 
                if(modifyInput){
                    etlist->prev = prev; //in this case prev is NULL * etlist is the new HEAD
                    EtList* temp = etlist->next;
                    etlist->next = this;
                    prev = etlist; //this is no more HEAD, so its prev becomes non-null
                    this->insert(temp, modifyInput);
                    return etlist;
                }else{ //create new node and insert it
                    #ifdef DBG_COLLECT_STATS
                        Stats::egEdgeCount++;
                    #endif
                    EtList* newNode = new EtList(etlist->task, etlist->lbl);
                    newNode->prev = prev; //in this case prev is NULL * etlist is the enw HEAD
                    newNode->next = this;
                    prev = newNode; //this is no more HEAD, so its prev becomes non-null
                    this->insert(etlist->next, modifyInput);
                    return newNode;
                }
            } 
        }else{ //etlist->task->id < this->task->id
            if(next != NULL){
                next->insert(etlist,modifyInput);
            }else{
                if(modifyInput){
                    etlist->prev = this;
                    next = etlist;
                }else{
                    #ifdef DBG_COLLECT_STATS
                        Stats::egEdgeCount++;
                    #endif
                    EtList* newNode = new EtList(etlist->task, etlist->lbl);
                    newNode->prev = this;
                    next = newNode; //make current node's next non-null
                    //newNode->next is null by initialization
                    newNode->insert(etlist->next, modifyInput);             
                }
            }
            return NULL; //since HEAD of current list will not be modified
        }
    }else{ //etlist is null
        return NULL;
    }
}

//used to insert only one node i.e. etlist->next = NULL
EtList* EtList::optimizedInsert(EtList* etlist, bool modifyInput){
    if(etlist != NULL && etlist->next == NULL){
       #ifdef DBG_COLLECT_STATS
           Stats::graphComputationCount++;
       #endif

       if(etlist->task->id == task->id){
            if(etlist->lbl > lbl){
                lbl = etlist->lbl;
            }
            if(modifyInput){
                delete etlist;
            }
            return NULL; //since etlist is sorted, etlist->next->id <= this->id
        }else if(etlist->task->id > task->id){
            if(prev != NULL){
                if(modifyInput){
                    bool inserted = false;
                    if(lbl == etlist->lbl){
                        if(lbl == KNOWN_POST){
                            if(dynamic_cast<PostOp*>(etlist->task->postOp)->delay == dynamic_cast<PostOp*>(task->postOp)->delay){
                                inserted = true;
                                task = etlist->task;
                            }
                        }else if(lbl == KNOWN_BROADCAST){
                            inserted = true;
                            task = etlist->task;
                        }else if(lbl == KNOWN_IDLE_POST){
                            inserted = true;
                            task = etlist->task;
                        }
                    }
                    if(inserted){
                        delete etlist;
                        return NULL;
                    }

                    prev->next = etlist;
                    etlist->prev = prev;
                    etlist->next = this;
                    prev = etlist;
                }else{ //create new node and insert it
                    bool inserted = false;
                    if(lbl == etlist->lbl){
                        if(lbl == KNOWN_POST){
                            if(dynamic_cast<PostOp*>(etlist->task->postOp)->delay == dynamic_cast<PostOp*>(task->postOp)->delay){
                                inserted = true;
                                task = etlist->task;
                            } 
                        }else if(lbl == KNOWN_BROADCAST){
                            inserted = true;
                            task = etlist->task;
                        }else if(lbl == KNOWN_IDLE_POST){
                            inserted = true;
                            task = etlist->task;
                        }
                    }
                    if(inserted){
                        return NULL;
                    }

                    #ifdef DBG_COLLECT_STATS
                        Stats::egEdgeCount++;
                    #endif
                    EtList* newNode = new EtList(etlist->task, etlist->lbl);
                    prev->next = newNode;
                    newNode->prev = prev;
                    newNode->next = this;
                    prev = newNode;
                }
                return NULL; //this is a dummy return which returns only after children have returned.
            }else{ //we will be modifying HEAD of current list 
                if(modifyInput){
                    bool inserted = false;
                    if(lbl == etlist->lbl){
                        if(lbl == KNOWN_POST){
                            if(dynamic_cast<PostOp*>(etlist->task->postOp)->delay == dynamic_cast<PostOp*>(task->postOp)->delay){
                                inserted = true;
                                task = etlist->task;
                            }
                        }else if(lbl == KNOWN_BROADCAST){
                            inserted = true;
                            task = etlist->task;
                        }else if(lbl == KNOWN_IDLE_POST){
                            inserted = true;
                            task = etlist->task;
                        }
                    }
                    if(inserted){
                        delete etlist;
                        return NULL; //becaue we did not change the pointer, just replaced some field values
                    }

                    etlist->prev = prev; //in this case prev is NULL * etlist is the new HEAD
                    etlist->next = this;
                    prev = etlist; //this is no more HEAD, so its prev becomes non-null
                    return etlist;
                }else{ //create new node and insert it
                    bool inserted = false;
                    if(lbl == etlist->lbl){
                        if(lbl == KNOWN_POST){
                            if(dynamic_cast<PostOp*>(etlist->task->postOp)->delay == dynamic_cast<PostOp*>(task->postOp)->delay){
                                inserted = true;
                                task = etlist->task;
                            }
                        }else if(lbl == KNOWN_BROADCAST){
                            inserted = true;
                            task = etlist->task;
                        }else if(lbl == KNOWN_IDLE_POST){
                            inserted = true;
                            task = etlist->task;
                        }
                    }
                    if(inserted){
                        return NULL; //becaue we did not change the pointer, just replaced some field values
                    }

                    #ifdef DBG_COLLECT_STATS
                        Stats::egEdgeCount++;
                    #endif
                    EtList* newNode = new EtList(etlist->task, etlist->lbl);
                    newNode->prev = prev; //in this case prev is NULL * etlist is the new HEAD
                    newNode->next = this;
                    prev = newNode; //this is no more HEAD, so its prev becomes non-null
                    return newNode;
                }
            } 
        }else{ //etlist->task->id < this->task->id
            if(next != NULL){
                next->optimizedInsert(etlist,modifyInput);
            }else{
                if(modifyInput){
                    etlist->prev = this;
                    next = etlist;
                }else{
                    #ifdef DBG_COLLECT_STATS
                        Stats::egEdgeCount++;
                    #endif
                    EtList* newNode = new EtList(etlist->task, etlist->lbl);
                    newNode->prev = this;
                    next = newNode; //make current node's next non-null
                    //newNode->next is null by initialization
                }
            }
            return NULL; //since HEAD of current list will not be modified
        }
    }else{ //etlist is null
        PERROR("optimized insert called with null list or with etlist->next being non-null. Cannot continue further.");
        return NULL;
    }
}

void EtList::diagnosticPrint(){
    printf("node ID:%d, lbl:%d, this:%p  next:%p,  prev:%p \n", task->id, lbl, this, next, prev );
    if(next != NULL){
        next->diagnosticPrint();
    }
}


EventGraphNode::EventGraphNode(){
}

EventGraphNode::~EventGraphNode(){
    std::map<int, EtList*>::iterator it = incomingEdges.begin();
    for(; it != incomingEdges.end(); it++){
        delete it->second;
    }
}

EventGraphNode::EventGraphNode(const EventGraphNode& srcEgn){
    std::map<int, EtList*>::const_iterator egIt = srcEgn.incomingEdges.begin();
    for(; egIt != srcEgn.incomingEdges.end(); egIt++){
        if(egIt->second != NULL){
            EtList* newList = new EtList(*(egIt->second));  
            incomingEdges.insert(std::make_pair(egIt->first, newList));
        }          
    }

}

//returns NULL if there are no incoming edges from tasks on this input thread.
EtList* EventGraphNode::getIncomingEdgesForThread(int tid){
    std::map<int, EtList*>::iterator it = incomingEdges.find(tid);
    if(it != incomingEdges.end()){
        return it->second;
    }else{
        return NULL;
    }
}

/* returns EtList nodes matching a label in set of labels, and a new copy of all the identified nodes are 
 * created and returned. */
EtList* EventGraphNode::getCopyOfTypedIncomingEdgesForThread(int tid, std::set<edgeFlagType> lblSet){
    std::map<int, EtList*>::iterator it = incomingEdges.find(tid);
    if(it != incomingEdges.end()){
        EtList* resHead = NULL;
        EtList* prev = NULL;
        EtList* temp = it->second;
        while(temp != NULL){
            #ifdef DBG_COLLECT_STATS
                Stats::graphComputationCount++;
                //we call this function to compute some rule-specific candidate set. Hence we should report the count.
                Stats::incomingEdgesCount++;
            #endif

            if(lblSet.find(temp->lbl) != lblSet.end()){
                EtList* newNode = new EtList(temp->task,temp->lbl);
                newNode->prev = prev;
                newNode->next = NULL;
                if(prev == NULL){
                    resHead = newNode;
                }else{
                    prev->next = newNode;
                }
                prev = newNode;
            }

            temp = temp->next;
        }

        return resHead;
      
    }else{
        return NULL;
    }
}



/* returns EtList nodes matching the lbl and a new copy of all the identified nodes are 
 * created and returned. */
EtList* EventGraphNode::getCopyOfTypedIncomingEdgesForThread(int tid, edgeFlagType lbl){
    std::map<int, EtList*>::iterator it = incomingEdges.find(tid);
    if(it != incomingEdges.end()){
        EtList* resHead = NULL;
        EtList* prev = NULL;
        EtList* temp = it->second;
        while(temp != NULL){
            #ifdef DBG_COLLECT_STATS
                Stats::graphComputationCount++;
            #endif

            if(temp->lbl == lbl){
                EtList* newNode = new EtList(temp->task,temp->lbl);
                newNode->prev = prev;
                newNode->next = NULL;
                if(prev == NULL){
                    resHead = newNode;
                }else{
                    prev->next = newNode;
                }
                prev = newNode;
            }

            temp = temp->next;
        }

        return resHead;
      
    }else{
        return NULL;
    }
}

void EventGraphNode::insertEdgeUnmodified(int tid, EtList* src){
    std::map<int, EtList*>::iterator it = incomingEdges.find(tid);
    if(it != incomingEdges.end()){
        if(it->second != NULL){
            EtList* newHead = it->second->insert(src, false);
            //head of incomingEdges list has changed only if insert returns non-NULL
            if(newHead != NULL){
                it->second = newHead;
            }
        }else{
            //this scenario should not be hit as per the way we have set up this datastructure.
            printf("In EventGraphNode %p thread %d has a NULL head and yet this thread entry is not removed.",
                        this, tid);
            //we cannot modify input src. Hence create a new list
            EtList* newList = new EtList(*src);
            newList->prev = NULL; //to maintain well-formedness, if this was not the case
            it->second = newList;
        }
    }else{
        //we cannot modify input src. Hence create a new list
        EtList* newList = new EtList(*src);
        newList->prev = NULL; //to maintain well-formedness, if this was not the case
        incomingEdges.insert(std::make_pair(tid, newList));
    }
}

/* typically used to insert only one edge. The input src will be directly
 * inserted without creating any new node. Also, the last node in the list 
 * should have NULL as next and the first node should have NULL in prev,
 * to maintain well-formedness.
 */
void EventGraphNode::insertEdge(int tid, EtList* src){
    std::map<int, EtList*>::iterator it = incomingEdges.find(tid);
    if(it != incomingEdges.end()){
        if(it->second != NULL){
            EtList* newHead = it->second->insert(src, true);
            //head of incomingEdges list has changed only if insert returns non-NULL
            if(newHead != NULL){
                it->second = newHead;
            }
        }else{
            //this scenario should not be hit as per the way we have set up this datastructure.
            printf("In EventGraphNode %p thread %d has a NULL head and yet this thread entry is not removed.",
                        this, tid);
            src->prev = NULL;
            it->second = src; 
        }
    }else{
        src->prev = NULL; //to maintain well-formedness, if this was not the case
        incomingEdges.insert(std::make_pair(tid, src));
    }
}

void EventGraphNode::optimizedInsertEdge(int tid, EtList* src){
    std::map<int, EtList*>::iterator it = incomingEdges.find(tid);
    if(it != incomingEdges.end()){
        if(it->second != NULL){
            EtList* newHead = it->second->optimizedInsert(src, true);
            //head of incomingEdges list has changed only if insert returns non-NULL
            if(newHead != NULL){
                it->second = newHead;
            }
        }else{
            //this scenario should not be hit as per the way we have set up this datastructure.
            printf("In EventGraphNode %p thread %d has a NULL head and yet this thread entry is not removed.",
                        this, tid);
            src->prev = NULL;
            it->second = src;
        }
    }else{
        src->prev = NULL; //to maintain well-formedness, if this was not the case
        incomingEdges.insert(std::make_pair(tid, src));
    }
}

// EventGraph does not allow self edges. Hence, we pass selfNodeId to prevent adding self edges
EtList* EventGraphNode::sparseJoinRecursive(EtList* srcEdges, EtList* destEdges, VectorClock* vc, 
        bool modifySrc, int selfNodeId, OpHelper* oph){

    std::pair<int, u4> srcEpoch, destEpoch;
    //OpHelper::taskMapIterator taskIt;
    EtTask* destTask = NULL;
    EtTask* srcTask = NULL;
    //int destTaskId, srcTaskId;
    int srcTaskId;

    if(destEdges != NULL){
        #ifdef DBG_COLLECT_STATS
           Stats::graphComputationCount++;
        #endif

      /*destTaskId = destEdges->task->id;
        taskIt = oph->taskMap.find(destTaskId);
        if(taskIt != oph->taskMap.end()){*/
            destTask = destEdges->task;
            destEpoch = destTask->getEpoch();
            if(destEpoch.first != -1 && vc->doesSubsumeClock(destEpoch.first, destEpoch.second)){
                EtList* tmpList = destEdges->next;
                if(tmpList != NULL){
                    tmpList->prev = destEdges->prev;
                }
                if(destEdges->prev != NULL){
                    destEdges->prev->next = tmpList;
                }
                destEdges->next = NULL;
                delete destEdges;
                destEdges = tmpList;
                return sparseJoinRecursive(srcEdges, destEdges, vc, modifySrc, selfNodeId, oph);
            }else{

                EtList* newHead = NULL;

                if(srcEdges != NULL){
                    bool stopInsertion = false;

                    while(!stopInsertion){
                        #ifdef DBG_COLLECT_STATS
                            Stats::graphComputationCount++;
                        #endif

                      /*srcTaskId = srcEdges->task->id;
                        taskIt = oph->taskMap.find(srcTaskId);
                        if(taskIt != oph->taskMap.end()){*/
                            srcTask = srcEdges->task;
                            srcEpoch = srcTask->getEpoch();
                            if( srcTask->id == selfNodeId || (srcEpoch.first != -1 && 
                                    vc->doesSubsumeClock(srcEpoch.first, srcEpoch.second)) ){
                                if(modifySrc){
                                    EtList* tmpList = srcEdges;
                                    srcEdges = srcEdges->next;
                                    tmpList->next = NULL;
                                    delete tmpList;
                                }else{//src cant be modified
                                    srcEdges = srcEdges->next;
                                }
                                
                                if(srcEdges == NULL){
                                    stopInsertion = true;
                                    break;
                                }else{
                                    continue; //need to find a srcEdge that can be inserted
                                }
                            }
                      

                       /* }else{
                            PERROR("Missing src task in taskMap for taskId %d, when performing sparse join on EventGraph"
                                   " node.", srcTaskId);
                        }*/


                        if(modifySrc){
                            if(srcTask->id == destTask->id){

                                if(srcEdges->lbl > destEdges->lbl){
                                    destEdges->lbl = srcEdges->lbl;
                                } 

                                EtList* tmpList = srcEdges;
                                srcEdges = srcEdges->next;
                                tmpList->next = NULL;
                                delete tmpList;

                            }else if(srcTask->id > destTask->id){

                                if(newHead == NULL){
                                    newHead = srcEdges;
                                }

                                EtList* tmpList = srcEdges->next;
                                srcEdges->prev = destEdges->prev;
                                srcEdges->next = destEdges;
                                if(destEdges->prev != NULL){
                                    destEdges->prev->next = srcEdges;
                                }
                                destEdges->prev = srcEdges;
                                srcEdges = tmpList;

                            }else if(srcTask->id < destTask->id){
                                stopInsertion = true;
                            }
                        }else{ //cannot modify input
                            if(srcTask->id == destTask->id){
               
                                if(srcEdges->lbl > destEdges->lbl){
                                    destEdges->lbl = srcEdges->lbl;
                                }

                                srcEdges = srcEdges->next;

                            }else if(srcTask->id > destTask->id){
       
                                #ifdef DBG_COLLECT_STATS
                                    Stats::egEdgeCount++;
                                #endif
                                EtList* newNode = new EtList(srcTask, srcEdges->lbl);
          

                                if(newHead == NULL){
                                    newHead = newNode;
                                }

                                newNode->prev = destEdges->prev;
                                newNode->next = destEdges;
                                if(destEdges->prev != NULL){
                                    destEdges->prev->next = newNode;
                                }
                                destEdges->prev = newNode;
                                srcEdges = srcEdges->next;

                            }else if(srcTask->id < destTask->id){

                                stopInsertion = true;
                            }

                        }

                        if(srcEdges == NULL){
                            stopInsertion = true;
                        }
    
                    }//end of while

                }
 
                if(newHead == NULL){
                    newHead = destEdges;
                }

                //if(destEdges->next != NULL){
                //retain current destEdges node as vc was found not to subsume epoch of its task and proceed to process the next one
                //    sparseJoinRecursive(srcEdges, destEdges->next, vc, modifySrc, oph);
               // }else{
                    destEdges->next = sparseJoinRecursive(srcEdges, destEdges->next, vc, modifySrc, selfNodeId, oph);
                    if(destEdges->next != NULL){
                        destEdges->next->prev = destEdges;
                    }
               // }
                //only the very first value of newHead matters. We only need to know if the head node of dest has changed.
                return newHead;

            }
       /* }else{
            PERROR("Missing dest task in taskMap for taskId %d, when performing sparse join on EventGraph"
                   " node.", destTaskId);
        }*/
    }else{

        EtList* newHead = NULL;
        while(srcEdges != NULL){
            #ifdef DBG_COLLECT_STATS
                Stats::graphComputationCount++;
            #endif

          /*srcTaskId = srcEdges->id;
            taskIt = oph->taskMap.find(srcTaskId);
            if(taskIt != oph->taskMap.end()){*/
                srcTask = srcEdges->task;
                srcEpoch = srcTask->getEpoch();
                if( srcTask->id == selfNodeId || (srcEpoch.first != -1 && 
                        vc->doesSubsumeClock(srcEpoch.first, srcEpoch.second)) ){
                    if(modifySrc){
                        EtList* tmpList = srcEdges;
                        srcEdges = srcEdges->next;
                        tmpList->next = NULL;
                        delete tmpList;
                    }else{//src cant be modified
                        srcEdges = srcEdges->next;
                    }
                    continue; //need to find a srcEdge that can be inserted
                }

                if(modifySrc){

                    if(destEdges == NULL){
                        newHead = srcEdges;
                        srcEdges->prev = NULL;
                    }else{
                        srcEdges->prev = destEdges;
                    }

                    EtList* tmpList = srcEdges->next;
                    srcEdges->next = NULL;
                    destEdges = srcEdges;
                    srcEdges = tmpList;

                }else{

                    #ifdef DBG_COLLECT_STATS
                        Stats::egEdgeCount++;
                    #endif
                    EtList* newNode = new EtList(srcTask, srcEdges->lbl);
                    if(destEdges == NULL){
                        newHead = newNode;
                        newNode->prev = NULL;
                    }else{
                        newNode->prev = destEdges;
                        destEdges->next = newNode;
                    }

                    newNode->next = NULL;
                    destEdges = newNode;
                    srcEdges = srcEdges->next;
                }
 
            /*}else{
                PERROR("Missing src task in taskMap for taskId %d, when performing sparse join on EventGraph"
                       " node.", srcTaskId);
            }*/
        }

        return newHead;

    }
}


//do this when you are sure that sparse join has no effect, else will break all assumptions
void EventGraphNode::insertEdgesAcrossThreads(EventGraphNode* srcEgNode){
    std::map<int, EtList*>::iterator egIt = srcEgNode->incomingEdges.begin();
    for(; egIt != srcEgNode->incomingEdges.end(); egIt++){
        insertEdgeUnmodified(egIt->first, egIt->second);
    }
}


// EventGraph does not allow self edges. Hence, we pass selfNodeId to prevent adding self edges
bool EventGraphNode::sparseJoinEdges(int tid, EtList* srcEdges, VectorClock* vc, bool modifySrc,
        int selfNodeId, OpHelper* oph){
    bool isHeadNull = false;
    std::map<int, EtList*>::iterator it = incomingEdges.find(tid);
    if(it != incomingEdges.end() && it->second != NULL){
        EtList* newHead = sparseJoinRecursive(srcEdges, it->second, vc, modifySrc, selfNodeId, oph);
        if(newHead == NULL){
            isHeadNull = true;
        }
        it->second = newHead;
    }else{
        EtList* newHead = sparseJoinRecursive(srcEdges, NULL, vc, modifySrc, selfNodeId, oph);
        if(newHead != NULL){
            incomingEdges.insert(std::make_pair(tid, newHead));
        }else{
            isHeadNull = true;
        }
    }

    return isHeadNull;
}

void EventGraphNode::sparseJoinAcrossThreads(EventGraphNode* egNode, VectorClock* vc, 
        bool modifyInput, int selfNodeId, OpHelper* oph){
    std::set<int> tobeDeletedEntries;
    std::map<int, EtList*>::iterator egIt = egNode->incomingEdges.begin();
    for(; egIt != egNode->incomingEdges.end(); ){
        bool isHeadNull = this->sparseJoinEdges(egIt->first, egIt->second, vc, modifyInput, selfNodeId, oph);
        if(isHeadNull){
            tobeDeletedEntries.insert(egIt->first);
        }

        if(modifyInput){
            egIt->second = NULL; //so that two EGNs dont own same nodes
            egNode->incomingEdges.erase(egIt++); //we dont store threads with empty lists
        }else{
            egIt++;
        }
    }

    std::set<int>::iterator setIt = tobeDeletedEntries.begin();
    for(; setIt != tobeDeletedEntries.end(); setIt++){
        #ifdef DBG_COMPUTEHB
            printf("deleting tid %d from incoming edges map of an event graph node.", *setIt);
        #endif
        incomingEdges.erase(*setIt);
    }
}

void EventGraphNode::diagnosticPrint(){
    std::map<int, EtList*>::iterator egIt = incomingEdges.begin();
    for(; egIt != incomingEdges.end(); egIt++){
        printf("\nprinting incoming edges from tasks in tid %d:\n", egIt->first);
        if(egIt->second != NULL){
            egIt->second->diagnosticPrint();
        }
    }
}


EventAwareVC::EventAwareVC(){
}


EventAwareVC::~EventAwareVC(){
    std::map<int, VectorClock*>::iterator vcIt = vcMap.begin();
    for(; vcIt != vcMap.end(); vcIt++){
        delete vcIt->second;
    }

   
    std::map<int, EventGraphNode*>::iterator egIt = eventGraph.begin();
    for(; egIt != eventGraph.end(); egIt++){
        delete egIt->second;
    }
}

/* we need this only when taking joins between vector clocks and eventgraphs across 
 * EventTrack iterations.
 */
void EventAwareVC::joinEVCWith(EventAwareVC* srcEvc, OpHelper* oph, bool modifyInputEg){
    std::map<int, VectorClock*>::iterator srcVcIt = srcEvc->vcMap.begin();
    std::map<int, VectorClock*>::iterator vcIt;

    std::map<int, EventGraphNode*>::iterator srcEgIt = srcEvc->eventGraph.begin();
    std::map<int, EventGraphNode*>::iterator egIt;

    for(; srcVcIt != srcEvc->vcMap.end(); srcVcIt++){
        VectorClock* vc = NULL;
        vcIt = vcMap.find(srcVcIt->first);
        if(vcIt != vcMap.end()){
            vcIt->second->vcJoin(srcVcIt->second);
            vc = vcIt->second;
        }else{
            vc = new VectorClock();
            vc->vcJoin(srcVcIt->second);
            vcMap.insert(std::make_pair(srcVcIt->first, vc));
        }

        
        if(srcEgIt->first == srcVcIt->first){
            egIt = eventGraph.find(srcEgIt->first);    
            EventGraphNode* egn = NULL;
            if(egIt == eventGraph.end()){
                egn = new EventGraphNode();
                eventGraph.insert(std::make_pair(srcEgIt->first, egn));
            }else{
                egn = egIt->second;
            }
            
            egn->sparseJoinAcrossThreads(srcEgIt->second, vc, modifyInputEg, srcEgIt->first, oph);

            srcEgIt++;    
        }else{
            PERROR("A vector clock entry for task %d is missing corresponding node in the EventGraph.",
                    srcVcIt->first);
        }
    }

}


}
