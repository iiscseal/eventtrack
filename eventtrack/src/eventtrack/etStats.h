/*
 * Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ETSTATS_H_
#define ETSTATS_H_

#include <common.h>

namespace etrack{

class Stats;

class Stats{

public:

static void initializeHbComputationStats();


// to compute avg.no.of incoming edges at begin of a task.
static u4 incomingEdgesCount; 
// Among the incoming edges which all are identified as to be HB ordered.
static u4 tobeOrderedIncomingEdgesCount;
// Among the identified tasks to be directly ordered, which are actually sparse joined.
static u4 sparseJoinedTaskCount;
/* number of graph computations including edges added, sparsification,inspection etc. 
 * Basically, gives the dynamic complexity of EventGraph.
 */
static u4 graphComputationCount;
/* Stats related to size of EventGraph. This will only give an idea of what may be the
 * maximum size of EventGraph if no edge added to graph is ever removed.
 */
static u4 egEdgeCount;
/* Number of edges deleted form the EventGraph. This includes edges deleted due to sparsification
 * and as well as deletion of temporary edges due to optimal insert. Implementing a more fine grained
 * technique is more involved.
 */
static u4 egDeletedEdgeCount;
/* Total space occupied by VCs */
static u8 vcMemory;

static u4 eventCount;
static u4 eventLoopCount;

};

}

#endif

