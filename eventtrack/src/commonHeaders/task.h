/*
 * Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TASK_H_
#define TASK_H_

#include <common.h>

class Task;

class Task{

public:

Task(int tid, u4 eventId);

virtual ~Task();

virtual void diagnosticPrint(int taskId){
    printf("Task %d  ---  tid:%d, event:%u, chain:%d, post:%d, begin:%d, typeOfPost:%d \n",
            taskId, tid, eventId, chain, postOpId, beginOpId, typeOfPost);

    printf("Environment ordered tasks:\n");
    std::set<int>::iterator it = envOrderedTasks.begin();
    for(; it != envOrderedTasks.end(); it++){
        printf(" %d,", *it);
    }
    printf("\n");

    printf("\nEnvironment Ordered Enable Operations:");
    it = envOrderedEnableOps.begin();
    for(; it != envOrderedEnableOps.end(); it++){
        printf(" %d,", *it);
    }
    printf("\n");

    printf("\nEnvironment Ordered Posts:");
    it = envOrderedPosts.begin();
    for(; it != envOrderedPosts.end(); it++){
        printf(" %d,", *it);
    }
    printf("\n");
       
}

/* set of tasks on the same thread ordered w.r.t. this task.
 * This is trivially empty for non event-handler tasks.
 * Derive end -> begin HB ordering w.r.t. these tasks
 */
std::set<int> envOrderedTasks;   

/* set of enable ops/send broadcasts/registerReceiver/startService/startActivity etc 
 * causally ordered w.r.t. post of this task.
 * This is trivially empty for non event-handler tasks.
 */
std::set<int> envOrderedEnableOps; 

/* set of posts (typically native posts) causally ordered w.r.t. post
 * of this task. In case of EventTrack, need to adge KNOWN_POST edge
 * between these tasks and also post of this task should be initialized with
 * HB info of post ops in this set.*/
std::set<int> envOrderedPosts;

int tid;
u4 eventId; //eventId being 0 indicates a non-event handler as our msgIDs start from 1.
int chain;
int postOpId;
int beginOpId;
int typeOfPost; 
};

#endif  // TASK_H_


