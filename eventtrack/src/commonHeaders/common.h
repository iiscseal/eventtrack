/*
 * Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef COMMON_H_
#define COMMON_H_

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <map>
#include <set>
#include <string>
#include <list>
#include <vector>
#include <queue>
#include <climits>
#include <stack>

#define PERROR(args, ...)  printf("ERROR: "); printf(args , ##__VA_ARGS__); printf("\n"); exit(0);

#define NUM_TYPES 60
#define MAIN_TID 1

typedef uint32_t u4;
typedef uint64_t u8;

struct opLogStruct{
    int opType;
    int arg1;
    u4 arg2;
    int arg3;
    int arg4; //delay argument for post
    int arg5; //arg5 corresponds to a key in OpArgHelper and can be used to retrieve corresponding string
    int tid;  //thread executing the operation
    int taskId; //task in which this operation is executed
};
typedef struct opLogStruct OpLog;

class HbList;

class HbList{

public:

HbList();
~HbList();

HbList(int id, bool sorted);

HbList* insert(HbList* hblist, bool modifyInput);

virtual void diagnosticPrint(){
    printf("node ID:%d,  sorted:%d, this:%p  next:%p,  prev:%p \n", id, sorted, this, next, prev );
    if(next != NULL){
        next->diagnosticPrint();
    }
}

int id; //typically this list may be sorted based on this ID
bool sorted;
HbList* next;    
HbList* prev;

};


#endif  // COMMON_H_

