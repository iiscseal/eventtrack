/*
 * Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This file has functions to model ordering between lifecycle callbacks
 * of Android app components. The functions here have been adapted from
 * dalvik/vm/mcd/AbcModel.cpp of DroidRacer.
 *
 */

#ifndef ENVMODEL_H_
#define ENVMODEL_H_

#include<operationHelper.h>

#define APPBIND 4
#define APPBIND_DONE 16
#define ACT_PAUSE 1
#define ACT_RESUME 2
#define ACT_LAUNCH 3
#define ACT_RELAUNCH 5
#define ACT_DESTROY 6
#define APP_CHANGE_CONFIG 7
#define ACT_STOP 8
#define ACT_RESULT 9
#define ACT_CHANGE_ACT_CONFIG 10
#define SER_CREATE_SERVICE 11
#define SER_STOP_SERVICE 12
#define SER_BIND_SERVICE 13
#define SER_UNBIND_SERVICE 14
#define SER_SERVICE_ARGS 15
#define SER_CONNECT_SERVICE 17
#define RUN_TIMER_TASK 18
#define SER_REQUEST_START_SERVICE 19
#define SER_REQUEST_BIND_SERVICE 20
#define SER_REQUEST_STOP_SERVICE 21
#define SER_REQUEST_UNBIND_SERVICE 22
#define ACT_ACTIVITY_START 23
#define ACT_NEW_INTENT 24
#define ACT_START_NEW_INTENT 25
#define REC_REGISTER_RECEIVER 26
#define REC_SEND_BROADCAST 27
#define REC_SEND_STICKY_BROADCAST 28
#define REC_TRIGGER_ONRECIEVE 29
#define REC_UNREGISTER_RECEIVER 30
#define REC_REMOVE_STICKY_BROADCAST 31
#define REC_TRIGGER_ONRECIEVE_LATER 32

//known system broadcast
#define TIME_TICK_ACTION_STR 1


class ComponentInfo;
class ServiceClass;
class RequestBindClass;
class OverlappingRequestClass;
class EnvModel;

class ComponentInfo{

public:

ComponentInfo(int opId, int state, Task* compTask);
~ComponentInfo();

Task* compTask; //task in which opId resides
int opId; //opId of a trigger
int state; //activity state

};


class ServiceClass{

public:

ServiceClass();
~ServiceClass();

int firstBindServiceRequest;
int firstCreateServiceRequest;
int createServiceOp;
int bindServiceOp;
int firstStopServiceRequest;
bool isStartedService;//true means its a started service and may or may not be bound & false means definitely only bound

std::set<u4> connectionSet; //needed for bulk HB edges added on UNBIND-SERVICE
std::set<int> validRequestUnbindSet;

/* indicate the last state corresponding to state machines of started & bounded
 * behaviour of this service.
 */
ComponentInfo* prevStartedServiceOp;
ComponentInfo* prevBoundServiceOp;

};


class RequestBindClass{

public:

RequestBindClass(int requestBindOp, int serviceClassKey, RequestBindClass* prev);
~RequestBindClass();

//prev is needed as same connection obj can be used to connect to different services
RequestBindClass* prev;

int requestBindOp;
int requestUnbindOp;
int serviceClassKey;

};


class OverlappingRequestClass{

public:

OverlappingRequestClass(){
    firstBindServiceRequest = -1;
}

OverlappingRequestClass(int bindReq){
    firstBindServiceRequest = bindReq;
}

~OverlappingRequestClass(){}

int firstBindServiceRequest; //stores opId

std::set<u4> connectionSet;

};


class EnvModel{

public:

EnvModel();
~EnvModel();

bool isEventActivityEvent(int state);

void addEnvOrderedPostToTask(Task* srcTask, Task* destTask);

bool checkAndUpdateBroadcastState(int opId, OpLog* op, int opTaskId, Task* opTask);

void addEnableLifecycleEventToMap(int opId, OpLog* op);

/* adds enable to trigger, or enable to post of trigger, or end of enable to begin of trigger 
 * HB info. For the first case returns a set of enables with which direct HB should be added.
 * Only in the first case returned enable ops should be stored with trigger.
 * bool returns whether an edge was added or not.
 * If no edge is added, then something is wrong and need to abort.
 */
std::pair<bool, std::set<int> > connectEnableAndTriggerLifecycleEvents(int triggerOpid,
        OpLog* triggerOp, int triggerTaskId, Task* triggerTask, OpHelper* oph);

void mapInstanceWithIntentId(u4 instance, int intentId);

bool checkAndUpdateActivityComponentState(int opId, OpLog* op, int opTaskId, Task* opTask);

bool checkAndUpdateServiceState(int opId, OpLog* op, int opTaskId, Task* opTask, OpHelper* oph);

/* map from taskId of an onReceive handler to opId of sendBroadcast or RegisterReceiver,
 * which will be used to trigger environment modeling HB rules such as, ---
 * 1. if sendBroadcast(e1) HB sendBroadcast(e2) then onReceive(e1) HB onReceive(e2) 
 * 2. if sendBroadcast(e1) HB registerReceiver(e2) then onReceive(e1) HB onReceive(e2)
 * 3. if registerReceiver(e1) HB sendBroadcast(e2) then onReceive(e1) HB onReceive(e2)
 * 4. if registerReceiver(e1) HB registerReceiver(e2) then onReceive(e1) HB onReceive(e2)
 * Note that the above rules hold if the antecedent ops directly result in onReceive handler
 * to be posted. The checkAndUpdateBroadcastState() function identifies such sends and registers
 * and, stores taskId of the onReceive and opId of corresponding identified send or receive
 * in this map. Various HB computation algorithm may use this info in different way.
 */
std::map<int, int> onRecTaskToEnableOpMap;


//all onReceives are ordered w.r.t. this task whoch corresponds to onBind handler
int appBindTaskId;

/* enable OpId of ACT_RESUME with no instance specified */
int blankEnableResumeOp;


private:

//BroadcastReceiver related fields
/* set<acttionKey> */
std::set<int> stickyIntentActionSet;

/* map< <instance,actionKey>, registerReceiverOpId> */
std::map<std::pair<u4, int>, int> registerReceiverMap;

/* map< <instance,actionKey>, registerReceiverOpId> where we know that 
 * a sticky broadcast with this action is around.*/
std::map<std::pair<u4, int>, std::list<int> > stickyRegisterReceiverMap;

std::set<int> handledStickySendsSet;

/* map<intentId, <sendBroadcastOpId, isSticky> >  */
std::map<int, std::pair<int, bool> > sentIntentMap;

/* task ID of the most recent periodic system broadcast with action TIME_TICK*/
int prevtimeTickBroadcastTaskId; //derive end -> begin edge between onReceives of these broadcasts


//Activity related fields
/* map< <instance, activity-state-machine-state>, set of enable OpIds> */
std::map<std::pair<u4, int>, std::set<int> > enableTriggerLcMap;

/* map<instance, State machine state stored as ComponentInfo object >  */
std::map<u4, ComponentInfo* > activityStateMap;

/* a mapping from activity instance to intentID */
std::map<u4, int> instanceIntentMap;


//Service related fields
/* map<serviceClassKey, ServiceObject> */
std::map<int, ServiceClass*> serviceMap;
std::map<int, std::set<int> > serviceClassToStartServiceRequestMap;

/* map<intentId, Start Service Request OpId> */
std::map<u4, int> requestStartServiceMap;

/* <service connection ID, RequestBindClass object>  */
std::map<u4, RequestBindClass*> serviceConnectMap;

// <serviceClassKey, <first-start-request, first-bind-request> > -- only stored when a
// destroy of service or bounded nature of it is going to happen while a new star or bind request comes in.
std::map<int, OverlappingRequestClass*> overlappingStartOrBindServiceRequestsMap;

};

#endif //ENVMODEL_H_
