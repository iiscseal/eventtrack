/*
 * Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <common.h>

HbList::HbList(){
    id = -1;
    sorted = true; //set this to false if that is not the case
    next = NULL;
    prev = NULL;
}

HbList::HbList(int id, bool sorted) : id(id), sorted(sorted), next(NULL), prev(NULL){
}


/* this will recursively free next pointer. If you do not want this,
 * then set HbList->next as null before invoking delete pointer.
 */
HbList::~HbList(){
    delete this->next;
}

/* assume the current list and hblist is sorted (descending order of id)
 * hblist is assumed to be the head of the list i.e. we dont care about its prev
 * returns NULL if head of current list is not modifed.
 * returns new HEAD if it is modified.
 * Arg modifyInput indicates whether we can modify hblist or not. 
 * If not, have to create new nodes.
 *
 */
HbList* HbList::insert(HbList* hblist, bool modifyInput){
    if(hblist != NULL){
        if(sorted && hblist->sorted){
            if(hblist->id == id){
                if(modifyInput){
                    HbList* temp = hblist;
                    hblist = hblist->next;
                    temp->next = NULL;
                    delete temp;
                    this->insert(hblist, modifyInput);
                }else{
                    this->insert(hblist->next, modifyInput);
                }
                return NULL; //since hblist is sorted, hblist->next->id <= this->id
            }else if(hblist->id > id){
                if(prev != NULL){
                    if(modifyInput){
                        prev->next = hblist;
                        hblist->prev = prev;
                        HbList* temp = hblist->next;
                        hblist->next = this;
                        prev = hblist;
                        this->insert(temp, modifyInput);
                    }else{ //create new node and insert it
                        HbList* newNode = new HbList(hblist->id, hblist->sorted);
                        prev->next = newNode;
                        newNode->prev = prev;
                        newNode->next = this;
                        prev = newNode;
                        this->insert(hblist->next, modifyInput);
                    }
                    return NULL; //this is a dummy return which returns only after children have returned.
                }else{ //we will be modifying HEAD of current list 
                    if(modifyInput){
                        hblist->prev = prev; //in this case prev is NULL * hblist is the new HEAD
                        HbList* temp = hblist->next;
                        hblist->next = this;
                        prev = hblist; //this is no more HEAD, so its prev becomes non-null
                        this->insert(temp, modifyInput);
                        return hblist;
                    }else{ //create new node and insert it
                        HbList* newNode = new HbList(hblist->id, hblist->sorted);
                        newNode->prev = prev; //in this case prev is NULL * hblist is the enw HEAD
                        newNode->next = this;
                        prev = newNode; //this is no more HEAD, so its prev becomes non-null
                        this->insert(hblist->next, modifyInput);
                        return newNode;
                    }
                } 
            }else{ //hblist->id < this->id
                if(next != NULL){
                    next->insert(hblist,modifyInput);
                }else{
                    if(modifyInput){
                        hblist->prev = this;
                        next = hblist;
                    }else{
                        HbList* newNode = new HbList(hblist->id, hblist->sorted);
                        newNode->prev = this;
                        next = newNode; //make current node's next non-null
                        //newNode->next is null by initialization
                        newNode->insert(hblist->next, modifyInput);             
                    }
                }
                return NULL; //since HEAD of current list will not be modified
            }
        }else{ //we are dealing with unsorted list. So we do not insert anything
            //stub: if there is a future requirement for unsorted list we will implement it here
            //this will simpy append input list mith current list.
            return NULL;
        }
    }else{ //hblist is null
        return NULL;
    }
}


