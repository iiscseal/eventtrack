/*
 * Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OPERATIONHELPER_H_
#define OPERATIONHELPER_H_

#include <operation.h>
#include <task.h>
#include <vectorclock.h>

class ThreadClass;
class OpHelper;

class ThreadClass{
 
public:

ThreadClass(int curTaskId);
virtual ~ThreadClass();

int curTaskId; //current task being processed
int loopOpId; //this value is -1 for threads with no event queue

std::set<int> taskIdSet;

};

//only one instance of this class is created
class OpHelper{

public:

OpHelper();
~OpHelper();

/* if a task has already been assigned a chain returns that as is,
 * if not, assigns either an existing chain or a new chain to the
 * task and returns the same. 
 */
int getChain(Task* task, VectorClock* vc);

void cleanupAfterPreProcess();


/* we need below field  only in the first iteration as chain
 * assignment of operations happens only in the first iteration.
 */
VectorClock* curClockOnChains;
int highestChainId;

std::map<int, Operation*> opMap;
typedef std::map<int, Operation*>::iterator opMapIterator;

std::map<int, Task*> taskMap;
typedef std::map<int, Task*>::iterator taskMapIterator;

//map from tid to Thread object
std::map<int, ThreadClass*> threadMap;
typedef std::map<int, ThreadClass*>::iterator threadMapIterator;

//given a UI view and a UI event returns the most recent enable on this pair
std::map<std::pair<u4, int>, int> recentUiEnableEventMap;

/* DroidRacer may emit additional enable event operations to be conservative.
 * These can be identified when processing the trace and can be removed
 */
std::set<int> enableEventsToBeDeleted;

//given window ID returns most recent operation that enabled focus change on this window
std::map<u4, int> recentWindowFocusEnableMap;

//map from waiting thread to the most recent operation which issued notify on it
std::map<int,int> waitThreadToNotifyMap;

/* collection of non UI event enable ops such as notify which will be deleted if they do not
 * get mapped to any waiting thread, and so on.
 */
std::set<int> opsToBeDeleted;

/* maps eventId to operation ID of corresponding post.
 * Will be used to perform cleanup of unhandled and removed posts.
 * Also, used for some bookkeeping.
 */
std::map<u4,int> eventToPostMap;

/* map<event, <tid, taskId> > handlers for which we have seen begin but not end.
 * We use this to emit end operations manually, so that HB datastructures are set up properly.
 */
std::map<u4, std::pair<int, int> > onlyBeginSeenMap;

std::map<u4, int> recentEnableTimerTaskMap;

int opCount;
int taskCount;
bool addOpToMap; //used in pre-procesing in case of some operations

};

#endif  // OPERATIONHELPER_H_

