/*
 * Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <task.h>

Task::Task(int tid, u4 eventId): tid(tid), eventId(eventId){
    chain = -1;
    postOpId = -1;
    beginOpId = -1;
    typeOfPost = -1;
}

Task::~Task(){
    #ifdef DBGPREPROCESS
        printf("\nTASK DESTROYED: Task with thread %d and event %u is being deallocated.\n", tid, eventId);
    #endif
}
