/*
 * Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef VECTORCLOCK_H_
#define VECTORCLOCK_H_

#include <common.h>

class VectorClock;

class VectorClock{

public:

VectorClock();

~VectorClock();

VectorClock(const VectorClock& srcVC); 

u4 getClockValue(int chain);

u4 incrementClock(int chain);

long unsigned int getVCSize();

void resetVector(); //clears VC making it size 0

void resizeVC(int size, u4 initialValue);

void vcJoin(VectorClock* srcVC);

bool doesSubsumeClock(int chain, u4 clockValue);

int assignChain(VectorClock* curChainVC);

void diagnosticPrint();

private:

std::vector<u4> vc;

};


#endif

