/*
 * Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This file has functions to model ordering between lifecycle callbacks
 * of Android app components. The functions here have been adapted from
 * dalvik/vm/mcd/AbcModel.cpp of DroidRacer.
 *
 */

#include <envModel.h>

ComponentInfo::ComponentInfo(int opId, int state, Task* compTask) :
    opId(opId), state(state), compTask(compTask){
}

ComponentInfo::~ComponentInfo(){
}


ServiceClass::ServiceClass(){
}

ServiceClass::~ServiceClass(){
    #ifdef DBGPROCESS
        printf("\ndeleting ServiceClass object created by request %d.\n", firstCreateServiceRequest);
    #endif
    delete prevStartedServiceOp;
    delete prevBoundServiceOp;
}


RequestBindClass::RequestBindClass(int requestBindOp, int serviceClassKey, RequestBindClass* prev) :
        requestBindOp(requestBindOp), serviceClassKey(serviceClassKey), prev(prev){
    requestUnbindOp = -1;
}

RequestBindClass::~RequestBindClass(){
}


EnvModel::EnvModel(){
    appBindTaskId = -1;
    blankEnableResumeOp = -1;
    prevtimeTickBroadcastTaskId = -1;
}

EnvModel::~EnvModel(){

}

bool EnvModel::isEventActivityEvent(int state){
    bool isActivityEvent = false;
    switch(state){
      case ACT_LAUNCH:
      case ACT_RESUME:
      case ACT_ACTIVITY_START:
      case ACT_PAUSE:
      case ACT_STOP:
      case ACT_DESTROY:
      case ACT_RESULT:
      case ACT_NEW_INTENT:
      case ACT_START_NEW_INTENT:
      case ACT_RELAUNCH:
      case ACT_CHANGE_ACT_CONFIG:
      case APP_CHANGE_CONFIG:
           isActivityEvent = true;
           break;
      default: isActivityEvent = false;
    }

    return isActivityEvent;
}

void EnvModel::addEnvOrderedPostToTask(Task* srcTask, Task* destTask){
    if(srcTask != destTask){
        destTask->envOrderedPosts.insert(srcTask->postOpId);
    }
}


bool EnvModel::checkAndUpdateBroadcastState(int opId, OpLog* op, int opTaskId, Task* opTask){
    bool updated = false;
    int state = op->arg1;
    u4 component = op->arg2;
    int intentId = op->arg3;
    int action = op->arg5;

    HbList* newNode = NULL; 
    
    if(state == REC_SEND_BROADCAST){

        sentIntentMap.insert(std::make_pair( intentId, std::make_pair(opId,false) ));
        updated = true;

    }else if(state == REC_SEND_STICKY_BROADCAST){

        sentIntentMap.insert(std::make_pair( intentId, std::make_pair(opId,true) ));
        stickyIntentActionSet.insert(action);
        updated = true;

    }else if(state == REC_REGISTER_RECEIVER){

        std::pair<u4, int> compActionPair = std::make_pair(component, action);
        registerReceiverMap.insert(std::make_pair(compActionPair, opId));
        
        //check if corresponding action has sticky intent
        if(stickyIntentActionSet.find(action) != stickyIntentActionSet.end()){
            std::map<std::pair<u4, int>, std::list<int> >::iterator it =
                     stickyRegisterReceiverMap.find(compActionPair);
            if(it != stickyRegisterReceiverMap.end()){
                it->second.push_back(opId);
            }else{
                std::list<int> registerList;
                registerList.push_back(opId);
                stickyRegisterReceiverMap.insert(std::make_pair(compActionPair, registerList));
            }
        }

        updated = true;

    }else if(state == REC_UNREGISTER_RECEIVER){

        std::map<std::pair<u4, int>, int>::iterator regIt = registerReceiverMap.begin();
        for( ; regIt != registerReceiverMap.end(); ){
            if(regIt->first.first == component){
                registerReceiverMap.erase(regIt++);
            }else{
                ++regIt;
            }
        }

        //maybe this cleanup is not needed as each registerReceiver called when a 
        //a corresponding sticky intent is around is supposed to thrigger onReceive
        //which in turn will clear the corresponding entry in this map. 
        //But had observed that on issuing unregister request, none of the unexecuted onReceives were executed,
        //So, cleaning up here without taking a chance.
        std::map<std::pair<u4, int>, std::list<int> >::iterator stickyIt = 
                stickyRegisterReceiverMap.begin();
        for( ; stickyIt != stickyRegisterReceiverMap.end(); ){
            if(stickyIt->first.first == component){
                stickyIt->second.clear();
                stickyRegisterReceiverMap.erase(stickyIt++);
            }else{
                ++stickyIt;
            }
        }

        updated = true;

    }else if(state == REC_REMOVE_STICKY_BROADCAST){

        stickyIntentActionSet.erase(action);
        updated = true;

    }else if(state == REC_TRIGGER_ONRECIEVE_LATER){

        updated = true;
    
    }else if(state == REC_TRIGGER_ONRECIEVE){
        //opAsync = opTask

        //check if action is android.intent.action.TIME_TICK'saction key
        if(action == TIME_TICK_ACTION_STR){
            //TIME_TICK is a periodic broadcast and hence inherently happens-before
            //exists between adjacent TIME_TICKs and thus between posts of corresponding onReceive
            if(prevtimeTickBroadcastTaskId != -1){

                opTask->envOrderedTasks.insert(prevtimeTickBroadcastTaskId);
            }
            prevtimeTickBroadcastTaskId = opTaskId;
        }

        //broadcast receiver's onReceive could execute after BIND_APPLICATION's event handler 
        opTask->envOrderedTasks.insert(appBindTaskId);


        std::pair<u4, int> compActionPair = std::make_pair(component, action);
        std::map<std::pair<u4, int>, int>::iterator recIter = registerReceiverMap.find(compActionPair);
        int regOpid = -1; 
        bool isSticky = false;

        std::map<int, std::pair<int, bool> >::iterator siIt = sentIntentMap.find(intentId);

        if(siIt != sentIntentMap.end()){
            int sendOpId = siIt->second.first;
            //add edge from sendIntent to post of onReceive
            opTask->envOrderedEnableOps.insert(sendOpId);

           
            //check if edge should be added from sticky register or otherwise
            if(siIt->second.second == true){ //check if the broadcast was a sticky broadcast 

                /* check if a sticky send happened after the receiver was registered. But,
                 * for such a sticky send if you have not yet seen corresponding handler,
                 * then this onReceive corresponds to that.
                 */
                if(recIter != registerReceiverMap.end() && recIter->second < sendOpId &&
                        handledStickySendsSet.find(sendOpId) == handledStickySendsSet.end()){
                    handledStickySendsSet.insert(sendOpId);
                    regOpid = recIter->second;
                    isSticky = false;
                }else{
                
                std::map<std::pair<u4, int>, std::list<int> >::iterator stickyIt =
                    stickyRegisterReceiverMap.find(compActionPair);
                if(stickyIt != stickyRegisterReceiverMap.end() && !stickyIt->second.empty()){
                    regOpid = stickyIt->second.front();
                    stickyIt->second.pop_front();
                    isSticky = true;
                }else{
                    //even the send was sticky, register seems to have been issued prior to sticky send
                    if(recIter != registerReceiverMap.end()){
                        regOpid = recIter->second;
                        isSticky = false;
                    }
                }
 
                }
            }else{
                //non sticky send
                if(recIter != registerReceiverMap.end()){
                    regOpid = recIter->second;
                    isSticky = false;
                }
            }

                /*add entry to AbcSendBroadcastOnReceiveMap. This map is used to
                 *add the edge onReceive1 < onReceive2 if sendBroadcast1 < sendBroadcast2
                 *For this we should only consider those onReceives which are due to
                 *sendBroadcast and not due to registerReceiver for a sticky broadcast
                 */
            if(!isSticky){
                    onRecTaskToEnableOpMap.insert(std::make_pair(opTaskId, sendOpId));
            }
        }else{
            /* no corresponding sendIntent for onReceive (i.e intent has been fired external to app).
             * we trivially set such onReceives to have non sticky intent.
             * I know how to get accurate info on this, will implement it in later version.
             * note that its fine to set isSticky to false in the absence of sendBroadcast as
             * it will only not add some HB edges, which is not unsound.
             */
             std::map<std::pair<u4, int>, int>::iterator recIter = registerReceiverMap.find(compActionPair);
             if(recIter != registerReceiverMap.end()){
                 regOpid = recIter->second;
                 isSticky = false;
             }
        }

        //add edges for registerReceiver operation
        if(regOpid != -1){
            opTask->envOrderedEnableOps.insert(regOpid);
            if(isSticky){
                onRecTaskToEnableOpMap.insert(std::make_pair(opTaskId, regOpid));
            }
        }
        
        updated = true;
    }

    return updated;
}


void EnvModel::addEnableLifecycleEventToMap(int opId, OpLog* op){
    u4 instance = op->arg2;
    int state = op->arg1;
    std::pair<u4, int> instanceStatePair = std::make_pair(instance, state);
    std::map<std::pair<u4, int>, std::set<int> >::iterator it =
            enableTriggerLcMap.find(instanceStatePair);

    if(it != enableTriggerLcMap.end()){
        it->second.insert(opId);
    }else{
        std::set<int> enableOpSet; 
        enableOpSet.insert(opId);
        enableTriggerLcMap.insert(std::make_pair(instanceStatePair, enableOpSet));
    }
}   



std::pair<bool, std::set<int> > EnvModel::connectEnableAndTriggerLifecycleEvents(int triggerOpid, 
        OpLog* triggerOp, int triggerTaskId, Task* triggerTask, OpHelper* oph){

    std::set<int> enableOpsToBeStored; 
    bool edgeAdded = false;

    u4 instance = triggerOp->arg2;
    int state = triggerOp->arg1;
    std::pair<u4, int> instanceStatePair = std::make_pair(instance, state);
    std::map<std::pair<u4, int>, std::set<int> >::iterator it =  enableTriggerLcMap.find(instanceStatePair);
    

    if(triggerTaskId == -1 && triggerTask != NULL){
        PERROR("Found a TRIGGER-LIFECYCLE of activity state %d on component instance %u, outside task. Cannot continue further.",
                state, instance);
    }

    if(it != enableTriggerLcMap.end()){
        
        /* iterator over all enable operations stored in enableTriggerLcMap 
         * and add store info so as to later add HB edge from enable to post of trigger's task.
         * All these triggers are related to Activity lifecycle callback  & hence guaranteed to
         * in an event handler, that too of the main thread.
         * Since we conservatively emit enable operations, if we find an enable operation after
         * the post of trigger's task then we only add HB from end of enable's task to begin of
         * trigger'stask if both or on same thread, or edge from enable to trigger if on different thread,
         * which is unlikely. 
         */
        std::set<int>::iterator enIt = it->second.begin();
        for(; enIt != it->second.end(); enIt++){
 
            int enableOpId = *enIt;
            if(enableOpId < triggerTask->postOpId){
                triggerTask->envOrderedEnableOps.insert(enableOpId);
            }else{
                OpHelper::opMapIterator opIt = oph->opMap.find(enableOpId);
                if(opIt != oph->opMap.end()){
                    if(triggerTask->tid == opIt->second->tid){
                        if(triggerTaskId != opIt->second->taskId){
                            triggerTask->envOrderedTasks.insert(opIt->second->taskId);
                        }
                    }else{
                        enableOpsToBeStored.insert(enableOpId);
                    }
                }else{
                    PERROR("ENABLE-LIFECYCLE operation with opid %d missing in opMap", enableOpId);
                }
            }
            edgeAdded = true;
        }
    }

    //if the operation is RESUME then add edge from existing blank enable to this 
    //if this resume has not yet been connected to an enable. 
    //Note: we dont have a better way of doing this than with a guess work
    if(state == ACT_RESUME){
        if(blankEnableResumeOp != -1 && !edgeAdded){

            if(blankEnableResumeOp < triggerTask->postOpId){
                triggerTask->envOrderedEnableOps.insert(blankEnableResumeOp);
            }else{
                OpHelper::opMapIterator opIt = oph->opMap.find(blankEnableResumeOp);
                if(opIt != oph->opMap.end()){
                    if(triggerTask->tid == opIt->second->tid){
                        if(triggerTaskId != opIt->second->taskId){
                            triggerTask->envOrderedTasks.insert(opIt->second->taskId);
                        }
                    }else{
                        enableOpsToBeStored.insert(blankEnableResumeOp);
                    }
                }else{ 
                    PERROR("ENABLE-LIFECYCLE operation with opid %d missing in opMap", blankEnableResumeOp);
                }
            } 


            edgeAdded = true;

            blankEnableResumeOp = -1;
        }
    }

    return std::make_pair(edgeAdded, enableOpsToBeStored);
}


void EnvModel::mapInstanceWithIntentId(u4 instance, int intentId){

    //change key from intentId to instance in all maps, now that you have activity instance info

    if(activityStateMap.find(intentId) != activityStateMap.end()){
        ComponentInfo* tmpComp = activityStateMap.find(intentId)->second;
        activityStateMap.erase(intentId);
        activityStateMap.insert(std::make_pair(instance, tmpComp));
    }else{
        PERROR("Activity state machine error. instance %d supplied without "
             "intentId %d entry in stateMap", instance, intentId);
    }

    //for LAUNCH and RESUME enable may be issued with intent id. Reassign it to instance
    int state = ACT_LAUNCH;
    std::pair<u4, int> intentStatePair = std::make_pair(intentId, state);
    std::map<std::pair<u4, int>, std::set<int> >::iterator it =
            enableTriggerLcMap.find(intentStatePair);
    if(it != enableTriggerLcMap.end()){
        std::set<int> enableOps = it->second;
        enableTriggerLcMap.erase(intentStatePair);

        std::pair<u4, int> instanceStatePair = std::make_pair(instance, state);
        enableTriggerLcMap.insert(std::make_pair(instanceStatePair, enableOps));
    }

    state = ACT_RESUME;
    intentStatePair = std::make_pair(intentId, state);
    it = enableTriggerLcMap.find(intentStatePair);
    if(it != enableTriggerLcMap.end()){
        std::set<int> enableOps = it->second;
        enableTriggerLcMap.erase(intentStatePair);

        std::pair<u4, int> instanceStatePair = std::make_pair(instance, state);
        enableTriggerLcMap.insert(std::make_pair(instanceStatePair, enableOps));
    }

}


/* this state machine stores info to add HB edge from post of previous activity lifecycle
 * callback to post of current one. Since all are Activity lifecycle callbacks, they
 * are guaranteed to execute on the main thread.*/
bool EnvModel::checkAndUpdateActivityComponentState(int opId, OpLog* op, int opTaskId, Task* opTask){
    bool updated = false;
    u4 instance = op->arg2;
    int state = op->arg1;

    ComponentInfo* prevActInfo = NULL;

    if(state != ACT_LAUNCH){
        std::map<u4, ComponentInfo* >::iterator actIt = activityStateMap.find(instance);
        if(actIt != activityStateMap.end()){
            prevActInfo = actIt->second;
        }else{
            if(state == ACT_START_NEW_INTENT || state == ACT_CHANGE_ACT_CONFIG
                    || state == ACT_RELAUNCH || state == APP_CHANGE_CONFIG){
                //currently these states are only handled on a best effort basis i.e we have a few locations
                //which can capture enable ops for these events. However, we may have missed some paths.
                return true;
            }
            PERROR("Activity state machine error. state %d seen on activity instance %u before instantiation", 
                    state, instance);
        }
    }

    /*in all the states except LAUNCH check if prevOperation of the activity is in 
     *the expected state, only then perform other task
     */
    switch(state){
      case ACT_LAUNCH:
        //if any instance with same id remaining clear it
        //should not happen usually. but not considering as a serious error...
        activityStateMap.erase(instance); 
 
        //add an entry to activity state tracking map
        /*for LAUNCH instance is actually intentId. Instance will be sent later
         *and this will be rewritten
         */
        prevActInfo = new ComponentInfo(opId, state, opTask); 
        activityStateMap.insert(std::make_pair(instance, prevActInfo));
        updated = true;
        break;
      case ACT_RESUME:
        switch(prevActInfo->state){
            case ACT_LAUNCH:
            case ACT_ACTIVITY_START:
            case ACT_PAUSE:
            case ACT_STOP:
            case ACT_NEW_INTENT:
            case ACT_RESULT: 
              addEnvOrderedPostToTask(prevActInfo->compTask, opTask);
              //opTask->envOrderedPosts.insert(prevActInfo->compTask->postOpId);
              updated = true;
              break;
            default: PERROR("state %d of instance %u encountered spurious previous state %d", state, instance, prevActInfo->state);
        } 
        break;	  
      case ACT_ACTIVITY_START:
        switch(prevActInfo->state){
            case ACT_STOP: 
              addEnvOrderedPostToTask(prevActInfo->compTask, opTask);
              // opTask->envOrderedPosts.insert(prevActInfo->compTask->postOpId);
              updated = true;
              break;
            default: PERROR("state %d of instance %u encountered spurious previous state %d", state, instance, prevActInfo->state);
        }
        break;
      case ACT_PAUSE:
        switch(prevActInfo->state){
            case ACT_RESUME:
              addEnvOrderedPostToTask(prevActInfo->compTask, opTask);
              //opTask->envOrderedPosts.insert(prevActInfo->compTask->postOpId);
              updated = true;
              break;
            default: PERROR("state %d of instance %u encountered spurious previous state %d", state, instance, prevActInfo->state);
        }
        break;
      case ACT_STOP:
        switch(prevActInfo->state){
            case ACT_PAUSE:
            case ACT_ACTIVITY_START:
              addEnvOrderedPostToTask(prevActInfo->compTask, opTask);  
              //opTask->envOrderedPosts.insert(prevActInfo->compTask->postOpId);
              updated = true;
              break;
            default: PERROR("state %d of instance %u encountered spurious previous state %d", state, instance, prevActInfo->state);
        }
        break;
      case ACT_DESTROY:
        switch(prevActInfo->state){
            case ACT_PAUSE: //If an Activity calls finish() when resuming it will invoke onPause followed by onDestroy
            case ACT_STOP:
              addEnvOrderedPostToTask(prevActInfo->compTask, opTask);
              //opTask->envOrderedPosts.insert(prevActInfo->compTask->postOpId);
              updated = true;
              break;
            default: PERROR("state %d of instance %u encountered spurious previous state %d", state, instance, prevActInfo->state);
        }
        break;
      case ACT_RESULT:
        switch(prevActInfo->state){
            case ACT_PAUSE:
            case ACT_STOP:
            case ACT_NEW_INTENT:
              addEnvOrderedPostToTask(prevActInfo->compTask, opTask);
              //opTask->envOrderedPosts.insert(prevActInfo->compTask->postOpId);
              updated = true;
              break;
            default: PERROR("state %d of instance %u encountered spurious previous state %d", state, instance, prevActInfo->state);
        }
        break;
      case ACT_NEW_INTENT:
        switch(prevActInfo->state){
            case ACT_RESULT:
            case ACT_PAUSE:
            case ACT_STOP:
              addEnvOrderedPostToTask(prevActInfo->compTask, opTask);
              //opTask->envOrderedPosts.insert(prevActInfo->compTask->postOpId);
              updated = true;
              break;
            default: PERROR("state %d of instance %u encountered spurious previous state %d", state, instance, prevActInfo->state);
        }
        break;
      /*case ACT_START_NEW_INTENT:
        updated = true;
        break;*/
      case ACT_RELAUNCH:
        updated = true; 
        break;
      /*  switch(prevActInfo->state){
            case ACT_DESTROY://pointless as new instance gets created during Relaunch and ACT_DESTROY erases the instance indicated by RELAUNCH
              addEnvOrderedPostToTask(prevActInfo->compTask, opTask);
              updated = true;
              break;
            default: PERROR("state %d of instance %u encountered spurious previous state %d", state, instance, prevActInfo->state);
        }
        break;*/
      default: PERROR("unknown or unhandled activity state %d encountered by activity state machine of instance %u.", state, instance);
    } 

    //update information of activity state
    if(state != ACT_DESTROY && state != ACT_LAUNCH && state != ACT_RELAUNCH){
        prevActInfo->opId = opId;
        prevActInfo->state = state;
        prevActInfo->compTask = opTask;
    }else if(state != ACT_LAUNCH){//the activity instance will get destroyed due to relaunch as well as destroy
        activityStateMap.erase(instance);
        delete prevActInfo;
    }

    return updated;
}


/* this state machine stores info to add HB edge from post of previous service lifecycle
 * callback to post of current one. All  Service lifecycle callbacks tracked here
 * are guaranteed to execute on the main thread.
 *
 * Note that below code achieves the HB info required to be maintained for both service server process
 * as well as the service client process. The state machine we maintain which stores the most recent
 * Service lifecycle callback executed for a started or a bound process, is required by the app
 * that created the service. Whereas, this is not required by the client process that is requesting to
 * start/bind to a service or stop/unbind from a service. For this we maintain various kinds of maps.
 * Some of these maps like those which store request to start a started service is required by server
 * app too, because the number of time onStartCommand (SERVICE_ARGS) handler gets executed is same
 * as the number of times request was made and the reequest and the handler can be mapped using intents.
 * However, this mapping in case of onStartCommand works only when the request was made by the server
 * process itself. If the server and client are different either request will not be seen on onStartCommand
 * wont be seen. This is because for a started service no lifecycle message gets sent back to the client.
 * However for bound service client, the map serviceConnectMap is very useful to map bindService requests
 * to onServiceConnected handler that gets executed on the client after binding with the service on the
 * server. Hence, this is useful even if client and server run on different process.
 * Since we will not know upfront whether client and server are on the process being tracked or not,
 * we initialize the state machine and maps for appropriate client as well as server executed operations.
 * There is no harm in that, just may result in some unnecessary bookkeeping.
 */
bool EnvModel::checkAndUpdateServiceState(int opId, OpLog* op, int opTaskId, Task* opTask, OpHelper* oph){
    bool updated = false;
    int state = op->arg1;
    int identifier = op->arg2;

    int serviceClassKey = -1;

    if(state != SER_REQUEST_UNBIND_SERVICE){

        serviceClassKey = op->arg5;
        std::map<int, ServiceClass*>::iterator serIter = serviceMap.find(serviceClassKey); 
        std::map<u4, RequestBindClass*>::iterator bindIter;
        
        if(state == SER_REQUEST_START_SERVICE){
            requestStartServiceMap.insert(std::make_pair(identifier, opId));

            std::map<int, std::set<int> >::iterator serReqIt = serviceClassToStartServiceRequestMap.find(serviceClassKey);
            if(serReqIt != serviceClassToStartServiceRequestMap.end()){
                serReqIt->second.insert(opId);
            }else{
                std::set<int> requestSet;
                requestSet.insert(opId);
                serviceClassToStartServiceRequestMap.insert(std::make_pair(serviceClassKey, requestSet));
            }
            
            updated = true;
            
        }
        else if(state == SER_REQUEST_BIND_SERVICE){

            bindIter = serviceConnectMap.find(identifier);
            if(bindIter == serviceConnectMap.end()){
                RequestBindClass* rb = new RequestBindClass(opId, serviceClassKey, NULL);
                serviceConnectMap.insert(std::make_pair(identifier, rb));
            }//end bindIter = NULL
            else{
                //search for entry corresponding to the service
                bool foundEntry = false;
                RequestBindClass* tmpArb = bindIter->second;
                do{
                    if(tmpArb->serviceClassKey == serviceClassKey){
                        if(tmpArb->requestUnbindOp != -1){
                            //an unbind has been performed on this connection and hence can be rebound.
                            //this will lose previous request info but this is best we can do as of now
                            //and may work for most of the cases.
                            tmpArb->requestBindOp = opId;
                            tmpArb->requestUnbindOp = -1;
                        }
                        foundEntry = true;
                        break;
                    }
                    tmpArb = tmpArb->prev;
                }while(tmpArb != NULL);
                //this connection has been unbound. So rewrite its entries
                if(!foundEntry){
                    RequestBindClass* rb = new RequestBindClass(opId, serviceClassKey, bindIter->second);
                    bindIter->second = rb;
                }
            }

            std::map<int, OverlappingRequestClass*>::iterator reqIt = overlappingStartOrBindServiceRequestsMap.find(serviceClassKey);
            if(reqIt != overlappingStartOrBindServiceRequestsMap.end()){
                //the service object may be there but will get destroyed soon as UNBIND has already been performed. So dont store things into it.
                //Instead add info to overlappingRequestClass object,so that this info can be passed on when the service gets recreated.
                reqIt->second->connectionSet.insert(identifier);

            }else{

                //check if this is the first request to start service
                if(serIter == serviceMap.end()){
                    ServiceClass* service = new ServiceClass();
                     std::map<int, std::set<int> >::iterator serReqIt = serviceClassToStartServiceRequestMap.find(serviceClassKey);
                    if(serReqIt != serviceClassToStartServiceRequestMap.end()){
                        if(*(serReqIt->second.begin()) > opId){
                            service->firstCreateServiceRequest = opId;
                        }else{
                            service->firstCreateServiceRequest = -1;
                        }
                    }else{
                        service->firstCreateServiceRequest = opId;
                    }
    
                    service->firstBindServiceRequest = opId;
    
                    service->prevStartedServiceOp = NULL;
                    service->prevBoundServiceOp = NULL;
                    service->firstStopServiceRequest = -1;
                    service->createServiceOp = -1;
                    service->bindServiceOp = -1;
                    service->isStartedService = false;
    
                    service->connectionSet.insert(identifier);
                    serviceMap.insert(std::make_pair(serviceClassKey, service));
                }else{
                    if(serIter->second->firstBindServiceRequest == -1 && serIter->second->bindServiceOp == -1){
                        //make sure BIND_SERVICE has not already been handled due to bindService request from some other app
                        serIter->second->firstBindServiceRequest = opId;
                    }
                    serIter->second->connectionSet.insert(identifier);
                }
    
            }

            updated = true;

        }
        else if(state == SER_CREATE_SERVICE){

            if(serIter != serviceMap.end() && serIter->second->createServiceOp == -1){
                ServiceClass* service = serIter->second;
                if(service->firstCreateServiceRequest != -1 && service->firstCreateServiceRequest < opTask->postOpId){
                    opTask->envOrderedEnableOps.insert(service->firstCreateServiceRequest);
                }else{
                    int pendingStartServiceRequest = INT_MAX; //I dont think the opId will ever validly reach this
                    int pendingBindServiceRequest = INT_MAX; //I dont think the opId will ever validly reach this
 
                    std::map<int, std::set<int> >::iterator serReqIt = serviceClassToStartServiceRequestMap.find(serviceClassKey);
                    if(serReqIt != serviceClassToStartServiceRequestMap.end() && !serReqIt->second.empty()){
                        pendingStartServiceRequest = *(serReqIt->second.begin()); //since std::set is sorted chronological order of ops gets maintained
                    }

                    std::map<int, OverlappingRequestClass*>::iterator reqIt = overlappingStartOrBindServiceRequestsMap.find(serviceClassKey);
                    if(reqIt != overlappingStartOrBindServiceRequestsMap.end()){
                        OverlappingRequestClass* overlapObj = reqIt->second;
                        pendingBindServiceRequest = overlapObj->firstBindServiceRequest;

                        for(std::set<u4>::iterator connIt = overlapObj->connectionSet.begin(); connIt != overlapObj->connectionSet.end(); connIt++){
                            service->connectionSet.insert(*connIt);
                        }
                 
                        delete overlapObj;
                        overlappingStartOrBindServiceRequestsMap.erase(reqIt);
                    }
 
                    if(service->firstBindServiceRequest != -1 && service->firstBindServiceRequest < pendingBindServiceRequest){
                        //since serIter pointed to a valid entry we should check this
                        pendingBindServiceRequest = service->firstBindServiceRequest;
                    }

                    service->firstCreateServiceRequest = -1;
                    service->firstBindServiceRequest = -1;

                    if(pendingStartServiceRequest != INT_MAX || pendingBindServiceRequest != INT_MAX){
                        if(pendingStartServiceRequest < pendingBindServiceRequest){
                            if(pendingStartServiceRequest < opTask->postOpId){
                                service->firstCreateServiceRequest = pendingStartServiceRequest;
                            }
                            service->isStartedService = true;
                            if(pendingBindServiceRequest != INT_MAX){
                                service->firstBindServiceRequest = pendingBindServiceRequest;
                            }
                        }else{
                            if(pendingBindServiceRequest < opTask->postOpId){
                                service->firstCreateServiceRequest = pendingBindServiceRequest;
                            }
                            service->firstBindServiceRequest = pendingBindServiceRequest;
                            if(pendingStartServiceRequest != INT_MAX){
                                service->isStartedService = true;
                            }else{
                                service->isStartedService = false;
                            }
                        }
                    }//else -> probably request came from outside app. Nothing much can be done then.
                }


                //add info to service datastructure
                service->createServiceOp = opId;

                service->prevStartedServiceOp = new ComponentInfo(opId, state, opTask);
                service->prevBoundServiceOp = new ComponentInfo(opId, state, opTask);
                        
            }else{
                if(serIter != serviceMap.end() && serIter->second->createServiceOp != -1){
                    PERROR("CREATE-SERVICE hit for service class key %d "
                           " with an existing CREATE-SERVICE." 
                           " Hit a service usage pattern that is not"
                           " supported by our environment model", serviceClassKey);
                }
                  
                #ifdef DBGPROCESS
                    printf("CREATE-SERVICE hit due to request from another app, for service classkey %d.\n",serviceClassKey);
                #endif
                //probably the start service or bind service request is made by some other app. 
                ServiceClass* service = new ServiceClass();

                int pendingStartServiceRequest = INT_MAX; //I dont think the opId will ever validly reach this
                int pendingBindServiceRequest = INT_MAX; //I dont think the opId will ever validly reach this

                std::map<int, std::set<int> >::iterator serReqIt = serviceClassToStartServiceRequestMap.find(serviceClassKey);
                if(serReqIt != serviceClassToStartServiceRequestMap.end() && !serReqIt->second.empty()){
                    pendingStartServiceRequest = *(serReqIt->second.begin()); //since std::set is sorted chronological order of ops gets maintained
                }

                std::map<int, OverlappingRequestClass*>::iterator reqIt = overlappingStartOrBindServiceRequestsMap.find(serviceClassKey);
                if(reqIt != overlappingStartOrBindServiceRequestsMap.end()){
                    OverlappingRequestClass* overlapObj = reqIt->second;
                    pendingBindServiceRequest = overlapObj->firstBindServiceRequest;

                    for(std::set<u4>::iterator connIt = overlapObj->connectionSet.begin(); connIt != overlapObj->connectionSet.end(); connIt++){
                        service->connectionSet.insert(*connIt);
                    }
                 
                    delete overlapObj;
                    overlappingStartOrBindServiceRequestsMap.erase(reqIt);
                }

                service->firstCreateServiceRequest = -1;
                service->firstBindServiceRequest = -1;

                if(pendingStartServiceRequest != INT_MAX || pendingBindServiceRequest != INT_MAX){
                    if(pendingStartServiceRequest < pendingBindServiceRequest){
                        if(pendingStartServiceRequest < opTask->postOpId){
                            service->firstCreateServiceRequest = pendingStartServiceRequest;
                        }
                        service->isStartedService = true;
                        if(pendingBindServiceRequest != INT_MAX){
                            service->firstBindServiceRequest = pendingBindServiceRequest;
                        }
                    }else{
                        if(pendingBindServiceRequest < opTask->postOpId){
                            service->firstCreateServiceRequest = pendingBindServiceRequest;
                        }
                        service->firstBindServiceRequest = pendingBindServiceRequest;
                        if(pendingStartServiceRequest != INT_MAX){
                            service->isStartedService = true;
                        }else{
                            service->isStartedService = false;
                        }
                    }
                }//else -> probably request came from outside app. Nothing much can be done then.
                

                service->prevStartedServiceOp = new ComponentInfo(opId, state, opTask);
                service->prevBoundServiceOp = new ComponentInfo(opId, state, opTask);
                service->firstStopServiceRequest = -1;
                service->createServiceOp = opId;
                service->bindServiceOp = -1;

                serviceMap.insert(std::make_pair(serviceClassKey, service));

                if(service->firstCreateServiceRequest != -1 && service->firstCreateServiceRequest < opTask->postOpId){
                    opTask->envOrderedEnableOps.insert(service->firstCreateServiceRequest);
                }//else happens if this service already got created due to request from other app

            }
           
            //onCreate Service could execute only after BIND_APPLICATION's event handler 
            opTask->envOrderedTasks.insert(appBindTaskId);
            updated = true;
        }
        else if(state == SER_BIND_SERVICE){

            if(serIter != serviceMap.end() && serIter->second->createServiceOp != -1
                   && serIter->second->bindServiceOp == -1){

                ServiceClass* service = serIter->second;
                if(service->firstBindServiceRequest != -1 && service->firstBindServiceRequest < opTask->postOpId){
                    opTask->envOrderedEnableOps.insert(service->firstBindServiceRequest);
                }//else happens if this service already got created and cound due to request from other app

                service->bindServiceOp = opId;

                //add edge from previous state to current one
                ComponentInfo* prevOp = service->prevBoundServiceOp;
                switch(prevOp->state){
                  case SER_UNBIND_SERVICE:
                  case SER_CREATE_SERVICE:
                    addEnvOrderedPostToTask(prevOp->compTask, opTask);
                    //opTask->envOrderedPosts.insert(prevOp->compTask->postOpId);
                    updated = true;
                  break;
                  default: PERROR("state %d of service %d encountered spurious previous state %d", state, serviceClassKey, prevOp->state);
                }

                if(updated){
                    service->prevBoundServiceOp->opId = opId;
                    service->prevBoundServiceOp->state = state;
                    service->prevBoundServiceOp->compTask = opTask;            
                }
            }else{
                PERROR("BIND-SERVICE seen on service with classKey %d without a prior CREATE-SERVICE or a ServiceMap entry,"
                     " or with an existing BIND-SERVICE call", serviceClassKey);
            }

        }
        else if(state == SER_UNBIND_SERVICE){

            if(serIter != serviceMap.end() && serIter->second->bindServiceOp != -1){
                ServiceClass* service = serIter->second;

                /*add edges from all previous valid (those made when an active connection was present)
                 *unbind requests to UNBIND-SERVICE as they are the cause for the service to exit its
                 *bounded state
                 */
                std::set<int>::iterator setIt = service->validRequestUnbindSet.begin();
                if(service->validRequestUnbindSet.size() > 0){
                    for( ; setIt != service->validRequestUnbindSet.end(); ++setIt){
                        if(*setIt < opTask->postOpId){ //to keep things well-formed in case we have missed something in modeling
                            opTask->envOrderedEnableOps.insert(*setIt);
                        }else{
                        //there is no else case because if *setIt > opTask->postOpId then
                        //such an unbind request was not the cause of unbind to happen. 
                            #ifdef DBGPROCESS
                            printf("An unbind request by opid %d to unbind service with class key %d "
                                   "seems to be fired after post of UNBIND_SERVICE event handler.\n", *setIt, serviceClassKey);
                            #endif
                        }
                    }
                }else{
                    //we dont consider this an error. Because, maybe all requests came from outside app i=which do not get logged.
                    #ifdef DBGPROCESS
                    printf("UNBIND-SERVICE made on service %d without any prior request unbind.\n", serviceClassKey);
                    #endif
                }

                //add edge from previous state to current one
                ComponentInfo* prevOp = service->prevBoundServiceOp;
                switch(prevOp->state){
                  case SER_BIND_SERVICE:
                    addEnvOrderedPostToTask(prevOp->compTask, opTask);
                    //opTask->envOrderedPosts.insert(prevOp->compTask->postOpId);
                    updated = true;
                  break;
                  default: PERROR("state %d of service %d encountered spurious previous state %d", state, serviceClassKey, prevOp->state);
                }
    
                //update datastructure
                if(updated){
                    //update bound service state machine
                    service->prevBoundServiceOp->opId = opId;
                    service->prevBoundServiceOp->state = state;
                    service->prevBoundServiceOp->compTask = opTask;

                    //since we have seen unbindService we can see bindService now
                    service->bindServiceOp = -1;
                    service->validRequestUnbindSet.clear();
                    service->firstBindServiceRequest = -1;

                    //clear this service entry in each connection that was associated with this service
                    std::set<u4>::iterator connIt = service->connectionSet.begin();
                    for( ; connIt != service->connectionSet.end(); ++connIt){
                        bindIter = serviceConnectMap.find(*connIt);
                        if(bindIter != serviceConnectMap.end()){
                            RequestBindClass* tmpArb = bindIter->second;
                            RequestBindClass* tmpArbAhead = NULL;
                            do{
                                if(tmpArb->serviceClassKey == serviceClassKey && tmpArb->requestUnbindOp != -1){
                                    if(tmpArbAhead == NULL){
                                        if(tmpArb->prev == NULL){
                                            serviceConnectMap.erase(bindIter);
                                            delete tmpArb;
                                            break;
                                        }else{
                                            bindIter->second = tmpArb->prev;
                                            delete tmpArb;
                                            break;
                                        }
                                    }else{
                                        tmpArbAhead->prev = tmpArb->prev;
                                        delete tmpArb;
                                        break;
                                    }
                                }else if(tmpArb->serviceClassKey == serviceClassKey){
                                    //some client has rebound to this service and hence after this UNBIND_SER we will be seein a 
                                    //BIND_SER again on the server. Store info temporarily in a map to pass HB info to the new BIND state.
                                    std::map<int, OverlappingRequestClass*>::iterator reqIt = overlappingStartOrBindServiceRequestsMap.find(serviceClassKey);
                                    if(reqIt == overlappingStartOrBindServiceRequestsMap.end()){
                                        OverlappingRequestClass* overlapObj = new OverlappingRequestClass(tmpArb->requestBindOp);
                                        overlapObj->connectionSet.insert(*connIt);
                                        overlappingStartOrBindServiceRequestsMap.insert(std::make_pair(serviceClassKey, overlapObj));
                                    }else{
                                        reqIt->second->connectionSet.insert(*connIt);
                                        if(reqIt->second->firstBindServiceRequest > tmpArb->requestBindOp){
                                            reqIt->second->firstBindServiceRequest = tmpArb->requestBindOp;
                                        }
                                    }
                                }

                                tmpArbAhead = tmpArb;
                                tmpArb = tmpArb->prev;
                            }while(tmpArb != NULL);

                        }else{
                            PERROR("missing connection entry for connection %u in serviceConnectMap when "
                                 "performing UNBIND-SERVICE for service %d", *connIt, serviceClassKey);
                        }
                    }

                    service->connectionSet.clear();
 
                    //re-initialize request and connection set info related to bounded aspect of this service, if this service is not going to be stopped soon.
                    //If its going to be stopped then dont re-initialize things here as this service object will be de-allocated when stopped. Then, wait till its recreated.
                    std::map<int, OverlappingRequestClass*>::iterator reqIt = overlappingStartOrBindServiceRequestsMap.find(serviceClassKey);
                    if(reqIt != overlappingStartOrBindServiceRequestsMap.end()){
                        if(service->isStartedService && (service->firstStopServiceRequest == -1  
                                || reqIt->second->firstBindServiceRequest < service->firstStopServiceRequest)){
                            //this service will not stop but will re-execute onBind
                            OverlappingRequestClass* overlapObj = reqIt->second;
                            service->firstBindServiceRequest = overlapObj->firstBindServiceRequest;
                            for(std::set<u4>::iterator reqSetIt = overlapObj->connectionSet.begin();
                                    reqSetIt != overlapObj->connectionSet.end(); reqSetIt++){
                                service->connectionSet.insert(*reqSetIt);
                            }

                            delete overlapObj;
                            overlappingStartOrBindServiceRequestsMap.erase(reqIt);
                            //if bind requests are seen after this point they will neatly get added to connectionset of this service
                            //and also will not overwrite firstBindServiceRequest opid as its no more -1
                        }
                        //else -> wait till service is recreated. Any bind requests seen after this point till stopservice
                        //is executed may get lost. But we will not do naything about it now. This corner case will be addressed
                        //only if we see a trace do this.
                    }
                }

            }else{
                PERROR("UNBIND-SERVICE seen without a prior ServiceMap entry"
                     " or with no existing BIND-SERVICE call, for service %d", serviceClassKey);
            }

        }
        else if(state == SER_STOP_SERVICE){
            if(serIter != serviceMap.end() && serIter->second->createServiceOp != -1){
                ServiceClass* service = serIter->second;

                //add edge from previous state to current one
                ComponentInfo* prevBoundOp = service->prevBoundServiceOp;
                ComponentInfo* prevStartedOp = service->prevStartedServiceOp;
                bool correctState = false;
                switch(prevBoundOp->state){
                  case SER_CREATE_SERVICE: 
                  case SER_UNBIND_SERVICE: correctState = true;
                }

                switch(prevStartedOp->state){
                  case SER_CREATE_SERVICE:
                  case SER_SERVICE_ARGS: correctState = true;
                }

                if(prevBoundOp->state == SER_CREATE_SERVICE && prevStartedOp->state == SER_CREATE_SERVICE){
                    PERROR("Found a stop service at opId:%d which was neither unbound nor executed service-args.", opId);
                }


                //atleast one of bind-ser or service-args should have happened before stopService
                if(correctState && (prevBoundOp->state != SER_CREATE_SERVICE ||
                           prevStartedOp->state != SER_CREATE_SERVICE)){
                    if(prevBoundOp->state == SER_UNBIND_SERVICE){
                        addEnvOrderedPostToTask(prevBoundOp->compTask, opTask);
                    }
                    if(prevStartedOp->state == SER_SERVICE_ARGS){
                        addEnvOrderedPostToTask(prevStartedOp->compTask, opTask);
                    }

                    //if(prevStartedOp->state != SER_CREATE_SERVICE){
                        if(service->firstStopServiceRequest != -1 && service->firstStopServiceRequest < opTask->postOpId){
                            opTask->envOrderedEnableOps.insert(service->firstStopServiceRequest);
                        }else{
                            //maybe request came from outside app.
                            #ifdef DBGPROCESS
                            printf("STOP-SERVICE seen on started service %d without"
                                 " a stopService or stopSelf request.\n", serviceClassKey);
                            #endif
                        }
                    //}

                    updated = true;

                    delete service;
                    serviceMap.erase(serviceClassKey);
                }else{
                    PERROR("state %d of service %d encountered spurious previous states "
                         "started: %d and bound:%d", state, serviceClassKey, prevStartedOp->state, prevBoundOp->state);
                }

            }else{
                PERROR("STOP-SERVICE seen without a prior ServiceMap entry"
                     " or with no existing CREATE-SERVICE call, for service %d", serviceClassKey);
            } 

        }
        else if(state == SER_SERVICE_ARGS){

            if(serIter != serviceMap.end() && serIter->second->createServiceOp != -1){
                ServiceClass* service = serIter->second;
                //now that we definitely know this is a started service,feed this info
                service->isStartedService = true;

                //add edge startService request to this op connected by intentID
                std::map<u4, int>::iterator startIter = requestStartServiceMap.find(identifier);
                if(startIter != requestStartServiceMap.end()){
                    opTask->envOrderedEnableOps.insert(startIter->second);
                    //some heuristic update to state machine variables. This update is useful
                    //when the service is both started and bound.
                    if(service->firstStopServiceRequest != -1 && service->firstStopServiceRequest < startIter->second){
                        /* if stop service request came in prior to start service request and yet,
                         * we see this onStartCommand (SERVICE_ARGS handler) then the stopService request
                         * has not stopped the service..maybe this is a bound service too whose clients have not unbound.
                         * So, for the service to stop all clients need to unbind and atleast one more stop service request is needed.
                         * So, reset firstStopServiceRequest.
                         */
                         service->firstStopServiceRequest = -1;
                    }

                    std::map<int, std::set<int> >::iterator serReqIt = serviceClassToStartServiceRequestMap.find(serviceClassKey);
                    if(serReqIt != serviceClassToStartServiceRequestMap.end()){
                        //relieve this info as we have seen matching onStartCommand. 
                        serReqIt->second.erase(startIter->second);
                        if(serReqIt->second.empty()){
                            serviceClassToStartServiceRequestMap.erase(serReqIt); 
                        }
                    }else{
                        PERROR("Start service request for intent %d presnet in requestStartServiceMap but not logged in "
                               "serviceClassToStartServiceRequestMap.", identifier);
                    }

                    requestStartServiceMap.erase(identifier);

                }else{
                    //maybe request came from a different app
                    #ifdef DBGPROCESS
                    printf("onStartCommand seen without corresponding startService.\n");
                    #endif
                }

                //add edge from previous state to current one
                ComponentInfo* prevOp = service->prevStartedServiceOp;
                switch(prevOp->state){
                  case SER_CREATE_SERVICE:
                  case SER_SERVICE_ARGS:
                    addEnvOrderedPostToTask(prevOp->compTask, opTask);
                    //opTask->envOrderedPosts.insert(prevOp->compTask->postOpId);
                    updated = true;
                  break;
                  default: PERROR("state %d of service %d encountered spurious previous state %d", state, serviceClassKey, prevOp->state);
                }

                if(updated){
                    service->prevStartedServiceOp->opId = opId;
                    service->prevStartedServiceOp->state = state;
                    service->prevStartedServiceOp->compTask = opTask;
                }

            }else{
                PERROR("ABC-ABORT: onStartCommand seen without a prior ServiceMap entry"
                     " or with no existing CREATE-SERVICE call, for service %d", serviceClassKey);
            }  

        }
        else if(state == SER_CONNECT_SERVICE){
            if(serIter != serviceMap.end() && serIter->second->bindServiceOp != -1){
                ServiceClass* service = serIter->second;

                /* add edge from bindService to onServiceConnected
                 * from code it is clear that onServiceConnected is executed only after BIND-SERVICE handler.
                 * Android documentation https://developer.android.com/reference/android/content/ServiceConnection.html
                 * clearly states that onServiceConnected * onServiceDisconnected are executed on the main thread.
                 * So its safe to do the below.
                 */
                OpHelper::opMapIterator opItr = oph->opMap.find(service->bindServiceOp);
                opTask->envOrderedTasks.insert(opItr->second->taskId);

                //add edge from request-bind to onServiceConnected
                bindIter = serviceConnectMap.find(identifier);
                if(bindIter != serviceConnectMap.end()){
                    RequestBindClass* tmpArb = bindIter->second;
                    do{
                        if(serviceClassKey == tmpArb->serviceClassKey){
                            if(tmpArb->requestBindOp < opTask->postOpId){
                                opTask->envOrderedEnableOps.insert(tmpArb->requestBindOp);
                            }//else onServiceConnected seen is corresponding to a request which has been unbound
                             //our modelling cannot capture all possible states for service accurately but 
                             //can work in most cases
                            break;
                        }
                        tmpArb = tmpArb->prev;  
                    }while(tmpArb != NULL);
                }else{
                    PERROR("missing bindService request from this app, for connection identifier %u.", identifier);
                }

                updated = true;
            }else{
                //add edge from request-bind to onServiceConnected
                bindIter = serviceConnectMap.find(identifier);
                if(bindIter != serviceConnectMap.end()){
                    RequestBindClass* tmpArb = bindIter->second;
                    do{
                        if(serviceClassKey == tmpArb->serviceClassKey){
                            if(tmpArb->requestBindOp < opTask->postOpId){
                                opTask->envOrderedEnableOps.insert(tmpArb->requestBindOp);
                            }//else onServiceConnected seen is corresponding to a request which has been unbound
                             //our modelling cannot capture all possible states for service accurately but 
                             //can work in most cases
                            break;
                        }
                        tmpArb = tmpArb->prev;
                    }while(tmpArb != NULL);
                }else{
                    PERROR("missing bindService request from this app, for connection identifier %u.", identifier);
                }

                updated = true;
            }
        }
        else if(state == SER_REQUEST_STOP_SERVICE){
            if(serIter != serviceMap.end()){
                if(serIter->second->firstStopServiceRequest == -1){
                    serIter->second->firstStopServiceRequest = opId;
                }
            }//a service entry maybe missing if the service is not running in this app. So not an error.
            updated = true;
            
        }
    }else{
        //because, REQUEST_UNBIND_SERVICE only gets connnection obj as input
        std::map<u4, RequestBindClass*>::iterator connIter =  serviceConnectMap.find(identifier);
        if(connIter != serviceConnectMap.end()){
            //for all services using this connection update their requestUnbindOp value
            RequestBindClass* tmpArb = connIter->second;
            
            do{
                //ignore if unbind requests are made after a connection is unbound
                if(tmpArb->requestUnbindOp == -1){
                    //no HB edges will be added now. This info is used only on seeing a UNBIND-SER
                    tmpArb->requestUnbindOp = opId;
                    serviceClassKey = tmpArb->serviceClassKey;
                    std::map<int, ServiceClass*>::iterator serIter = serviceMap.find(serviceClassKey);
                    if(serIter != serviceMap.end()){
                        serIter->second->validRequestUnbindSet.insert(opId);
                    }else{
                        PERROR("service state machine not created for %d even though bindService seen."
                             "something has gone wrong, cant continue further", serviceClassKey);
                    }
                }
                
                tmpArb = tmpArb->prev;
            }while(tmpArb != NULL);

            updated = true;
        }else{
            //calling unbind without prior bind might lead to illegalState exception
            //but calling unbind first wont affect our state machine
            updated = true; //unbind-request need not come only on active connection. Ignore it and comtinue
        }
    }

    return updated;
}


