/*
 * Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OPERATION_H_
#define OPERATION_H_

#include "common.h"
// DR = DroidRacer 
#define DR_OP 0
#define DR_HB_OP 1
#define DR_BASE_POST 2
#define DR_POST 3
#define DR_BEGIN 4
#define DR_END 5
#define DR_ATTACH_Q 6
#define DR_FORK 7
#define DR_JOIN 8
#define DR_THREADINIT 9
#define DR_THREADEXIT 10
#define DR_START 15
#define DR_REMOVE_EVENT 16
#define DR_LOOP 18
#define DR_LOOP_EXIT 23
#define DR_ENABLE_EVENT 19
#define DR_TRIGGER_EVENT 20
#define DR_ENABLE_LIFECYCLE 21
#define DR_TRIGGER_LIFECYCLE 22
#define DR_TRIGGER_RECEIVER 24
#define DR_TRIGGER_SERVICE 25
#define DR_INSTANCE_INTENT 26
#define DR_ENABLE_WINDOW_FOCUS 27
#define DR_TRIGGER_WINDOW_FOCUS 28
#define DR_WAIT 29
#define DR_TIMED_WAIT 50
#define DR_NOTIFY 30
#define DR_NATIVE_POST 40
#define DR_UI_POST 41
#define DR_IDLE_POST 43
#define DR_NATIVE_NOTIFY 44
#define DR_POST_FOQ 45
#define DR_NATIVE_POST_FOQ 46
#define DR_POST_NEG 47
#define DR_ENABLE_TIMER_TASK 48
#define DR_TRIGGER_TIMER_TASK 49
#define DR_AT_TIME_POST 51

class Operation;

class Operation{

public:

Operation(){
    tid = -1;
    taskId = -1;
    ignore = false;
}

Operation(int tid, int taskId) : tid(tid), taskId(taskId){
    ignore = false;
}

virtual ~Operation(){}

virtual int getTypeId(){
    return DR_OP;
}

virtual void diagnosticPrint(){
    printf("tid:%d, taskId:%d, ignore:%d \n", tid, taskId, ignore);
}

int tid; //thread on which the operation is executed
int taskId; //task inside which the operation is executed
bool ignore; //true if the operation need not be processed

};


#endif  // OPERATION_H_
