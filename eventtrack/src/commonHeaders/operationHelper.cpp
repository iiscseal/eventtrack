/*
 * Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <operationHelper.h>

ThreadClass::ThreadClass(int curTaskId) : curTaskId(curTaskId){
    loopOpId = -1;
}

ThreadClass::~ThreadClass(){}


OpHelper::OpHelper(){
    opCount = 1;
    taskCount = 1;
    addOpToMap = true;
    highestChainId = 0;
    curClockOnChains = new VectorClock();
}

OpHelper::~OpHelper(){}

/* whenever this function creates a new chain it also initializes its
 * clock value in curClockOnChains as 1, as the operation after receiving
 * this chain will anyways increment this chain's clock value.
 */
int OpHelper::getChain(Task* task, VectorClock* vc){
    if(task->chain != -1){
        return task->chain;
    }else if(task->eventId <= 0){ 
        //not an event handler.Hence, definitely new chain as per our chain
        //assignment strategy.
        int chainId = highestChainId++;
        curClockOnChains->resizeVC(chainId + 1, 0);

        task->chain = chainId;
        return chainId;
    }else{
        int chainId = vc->assignChain(curClockOnChains);
        if(chainId == -1){
            chainId = highestChainId++;
            curClockOnChains->resizeVC(chainId + 1, 0);
        }

        task->chain = chainId;
        return chainId;
    }
}

void OpHelper::cleanupAfterPreProcess(){
    std::set<int>::iterator enEvIt = enableEventsToBeDeleted.begin();
    OpHelper::opMapIterator opIt;
    #ifdef DBGPREPROCESS
    printf("\nDeleting foll. enable operations: \n");
    #endif
    for(; enEvIt != enableEventsToBeDeleted.end(); enEvIt++){
        #ifdef DBGPREPROCESS
        printf("  %d,", *enEvIt);
        #endif
        opIt = opMap.find(*enEvIt);
        delete opIt->second;
        opMap.erase(opIt);
    }
    enableEventsToBeDeleted.clear();

    #ifdef DBGPREPROCESS
    printf("\nDeleting foll. post operations: \n");
    #endif
    std::map<u4,int>::iterator epIt = eventToPostMap.begin();
    for(; epIt != eventToPostMap.end(); epIt++){
        #ifdef DBGPREPROCESS
        printf("  %d,", epIt->second);
        #endif
        opIt = opMap.find(epIt->second);
        delete opIt->second;
        opMap.erase(opIt);
    }
    eventToPostMap.clear();

    #ifdef DBGPREPROCESS
    printf("\nDeleting foll. operations: \n");
    #endif
    std::set<int>::iterator it = opsToBeDeleted.begin();
    for(; it != opsToBeDeleted.end(); it++){
        #ifdef DBGPREPROCESS
        printf("  %d,", *it);
        #endif
        opIt = opMap.find(*it);
        delete opIt->second;
        opMap.erase(opIt);
    }
    opsToBeDeleted.clear();

    //we dont need these maps after preprocessing
    waitThreadToNotifyMap.clear();
    recentUiEnableEventMap.clear(); 
    recentWindowFocusEnableMap.clear();
    recentEnableTimerTaskMap.clear();
}


