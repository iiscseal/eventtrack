/*
 * Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <vectorclock.h>

VectorClock::VectorClock(){
}

VectorClock::~VectorClock(){
}

VectorClock::VectorClock(const VectorClock& srcVC) {
    vc = srcVC.vc;
}

long unsigned int VectorClock::getVCSize(){
    return vc.size();
}

void VectorClock::resetVector(){
    vc.clear();
}

void VectorClock::resizeVC(int size, u4 initialValue){
    if(size > 0){
        vc.resize(size, initialValue);
    }
}

//we assume chain is never negative
u4 VectorClock::getClockValue(int chain){
    if(chain >= vc.size()){
        return 0;
    }else{
        return vc[chain];
    }
}

u4 VectorClock::incrementClock(int chain){
    if(vc.size() <= chain){
        vc.resize(chain + 1, 0);
    } 
    return ++vc[chain];
}

void VectorClock::vcJoin(VectorClock* srcVC){
    if(vc.size() < srcVC->vc.size()){
        vc.resize(srcVC->vc.size(), 0);
    }

    for(int i = 0; i < srcVC->vc.size(); i++){
        if(vc[i] < srcVC->vc[i]){
            vc[i] = srcVC->vc[i];
        }
    }
}

bool VectorClock::doesSubsumeClock(int chain, u4 clockValue){
    if(chain >= vc.size()){
        return (clockValue == 0)? true : false;
    }else{
        return (clockValue <= vc[chain])? true : false;
    }
}

//curChainVC holds the current clock values on each chain
int VectorClock::assignChain(VectorClock* curChainVC){
    for(int i = 0; i < vc.size(); i++){
        if(vc[i] == curChainVC->vc[i]){
            return i;
        }
    }        
    return -1;
}

void VectorClock::diagnosticPrint(){
    printf("\nprinting contents of VC of size %lu:\n", vc.size());
    for(int i=0; i < vc.size(); i++){
        printf("%d,  ", vc[i]);
    }
    printf("\n");
}

