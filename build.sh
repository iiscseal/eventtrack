#!/bin/bash

#
# Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


if [ $# -lt 1 ]; then
    echo "Aborting! Requires atleast one argument which indicates the HB computations tools to be built."
    exit 1
fi

if [ -d "eventtrack/bin" ]; then
    echo "cleanup directory eventtrack/bin"
    rm -r eventtrack/bin/*
fi

echo ""

if [ "$1" == "stats-all" ]; then
    echo "Building tools eventtrack, er-baseline and er-opt to collect stats related to trace, candidate events and events directly ordered by each of the techniques."
    #build eventtrack
    make -C eventtrack/ clean
    make -C eventtrack/ TOOL_MODE="-DEVENTTRACK -DET_ENABLE_OPT_INSERT -DDBG_COLLECT_STATS" EXEC_NAME="eventtrack"
    #build er-baseline
    echo ""
    make -C eventtrack/ clean
    make -C eventtrack/ TOOL_MODE="-DERBASELINE -DDBG_COLLECT_STATS" EXEC_NAME="er-baseline"
    #build er-opt
    echo ""
    make -C eventtrack/ clean
    make -C eventtrack/ TOOL_MODE="-DERBASELINE -DER_ENABLE_EVENT_COVER -DDBG_COLLECT_STATS" EXEC_NAME="er-opt"
elif [ "$1" == "time-all" ]; then
    echo "Building tools eventtrack, er-baseline and er-opt to collect execution times of various techniques. This version only reports time and no other stats."
    #build eventtrack
    make -C eventtrack/ clean
    make -C eventtrack/ TOOL_MODE="-DEVENTTRACK -DET_ENABLE_OPT_INSERT" EXEC_NAME="eventtrack"
    #build er-baseline
    echo ""
    make -C eventtrack/ clean
    make -C eventtrack/ TOOL_MODE="-DERBASELINE" EXEC_NAME="er-baseline"
    #build er-opt
    echo ""
    make -C eventtrack/ clean
    make -C eventtrack/ TOOL_MODE="-DERBASELINE -DER_ENABLE_EVENT_COVER" EXEC_NAME="er-opt"
elif [ "$1" == "memory-all" ]; then
    echo "Building tools eventtrack and er-baseline to collect peak memory consumed by various techniques. All stats except time have been disabled."
    #build eventtrack
    make -C eventtrack/ clean
    make -C eventtrack/ TOOL_MODE="-g -DEVENTTRACK -DET_ENABLE_OPT_INSERT" EXEC_NAME="eventtrack"
    #build er-baseline
    echo ""
    make -C eventtrack/ clean
    make -C eventtrack/ TOOL_MODE="-g -DERBASELINE" EXEC_NAME="er-baseline"
    #build er-opt
    #echo ""
    #make -C eventtrack/ clean
    #make -C eventtrack/ TOOL_MODE="-DERBASELINE -DER_ENABLE_EVENT_COVER" EXEC_NAME="er-opt"
elif [ "$1" == "custom" ]; then
    if [ $# -eq 3 ]; then
        echo "Building tool $3 in mode $2. Take care to only pass valid flag combinations as second argument to avoid build failures (error may not be thrown always)."
        #build custom tool
        make -C eventtrack/ clean
        make -C eventtrack/ TOOL_MODE="$2" EXEC_NAME="$3"
    else
        echo "Aborting build! custom build needs the flags to be passed to TOOL_MODE arg of Makefile as 2nd argument and the name of the executable as 3rd argument."
    fi
else
    echo "Aborting build! First argument passed is invalid."
fi

