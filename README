LICENSE
-------
EventTrack is available under the Apache License, version 2.0. Please see the 
LICENSE and NOTICE files for details.


Brief Description
-----------------
This repository contains the implementation of happens-before relation
computation algorithm called EventTrack and its related artifacts. Please refer
to the enclosed README.pdf in the root directory for details. The implementation 
of EventTrack is based on the algorithm presented in the paper 
"Efficient Computation of Happens-before Relation for Event-driven Programs",
Pallavi Maiya and Aditya Kanade, ISSTA 2017.


Acknowledgments
---------------
This work is supported in part by Google India through a PhD Fellowship in Programming 
Languages and Compilers awarded to Pallavi Maiya.


Contact
-------
In case of any queries write to pallavi.maiya@csa.iisc.ernet.in