#!/bin/bash

#
# Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


    /bin/bash ./build.sh stats-all

    dir="eventtrack/test_dir/bbc"
    `find $dir -type f -name '*.log' -delete`

    #create directories where results will be stored
    mkdir -p "$dir/eventtrack"
    mkdir -p "$dir/er-baseline"
    mkdir -p "$dir/er-opt"

    echo "Testing droidracer.blog in directory $dir to collect non-time related statistics."
    file="$dir/droidracer.blog"
    if [ -f "$file" ]
    then
        resEt="$dir/eventtrack/stats.log"
        echo "Computing HB relation for $file using eventtrack. Result stored in $resEt file."
        timeout 5m eventtrack/bin/eventtrack $file &> "$resEt" ; if [ $? -eq 124 ] ; then echo HB computation timed out. ; fi
        resErB="$dir/er-baseline/stats.log"
        echo "" ; echo "Computing HB relation for $file using er-baseline. Result stored in $resErB file."
        timeout 5m eventtrack/bin/er-baseline $file &> "$resErB" ; if [ $? -eq 124 ] ; then echo HB computation timed out. ; fi
        resErO="$dir/er-opt/stats.log"
        echo "" ; echo "Computing HB relation for $file using er-opt. Result stored in $resErO file."
        timeout 5m eventtrack/bin/er-opt $file &> "$resErO" ; if [ $? -eq 124 ] ; then echo HB computation timed out. ; fi
    else
        echo "$file not found."
    fi

    echo ""
    /bin/bash ./build.sh time-all
    echo ""

    echo "Testing droidracer.blog in directory $dir to collect execution time statistics."
    file="$dir/droidracer.blog"
    if [ -f "$file" ]
    then
        resEt="$dir/eventtrack/time.log"
        echo "Computing HB relation for $file using eventtrack. Result stored in $resEt file."
        timeout 5m eventtrack/bin/eventtrack $file &> "$resEt" ; if [ $? -eq 124 ] ; then echo HB computation timed out. ; fi
        resErB="$dir/er-baseline/time.log"
        echo "" ; echo "Computing HB relation for $file using er-baseline. Result stored in $resErB file." 
        timeout 5m eventtrack/bin/er-baseline $file &> "$resErB" ; if [ $? -eq 124 ] ; then echo HB computation timed out. ; fi
        resErO="$dir/er-opt/time.log"
        echo "" ; echo "Computing HB relation for $file using er-opt. Result stored in $resErO file."
        timeout 5m eventtrack/bin/er-opt $file &> "$resErO" ; if [ $? -eq 124 ] ; then echo HB computation timed out. ; fi
    else
        echo "$file not found."
    fi

    echo ""
    /bin/bash ./build.sh memory-all
    echo ""
    rm massif.out.*

    echo "Testing droidracer.blog in directory $dir to collect peak memory statistics."
    file="$dir/droidracer.blog"
    if [ -f "$file" ]
    then
        resEt="$dir/eventtrack/memory.log"
        echo "Computing HB relation for $file using eventtrack. Result stored in $resEt file."
        timeout 20m valgrind --tool=massif --stacks=yes eventtrack/bin/eventtrack $file ;
        if [ $? -eq 124 ] ; then
            echo "HB computation timed out. Cannot report peak memory." 
        else
            ms_print massif.out.* &> "$resEt"
        fi
        rm massif.out.*

        resErB="$dir/er-baseline/memory.log"
        echo "" ; echo "Computing HB relation for $file using er-baseline. Result stored in $resErB file."
        timeout 20m valgrind --tool=massif --stacks=yes eventtrack/bin/er-baseline $file ;
        if [ $? -eq 124 ] ; then
            echo "HB computation timed out. Cannot report peak memory." 
        else
            ms_print massif.out.* &> "$resErB"
        fi
        rm massif.out.*

    else
        echo "$file not found."
    fi


    #generate tables and graphs corresponding to BBC app trace in test_dir
    #print preamble of tex document to graphs.tex
    cat graphs/partial_tex_files/preamble.tex > graphs/graphs.tex

    #create table of Android app trace statistics
    printf "\n\n" >> graphs/graphs.tex
    cat graphs/partial_tex_files/stats_table_begin.tex >> graphs/graphs.tex
    printf "\n" >> graphs/graphs.tex
    echo ""
    echo "Generating the Table listing concurrency behaviour related stats of execution trace of Android app in $dir folder."
    printf "%s\t&" "bbc" >> graphs/graphs.tex
    file="$dir/eventtrack/stats.log"
    if [ -f "$file" ]
    then
        res=`grep "Trace length after pre-processing" "$file" | head -1`
        ops=$(echo $res | awk '{print $NF}')
        printf "%d\t&" $ops >> graphs/graphs.tex

        res=`grep "No. of threads" "$file" | head -1`
        threads=$(echo $res | awk '{print $NF}')
        printf "%d\t&" $threads >> graphs/graphs.tex

        res=`grep "No. of events" "$file" | head -1`
        events=$(echo $res | awk '{print $NF}')
        printf "%d\t&" $events >> graphs/graphs.tex

        res=`grep "No. of event-loops" "$file" | head -1`
        loops=$(echo $res | awk '{print $NF}')
        printf "%d\t&" $loops >> graphs/graphs.tex

        res=`grep "No. of chains" "$file" | head -1`
        chains=$(echo $res | awk '{print $NF}')
        printf "%d" $chains >> graphs/graphs.tex
    else
        echo "$file not found. It looks like experiments could not be run successfully on droidracer.log trace of bbc app."
        exit 1
    fi

    printf "\\%s\n" "\\" >> graphs/graphs.tex

    cat graphs/partial_tex_files/stats_table_end.tex >> graphs/graphs.tex


    #create table of memory consumed by EventTrack and ER-baseline
    printf "\n\n" >> graphs/graphs.tex
    cat graphs/partial_tex_files/memory_table_begin.tex >> graphs/graphs.tex
    printf "\n" >> graphs/graphs.tex
    echo ""
    echo "Generating the Table reporting memory consumed by EventTrack and ER-baseline when computing HB relation for Android app in $dir folder."
    printf "%s\t&" "bbc" >> graphs/graphs.tex

    file="$dir/eventtrack/memory.log"
    if [ -f "$file" ]
    then
        etUnit=`grep -E 'GB|MB' "$file" | head -1 | awk '{print $NF}'`
        etMem=`grep '^[0-9].*#$' "$file" | head -1 | cut -d'^' -f 1`
        printf "%s " $etMem $etUnit >> graphs/graphs.tex
        printf "\t&" >> graphs/graphs.tex
    else
        echo "$file not found. It looks like valgrind could not be run successfully on droidracer.log trace of bbc app to compute peak memory."
        exit 1
    fi

    file="$dir/er-baseline/memory.log"
    if [ -f "$file" ]
    then
        erUnit=`grep -E 'GB|MB' "$file" | head -1 | awk '{print $NF}'`
        erMem=`grep '^[0-9].*#$' "$file" | head -1 | cut -d'^' -f 1`
        printf "%s " $erMem $erUnit >> graphs/graphs.tex
    else
        echo "$file not found. It looks like valgrind could not be run successfully on droidracer.log trace of bbc app to compute peak memory."
        exit 1
    fi

    printf "\\%s\n" "\\" >> graphs/graphs.tex

    cat graphs/partial_tex_files/memory_table_end.tex >> graphs/graphs.tex


    #create a pgfplots data table for execution time
    printf "\n\n" >> graphs/graphs.tex
    echo "\pgfplotstableread{" >> graphs/graphs.tex
    printf "%s\t" "app" "etrack" "erbase" "eropt" >> graphs/graphs.tex
    printf "\n" >> graphs/graphs.tex

    echo ""
    echo "Generating the graph comparing time taken by EventTrack, ER-baseline and ER-opt to compute HB relation of traces from Android app in $dir folder."
    printf "%s\t" "bbc" >> graphs/graphs.tex

    file="$dir/eventtrack/time.log"
    if [ -f "$file" ]
    then
        res=`grep "Time taken for HB computation" "$file" | head -1`
        time=$(echo $res | cut -d' ' -f 6)
        etTime=`echo $time \* 1000 | bc -l`
        printf "%f\t" $etTime >> graphs/graphs.tex
    else
        echo "$file not found. It looks like execution time collection experiments could not be run successfully on droidracer.log trace of bbc app."
        exit 1
    fi

    file="$dir/er-baseline/time.log"
    if [ -f "$file" ]
    then
        res=`grep "Time taken for HB computation" "$file" | head -1`
        time=$(echo $res | cut -d' ' -f 6)
        erbTime=`echo $time \* 1000 | bc -l`
        printf "%f\t" $erbTime >> graphs/graphs.tex
    else
        echo "$file not found. It looks like execution time collection experiments could not be run successfully on droidracer.log trace of bbc app."
        exit 1
    fi

    file="$dir/er-opt/time.log"
    if [ -f "$file" ]
    then
        res=`grep "Time taken for HB computation" "$file" | head -1`
        time=$(echo $res | cut -d' ' -f 6)
        eroTime=`echo $time \* 1000 | bc -l`
        printf "%f\t" $eroTime >> graphs/graphs.tex
    else
        echo "$file not found. It looks like execution time collection experiments could not be run successfully on droidracer.log trace of bbc app."
        exit 1
    fi

    printf "\n" >> graphs/graphs.tex

    printf "}{\\%s}\n\n" "timedata" >> graphs/graphs.tex
    cat graphs/partial_tex_files/time_begin.tex >> graphs/graphs.tex
    printf "\n    symbolic x coords={" >> graphs/graphs.tex
    printf "%s," "bbc" >> graphs/graphs.tex 
    printf "},\n" >> graphs/graphs.tex
    cat graphs/partial_tex_files/time_end.tex >> graphs/graphs.tex


    #create a pgfplots data table for candidate events size
    printf "\n\n\n" >> graphs/graphs.tex
    echo "\pgfplotstableread{" >> graphs/graphs.tex
    printf "%s\t" "app" "etrack" "erbase" >> graphs/graphs.tex
    printf "\n" >> graphs/graphs.tex

    echo ""
    echo "Generating the graph comparing sizes of candidate events sets computed by EventTrack and ER-baseline for traces from Android app in $dir folder."
    printf "%s\t" "bbc" >> graphs/graphs.tex

    file="$dir/eventtrack/stats.log"
    if [ -f "$file" ]
    then
        res=`grep "candidate" "$file" | head -1`
        etCandidateSize=$(echo $res | awk '{print $NF}')
        printf "%d\t" $etCandidateSize >> graphs/graphs.tex
    else
        echo "$file not found. It looks like stats collection experiments could not be run successfully on droidracer.log trace of bbc app."
        exit 1
    fi

    file="$dir/er-baseline/stats.log"
    if [ -f "$file" ]
    then
        res=`grep "candidate" "$file" | head -1`
        erbCandidateSize=$(echo $res | awk '{print $NF}')
        printf "%d\t" $erbCandidateSize >> graphs/graphs.tex
    else
        echo "$file not found. It looks like stats collection experiments could not be run successfully on droidracer.log trace of bbc app."
        exit 1
    fi

    printf "\n" >> graphs/graphs.tex

    printf "}{\\%s}\n\n" "candidatedata" >> graphs/graphs.tex
    cat graphs/partial_tex_files/candidate_begin.tex >> graphs/graphs.tex
    printf "\n    symbolic x coords={" >> graphs/graphs.tex
    printf "%s," "bbc" >> graphs/graphs.tex 
    printf "},\n" >> graphs/graphs.tex
    cat graphs/partial_tex_files/candidate_end.tex >> graphs/graphs.tex


    #create a pgfplots data table to count directly ordered pair of events
    printf "\n\n\n" >> graphs/graphs.tex
    echo "\pgfplotstableread{" >> graphs/graphs.tex
    printf "%s\t" "app" "etrack" "erbase" >> graphs/graphs.tex
    printf "\n" >> graphs/graphs.tex

    echo ""
    echo "Generating the graph comparing number of pairs of events directly ordered by performing VC-JOINS, by EventTrack and ER-baseline for traces from Android app in $dir folder."
    printf "%s\t" "bbc" >> graphs/graphs.tex

    file="$dir/eventtrack/stats.log"
    if [ -f "$file" ]
    then
        res=`grep "covering" "$file" | head -1`
        etCandidateSize=$(echo $res | awk '{print $NF}')
        printf "%d\t" $etCandidateSize >> graphs/graphs.tex
    else
        echo "$file not found. It looks like stats collection experiments could not be run successfully on droidracer.log trace of bbc app."
        exit 1
    fi

    file="$dir/er-baseline/stats.log"
    if [ -f "$file" ]
    then
        res=`grep "end to begin edges" "$file" | head -1`
        erbCandidateSize=$(echo $res | awk '{print $NF}')
        printf "%d\t" $erbCandidateSize >> graphs/graphs.tex
    else
        echo "$file not found. It looks like stats collection experiments could not be run successfully on droidracer.log trace of bbc app."
        exit 1
    fi

    printf "\n" >> graphs/graphs.tex

    printf "}{\\%s}\n\n" "orderedeventsdata" >> graphs/graphs.tex
    cat graphs/partial_tex_files/ordered_events_begin.tex >> graphs/graphs.tex
    printf "\n    symbolic x coords={" >> graphs/graphs.tex
    printf "%s," "bbc" >> graphs/graphs.tex 
    printf "},\n" >> graphs/graphs.tex
    cat graphs/partial_tex_files/ordered_events_end.tex >> graphs/graphs.tex



printf "\n\n" >> graphs/graphs.tex
cat graphs/partial_tex_files/closing.tex >> graphs/graphs.tex

#create graphs.pdf
make -C graphs/ clean
make -C graphs/ 
echo "" ; echo "";  
echo "Test ran successfully. View the tables and graphs created at graphs/graphs.pdf"
