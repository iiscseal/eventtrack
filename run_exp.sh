#!/bin/bash

#
# Copyright 2017 Pallavi Maiya and Aditya Kanade, Indian Institute of Science
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


if [ $# -lt 1 ]; then
    echo "Aborting! Requires atleast one argument which indicates the experiments to be performed."
    exit 1
fi

if [ "$1" == "run-all" ] || [ "$1" == "reproduce-paper" ]; then
    /bin/bash ./build.sh stats-all
    echo ""
    if [ "$1" == "run-all" ]; then
        parentDir="eventtrack/test_dir"
    else
        parentDir="eventtrack/paper_traces"
    fi

    echo "Removing any existing result files in the app folders within $parentDir."
    `find $parentDir -type f -name '*.log' -delete`


    for dir in $(find $parentDir/* -maxdepth 0 -type d); do
        #create directories where results will be stored
        mkdir -p "$dir/eventtrack"
        mkdir -p "$dir/er-baseline"
        mkdir -p "$dir/er-opt"

        echo "Testing droidracer.blog in directory $dir to collect non-time related statistics."
        file="$dir/droidracer.blog"
        if [ -f "$file" ]
        then
            resEt="$dir/eventtrack/stats.log"
            echo "Computing HB relation for $file using eventtrack. Result stored in $resEt file."
            timeout 5m eventtrack/bin/eventtrack $file &> "$resEt" ; if [ $? -eq 124 ] ; then echo HB computation timed out. ; fi
            resErB="$dir/er-baseline/stats.log"
            echo "" ; echo "Computing HB relation for $file using er-baseline. Result stored in $resErB file."
            timeout 5m eventtrack/bin/er-baseline $file &> "$resErB" ; if [ $? -eq 124 ] ; then echo HB computation timed out. ; fi
            resErO="$dir/er-opt/stats.log"
            echo "" ; echo "Computing HB relation for $file using er-opt. Result stored in $resErO file."
            timeout 5m eventtrack/bin/er-opt $file &> "$resErO" ; if [ $? -eq 124 ] ; then echo HB computation timed out. ; fi
        else
            echo "$file not found."
        fi
        echo "" ; echo ""
    done



    echo ""
    /bin/bash ./build.sh time-all
    echo ""

    for dir in $(find $parentDir/* -maxdepth 0 -type d); do
        echo "Testing droidracer.blog in directory $dir to collect execution time statistics."
        file="$dir/droidracer.blog"
        if [ -f "$file" ]
        then
            resEt="$dir/eventtrack/time.log"
            echo "Computing HB relation for $file using eventtrack. Result stored in $resEt file."
            timeout 5m eventtrack/bin/eventtrack $file &> "$resEt" ; if [ $? -eq 124 ] ; then echo HB computation timed out. ; fi
            resErB="$dir/er-baseline/time.log"
            echo "" ; echo "Computing HB relation for $file using er-baseline. Result stored in $resErB file." 
            timeout 5m eventtrack/bin/er-baseline $file &> "$resErB" ; if [ $? -eq 124 ] ; then echo HB computation timed out. ; fi
            resErO="$dir/er-opt/time.log"
            echo "" ; echo "Computing HB relation for $file using er-opt. Result stored in $resErO file."
            timeout 5m eventtrack/bin/er-opt $file &> "$resErO" ; if [ $? -eq 124 ] ; then echo HB computation timed out. ; fi
        else
            echo "$file not found."
        fi
        echo "" ; echo ""
    done



    echo ""
    /bin/bash ./build.sh memory-all
    echo ""
    rm massif.out.*

    for dir in $(find $parentDir/* -maxdepth 0 -type d); do
        echo "Testing droidracer.blog in directory $dir to collect peak memory statistics."
        echo "Note: Not computing peak memory when run using er-opt as peak memory consumed by er-opt and er-baseline is similar." 
        file="$dir/droidracer.blog"
        if [ -f "$file" ]
        then
            resEt="$dir/eventtrack/memory.log"
            echo "Computing HB relation for $file using eventtrack. Result stored in $resEt file."
            timeout 20m valgrind --tool=massif --stacks=yes eventtrack/bin/eventtrack $file ;
            if [ $? -eq 124 ] ; then
                echo "HB computation timed out. Cannot report peak memory." 
            else
                ms_print massif.out.* &> "$resEt"
            fi
            rm massif.out.*

            resErB="$dir/er-baseline/memory.log"
            echo "" ; echo "Computing HB relation for $file using er-baseline. Result stored in $resErB file."
            timeout 20m valgrind --tool=massif --stacks=yes eventtrack/bin/er-baseline $file ;
            if [ $? -eq 124 ] ; then
                echo "HB computation timed out. Cannot report peak memory." 
            else
                ms_print massif.out.* &> "$resErB"
            fi
            rm massif.out.*

        else
            echo "$file not found."
        fi
        echo "" ; echo ""
    done


elif [ "$1" == "stats-all" ]; then
    /bin/bash ./build.sh stats-all
    echo ""

    echo "Removing any existing stats.log files in the app folders within eventtrack/test_dir."
    `find eventtrack/test_dir -type f -name 'stats.log' -delete`

    for dir in $(find eventtrack/test_dir/* -maxdepth 0 -type d); do
        #create directories where results will be stored
        mkdir -p "$dir/eventtrack"
        mkdir -p "$dir/er-baseline"
        mkdir -p "$dir/er-opt"

        echo "Testing droidracer.blog in directory $dir to collect non-time related statistics."
        file="$dir/droidracer.blog"
        if [ -f "$file" ]
        then
            resEt="$dir/eventtrack/stats.log"
            echo "Computing HB relation for $file using eventtrack. Result stored in $resEt file."
            timeout 5m eventtrack/bin/eventtrack $file &> "$resEt" ; if [ $? -eq 124 ] ; then echo HB computation timed out. ; fi
            resErB="$dir/er-baseline/stats.log"
            echo "" ; echo "Computing HB relation for $file using er-baseline. Result stored in $resErB file."
            timeout 5m eventtrack/bin/er-baseline $file &> "$resErB" ; if [ $? -eq 124 ] ; then echo HB computation timed out. ; fi
            resErO="$dir/er-opt/stats.log"
            echo "" ; echo "Computing HB relation for $file using er-opt. Result stored in $resErO file."
            timeout 5m eventtrack/bin/er-opt $file &> "$resErO" ; if [ $? -eq 124 ] ; then echo HB computation timed out. ; fi
        else
            echo "$file not found."
        fi
        echo "" ; echo ""
    done


elif [ "$1" == "time-all" ]; then
    echo ""
    /bin/bash ./build.sh time-all
    echo ""

    echo "Removing any existing time.log files in the app folders within eventtrack/test_dir."
    `find eventtrack/test_dir -type f -name 'time.log' -delete`

    for dir in $(find eventtrack/test_dir/* -maxdepth 0 -type d); do
        #create directories where results will be stored
        mkdir -p "$dir/eventtrack"
        mkdir -p "$dir/er-baseline"
        mkdir -p "$dir/er-opt"

        echo "Testing droidracer.blog in directory $dir to collect execution time statistics."
        file="$dir/droidracer.blog"
        if [ -f "$file" ]
        then
            resEt="$dir/eventtrack/time.log"
            echo "Computing HB relation for $file using eventtrack. Result stored in $resEt file."
            timeout 5m eventtrack/bin/eventtrack $file &> "$resEt" ; if [ $? -eq 124 ] ; then echo HB computation timed out. ; fi
            resErB="$dir/er-baseline/time.log"
            echo "" ; echo "Computing HB relation for $file using er-baseline. Result stored in $resErB file."
            timeout 5m eventtrack/bin/er-baseline $file &> "$resErB" ; if [ $? -eq 124 ] ; then echo HB computation timed out. ; fi
            resErO="$dir/er-opt/time.log"
            echo "" ; echo "Computing HB relation for $file using er-opt.  Result stored in $resErO file."
            timeout 5m eventtrack/bin/er-opt $file &> "$resErO" ; if [ $? -eq 124 ] ; then echo HB computation timed out. ; fi
        else
            echo "$file not found."
        fi
        echo "" ; echo ""
    done


elif [ "$1" == "memory-all" ]; then
    echo ""
    /bin/bash ./build.sh memory-all
    echo ""
    rm massif.out.*

    echo "Removing any existing memory.log files in the app folders within eventtrack/test_dir."
    `find eventtrack/test_dir -type f -name 'memory.log' -delete`

    for dir in $(find eventtrack/test_dir/* -maxdepth 0 -type d); do
        #create directories where results will be stored
        mkdir -p "$dir/eventtrack"
        mkdir -p "$dir/er-baseline"
        mkdir -p "$dir/er-opt"

        echo "Testing droidracer.blog in directory $dir to collect peak memory statistics."
        echo "Note: Not computing peak memory when run using er-opt as peak memory consumed by er-opt and er-baseline is similar." 
        file="$dir/droidracer.blog"
        if [ -f "$file" ]
        then
            resEt="$dir/eventtrack/memory.log"
            echo "Computing HB relation for $file using eventtrack. Result stored in $resEt file."
            timeout 20m valgrind --tool=massif --stacks=yes eventtrack/bin/eventtrack $file ;
            if [ $? -eq 124 ] ; then
                echo "HB computation timed out. Cannot report peak memory." 
            else
                ms_print massif.out.* &> "$resEt"
            fi
            rm massif.out.*

            resErB="$dir/er-baseline/memory.log"
            echo "" ; echo "Computing HB relation for $file using er-baseline. Result stored in $resErB file."
            timeout 20m valgrind --tool=massif --stacks=yes eventtrack/bin/er-baseline $file ;
            if [ $? -eq 124 ] ; then
                echo "HB computation timed out. Cannot report peak memory." 
            else
                ms_print massif.out.* &> "$resErB"
            fi
            rm massif.out.*

        else
            echo "$file not found."
        fi
        echo "" ; echo ""
    done


#run the custom config of tool specified as second argument on all the traces in eventtrack/test_dir
elif [ "$1" == "custom-all" ]; then
    echo ""
    if ! [[ $# -lt 4 ]]; then
        /bin/bash ./build.sh custom "$2" "$3"
#        echo "***** Ensure that in all the folders inside test_dir you have created a folder named $3 to store results of this run *****"
        echo "Peak memory stats cannot be collected using this mode. Pass memory-all as first argument to obtain peak memory stats."
        echo ""

        echo "Removing any existing $4 files in the app folders within eventtrack/test_dir."
        `find eventtrack/test_dir -type f -name $4 -delete`
 
        for dir in $(find eventtrack/test_dir/* -maxdepth 0 -type d); do
            #create directories where results will be stored
            mkdir -p "$dir/$3"

            echo "Testing droidracer.blog in directory $dir to collect statistics related to tool mode $2."
            file="$dir/droidracer.blog"
            if [ -f "$file" ]
            then
                res="$dir/$3/$4"
                echo "Computing HB relation for $file using $3. Result stored in $res file."
                timeout 10m eventtrack/bin/$3 $file &> "$res" ; if [ $? -eq 124 ] ; then echo HB computation timed out. ; fi
            else
                echo "$file not found."
            fi
            echo "" ; echo ""
        done

    else
        echo "Aborting experimentation! run_exp.sh in custom-all mode needs 4 args: the flags to be passed to TOOL_MODE arg of Makefile as 2nd argument, name of the executable as 3rd argument and name of log file to be created as 4th argument."
    fi

#run the custom config of tool specified as second argument on only the droidracer.blog trace in the app directory specified as 5th arg.
elif [ "$1" == "custom-single" ]; then
    echo ""
    if ! [[ $# -lt 5 ]]; then
        /bin/bash ./build.sh custom "$2" "$3"
#        echo "***** Ensure that in all the folders inside test_dir you have created a folder named $3 to store results of this run *****"
        echo "***** The 5th arg which indicates the folder path of an app's trace can be absolute path or relative to eventtrack-artifact folder. *****"
        echo "Peak memory stats cannot be collected using this mode. Pass custom-memory-single as first argument to obtain peak memory stats."
        echo ""

        dir=$5
        echo "Removing any existing $4 files in the folder $dir."
        `find $dir -type f -name $4 -delete`

        #create directories where results will be stored
        mkdir -p "$dir/$3"

        echo "Testing droidracer.blog in directory $dir to collect statistics related to tool mode $2."
        file="$dir/droidracer.blog"
        if [ -f "$file" ]
        then
            res="$dir/$3/$4"
            echo "Computing HB relation for $file using $3. Result stored in $res file."
            timeout 10m eventtrack/bin/$3 $file &> "$res" ; if [ $? -eq 124 ] ; then echo HB computation timed out. ; fi
        else
            echo "$file not found."
        fi
        echo "" ; echo ""

    else
        echo "Aborting experimentation! run_exp.sh in custom-single mode needs 5 args: the flags to be passed to TOOL_MODE arg of Makefile as 2nd argument, name of the executable as 3rd argument, name of log file to be created as 4th argument and path to the app-directory as 5th argument."
    fi


#Compute peak memory consumed by custom config of tool specified as second argument on only the droidracer.blog trace in the app directory specified as 5th arg.
elif [ "$1" == "custom-memory-single" ]; then
    echo ""
    if ! [[ $# -eq 5 ]]; then
        buildmode="-g $2"
        /bin/bash ./build.sh custom "$buildmode" "$3"
#        echo "***** Ensure that in all the folders inside test_dir you have created a folder named $3 to store results of this run *****"
        echo "***** The 5th arg which indicates the folder path of an app's trace can be absolute path or relative to eventtrack-artifact folder. *****"
        echo ""

        dir=$5
        echo "Removing any existing $4 files in the folder $dir."
        `find $dir -type f -name $4 -delete`

        #create directories where results will be stored
        mkdir -p "$dir/$3"

        echo "Testing droidracer.blog in directory $dir to collect peak memory statistics related to tool mode $2."
        file="$dir/droidracer.blog"
        if [ -f "$file" ]
        then
            res="$dir/$3/$4"
            #remove any stray massif files before running the experiment
            rm massif.out.* 
            echo "Computing HB relation for $file using $3. Result stored in $res file."
            timeout 20m valgrind --tool=massif --stacks=yes eventtrack/bin/$3 $file ;
            if [ $? -eq 124 ] ; then
                echo "HB computation timed out. Cannot report peak memory." 
            else
                ms_print massif.out.* &> "$res"
            fi
            rm massif.out.*

        else
            echo "$file not found."
        fi
        echo "" ; echo ""

    else
        echo "Aborting experimentation! run_exp.sh in custom-memory-single mode needs 5 args: the flags to be passed to TOOL_MODE arg of Makefile as 2nd argument, name of the executable as 3rd argument, name of log file to be created as 4th argument and path to the app-directory as 5th argument."
    fi


elif [ "$1" == "run-all-on-one" ]; then
    if  [[ $# -lt 2 ]]; then
        echo "Aborting experimentation! run_exp.sh in run-all-on-one mode needs 2 args with the name of the app directory in test_dir folder on which all the experiments are to be run as the second argument."
        exit 1
    fi
        

    /bin/bash ./build.sh stats-all

    dir="eventtrack/test_dir/$2"
    `find $dir -type f -name '*.log' -delete`

    #create directories where results will be stored
    mkdir -p "$dir/eventtrack"
    mkdir -p "$dir/er-baseline"
    mkdir -p "$dir/er-opt"

    echo "Testing droidracer.blog in directory $dir to collect non-time related statistics."
    file="$dir/droidracer.blog"
    if [ -f "$file" ]
    then
        resEt="$dir/eventtrack/stats.log"
        echo "Computing HB relation for $file using eventtrack. Result stored in $resEt file."
        timeout 5m eventtrack/bin/eventtrack $file &> "$resEt" ; if [ $? -eq 124 ] ; then echo HB computation timed out. ; fi
        resErB="$dir/er-baseline/stats.log"
        echo "" ; echo "Computing HB relation for $file using er-baseline. Result stored in $resErB file."
        timeout 5m eventtrack/bin/er-baseline $file &> "$resErB" ; if [ $? -eq 124 ] ; then echo HB computation timed out. ; fi
        resErO="$dir/er-opt/stats.log"
        echo "" ; echo "Computing HB relation for $file using er-opt. Result stored in $resErO file."
        timeout 5m eventtrack/bin/er-opt $file &> "$resErO" ; if [ $? -eq 124 ] ; then echo HB computation timed out. ; fi
    else
        echo "$file not found."
    fi

    echo ""
    /bin/bash ./build.sh time-all
    echo ""

    echo "Testing droidracer.blog in directory $dir to collect execution time statistics."
    file="$dir/droidracer.blog"
    if [ -f "$file" ]
    then
        resEt="$dir/eventtrack/time.log"
        echo "Computing HB relation for $file using eventtrack. Result stored in $resEt file."
        timeout 5m eventtrack/bin/eventtrack $file &> "$resEt" ; if [ $? -eq 124 ] ; then echo HB computation timed out. ; fi
        resErB="$dir/er-baseline/time.log"
        echo "" ; echo "Computing HB relation for $file using er-baseline. Result stored in $resErB file." 
        timeout 5m eventtrack/bin/er-baseline $file &> "$resErB" ; if [ $? -eq 124 ] ; then echo HB computation timed out. ; fi
        resErO="$dir/er-opt/time.log"
        echo "" ; echo "Computing HB relation for $file using er-opt. Result stored in $resErO file."
        timeout 5m eventtrack/bin/er-opt $file &> "$resErO" ; if [ $? -eq 124 ] ; then echo HB computation timed out. ; fi
    else
        echo "$file not found."
    fi

    echo ""
    /bin/bash ./build.sh memory-all
    echo ""
    rm massif.out.*

    echo "Testing droidracer.blog in directory $dir to collect peak memory statistics."
    file="$dir/droidracer.blog"
    if [ -f "$file" ]
    then
        resEt="$dir/eventtrack/memory.log"
        echo "Computing HB relation for $file using eventtrack. Result stored in $resEt file."
        timeout 20m valgrind --tool=massif --stacks=yes eventtrack/bin/eventtrack $file ;
        if [ $? -eq 124 ] ; then
            echo "HB computation timed out. Cannot report peak memory." 
        else
            ms_print massif.out.* &> "$resEt"
        fi
        rm massif.out.*

        resErB="$dir/er-baseline/memory.log"
        echo "" ; echo "Computing HB relation for $file using er-baseline. Result stored in $resErB file."
        timeout 20m valgrind --tool=massif --stacks=yes eventtrack/bin/er-baseline $file ;
        if [ $? -eq 124 ] ; then
            echo "HB computation timed out. Cannot report peak memory." 
        else
            ms_print massif.out.* &> "$resErB"
        fi
        rm massif.out.*

    else
        echo "$file not found."
    fi

else
    echo "Aborting! First argument passed is invalid."
fi

